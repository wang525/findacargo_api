var express = require('express');
var inventary = require('../lib/carrier').Inventary;
var vehicleSwitcher = require('../lib/carrier').VehicleSwitcher;
var inputValidator = require("../lib/validator/input");
var Controller = require("../lib/framework/controller");
var rateCard = require("../lib/carrier").RateCard;
var earnings = require("../lib/delivery").CarrierEarnings;
var router = express.Router();

var CarriersController = new Controller({
    availability: inventary.listAvailable,
    availabilityAll: inventary.listAvailableAll,
    online: vehicleSwitcher.turnOn,
    offline: vehicleSwitcher.turnOff,
    location: inventary.setVehicleLocation,
    getLocation: inventary.getVehicleLocation,
    fleet: inventary.listVehicles,
    price: rateCard.set
},{
    availabilityAll:{
        auth:false
    },
    location: {
        schema: 'validator/schema/update-location'
    },
    price: {
        schema: 'validator/schema/price'
    }
});
var EarningsController = new Controller({
    report: earnings.report
});


router.get('/availability/:pickupCoord/:dropoffCoord?', CarriersController.availability);
router.get('/availabilityAll/:pickupCoord', CarriersController.availabilityAll);
router.get('/fleet', CarriersController.fleet);
router.put('/online/:vehicleId', CarriersController.online);
router.put('/offline/:vehicleId', CarriersController.offline);
router.put('/location/:vehicleId', CarriersController.location);
router.get('/location/:vehicleId', CarriersController.getLocation);
router.put('/price/:typeId', CarriersController.price);
router.get('/earnings/:date?', EarningsController.report)
module.exports = router;
