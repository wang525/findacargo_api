var express = require("express");
var Controller = require("../lib/framework/controller");
var carrierAssigned = require("../lib/admin").CarrierAssigned;

var router = express.Router();

var AdminController = new Controller({
    assignCarrier: carrierAssigned.assign,
    setCarrierStatus: carrierAssigned.setStatus
},{
    setCarrierStatus: {
        auth: true,
        schema: "validator/schema/admin-delivery-status"
    }
});
AdminController.setAdmin(true);

router.put('/:deliveryId/vehicle-assign/:vehicleId', AdminController.assignCarrier);
router.put('/:deliveryId/carrier-status', AdminController.setCarrierStatus);

module.exports = router;
