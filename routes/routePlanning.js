let express = require("express");
let Controller = require("../lib/framework/controller");
let forDate = require("../lib/route-planning/forDate");
let planRoutes = require("../lib/route-planning/planRoutes");
let planRoutesForWarehouse = require("../lib/route-planning/planRoutesForWarehouse");
let planRoutesForArea = require("../lib/route-planning/planRoutesForArea");

let router = express.Router();

let RoutePlanning = new Controller({
    forDate: forDate
}, {
    forDate: {
        auth: false
    }
});

router.get('/:date', RoutePlanning.forDate);
router.post('/zone', planRoutes);
router.post('/warehouse', planRoutesForWarehouse);
router.post('/area', planRoutesForArea);

module.exports = router;
