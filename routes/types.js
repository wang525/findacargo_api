var express = require('express');
var types = require('../lib/types');
var Controller = require("../lib/framework/controller");

var router = express.Router();

var TypesController = new Controller({
    vehicle: types.vehicle,
});

router.get('/vehicles', TypesController.vehicle);

module.exports = router;
