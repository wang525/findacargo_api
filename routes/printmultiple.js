var db = require('../lib/framework/db-connector');
ScheduledDeliveriesRepository = require('../lib/carrier-deliveries/repository/scheduleDeliveriesRepository');
let clientProfile = db.collection('accounts');
var Promise = require("bluebird");
let PostalCodeModel = require('../models/postalCode');


module.exports = {
    fetchMultipleDeliveries: function (req, res) {

        var deliveryids = req.body.deliveryids ? req.body.deliveryids : [];
        var userId = req.body.userId ? req.body.userId : '';

        userData = new Promise((resolve, reject) => {
            clientProfile.findOne({ '_id': userId.toObjectId() }, function (e, profile) {
                if (e) {
                    return reject(e);
                }
                if (profile) {
                    if (typeof profile.groupId == 'undefined') {
                        return resolve(profile);
                    } else {
                        clientProfile.findOne({ '_id': profile.groupId.toObjectId() }, function (e, profileUser) {
                            if (e) {
                                return reject(e);
                            }
                            return resolve(profileUser);
                        });
                    }
                }
            });
        });

        userData.then(profile => {

            if (deliveryids.length === 0) {
                res.send(deliveryids);
            }

            var deliveryObjectIDs = [];
            for (i = 0; i < deliveryids.length; i++) {
                deliveryObjectIDs[i] = deliveryids[i].toObjectId();
            }

            var query = {
                "_id": {
                    "$in": deliveryObjectIDs
                }
            }

            db.collection('scheduleddeliveries').find(query).toArray(function (err, docs) {
                var deliveryArr = [];
                var totalDelivery = docs.length;
                var count = 1;
                var createrId = "";
                docs.forEach(function (doc) {

                    let del = {};
                    del.id = doc._id;
                    del.dest_name = doc.recipientname;
                    del.dest_addr1 = doc.deliveryaddress;
                    del.dest_addr2 = doc.deliveryaddress2;
                    del.dest_zip = doc.deliveryzip;
                    del.dest_city = doc.deliverycity;
                    del.dest_phone = getPhoneNumber(doc.recipientphone);
                    del.delivery_id = doc.deliveryid;
                    del.delivery_notes = doc.deliverynotes;
                    del.delivery_number_of_packages = doc.deliverynumberofpackages;
                    del.client_id = doc.recipientid;
                    del.order_index = doc.orderIndex;
                    del.status = doc.status;
                    createrId = doc.creator;
                    PostalCodeModel.findOne({ 'ZIP': del.dest_zip }, function (error, zipCode) {

                        del.area = zipCode.AREA ? zipCode.AREA : '';
                        del.zone = zipCode.ZONE ? zipCode.ZONE : '';

                        if (profile) {
                            del.client_name = profile.name;
                            del.client_emailid = profile.supportEmail ? profile.supportEmail : '';
                            del.client_phone = profile.supportPhone ? profile.supportPhone : '';
                            del.client_addr = profile.companyDetails ? profile.companyDetails.companyAddress : '';
                            del.client_zip = profile.zip;
                            del.client_city = profile.city;

                        }
                        deliveryArr.push(del);
                        if (count == totalDelivery) {

                            deliveryArr.sort(compareValues('zone'));
                            deliveryArr.sort(compareValues('area'));
                            deliveryArr.sort(compareValues('zone'));

                            res.send(deliveryArr);
                        }
                        count++;

                    });
                })
            });
        });
    }
}

function compareValues(key, order = 'asc') {
    return function (a, b) {
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // property doesn't exist on either object
            return 0;
        }

        const varA = (typeof a[key] === 'string') ?
            a[key].toUpperCase() : a[key];
        const varB = (typeof b[key] === 'string') ?
            b[key].toUpperCase() : b[key];

        let comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }
        return (
            (order == 'desc') ? (comparison * -1) : comparison
        );
    };
}

function getPhoneNumber(phone) {
    if (!phone || (typeof phone === 'string'))
        return phone;

    let dialCode = phone.country_dial_code || "45";

    return `(${dialCode}) ${phone.phone}`;
}