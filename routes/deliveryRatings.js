var express = require("express");
var Response = require('../services/Response');
var DeliveryRatingModel = require('../models/deliveryRatings');
var mailgun = require('mailgun-js')({apiKey: process.env.MAILGUN_API_KEY||'key-52e3854a2fd0ebefea915d71ee3080d3', domain: process.env.MAILGUN_API_DOMAIN||'nemlevering.dk'});

var router = express.Router();	

router.post('/new', function(req, res) {
    let deliveryRatingData = req.body;
    if (!deliveryRatingData) {
        Response.error(res, 'Missed data');
        return false;
    }

    //let deliveryRating = new DeliveryRatingModel(deliveryRatingData);

    DeliveryRatingModel.findOne({
        delivery_id: deliveryRatingData.delivery_id })
        // client_name:deliveryRatingData.client_name})
    .then(function (deliveryRating) {
        console.log("response success")
        if (deliveryRating) {
            DeliveryRatingModel.update({_id:deliveryRating._id}, {$set:deliveryRatingData})
            .then(()=>{
                console.log("update success")
                Response.success(res, 'Successfully updated the deliveryRating.', deliveryRatingData);
            })
            .catch(err=>{
                Response.error(res, err);
                console.log("update err",err)
            })

        }
        else {
            let testData = {}
            DeliveryRatingModel.create(deliveryRatingData).then(()=>{
                console.log("create success")
                Response.success(res, 'Successfully created the deliveryRating.', deliveryRatingData);
            })
            .catch(err=>{
                Response.error(res, err);
                console.log("create err",err)
            })
        }
    })
    .catch(function (err) {
        console.log("response err", err)
        Response.error(res, err);
    });
    
    /*deliveryRating.save((err, record) => {
        if (err) {
            Response.error(res, err);
            return false;
        }
        Response.success(res, 'Successfully created the deliveryRating.');
    });*/
});

router.post('/email', function(req, res) {
    let emailData = req.body;
    console.log("email *********===>req.body", req.body)
    if (!emailData) {
        Response.error(res, 'Missed data');
        return false;
    }

    var data = {
        from: 'System Generated Email <noreply@goidag.com>',
        to: 'kundeservice@goidag.com',
        subject: 'The Review of delivery is lower than 3.',
        html: `<p>Hi Admin,</p>
                <p>Below is the Delivery Details</p>
                <p><strong>Deliver Review:&nbsp; `+ emailData.review + `</strong> </p>
                <p><strong>Delivery Name:</strong>&nbsp; `+ emailData.deliveryid + ` </p>
                <p><strong>Delivery ID:</strong>&nbsp; `+ emailData.delivery_id + ` </p>
                <p><strong>Deliver Content:</strong>&nbsp; `+ emailData.content + ` </p>
                <p><strong>Client Name:</strong>&nbsp; `+ emailData.client_name + ` </p>
                <p><strong>Client Address:</strong>&nbsp; `+ emailData.client_address + ` </p>
                <p>Thank You</p>`
    };

    mailgun.messages().send(data, function (error, body) {
        console.log("mail body", body);
        if (error) {
            console.log("mail error", error);
            Response.error(res, error);
        } else {
            Response.success(res, 'Successfully send email.');
        }
    });
});


module.exports = router;
