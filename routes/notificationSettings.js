var express = require("express");
var Response = require('../services/Response');
var NotificationsSettings = require('../models/notificationssettings');
var NotificationsSettingsLogs = require('../models/notificationssettingslogs');
var mongoose = require('mongoose');

var router = express.Router();	

router.post('/', function(req, res) {
    var data = req.body;
    var user = req.user;
    if(user._id!=null){
        data["client_id"] = user._id;
    }
    NotificationsSettings.findOneAndUpdate({email: data.email}, data, {upsert: true, setDefaultsOnInsert: true})
        .then(function (created) {
            NotificationsSettingsLogs.create({client_id: user._id, notification_settings_id: created._id});
            Response.created(res, 'Notifications Settings Updated.', data);
        })
        .catch(function (err) {
            if(err.name == 'ValidationError'){
                var required = Response.mongooseValidation(err.errors);
                Response.malformed(res, required.join(', '));
            }
        });
});

router.get('/:id', function (req, res) {
    var _email = req.params.id.split("++").join("@").split("+").join(".");
    NotificationsSettings.findOne({email: _email})
        .then(function (settings) {
            if(settings){
                Response.success(res, 'Notifications Settings.', settings);
            }else{
                Response.noData(res, 'Notifications Settings not found.')
            }
        });

});

router.delete('/:id', function (req, res) {
    var id = req.params.id.toObjectId();
    NotificationsSettings.remove({_id: id})
        .then(function (settings) {
            if(settings){
                Response.success(res, 'Notifications Settings.', settings);
            }else{
                Response.noData(res, 'Notifications Settings not found.')
            }
        });

});

router.get('/client/details', function (req, res) {
    var user = req.user;
    NotificationsSettings.find({client_id: user._id})
        .then(function (settings) {
            if(settings){
                settings.map(function(item) {
                    delete item._id;
                    delete item.__v;
                    delete item.client_id;
                    return item;
                });
                Response.success(res, 'Notifications Settings.', settings);
            }else{
                Response.noData(res, 'Notifications Settings not found.')
            }
        });

});

module.exports = router;
