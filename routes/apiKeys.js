var express = require("express");
var ApiKeys = require('../lib/user/index').ApiKeyController;
var Controller = require("../lib/framework/controller");

var ApiKeysController = new Controller({
    updateApiKey: ApiKeys.updateApiKey,
    getApiKey: ApiKeys.getApiKey
}, {
    updateApiKey: {
        auth: false
    },
    getApiKey: {
        auth: false
    }
});

var router = express.Router();	

router.put('/:clientId', ApiKeysController.updateApiKey);
router.get('/:clientId', ApiKeysController.getApiKey);

module.exports = router;
