let express = require("express");
let ExposedScheduledDeliveryController = require("../lib/exposed/ExposedScheduledDeliveryController");
let router = express.Router();

const EXPOSED = `/exposed-api`;

router.get(`${EXPOSED}/delivery/:id`, ExposedScheduledDeliveryController.getDelivery);
router.put(`${EXPOSED}/delivery/:id`, ExposedScheduledDeliveryController.updateDelivery);
router.put(`${EXPOSED}/delivery-address/:id`, ExposedScheduledDeliveryController.changeDeliveryAddress);
router.put(`${EXPOSED}/delivery-date/:id`, ExposedScheduledDeliveryController.changeDeliveryDateTime);
router.put(`${EXPOSED}/delivery-note/:id`, ExposedScheduledDeliveryController.changeDeliveryNote);
router.get(`${EXPOSED}/createId/:type`, ExposedScheduledDeliveryController.createIdByType);

module.exports = router;