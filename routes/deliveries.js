var express = require("express");
var inputValidator = require("../lib/validator/input");
var deliveryRequest = require("../lib/delivery/request");
var Controller = require("../lib/framework/controller");
var DeliveryPayment = require("../lib/delivery").DeliveryPayment;
var Cancelation = require("../lib/delivery").Cancelation;
var Scheduler = require("../lib/delivery").Scheduler;
var DeliveryCarrierActions = require("../lib/delivery").DeliveryCarrierActions;
var DeliveryRecoveryActions = require("../lib/delivery").DeliveryRecoveryActions;
var MyRequests = require("../lib/delivery").MyRequests;
var MonthlyInvoice= require("../lib/delivery").MonthlyInvoice;
var Opportunities = require("../lib/delivery").Opportunities;
var Purchases = require("../lib/delivery").Purchases;
var Claims = require("../lib/delivery").Claims;
var router = express.Router();

var PaymentController = new Controller({
    pay: DeliveryPayment.pay
},{
    pay: {
        schema: 'validator/schema/delivery-pay',
    }
});

router.post('/:deliveryId/pay', PaymentController.pay);

var MonthlyInvoiceController = new Controller({
    authorize: MonthlyInvoice.authorize
});

router.post('/:deliveryId/monthly-invoice', MonthlyInvoiceController.authorize);

var CancelationController = new Controller({
    cancel: Cancelation.cancel
},{});

router.post('/:deliveryId/cancel', CancelationController.cancel);

SchedulerController = new Controller({
    create: Scheduler.createDelivery
},{
    create: {
        schema: 'validator/schema/request-delivery'
    }
});

router.post('/', SchedulerController.create);

MyRequestsController = new Controller({
    list: MyRequests.list
},{});

router.get('/', MyRequestsController.list);

OpportunitiesController = new Controller({
    list: Opportunities.list
},{});

router.get('/opportunities', OpportunitiesController.list);
PurchasesController = new Controller({
    list: Purchases.list
},{});

router.get('/purchases', PurchasesController.list);
DeliveryController = new Controller({
    details: deliveryRequest.getDetails,
    status: deliveryRequest.getStatus,
    detailsAsCarrier: deliveryRequest.getDetailsAsCarrier
},{});

router.get('/:deliveryId', DeliveryController.details);
router.get('/:deliveryId/status', DeliveryController.status);
router.get('/:deliveryId/carrier', DeliveryController.detailsAsCarrier);

DeliveryCarrierController = new Controller({
    accept: DeliveryCarrierActions.accept,
    reject: DeliveryCarrierActions.reject,
    arrive: DeliveryCarrierActions.arrive,
    onRoute: DeliveryCarrierActions.onRoute,
    atDestination: DeliveryCarrierActions.atDestination,
    finish: DeliveryCarrierActions.finish,
    pickedUp: DeliveryCarrierActions.pickedUp,
    recover: DeliveryRecoveryActions.carrierRecover
},{
    recover: {
        schema: 'validator/schema/carrier-recover'
    }
});

router.post('/:deliveryId/carrier/accept', DeliveryCarrierController.accept);
router.post('/:deliveryId/carrier/reject', DeliveryCarrierController.reject);
router.post('/:deliveryId/carrier/arrive', DeliveryCarrierController.arrive);
router.post('/:deliveryId/carrier/on-route', DeliveryCarrierController.onRoute);
router.post('/:deliveryId/carrier/at-destination', DeliveryCarrierController.atDestination);
router.post('/:deliveryId/carrier/finish', DeliveryCarrierController.finish);
router.post('/:deliveryId/carrier/picked-up', DeliveryCarrierController.pickedUp);
router.post('/:deliveryId/carrier/recover', DeliveryCarrierController.recover);

DeliveryClaimsController = new Controller({
    claim: Claims.create,
    getClaim: Claims.getClaim
},
{
    claim: {
        schema: 'validator/schema/claims'
    },
    getClaim: {
        auth: false
    }
})

router.post('/:deliveryId/claim', DeliveryClaimsController.claim);
router.get('/:deliveryId/claim', DeliveryClaimsController.getClaim);
module.exports = router;
