var express = require('express');
var mongoose = require("mongoose");
var Account = mongoose.models.accounts;
var router = express.Router();
var moment = require('moment');
const momentTZ = require('moment-timezone');
var async = require('async');
let GeoCoder = require('../services/GeoCoder');

let db = require('../lib/framework/db-connector'),
    ScheduledDeliveriesCollection = db.collection('scheduleddeliveries'),
    ScheduledDeliveriesModel = require('../models/ScheduledDelivery');
let TempCollection = db.collection('temp');
EventHistoryService = require('../services/EventHistoryService');

router.post('/execute', function (req, res) {
    
    ScheduledDeliveriesCollection.find({deliverycoordinates:{$exists:false}}).toArray((err, deliveries)=>{
        async.mapSeries(deliveries, (delivery, cb)=>{
            GeoCoder.codeAddress(
                delivery.deliveryaddress,
                delivery.deliveryzip
            ).then(coordinates => {
                console.log('coordinates', coordinates)
                ScheduledDeliveriesCollection.update({_id: delivery._id}, {$set: {
                    deliverycoordinates:[
                        parseFloat(coordinates.longitude),
                        parseFloat(coordinates.latitude)
                    ]
                }}).then(()=>{
                    cb(null, delivery._id.toString())
                });
            }).catch(err=>{
                cb(null)
            })  
        }, (err, ret)=>{
            return res.send({
                error:err,
                result:ret
            });
        })  
    })
});
router.post('/execute2', function (req, res) {
    
    ScheduledDeliveriesCollection.find({'deliverycoordinates.0':{$lt:0}}).toArray((err, deliveries)=>{
        async.mapSeries(deliveries, (delivery, cb)=>{
            GeoCoder.codeAddress(
                delivery.deliveryaddress,
                delivery.deliveryzip
            ).then(coordinates => {
                console.log('coordinates', coordinates)
                ScheduledDeliveriesCollection.update({_id: delivery._id}, {$set: {
                    deliverycoordinates:[
                        parseFloat(coordinates.longitude),
                        parseFloat(coordinates.latitude)
                    ]
                }}).then(()=>{
                    cb(null, delivery._id.toString())
                });
            }).catch(err=>{
                cb(null)
            })  
        }, (err, ret)=>{
            return res.send({
                error:err,
                result:ret
            });
        })  
    })
});
router.post('/execute3', function (req, res) {
    ScheduledDeliveriesCollection.find({deliverydate: {$gte: new Date("2018-12-20T00:00:00.000Z"), $lt: new Date("2018-12-30T23:59:59.000Z")}}).toArray((err, deliveries)=>{
        async.mapSeries(deliveries, (delivery, cb)=>{
            let deliveryDate = new Date(delivery.deliverydate);
            const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            let query = {
                deliverydayofweek:days[deliveryDate.getUTCDay()],
                weekNumber:deliveryDate.getISOWeek().toString()
            }
            if(query.deliverydayofweek != delivery.deliverydayofweek || query.weekNumber != delivery.weekNumber){
                ScheduledDeliveriesCollection.update({_id: delivery._id}, {$set: query}).then(()=>{
                    cb(null, delivery._id.toString())
                });
            } else {
                cb(null)
            }
        }, (err, ret)=>{
            return res.send({
                error:err,
                result:ret
            });
        })  
    })
});
router.post('/execute4', function (req, res) {
    TempCollection.find({}).toArray((err, phoneNumbers)=>{
        ScheduledDeliveriesCollection.find({deliverydate: {$gte: new Date("2018-11-30T00:00:00.000Z"), $lt: new Date("2018-12-23T23:59:59.000Z")}}).toArray((err, deliveries)=>{
            var total = 0
            async.mapSeries(deliveries, (delivery, cb)=>{
                var datestr = moment(delivery.deliverydate).zone('+0100').format('YYYY-MM-DD')
                //console.log('delivery.deliverydate', datestr, delivery.deliverydate)
                var phoneObj = phoneNumbers.filter((o)=>{
                    return o.phone == (delivery.recipientphone.country_dial_code + delivery.recipientphone.phone) && datestr == o.datetime.substr(0,10)
                })
                // if(phoneObj.length >= 2)
                //     console.log(phoneObj.length, delivery.recipientphone.country_dial_code + delivery.recipientphone.phone)
                // total += phoneObj.length
                //var today = moment.tz("Europe/Copenhagen");
                if(phoneObj.length > 0) {
                    let query = {
                        delivered_at:phoneObj[0].datetime.substr(11,5),
                        status:3
                    }
                    ScheduledDeliveriesCollection.update({_id: delivery._id}, {$set: query}).then(()=>{
                        EventHistoryService.addCustomEventWithTime(delivery._id.toString(), "Delivery completed", moment.tz(phoneObj[0].datetime, "Europe/Copenhagen").toDate());
                        cb(null, delivery._id.toString())
                    });
                } else {
                    cb(null)
                }
                // let deliveryDate = new Date(delivery.deliverydate);
                // const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                // let query = {
                //     deliverydayofweek:days[deliveryDate.getUTCDay()],
                //     weekNumber:deliveryDate.getISOWeek().toString()
                // }
                // if(query.deliverydayofweek != delivery.deliverydayofweek || query.weekNumber != delivery.weekNumber){
                //     ScheduledDeliveriesCollection.update({_id: delivery._id}, {$set: query}).then(()=>{
                //         cb(null, delivery._id.toString())
                //     });
                // } else {
                //     cb(null)
                // }
                //cb(null, phoneObj.length)
            }, (err, ret)=>{
                console.log('total', total)
                return res.send({
                    error:err,
                    result:ret
                });
            })  
        })
    })
});
router.post('/servicelist', function (req, res) {
    var requestParams = req.body ? req.body : null;

    //console.log(requestParams);

    if (typeof requestParams.carrier_attributes === 'undefined' || typeof requestParams.carrier_attributes.api_token === 'undefined') {
        return res.send(422, { status: false, errors: [
            "Validation error: Required paramteres api_token"
        ]});
    }

    if (typeof requestParams.carrier_code === 'undefined') {
        return res.send(422, { status: false, errors: [
            "Validation error: Required paramteres carrier_code"
        ]});
    }

    if (requestParams.carrier_code.toLowerCase() != 'nemlevering') {
        return res.send(422, { status: false, errors: [
            "Validation error:  carrier_code must be nemlevering" 
        ]});
    }

    var query = {
        apikey: requestParams.carrier_attributes.api_token,
    };

    Account.findOne(query, function (err, account) {
        if (err) {
            return res.send(422, { status: false, errors: [
                "Invalid Api Token"
            ]});
        }

        if (account === null) {
            return res.send(422, { status: false, errors: [
                "Invalid Api Token"
            ]});
        }

        let tomorrow = moment(new Date()).add(1, 'days');

        var responsedata = {
            "services": [
                {
                    "service_name": "Delivered Tonight",
                    "service_code": "delivered_tonight",
                    "is_return": false,
                    "supports_paperless": false,
                    "requires_drop_point": false,
                    "barcode_requirement": {  },
                    "waybill_requirement": {

                    },
                    "estimated_delivery_date_from": moment(new Date()).format("YYYY-MM-DD") + " 16:00:00 +0200",
                    "estimated_delivery_date_to": moment(new Date()).format("YYYY-MM-DD") + " 22:00:00 +0200",
                    // "add_ons": [
                    //     {
                    //         "add_on_name": "Cash on delivery",
                    //         "add_on_code": "COD"
                    //     }
                    // ],
                    "parameters": [
                        {
                            "attr_name": "department",
                            "attr_key": "department",
                            "attr_type": "text",
                            "required": false
                        },
                        {
                            "attr_name": "Insurance",
                            "attr_key": "insurance",
                            "attr_type": "bolean",
                            "required": false
                        },
                        // {
                        //     "attr_name": "weight",
                        //     "attr_key": "weight",
                        //     "attr_type": "integer",
                        //     "required": false
                        // },
                        // {
                        //     "attr_name": "volume",
                        //     "attr_key": "volume",
                        //     "attr_type": "integer",
                        //     "required": false
                        // },
                        // {
                        //     "attr_name": "Gods type",
                        //     "attr_key": "transport_type",
                        //     "attr_type": "enum",
                        //     "enum": [
                        //         [
                        //             "Deliveries up to 40kg",
                        //             "40kg"
                        //         ]
                        //     ]
                        // }
                    ],
                    "country_combinations": [
                        {
                            "sender_country": "DK",
                            "recipient_countries": [
                                "DK"
                            ]
                        },
                        {
                            "sender_country": "SE",
                            "recipient_countries": [
                                "DK"
                            ]
                        }
                    ]
                },
                {
                    "service_name": "Delivered tomorrow",
                    "service_code": "delivered_tomorrow",
                    "is_return": false,
                    "supports_paperless": false,
                    "requires_drop_point": false,
                    "barcode_requirement": {  },
                    "waybill_requirement": {

                    },
                    "estimated_delivery_date_from": tomorrow.format("YYYY-MM-DD") + " 16:00:00 +0200",
                    "estimated_delivery_date_to": tomorrow.format("YYYY-MM-DD") + " 22:00:00 +0200",
                    // "add_ons": [
                    //     {
                    //         "add_on_name": "Cash on delivery",
                    //         "add_on_code": "COD"
                    //     }
                    // ],
                    "parameters": [
                        {
                            "attr_name": "department",
                            "attr_key": "department",
                            "attr_type": "text",
                            "required": false
                        },
                        {
                            "attr_name": "Insurance",
                            "attr_key": "insurance",
                            "attr_type": "bolean",
                            "required": true
                        },
                        // {
                        //     "attr_name": "weight",
                        //     "attr_key": "weight",
                        //     "attr_type": "integer",
                        //     "required": false
                        // },
                        // {
                        //     "attr_name": "volume",
                        //     "attr_key": "volume",
                        //     "attr_type": "integer",
                        //     "required": false
                        // },
                        // {
                        //     "attr_name": "Gods type",
                        //     "attr_key": "transport_type",
                        //     "attr_type": "enum",
                        //     "enum": [
                        //         [
                        //             "Deliveries up to 40kg",
                        //             "40kg"
                        //         ]
                        //     ]
                        // }
                    ],
                    "country_combinations": [
                        {
                            "sender_country": "DK",
                            "recipient_countries": [
                                "DK"
                            ]
                        },
                        {
                            "sender_country": "SE",
                            "recipient_countries": [
                                "DK"
                            ]
                        }
                    ]
                }
            ]
        };
        return res.json(responsedata);
    });
});

router.post('/servicequote', function (req, res) {
    var requestParams = req.body ? req.body : null;

    //console.log(requestParams);
    if (typeof requestParams.carrier_attributes === 'undefined' || typeof requestParams.carrier_attributes.api_token === 'undefined') {
        return res.send(422, { status: false, errors: [
            "Validation error: Required paramteres api_token"
        ]});
    }

    if (typeof requestParams.carrier_code === 'undefined') {
        return res.send(422, { status: false, errors: [
            "Validation error: Required paramteres carrier_code"
        ]});
    }

    if (requestParams.carrier_code.toLowerCase() != 'nemlevering') {
        return res.send(422, { status: false, errors: [
            "Validation error:  carrier_code must be nemlevering"
        ]});
    }

    var query = {
        apikey: requestParams.carrier_attributes.api_token,
    };

    Account.findOne(query, function (err, account) {
        if (err) {
            return res.send(422, { status: false, errors: [
                "Error. Invalid Api Token."
            ]});
        }

        if (account === null) {
            return res.send(422, { status: false, errors: [
                "Invalid Api Token"
            ]});
        }
        let tomorrow = moment(new Date()).add(1, 'days');
        var responsedata = {
            "services": [
                {
                    "service_name": "Delivered Tonight",
                    "service_code": "delivered_tonight",
                    "is_return": false,
                    "supports_paperless": false,
                    "requires_drop_point": false,
                    "cost_price": 49,
                    //"currency": "DKK",
                    "vat_percent": 25.0,
                    "estimated_delivery_date_from": moment(new Date()).format("YYYY-MM-DD") + " 16:00:00 +0200",
                    "estimated_delivery_date_to": moment(new Date()).format("YYYY-MM-DD") + " 16:00:00 +0200",
                    // "add_ons": [
                    //     {
                    //         "add_on_name": "Cash on delivery",
                    //         "add_on_code": "COD"
                    //     }
                    // ],
                    "parameters": [
                        {
                            "attr_name": "department",
                            "attr_key": "department",
                            "attr_type": "text",
                            "required": false
                        },
                        {
                            "attr_name": "Insurance",
                            "attr_key": "insurance",
                            "attr_type": "bolean",
                            "required": false
                        },
                        // {
                        //     "attr_name": "weight",
                        //     "attr_key": "weight",
                        //     "attr_type": "integer",
                        //     "required": false
                        // },
                        // {
                        //     "attr_name": "volume",
                        //     "attr_key": "volume",
                        //     "attr_type": "integer",
                        //     "required": false
                        // },
                        // {
                        //     "attr_name": "Gods type",
                        //     "attr_key": "transport_type",
                        //     "attr_type": "enum",
                        //     "enum": [
                        //         [
                        //             "Deliveries up to 40kg",
                        //             "40kg"
                        //         ]
                        //     ]
                        // }
                    ],
                    "country_combinations": [
                        {
                            "sender_country": "DK",
                            "recipient_countries": ["DK"]
                        }
                    ]
                },
                {
                    "service_name": "Delivered tomorrow",
                    "service_code": "delivered_tomorrow",
                    "is_return": false,
                    "supports_paperless": false,
                    "requires_drop_point": false,
                    "cost_price": 49,
                    //"currency": "DKK",
                    "vat_percent": 25.0,
                    "estimated_delivery_date_from": tomorrow.format("YYYY-MM-DD") + " 16:00:00 +0200",
                    "estimated_delivery_date_to": tomorrow.format("YYYY-MM-DD") + " 22:00:00 +0200",
                    // "add_ons": [
                    //     {
                    //         "add_on_name": "Cash on delivery",
                    //         "add_on_code": "COD"
                    //     }
                    // ],
                    "parameters": [
                        {
                            "attr_name": "department",
                            "attr_key": "department",
                            "attr_type": "text",
                            "required": false
                        },
                        {
                            "attr_name": "Insurance",
                            "attr_key": "insurance",
                            "attr_type": "bolean",
                            "required": false
                        },
                        // {
                        //     "attr_name": "weight",
                        //     "attr_key": "weight",
                        //     "attr_type": "integer",
                        //     "required": false
                        // },
                        // {
                        //     "attr_name": "volume",
                        //     "attr_key": "volume",
                        //     "attr_type": "integer",
                        //     "required": false
                        // },
                        // {
                        //     "attr_name": "Gods type",
                        //     "attr_key": "transport_type",
                        //     "attr_type": "enum",
                        //     "enum": [
                        //         [
                        //             "Deliveries up to 40kg",
                        //             "40kg"
                        //         ]
                        //     ]
                        // }
                    ],
                    "country_combinations": [
                        {
                            "sender_country": "DK",
                            "recipient_countries": ["DK"]
                        }
                    ]
                }
            ]
        };


        return res.json(responsedata);
    });
});

router.post('/book', function (req, res) {

    var data = req.body ? req.body : '';
    var customerId = data.carrier_attributes.customer_id ? data.carrier_attributes.customer_id : '';
    var apiKey = data.carrier_attributes.api_token ? data.carrier_attributes.api_token : '';
    var isTestMode = data.test_mode ? data.test_mode : false;
    var serviceCode = data.service_code ? data.service_code : 'delivered_tonight';

    if (serviceCode == 'delivered_tomorrow') {
        let tomorrow = moment(new Date()).add(1, 'days');
        var deliveryDate = tomorrow.format("YYYY-MM-DD HH:mm:ss");
    } else {
        var deliveryDate = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    }
    //console.log("Is test Mode", isTestMode);
    var query = {
        apikey: apiKey
    };
    Account.findOne(query, function (err, account) {
        if (err) {
            return res.send(422, { status: false, errors: [
                "Invalid user account api key is wrong"
            ] });
        }

        //console.log('account', account)

        if (!account) {
            return res.send(422, { status: false, errors: [
                "Invalid user account api key is wrong"
            ]});
        }

        var apikey = account.apikey ? account.apikey : '';
        let tempJSON = [{
            "delivery_id": data.delivery_id ? data.delivery_id : "webshipper",
            "recipient_id": data.sender_address.att_contact ? data.sender_address.att_contact : '',
            "department": 'Default',
            "full_insurance": data.full_insurance ? data.full_insurance : '',
            "full_insurance_value": data.full_insurance_value ? data.full_insurance_value : '',
            "recipient_name": data.delivery_address.att_contact||data.delivery_address.company_name||'',
            "recipient_email": (data.email_notification != null && data.email_notification.email) ? data.email_notification.email : '',
            "delivery_date": data.delivery_date,
            "recipient_phone": {
                "country_iso_code": "DK",
                "country_dial_code": "45",
                "phone": (data.sms_notification != null && data.sms_notification.phone) ? data.sms_notification.phone : ''
            },
            "pickup_deadline": '',
            "delivery_location": {
                "description": data.delivery_address.company_name ? data.delivery_address.company_name : '',
                "zip": data.delivery_address.zip ? data.delivery_address.zip : '',
                "city": data.delivery_address.city ? data.delivery_address.city : '',
                "address_1": data.delivery_address.address_1 ? data.delivery_address.address_1 : '',
                "address_2": data.delivery_address.address_2 ? data.delivery_address.address_2 : ''
            },
            "delivery_window": data.delivery_window?data.delivery_window:{
                "from": "08.00",
                "to": "22.00"
            },
            // "pickup_window": data.pickup_window?data.pickup_window:{
            //     "from": "08:00",
            //     "to": "22:00"
            // },
            "delivery_label": data.delivery_label ? data.delivery_label : '',
            "delivery_notes": data.comment||'',
            "delivery_number_of_packages": data.packages ? data.packages.length.toString() : '1',
            "pickup_location": {
                "description": "",
                "zip": data.pickup_address.zip ? data.pickup_address.zip : '',
                "city": data.pickup_address.city ? data.pickup_address.city : '',
                "address_1": data.pickup_address.address_1 ? data.pickup_address.address_1 : '',
                "address_2": data.pickup_address.address_2 ? data.pickup_address.address_2 : '',
            },
            "delivery_date": deliveryDate
        }];

        var request = require("request");
        var hostname = req.headers.host;
        if (hostname === 'dev.api.nemlevering.dk') {
            apiurl = 'https://dev.api.nemlevering.dk';
            var labelURL = "https://dev.labels.goidag.com/";
        } else if (hostname === 'api.nemlevering.dk') {
            apiurl = 'https://api.nemlevering.dk';
            var labelURL = "https://labels.goidag.com/";
        } else if (hostname === 'localhost:3333') {
            apiurl = 'http://localhost:3333';
            var labelURL = "https://localhost:1234/";
        } else {
            apiurl = 'http://localhost:3333';
            var labelURL = "https://localhost:1234/";
        }
        var options = {
            method: 'POST',
            url: apiurl + '/v1.3/scheduled/create',
            headers:
            {
                token: apikey,
                'content-type': 'application/json'
            },
            body: tempJSON,
            json: true
        };

        //console.log('options', options)

        request(options, function (error, response, body) {

            //console.log('request', error, response, body)
            if (error) {
                return res.send(422, { status: false, errors: [
                    error.message ? error.message : ''
                ]});
            }

            if (typeof body.scheduled_id === 'undefined') {
                return res.send(422, { status: false, errors: [
                    body.message
                ]});
            } else {

                request({
                    method: 'GET',
                    encoding: null,
                    uri: labelURL + body.scheduled_id
                }, (err, resp, data) => {
                    if (typeof data === 'object') {
                        var labelData = data.toString('base64')
                        //console.log(labelData)
                    } else {
                        var labelData = '';
                    }
                    let responsData = {
                        "success": true,
                        "carrier_code": "nemlevering",
                        "tracking_links": [
                            {
                                "url": "https://track.goidag.com/" + body.scheduled_id,
                                "number": body.scheduled_id
                            }
                        ],
                        "labels": [
                            {
                                "label_size": "100X150",
                                "label_format": "PDF",
                                "label_data": labelData
                            }
                        ],
                        "documents": [

                        ]
                    };
                    return res.send(responsData);
                })
            }
        });
    });
});


module.exports = router;
