(function () {
    'use strict';
    var express = require('express');
    const router = express.Router();
    const multer = require('multer'); // require multer code
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            var docPath = process.env.IMAGE_PATH || './public/uploads/';
            cb(null, docPath)
        },
        filename: function (req, file, cb) {
            //console.log('file.mimetype.', file.mimetype)
            var type = file.mimetype.split('/');
            cb(null, Date.now() + '.' + type[type.length - 1]);
        }
    });
    const upload = multer({
        storage: storage
    });
    router.post('/', upload.single('file'), function (req, res) {
        if (req && req.file) {
            res.send({
                successMessage: 'Uploaded successfully',
                data: req.file.filename
            })
        } else {
            res.send({
                errorMessage: 'Error By Database',
                data: null
            })
        }
    });
    router.post('/multiupload', upload.array('file'), function (req, res) {
        if (req && req.files) {
            var filename = [];
            req.files.forEach(function (file) {
                filename.push(file.filename)
            })
            res.send({
                successMessage: 'Uploaded successfully',
                data: filename
            })
        } else {
            res.send({
                errorMessage: 'Error By Database',
                data: null
            })
        }
    });
    module.exports = router;
})();