var express = require('express');
var rating = require('../lib/user').Rating;
var userLocations = require('../lib/user').Locations;
var Controller = require("../lib/framework/controller");

var router = express.Router();

var RatingController = new Controller({
    rate: rating.rate,
},{
    rate: {
        schema: 'validator/schema/user-rating'
    }
});

router.post('/:userId/rate', RatingController.rate);

var LocationController = new Controller({
    preferred: userLocations.getLocations,
    add: userLocations.add,
    remove: userLocations.remove
},{
    add: {
        schema: 'validator/schema/user-location'
    }
})

router.get('/locations', LocationController.preferred);
router.post('/locations', LocationController.add);
router.delete('/locations/:locationId', LocationController.remove);

module.exports = router;
