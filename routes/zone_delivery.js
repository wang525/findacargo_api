let express = require('express'),
    Controller = require("../lib/framework/controller"),
    ZoneDelivery = require('../lib/zone-delivery/controller');

let router = express.Router();

let ZoneDeliveryController = new Controller({
    getAllForDriver: ZoneDelivery.getAllForDriver,
    create: ZoneDelivery.create,
    update: ZoneDelivery.update,
    updateMany: ZoneDelivery.updateMany,
    delete: ZoneDelivery.delete,
    forDate: ZoneDelivery.forDate,
    forDatePeriod: ZoneDelivery.forDatePeriod,
    pickups: ZoneDelivery.pickups,
    clientPickups: ZoneDelivery.clientPickups,
    updateStatus: ZoneDelivery.updateStatus,
    updateDate: ZoneDelivery.updateDate,
    deleteMany: ZoneDelivery.deleteMany,
    submitQrWithDetail: ZoneDelivery.submitQrWithDetail,
    codeVerification: ZoneDelivery.codeVerification,
    clientNewPickups: ZoneDelivery.clientNewPickups,    
}, {
        clientPickups: {
            auth: false
        },
        pickups: {
            auth: false
        },
        forDate: {
            auth: false
        },
        forDatePeriod: {
            auth: false
        },
        clientNewPickups: {
            auth: false
        },
        create: {
            auth: false,
            schema: 'validator/schema/zone-delivery'
        },
        update: {
            auth: false,
            schema: 'validator/schema/zone-delivery'
        },
        updateMany: {
            auth: false,
            schema: 'validator/schema/zone-deliveries'
        },
        delete: {
            auth: false,
        },
        updateStatus: {
            auth: false,
            schema: 'validator/schema/delivery-ids'
        },
        updateDate: {
            auth: false,
            schema: 'validator/schema/delivery-ids'
        },
        deleteMany: {
            auth: false,
            schema: 'validator/schema/delivery-ids'
        },
        submitQrWithDetail: {
            auth: true,
            schema: 'validator/schema/zone-submit-qr'
        },
    });

router.get('/date/:date', ZoneDeliveryController.forDate);
router.get('/datePeriod/:fromDate/:toDate', ZoneDeliveryController.forDatePeriod);
router.get('/pickups/:creator', ZoneDeliveryController.clientPickups);
router.get('/pickups/:creator/:startDate', ZoneDeliveryController.clientNewPickups);
router.get('/pickups-date/', ZoneDeliveryController.pickups);
router.get('/pickups-date/:date', ZoneDeliveryController.pickups);
router.get('/:driverId', ZoneDeliveryController.getAllForDriver);
router.post('/', ZoneDeliveryController.create);
router.put('/', ZoneDeliveryController.update);
router.put('/list/', ZoneDeliveryController.updateMany);
router.delete('/:deliveryId', ZoneDeliveryController.delete);
router.post('/updateStatus/:status', ZoneDeliveryController.updateStatus);
router.post('/updateDate/:date', ZoneDeliveryController.updateDate);
router.post('/deleteMany', ZoneDeliveryController.deleteMany);
router.get('/qrcodeVerification/:driverId/:deliveryId', ZoneDeliveryController.codeVerification);
router.post('/submit-all-qr', ZoneDeliveryController.submitQrWithDetail);

module.exports = router;
