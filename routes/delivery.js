let express = require("express");
let Controller = require("../lib/framework/controller");
var SheduledNotifier = require("../lib/scheduled/sendNotificationEvent");
var printmultiple = require('./printmultiple');
let router = express.Router();

let DeliveryController = new Controller({
    get: require('../lib/scheduled/get'),
    create: require('../lib/scheduled/createUpdate').create,
    update: require('../lib/scheduled/createUpdate').update,
    delete: require('../lib/scheduled/delete'),
    statusUpdate: require('../lib/scheduled/statusUpdate'),
    search: require('../lib/scheduled/search'),
    clientSearch: require('../lib/scheduled/clientSearch'),
    print: require("../lib/scheduled/print").print,
    printv4: require("../lib/scheduled/print").printv4,
    eventsHistory: require("../lib/scheduled/events"),
    locationUpdate: require("../lib/scheduled/locationUpdate"),
    locationGet: require("../lib/scheduled/currentLocation"),
    setSignature: require("../lib/scheduled/setSignature"),
    addEvent: require("../lib/scheduled/addEvent"),
    sendNotificationEvent: SheduledNotifier.sendNotificationEvent,
    getMessageTemplates: SheduledNotifier.getMessageTemplates,
    addCustomEvent: require("../lib/scheduled/addCustomEvent"),
    addNotificationSentEvent: require("../lib/scheduled/addNotificationSentEvent")
}, {
    get: {
        auth: false
    },
    create: {
        schema: 'validator/schema/scheduled-delivery',
        auth: false
    },
    update: {
        schema: 'validator/schema/scheduled-delivery',
        auth: false
    },
    delete: {
        auth: false
    },
    search: {
        schema: 'validator/schema/search'
    },
    clientSearch: {
        auth: false,
        schema: 'validator/schema/search'
    },
    print: {
        auth: false
    },
    printv4: {
        auth: false
    },
    eventsHistory: {
        auth: false
    },
    locationUpdate: {
        auth: false,
        schema: 'validator/schema/location-update'
    },
    locationGet: {
        auth: false
    },
    setSignature: {
        auth: false,
        schema: 'validator/schema/signature'
    },
    addEvent: {
        auth: false
    },
    sendNotificationEvent: {
        auth: false,
        schema: 'validator/schema/send-notification'
    },
    getMessageTemplates: {
        auth: false
    },
    addCustomEvent: {
        auth: false,
        schema: 'validator/schema/custom-event'
    },
    addNotificationSentEvent: {
        auth: false,
        schema: 'validator/schema/notification-sent-event'
    },
});

const ROUTE_BASE_VERSIONING = `/:version/deliveryservice`;
const ROUTE_BASE_STATIC = '/v1/deliveryservice';

router.get(`${ROUTE_BASE_VERSIONING}/:id`, DeliveryController.get);
router.get(`${ROUTE_BASE_STATIC}/:id/location`, DeliveryController.locationGet);
router.post(`${ROUTE_BASE_STATIC}/:id/location`, DeliveryController.locationUpdate);
router.get(`${ROUTE_BASE_STATIC}/print/:id`, DeliveryController.print);
router.post(`${ROUTE_BASE_STATIC}/:id/signature`, DeliveryController.setSignature);

router.post(`${ROUTE_BASE_VERSIONING}/create`, DeliveryController.create);
router.post(`${ROUTE_BASE_VERSIONING}/update`, DeliveryController.update);
router.delete(`${ROUTE_BASE_VERSIONING}/:deliveryId`, DeliveryController.delete);
router.put(`${ROUTE_BASE_STATIC}/:deliveryId/status/:status`, DeliveryController.statusUpdate);
router.get(`${ROUTE_BASE_STATIC}/:deliveryId/events`, DeliveryController.eventsHistory);
router.post(`${ROUTE_BASE_STATIC}/:deliveryId/events/:event`, DeliveryController.addEvent);
router.post(`${ROUTE_BASE_STATIC}/events/notification`, DeliveryController.sendNotificationEvent);
router.get(`${ROUTE_BASE_STATIC}/events/notification`, DeliveryController.getMessageTemplates);
router.post(`${ROUTE_BASE_STATIC}/:deliveryId/events`, DeliveryController.addCustomEvent);
router.post(`${ROUTE_BASE_STATIC}/:deliveryId/notificationSent`, DeliveryController.addNotificationSentEvent);
router.post(`${ROUTE_BASE_STATIC}/search`, DeliveryController.search);
router.post(`${ROUTE_BASE_STATIC}/clientSearch`, DeliveryController.clientSearch);

router.get(`/v1.4/deliveryservice/print/:id`,DeliveryController.printv4);

router.post(`${ROUTE_BASE_STATIC}/printmultiple`, function (req, res) {
    printmultiple.fetchMultipleDeliveries(req, res);
 });

module.exports = router;