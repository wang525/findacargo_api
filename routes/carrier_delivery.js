var express = require('express');
var todaysDelivery = require("../lib/carrier-deliveries/todaysDelivery");
var Controller = require("../lib/framework/controller");

var router = express.Router();

var TodayDeliveryController = new Controller({
    list: todaysDelivery.deliveries
});

router.get('/:date', TodayDeliveryController.list);
module.exports = router;
