var express = require('express');
var Controller = require("../lib/framework/controller");
var authUser = require("../lib/user/authenticator");

var AuthController = new Controller({
    login: authUser.login,
    otpRequest: authUser.otpRequest,
    otpCheck: authUser.otpCheck,
    register: authUser.register,
    facebookRegister: authUser.facebookRegister,
    facebookLogin: authUser.facebookLogin,
    onBoardingStepFirst: authUser.onBoardingStepFirst,
    onBoardingStepSecond: authUser.onBoardingStepSecond,
    registerDevice: authUser.registerDevice,
    logout: authUser.logout
}, {
    login: {
        auth: false,
        schema: "validator/schema/login"
    },
    otpRequest: {
        auth: false,
        schema: "validator/schema/otpRequest"
    },
    otpCheck: {
        auth: false,
        schema: "validator/schema/otpCheck"
    },
    register: {
        auth: false,
        schema: "validator/schema/register-user"
    },
    facebookRegister: {
        auth: false,
        schema: "validator/schema/register-user"
    },
    facebookLogin: {
        auth: false,
        schema: "validator/schema/login"
    },
    onBoardingStepFirst: {
        auth: true,
        schema: "validator/schema/onboarding-vehicle"
    },
    onBoardingStepSecond: {
        auth: true,
        schema: "validator/schema/onboarding-address"
    },
    registerDevice: {
        schema: "validator/schema/register-device"
    }
});


var router = express.Router();

router.post('/register', AuthController.register);
router.post('/login', AuthController.login);
router.post('/facebookRegister', AuthController.facebookRegister);
router.post('/facebookLogin', AuthController.facebookLogin);
router.post('/onBoardingStepFirst', AuthController.onBoardingStepFirst);
router.post('/onBoardingStepSecond', AuthController.onBoardingStepSecond);
router.post('/register-device', AuthController.registerDevice);
router.post('/logout', AuthController.logout);
router.post('/otpRequest', AuthController.otpRequest);
router.post('/otpCheck', AuthController.otpCheck);

module.exports = router;
