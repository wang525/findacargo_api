var express = require('express');
var router = express.Router();

let routes = require('../models/ScheduledDelivery');


let Promise = require('promise');

let nodeGeoCoder = require('node-geocoder');

const geoCoderKey = 'AIzaSyBmP1A3mxSdf5EH3zI0PIsDa2kUXiF2eao';
const coderOptions = {
    provider: 'google',
    httpAdapter: 'https',
    apiKey: geoCoderKey,
    formatter: null
};


const GeoCoder = nodeGeoCoder(coderOptions);

/* GET home page. */
router.get('/heartbeat', function (req, res, next) {
  console.log("rqerq")
  res.json("HEARTBEAT");
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds) {
      break;
    }
  }
}

/* GET home page. */
router.get('/denmarkaddress_client', function (req, res, next) {
  const fs = require('fs')
  var parse = require('csv-parse')

  var mongoose = require("mongoose");
  let DenmarkSAddresses = require('../models/StreetAddr');

  let DenmarkStreetAddress = mongoose.models.street_addresses;
  let DenmarkAddresses = require('../models/DenmarkAddresses');



  fs.readFile("Nemlevering.csv", function (err, fileData) {
    parse(fileData, { columns: true, trim: true }, function (err, rows) {

      var request = require("request");
      try {
        for (let j = 0; j < 2; j++) {
          var addressData = rows[j].Adresse;
          console.log(addressData);
          var add = addressData.split(",");
          var address = add[0];
          var addressArr = address.split(" ");
          var lastIndex = address.lastIndexOf(" ");
          var str = address.substring(0, lastIndex);
          console.log(str);
          var options = {
            method: 'GET',
            url: 'https://dawa.aws.dk/adresser',
            qs: { vejnavn: str, struktur: 'mini' },
            headers:
            {
            }
          };

          request(options, function (error, response, body) {
            if (error) throw new Error(error);

            if (body != null) {
              var responseData = JSON.parse(body);
              if (responseData.length > 0) {
                //console.log(responseData.length);
                let DenmarkAddress = mongoose.models.denmark_addresses;

                for (let i = 0; i < responseData.length; i++) {
                  var dataArr = {
                    "dm_id": responseData[i].id,
                    "status": responseData[i].status,
                    "vejkode": responseData[i].vejkode,
                    "vejnavn": responseData[i].vejnavn,
                    "adresseringsvejnavn": responseData[i].adresseringsvejnavn,
                    "husnr": responseData[i].husnr,
                    "supplerendebynavn": responseData[i].supplerendebynavn,
                    "postnr": responseData[i].postnr,
                    "postnrnavn": responseData[i].postnrnavn,
                    "kommunekode": responseData[i].kommunekode,
                    "x": responseData[i].x,
                    "y": responseData[i].y

                  };
                  console.log(dataArr);
                  var addressModel = new DenmarkAddress();

                  addressModel.save(function (err, doc) {

                  });

                }
              } else {
                //console.log(responseData);
                //console.log(`Invalid address supplied: Address: ${rows[j].adresseringsnavn}`);

              }

            } else {
              // console.log(`Invalid address supplied: Address: ${rows[j].adresseringsnavn}`);

            }

          });

          //  addressModel.save(function (err, doc) {

          //    if (err) { console.log(err); }
          //    else { console.log(doc); }
          //  });
        }

        return res.json({ success: true });
      } catch (error) {
        console.log(error);
      }

    })
  });
});


/* GET home page. */
router.get('/denmarkaddress', function (req, res, next) {
  const fs = require('fs')
  var parse = require('csv-parse')

  var mongoose = require("mongoose");
  let DenmarkSAddresses = require('../models/StreetAddr');

  let DenmarkStreetAddress = mongoose.models.street_addresses;
  let DenmarkAddresses = require('../models/DenmarkAddresses');


  fs.readFile('last_record.txt', 'utf-8', function (err, lastRecord) {
    lastRecord = parseInt(lastRecord);
    fs.readFile("vejstykker.csv", function (err, fileData) {
      parse(fileData, { columns: true, trim: true }, function (err, rows) {
        var request = require("request");
        try {
          for (let j = lastRecord; j < lastRecord + 50; j++) {
            // console.log(rows[j].adresseringsnavn);
            //console.log(rows[i].id);
            //  let addressModel = new DenmarkStreetAddress({
            //    dm_id: rows[i].id,
            //    adresseringsnavn: rows[i].adresseringsnavn,
            //  });

            var options = {
              method: 'GET',
              url: 'https://dawa.aws.dk/adresser',
              qs: { vejnavn: rows[j].navn, struktur: 'mini' },
              headers:
              {
              }
            };

            request(options, function (error, response, body) {
              if (error) throw new Error(error);

              if (body != null) {
                var responseData = JSON.parse(body);
                if (responseData.length > 0) {
                  //console.log(responseData.length);
                  let DenmarkAddress = mongoose.models.denmark_addresses;

                  for (let i = 0; i < responseData.length; i++) {
                    var addressModel = new DenmarkAddress({
                      "dm_id": responseData[i].id,
                      "status": responseData[i].status,
                      "vejkode": responseData[i].vejkode,
                      "vejnavn": responseData[i].vejnavn,
                      "adresseringsvejnavn": responseData[i].adresseringsvejnavn,
                      "husnr": responseData[i].husnr,
                      "supplerendebynavn": responseData[i].supplerendebynavn,
                      "postnr": responseData[i].postnr,
                      "postnrnavn": responseData[i].postnrnavn,
                      "kommunekode": responseData[i].kommunekode,
                      "x": responseData[i].x,
                      "y": responseData[i].y

                    });

                    addressModel.save(function (err, doc) {

                    });

                  }
                } else {
                  //console.log(responseData);
                  //  console.log(`Invalid address supplied: Address: ${rows[j].adresseringsnavn}`);

                }

              } else {
                //  console.log(`Invalid address supplied: Address: ${rows[j].adresseringsnavn}`);

              }

            });

            //  addressModel.save(function (err, doc) {

            //    if (err) { console.log(err); }
            //    else { console.log(doc); }
            //  });
          }
          var r = parseInt(lastRecord) + 50;
          fs.writeFile('last_record.txt', r, function (err, data) {
            if (err) console.log(err);
            /// console.log("Successfully Written to File.");
          });
          return res.json({ success: true });
        } catch (error) {
          console.log(error);
        }

      })
    });
  });


});

router.get('/denmarkaddress/:address', function (req, res, next) {
  const fs = require('fs')
  var address = req.params.address;
  //return res.json({ success: address });
  var mongoose = require("mongoose");
  let DenmarkSAddresses = require('../models/StreetAddr');

  let DenmarkStreetAddress = mongoose.models.street_addresses;
  let DenmarkAddresses = require('../models/DenmarkAddresses');
  var request = require("request");
  try {

    var options = {
      method: 'GET',
      url: 'https://dawa.aws.dk/adresser',
      qs: { vejnavn: address, struktur: 'mini' },
      headers:
      {
      }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);

      if (body != null) {
        var responseData = JSON.parse(body);
        console.log(responseData);
        if (responseData.length > 0) {
          //console.log(responseData.length);
          let DenmarkAddress = mongoose.models.denmark_addresses;

          for (let i = 0; i < responseData.length; i++) {
            var addressModel = new DenmarkAddress({
              "dm_id": responseData[i].id,
              "status": responseData[i].status,
              "vejkode": responseData[i].vejkode,
              "vejnavn": responseData[i].vejnavn,
              "adresseringsvejnavn": responseData[i].adresseringsvejnavn,
              "husnr": responseData[i].husnr,
              "supplerendebynavn": responseData[i].supplerendebynavn,
              "postnr": responseData[i].postnr,
              "postnrnavn": responseData[i].postnrnavn,
              "kommunekode": responseData[i].kommunekode,
              "x": responseData[i].x,
              "y": responseData[i].y

            });

            addressModel.save(function (err, doc) {

            });

          }
        } else {
          //console.log(responseData);
          console.log(`Invalid address supplied: Address: ${address}`);

        }

      } else {
        console.log(`Invalid address supplied: Address: ${address}`);

      }

    });
    return res.json({ success: true });
  }

  catch (error) {
    console.log(error);
  }
});

router.post('/updateLatLong', function (req, res, next) {
  var request = require("request");

  var body = req.body.body ? req.body.body : '';
  var globalDeliveryData = req.body.globalDeliveryData ? req.body.globalDeliveryData : '';
  console.log(globalDeliveryData);

  //return res.send({body : body, globalDeliveryData : globalDeliveryData})

  var p = -1;
  var pickAdd = globalDeliveryData.pickup_location.address_1;
  var pickAddArr = pickAdd.split(" ");
  var zip = globalDeliveryData.pickup_location.zip;
  var lastIndex = pickAdd.lastIndexOf(" ");
  if (lastIndex == -1) {
    var pickAddressFirst = pickAddressFirst;
  }
  var pickAddressFirst = pickAdd.substring(0, lastIndex);
  var optionsDawa = {
    method: 'GET',
    // rejectUnauthorized: false,
    // requestCert: true,
    // agent: false,
    url: 'http://dawa.aws.dk/adresser',
    qs: { vejnavn: pickAddressFirst, struktur: 'mini' },
    headers: {
      'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0'
    },
    //qs: { vejnavn: str, husnr: addressArr[addressArr.length - 1], postnr: zipCode, struktur: 'mini' },

  };

  let denmakrAddressPromise = new Promise(function (resolve, reject) {
    try {
      request(optionsDawa, function (error, response, denmarkAData) {
        if (denmarkAData != null) {
          var responseData = JSON.parse(denmarkAData);
          // console.log(responseData);
          if (responseData.length > 0) {

            for (let k = 0; k < responseData.length; k++) {

              if (responseData[k].husnr.trim() == pickAddArr[pickAddArr.length - 1] && responseData[k].postnr.trim() == zip) {
                console.log("in")
                var latLong = [responseData[k].x, responseData[k].y];
                break;
              }
            }
            if (typeof latLong == 'undefined') {
              var latLong = [responseData[0].x, responseData[0].y];
            }
            resolve(latLong);
          }
        } else {
          let geoCodeRequest = new Promise(function (resolve, reject) {
            resolve(GeoCoder.geocode({
              address: address,
              country: "Denmark",
              zipcode: zipCode
            }));
          });
          geoCodeRequest.then(location => {
            if (!location.length) {
              console.log("Google Error fetching error");
              throw new Error();
            } else {
              resolve([location[0].longitude, location[0].latitude]);
            }
          });
        }
      });
    } catch (error) {
      console.log(error);
      reject(error)
    }

  });
  denmakrAddressPromise.then(addData => {

    var asyncLoop = require('node-async-loop');
    asyncLoop(body, function (delivery, next) {
      p++;
     // var isdefault = globalDeliveryData[p].is_default_pickup;
     // if (isdefault) {
        var pickupLocationLatLong = addData;
     // } else {
     //   var pickupLocationLatLong = ["10", "10"];
     // }
      var address = delivery.dropoff_location.address.trim();
      var zipCode = delivery.dropoff_location.zip.trim();
      var pickupaddress = delivery.pickup_location.address.trim();
      var pickupzipCode = delivery.pickup_location.zip;
      var addressArr = address.split(" ");
      var lastIndex = address.lastIndexOf(" ");
      var str = address.substring(0, lastIndex);

      if (lastIndex == -1) {
        var str = address;
      }
      // var addressArrPick = pickupaddress.split(" ");
      var lastIndexPick = pickupaddress.lastIndexOf(" ");
      //var strPickup = pickupaddress.substring(0, lastIndexPick);
      var myquery = { _id: delivery.scheduled_id };
      // var addressCode = str + '_' + addressArr[addressArr.length - 1] + '_' + zipCode;
      // var addressCodePickup = str + '_' + addressArrPick[addressArrPick.length - 1] + '_' + pickupzipCode;

      var options = {
        method: 'GET',
        rejectUnauthorized: false,
        requestCert: true,
        agent: false,
        port: 443,
        url: 'https://dawa.aws.dk/adresser',

        //qs: { vejnavn: str, husnr: addressArr[addressArr.length - 1], postnr: zipCode, struktur: 'mini' },
        qs: { vejnavn: str, struktur: 'mini' },
        headers: {
          'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0'
        },
      };

      try {
        request(options, function (error, response, denmarkAData) {
          if (error) console.log(error);// throw new Error(error);

          if (denmarkAData != null) {
            var responseData = JSON.parse(denmarkAData);
            if (responseData.length > 0) {
              for (let k = 0; k < responseData.length; k++) {
                if (responseData[k].husnr.trim().toLowerCase() == addressArr[addressArr.length - 1].toLowerCase() && responseData[k].postnr.trim() == zipCode) {
                  //console.log("In")
                  var latLongDelivery = [responseData[k].x, responseData[k].y];
                  break;
                }
              }

              if (typeof latLongDelivery == 'undefined') {
                for (let k = 0; k < responseData.length; k++) {
                  if (parseInt(responseData[k].husnr.trim()) == parseInt(addressArr[addressArr.length - 1]) && responseData[k].postnr.trim() == zipCode) {
                    console.log("Innner")
                    var latLongDelivery = [responseData[k].x, responseData[k].y];
                    break;
                  }
                }
              }


              if (typeof latLongDelivery == 'undefined') {
                console.log("Innner undefined");
                var latLongDelivery = [responseData[0].x, responseData[0].y];
              }

              var newvalues = {
                $set: {
                  deliverycoordinates: latLongDelivery,
                  pickupcoordinates: pickupLocationLatLong
                }
              }

              routes.update(myquery, newvalues, function (err, update) {
                if (err) console.log(err);
                next();
              });
            } else {

              let geoCodeRequest = new Promise(function (resolve, reject) {
                resolve(GeoCoder.geocode({
                  address: address,
                  country: "Denmark",
                  zipcode: zipCode
                }));
              });
              geoCodeRequest.then(location => {
                if (!location.length)
                  throw new Error();

                var newvalues = {
                  $set: {
                    pickupcoordinates: pickupLocationLatLong,
                    deliverycoordinates: [location[0].longitude, location[0].latitude]
                  }
                };
                console.log(newvalues);
                routes.update(myquery, newvalues, function (err, update) {
                  if (err) console.log(err);
                  next();
                });
              });

            }
          } else {
            let geoCodeRequest = new Promise(function (resolve, reject) {
              resolve(GeoCoder.geocode({
                address: address,
                country: "Denmark",
                zipcode: zipCode
              }));
            });
            geoCodeRequest.then(location => {
              if (!location.length)
                throw new Error();

              var newvalues = {
                $set: {
                  pickupcoordinates: pickupLocationLatLong,
                  deliverycoordinates: [location[0].longitude, location[0].latitude]
                }
              };
              console.log(newvalues);
              routes.update(myquery, newvalues, function (err, update) {
                if (err) console.log(err);
                next();
              });
            });
          }
        });
      } catch (error) {
        let geoCodeRequest = new Promise(function (resolve, reject) {
          resolve(GeoCoder.geocode({
            address: address,
            country: "Denmark",
            zipcode: zipCode
          }));
        });
        geoCodeRequest.then(location => {
          if (!location.length)
            throw new Error();

          var newvalues = {
            $set: {
              pickupcoordinates: pickupLocationLatLong,
              deliverycoordinates: [location[0].longitude, location[0].latitude]
            }
          };
          console.log(newvalues);
          routes.update(myquery, newvalues, function (err, update) {
            if (err) console.log(err);
            next();
          });
        });
        //next();
      }



    }, function (err) {
      if (err) {
        console.error('Error: ' + err.message);
        return;
      }
      console.log('Finished! The Job');
    });
  });
  return res.send({body : true});
});

module.exports = router;
