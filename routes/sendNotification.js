var express = require('express');
var Controller = require("../lib/framework/controller");
var Notification = require("../lib/notification");

var SmsNotificationController = new Controller({
    AdminNotifierWithoutAlert: Notification.AdminNotifier.deliveryWithoutCarrierAlert,
    AdminNotifierWithScheduledAlert: Notification.AdminNotifier.deliveryScheduledAlert,
    CarrierInvitedNotification: Notification.CarrierInvitedNotification,
    CarrierAcceptedNotification: Notification.CarrierAcceptedNotification,
    DeliveryCanceledNotification: Notification.DeliveryCanceledNotification,
    TransRequestAlertNotification: Notification.TransRequestAlertNotification,
    TransRequestAdminNotification: Notification.TransRequestAdminNotification,
    StatusChangedNotification: Notification.StatusChangedNotification,
}, {
    AdminNotifierWithoutAlert: { auth: false },
    AdminNotifierWithScheduledAlert: { auth: false },
    CarrierInvitedNotification: { auth: false },
    CarrierAcceptedNotification: { auth: false },
    DeliveryCanceledNotification: { auth: false },
    TransRequestAlertNotification: { auth: false },
    TransRequestAdminNotification: { auth: false },
    StatusChangedNotification: { auth: false },
});

var router = express.Router();

router.post('/adminNotifierWithoutAlert', SmsNotificationController.AdminNotifierWithoutAlert);
router.post('/adminNotifierWithScheduledAlert', SmsNotificationController.AdminNotifierWithScheduledAlert);
router.post('/carrierInvited', SmsNotificationController.CarrierInvitedNotification);
router.post('/carrierAccepted', SmsNotificationController.CarrierAcceptedNotification);
router.post('/deliveryCanceled', SmsNotificationController.DeliveryCanceledNotification);
router.post('/transRequestAlert', SmsNotificationController.TransRequestAlertNotification);
router.post('/transRequestAdmin', SmsNotificationController.TransRequestAdminNotification);
router.post('/statusChanged', SmsNotificationController.StatusChangedNotification);

module.exports = router;
