var express = require("express");
var Response = require('../services/Response');
var SmsTemplate = require('../models/SmsTemplate');
var EmailTemplate = require('../models/EmailTemplate');
var mongoose = require('mongoose');

var router = express.Router();	

router.post('/email', function(req, res) {
    var data = req.body;
    var user = req.user;
    data["client_id"] = data["client_id"] || null;
    EmailTemplate.findOneAndUpdate({client_id: data.client_id, type:data.type}, data, {upsert: true, setDefaultsOnInsert: true})
        .then(function (created) {
            Response.success(res, 'Template setting.', data);
        })
        .catch(function (err) {
            Response.error(res, err);
        });
});

router.get('/email/:clientId/:type', function (req, res) {
    var clientId = null;
    if (req.params.clientId&&req.params.clientId!='admin') {
        clientId= req.params.clientId.toObjectId();
    }
    
    var type = req.params.type;
    EmailTemplate.findOne({client_id: clientId, type:type})
        .then(function (template) {
            if(template){
                Response.success(res, 'Template Setting.', template);
            }else{
                Response.noData(res, 'Template not found.')
            }
        });

});

router.post('/sms', function(req, res) {
    var data = req.body;
    var user = req.user;
    data["client_id"] = data["client_id"] || null;
    SmsTemplate.findOneAndUpdate({client_id: data.client_id, type:data.type}, data, {upsert: true, setDefaultsOnInsert: true})
        .then(function (created) {
            Response.success(res, 'Template setting.', data);
        })
        .catch(function (err) {
            Response.error(res, err);
        });
});

router.get('/sms/:clientId/:type', function (req, res) {
    var clientId = null;
    if (req.params.clientId&&req.params.clientId!='admin') {
        clientId= req.params.clientId.toObjectId();
    }
    var type = req.params.type;
    SmsTemplate.findOne({client_id: clientId, type:type})
        .then(function (template) {
            if(template){
                Response.success(res, 'Template Setting.', template);
            }else{
                Response.noData(res, 'Template not found.')
            }
        });

});


module.exports = router;
