var express = require("express");
var Response = require("../services/Response");
var DeliverySettings = require("../models/deliverysettings");
var mongoose = require("mongoose");
var AccountRepository = require("../lib/user/repository/account");
var DeliverySettingsViewModel = require("../viewModels/deliverySettings");
var ServiceModel = require("../models/Service");
var ResponseError = require("../lib/framework/responseError");
let Controller = require("../lib/framework/controller");
let Versioning = require("../utils/Versioning");
var ZonesModel = require("../models/postalCode");
let _ = require("underscore");
let moment = require("moment");
var router = express.Router();
let globalVersion;

var week_days = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday"
];

//addeed by Li, 2018/11/6
let delivery_areas = [
  {
    country: "DK",
    zip_from: "0",
    zip_to: "2999",
    delivery_windows: [
      "16:00:00Z/21:00:00Z",
      "16:00:00Z/18:00:00Z",
      "18:00:00Z/20:00:00Z"
    ]
  },
  {
    country: "DK",
    zip_from: "3000",
    zip_to: "7999",
    delivery_windows: ["16:00:00Z/21:00:00Z"]
  },
  {
    country: "DK",
    zip_from: "8000",
    zip_to: "8999",
    delivery_windows: ["16:30:00Z/21:00:00Z"]
  },
  {
    country: "DK",
    zip_from: "9000",
    zip_to: "9999",
    delivery_windows: ["17:30:00Z/21:00:00Z"]
  },
  {
    country: "FIN",
    zip_from: "00001",
    zip_to: "01531",
    delivery_windows: ["17:00:00Z/19:00:00Z", "19:00:00Z/21:00:00Z"]
  },
  {
    country: "SWE",
    zip_from: "10000",
    zip_to: "19999",
    delivery_windows: [
      "16:00:00Z/17:00:00Z",
      "17:00:00Z/19:00:00Z",
      "19:00:00Z/21:00:00Z"
    ]
  },
  {
    country: "SWE",
    zip_from: "40000",
    zip_to: "42999",
    delivery_windows: ["17:30:00Z/19:00:00Z", "19:00:00Z/21:00:00Z"]
  },
  {
    country: "SWE",
    zip_from: "44000",
    zip_to: "44999",
    delivery_windows: ["17:30:00Z/19:00:00Z", "19:00:00Z/21:00:00Z"]
  },
  {
    country: "SWE",
    zip_from: "55000",
    zip_to: "56999",
    delivery_windows: ["17:30:00Z/19:00:00Z", "19:00:00Z/21:00:00Z"]
  },
  {
    country: "SWE",
    zip_from: "23400",
    zip_to: "23499",
    delivery_windows: [
      "16:00:00Z/17:00:00Z",
      "17:00:00Z/19:00:00Z",
      "19:00:00Z/21:00:00Z"
    ]
  },
  {
    country: "SWE",
    zip_from: "20000",
    zip_to: "22999",
    delivery_windows: [
      "16:00:00Z/17:00:00Z",
      "17:00:00Z/19:00:00Z",
      "19:00:00Z/21:00:00Z"
    ]
  },
  {
    country: "SWE",
    zip_from: "26100",
    zip_to: "26399",
    delivery_windows: [
      "16:00:00Z/21:00:00Z",
      "17:00:00Z/19:00:00Z",
      "19:00:00Z/21:00:00Z"
    ]
  },
  {
    country: "SWE",
    zip_from: "25200",
    zip_to: "25699",
    delivery_windows: [
      "16:00:00Z/21:00:00Z",
      "17:00:00Z/19:00:00Z",
      "19:00:00Z/21:00:00Z"
    ]
  },
  {
    country: "SWE",
    zip_from: "30000",
    zip_to: "33999",
    delivery_windows: [
      "16:00:00Z/21:00:00Z",
      "17:00:00Z/19:00:00Z",
      "19:00:00Z/21:00:00Z"
    ]
  },
  {
    country: "SWE",
    zip_from: "27100",
    zip_to: "27199",
    delivery_windows: ["17:00:00Z/19:00:00Z", "19:00:00Z/21:00:00Z"]
  }
];

let createDefault = token => {
  if (!token) {
    throw new ResponseError(401, "Not authorized.");
  }

  return AccountRepository.getByApiToken(token)
    .then(user => {
      if (!user) {
        throw new ResponseError(400, "User not found.");
      }

      let defaultSettings = {
        clientId: user._id,
        addresses: [],
        delivery_window_end: "",
        allow_to_send_email: true,
        allow_to_send_SMS: true,
        allow_to_reschedule: true,
        allow_signature: true,
        allow_to_receive_daily_report: true,
        allow_to_receive_end_of_day_report: true,
        allow_to_receive_end_of_week_report: true,
        allow_to_receive_issue_report: true,
        home_delivery: true,
        store_pickup: true,
        week_days_available: [
          "Monday",
          "Tuesday",
          "Wednesday",
          "Thursday",
          "Friday"
        ],
        zip: [],
        allowed_pickups_zones: [
          {
            range_from: "0",
            range_to: "9999"
          }
        ],
        delivery_windows: [],
        pickup_deadline: "",
        pickup_deadline_to: "",
        delivery_window_start: "",
        dropshipping_enabled: true,
        deliveryaddress: "Axeltorv, København, Denmark"
      };

      return DeliverySettings.create(defaultSettings);
    })
    .then(settings => {
      if (settings) {
        return new DeliverySettingsViewModel(settings._doc);
      }
    });
};

let signupSettings = (body, token) => {
  if (!token) {
    throw new ResponseError(401, "Not authorized.");
  }
  let info = body;

  return AccountRepository.getByApiToken(token).then(user => {
    if (!user) {
      throw new ResponseError(400, "User not found.");
    }
    return AccountRepository.findOneAndUpdate(user._id, {
      info: info,
      approved: true
    });
  });
};

let get = (version, token, query = {}) => {
  if (!token) {
    throw new ResponseError(401, "Not authorized.");
  }

  //added by Li, 2018/11/05
  var address = query.address;
  //console.log("Query address = ", address)
  //return staticOutput;
  var settings = [];
  return AccountRepository.getByApiToken(token)
    .then(user => {
      if (!user) {
        throw new ResponseError(400, "User not found.");
      }

      globalVersion = Versioning.normaliseVersion(version);
      var groupId = user._id;
      if (user.groupId) {
        groupId = ensureObjectId(user.groupId);
      }

      return DeliverySettings.findOne({ clientId: groupId });
    })
    .then(rets => {
      if (!rets) {
        throw new ResponseError(400, "Settings not found.");
      }
      settings = rets;

      //   return ZonesModel.find({'EVENING':true}, {'ZIP':1})
      // })
      // .then( zones => {
      //     zones = zones.map(o=>o.ZIP)
      //updated by Li, 2018/11/05
      if (globalVersion == "v1.5") {
        var serviceIds = [];
        settings = settings._doc;
        //console.log('settings', settings.service_enabled)
        _.map(settings.service_enabled, item => {
          serviceIds.push(item.service_id);
        });

        return ServiceModel.find({ _id: { $in: serviceIds } }).then(
          services => {
            if (services.length == 0) {
              throw new ResponseError(400, "Service not found.");
            }

            var results = [];
            //console.log('services', services)
            _.map(services, service => {
              service = service._doc;
              var serviceOption = _.find(settings.service_enabled, item => {
                return item.service_id.toString() == service._id.toString();
              });

              var delivery_windows = [];
              var serviceDeliveryAreas = [];

              try {
                if (service.delivery_window) {
                  delivery_windows = JSON.parse(service.delivery_window) || [];
                  //console.log('delivery_windows', delivery_windows)
                }
                if (service.delivery_area) {
                  serviceDeliveryAreas =
                    JSON.parse(service.delivery_area) || [];
                    //console.log('serviceDeliveryAreas', serviceDeliveryAreas)
                }
              } catch (e) {}

              if (serviceDeliveryAreas.length == 0) {
                serviceDeliveryAreas = _.filter(delivery_areas, item => {
                  return (
                    (serviceOption.countries || ["DK"]).indexOf(item.country) !=
                    -1
                  );
                });
              }

              if (delivery_windows.length == 0) {
                _.map(serviceDeliveryAreas, item => {
                  if (
                    item.delivery_windows.indexOf("16:00:00Z/21:00:00Z") == -1
                  )
                    item.delivery_windows.unshift("16:00:00Z/21:00:00Z");

                  delivery_windows = delivery_windows.concat(
                    item.delivery_windows
                  );
                  delivery_windows = _.uniq(delivery_windows);
                });

                delivery_windows = _.sortBy(delivery_windows, o => {
                  return o;
                });
              }
              // if (!delivery_windows || !delivery_windows[0]) {
              //   return;
              // }

              var time = delivery_windows[0]
                ? delivery_windows[0].split("/")[0]
                : null;

              var weekDaysAvailable = settings.week_days_available || [];
              weekDaysAvailable = weekDaysAvailable.map(
                d => week_days.indexOf(d) + 1
              );

              var serviceSetting = {
                service_name: service.name,
                country_availability: serviceOption.countries,
                week_days_available: weekDaysAvailable,
                delivery_window_default: (service.delivery_window_default||"16:00:00Z/21:00:00Z"), //delivery_windows[0],
                delivery_windows: delivery_windows,
                limits: {
                  weight: 40,
                  volume: 0.2,
                  height: 1,
                  width: 1,
                  length: 2.2
                },
                pickup_point: {
                  enabled: false,
                  locations: []
                },
                home_delivery: true,
                estimated_arrival: {
                  time: time
                    ? moment().format("YYYY-MM-DD") + "T" + time
                    : null,
                  margin: 20
                },
                delivery_areas: serviceDeliveryAreas
              };

              results.push(serviceSetting);
            });

            return results;
          }
        );
      } else {
        if (!settings) {
          throw new ResponseError(400, "Settings not found.");
        }

        return new DeliverySettingsViewModel(settings._doc);
      }
    });
};

let SettingsController = new Controller(
  {
    createDefault: createDefault,
    get: get,
    signupSettings: signupSettings
  },
  {
    createDefault: {
      auth: false
    },
    get: {
      auth: false
    },
    signupSettings: {
      auth: false,
      schema: "validator/schema/payment-settings"
    }
  }
);

const ROUTE_BASE_VERSIONING = `/:version/settings`;
const ROUTE_BASE_STATIC = "/v1/settings";

router.post(
  `${ROUTE_BASE_STATIC}/signup-settings`,
  SettingsController.signupSettings
);
router.post(
  `${ROUTE_BASE_STATIC}/config/default`,
  SettingsController.createDefault
);
router.get(`${ROUTE_BASE_VERSIONING}/config`, SettingsController.get);

module.exports = router;
