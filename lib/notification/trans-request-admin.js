var Notification = require("./notification");
var MailStrategy = require("./strategy/mail");
var MailSender = require("./sender/mailgun");
var config = require("../config")

function TransRequestAdminNotification(transRequest, delivery) {
    Notification.call(this);
    var that = this;

    this.configure = configure;
    this.delivery = delivery;
    this.transRequest = transRequest;

    function configure() {
        var mailConfig = {
            sender: that.delivery.buyer.email,
            recipients: config.email.recipients,
            subject: "Delivery request #"+that.transRequest._id.toString()+" added to Marketplace.",
            template: "trans-request-admin.html"
        };
        var mail = new MailStrategy(mailConfig, MailSender);
        this.addStrategy(mail);
    }
}

TransRequestAdminNotification.prototype = Object.create(Notification.prototype);
TransRequestAdminNotification.prototype.constructor = TransRequestAdminNotification;

module.exports = TransRequestAdminNotification;
