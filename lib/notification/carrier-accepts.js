var Notification = require("./notification");
var PushStrategy = require("./strategy/push");
var FCMStrategy = require("./strategy/fcm");
var PushSender = require("./sender/push");

function CarrierAcceptedNotification(delivery) {
    Notification.call(this, delivery);
    var that = this;

    this.delivery = delivery;
    this.configure = configure;

    function configure() {
        var pushConfig = {
            channel: that.delivery.buyer.accountId.toString(),
            event: "carrier_accepted",
            body: "Delivery accepted by a carrier"
        };

        var push = new PushStrategy(pushConfig, PushSender);
        this.addStrategy(push);

        var fcm = new FCMStrategy(pushConfig);
        this.addStrategy(fcm);
    }
}

CarrierAcceptedNotification.prototype = Object.create(Notification.prototype);
CarrierAcceptedNotification.prototype.constructor = CarrierAcceptedNotification;

module.exports = CarrierAcceptedNotification;

