var Notification = require("./notification");
var TwilioStrategy = require("./strategy/twilio");
var TwilioSender = require("./sender/twilio");

function DeliveryCompleteNotification(delivery) {
    Notification.call(this, delivery);
    var that = this;

    this.delivery = delivery;
    this.configure = configure;

    function configure() {
        var pushConfig = {
          from: '+151368586565',
          body: 'You package was delivered'
        };

        var push = new TwilioStrategy(pushConfig, TwilioSender);

        this.addStrategy(push);
    }
}

DeliveryCompleteNotification.prototype = Object.create(Notification.prototype);
DeliveryCompleteNotification.prototype.constructor = DeliveryCompleteNotification;

module.exports = DeliveryCompleteNotification;

