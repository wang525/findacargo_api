var Promise = require("bluebird");

function Notification() {
    var that = this;

    this.strategies = [];

    this.addStrategy = addStrategy;
    this.send = send;
    this.configure = configure;

    function addStrategy(strategy) {
        this.strategies.push(strategy);

        return this;
    }

    function send(data) {
        this.configure();
        var promises = this.strategies.map(function(item){
            return item.send(data);
        });

        return Promise.all(promises)
                .catch(function(err){
                    console.error(err);
                });
    }

    function configure() {
        throw "Notification not configured properly";
    }
}

module.exports = Notification;
