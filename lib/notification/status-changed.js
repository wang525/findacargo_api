var Notification = require("./notification");
var PushStrategy = require("./strategy/push");
var PushSender = require("./sender/push");
var FCMStrategy = require("./strategy/fcm");

function StatusChangedNotification(buyerId) {
    Notification.call(this, buyerId);
    var that = this;

    this.buyerId = buyerId;
    this.configure = configure;

    function configure() {
        var pushConfig = {
            channel: that.buyerId.toString(),
            event: "status_changed",
            body: "Delivery updated"
        };

        var push = new PushStrategy(pushConfig, PushSender)
        this.addStrategy(push);

        var fcm = new FCMStrategy(pushConfig);
        this.addStrategy(fcm);
    }
}

StatusChangedNotification.prototype = Object.create(Notification.prototype);
StatusChangedNotification.prototype.constructor = StatusChangedNotification;

module.exports = StatusChangedNotification;
