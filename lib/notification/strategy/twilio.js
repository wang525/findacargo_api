var Promise = require("bluebird");
var Strategy = require("./strategy");

function TwilioStrategy(config, sender) {
    Strategy.call(this, config, sender);
    var that = this;
    this.send = send;

  function send(data) {// data comes from carrier-state.js line 53
      //
      // Check the data here to see if you have everything that is necessary
      //

        return new Promise(function(resolve, reject){
          this.sender.messages.create({
            to: '+' + data.ccode + data.phone, // Need correct data here!!!!!!
              from: config.from,
              body: config.body
          }, function (err, message) {
              if (err) {
                reject(err);
              } else {
                resolve(message);
              }
          });
        });
    }
}


TwilioStrategy.prototype = Object.create(Strategy.prototype);
TwilioStrategy.prototype.constructor = TwilioStrategy;

module.exports = TwilioStrategy;
