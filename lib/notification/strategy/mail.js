var Promise = require("bluebird");
var mailcomposer = require("mailcomposer");
var rs = require("fs");
var mustache = require("mustache");
var Strategy = require("./strategy");

function MailStrategy(config, sender) {
    Strategy.call(this, config, sender);
    var that = this;
    this.send = send;

    function send(data) {
        var filename = __dirname+"/../mail-templates/"+that.config.template;
        var template = rs.readFileSync(filename, "utf-8");
        var processedData = mustache.render(template, data);
        var mail = mailcomposer({
            from: that.config.sender,
            to: that.config.recipients,
            subject: that.config.subject,
            body: 'Test email text',
            html: processedData
        });
        
        return new Promise(function(resolve, reject){
            mail.build(function(mailBuildError, message){
                that.sender.sendRaw(that.config.sender, that.config.recipients, message.toString("ascii"), null, function(err){
                    if(err){
                        return reject(err);
                    }
                    resolve(true);
                });
            });

        });
    }
}


MailStrategy.prototype = Object.create(Strategy.prototype);
MailStrategy.prototype.constructor = MailStrategy;

module.exports = MailStrategy;

