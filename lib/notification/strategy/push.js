var Promise = require("bluebird");
var Strategy = require("./strategy");

function PushStrategy(config, sender) {
    Strategy.call(this, config, sender);
    var that = this;
    this.send = send;

    function send(data) {
        return new Promise(function(resolve, reject){
            that.sender.trigger(that.config.channel, that.config.event, data, false, function(err, req, res){
                if(err){
                    return reject(err);
                }

                resolve(res.body);
            });
        });
    }
}


PushStrategy.prototype = Object.create(Strategy.prototype);
PushStrategy.prototype.constructor = PushStrategy;

module.exports = PushStrategy;
