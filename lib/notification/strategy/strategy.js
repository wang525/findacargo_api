function NotificationStrategy(config, sender) {
    var that = this;

    this.config = config;
    this.sender = sender;

    this.send = send;

    function send(data) {
        throw "Can't send the notification";
    }
}

module.exports = NotificationStrategy;
