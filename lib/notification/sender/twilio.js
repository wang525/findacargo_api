var twilio = require("twilio");
var config = require("../../config").twilio;
var client = twilio(config.accountSid, config.authToken);


module.exports = client;
