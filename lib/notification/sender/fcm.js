var firebase = require("firebase-messaging");
var config = require("../../config").firebase;
var client = new firebase(config.key);

module.exports = client;
