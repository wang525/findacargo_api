var Notification = require("./notification");
var PushStrategy = require("./strategy/push");
var FCMStrategy = require("./strategy/fcm");
var PushSender = require("./sender/push");

function DeliveryStatusNotification(buyerId) {
    Notification.call(this, buyerId);
    var that = this;

    this.buyerId = buyerId;
    this.configure = configure;

    function configure() {
        var pushConfig = {
            channel: that.buyerId.toString(),
            event: "delivery_status",
            body: "Delivery status update"
        };

        var push = new PushStrategy(pushConfig, PushSender);
        this.addStrategy(push);

        var fcm = new FCMStrategy(pushConfig);
        this.addStrategy(fcm);
    }
}

DeliveryStatusNotification.prototype = Object.create(Notification.prototype);
DeliveryStatusNotification.prototype.constructor = DeliveryStatusNotification;

module.exports = DeliveryStatusNotification;
