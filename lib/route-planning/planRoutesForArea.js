let DriverZoneAssignment = require('../../models/DriverZoneAssignment'),
    DriverDepartmentAssignments = require('../../models/DriverDepartmentAssignments'),
    ScheduledDeliveryRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    AccountRepository = require('../user/repository/account.js'),
    _ = require('lodash'),
    RoutePlannerService = require('../../services/RoutePlannerService'),
    DepartmentRepository = require('../departments/repository/departmentRepository');
let PostalCodeRepository = require('../postal-codes/repository');

    let async = require('async')
var request = require("request");
let googleKey = 'AIzaSyAxGdRPq-XUIXeVRdGKOJHLNzO9Nj4h_hI'

const TYPE_DISTRIBUTION_ZONE = 'Distribution';
const TYPE_DEDICATED = 'Dedicated Drivers';

let maxDeliveryCount = 39
let maxDuration = 3600 * 4
let apiCallCount = 0
let bufferTime = 180 //(3 min)
let ORIGIN_LOCATION = "Værkstedvej 34, 2500 valby"

function PlanRoutesForArea(request, response) { 
    // let resultCandidates = []
    // let cacheMap = {}
    // let routes = []
    // let seeds = []
    // let apiCallCount = 0
    // let deliveryCount = 0
    // let areas = []

    var deliveries = request.body.deliveries;
    let ids = deliveries.map(x => {
        return x.delivery_id.toObjectId();
    });
    
    return new Promise((resolve, reject) => {
        ScheduledDeliveryRepository.scheduled.findAll(
            {'_id': { $in: ids }}
        )
        .toArray((err, res) => {
            if (err) return reject(err);
            return resolve(res);
        })
    })
    .then(rets => {
        var deliveries = rets

        var origin = ORIGIN_LOCATION //deliveries[0].pickupcoordinates[1]+','+deliveries[0].pickupcoordinates[0]
        var warehouse = getAddress(deliveries[0].pickupaddress, deliveries[0].pickupzip, deliveries[0].pickupcity)
        var destinations = deliveries.map((delivery)=>{
            return {
                location: delivery.deliverycoordinates[1]+','+delivery.deliverycoordinates[0],
                id: delivery._id
            }
        })

        getNearestDelivery(destinations, (err, nearIndex)=>{
            if(err) {
                return response.end(JSON.stringify({status: 500, msg: err}));
            }
            origin = destinations[nearIndex]
            destinations.splice(nearIndex)

            var currentRoute = {
                duration:0,
                distance:0,
                deliveries:[origin.id],
                durations:[],
                distances:[]
            }

            if(destinations.length == 0) {
                return response.end(JSON.stringify({status: 200, result: currentRoute}))
            } else {
                getDistance(origin.location, destinations, (err, ret)=>{
                    if(err) {
                        return response.end(JSON.stringify({status: 500, msg: err}));
                    } else {
                        var {durations, distances, orders} = ret
                        var index = 0
                        durations.forEach((duration)=>{
                            var currentDuration = duration + bufferTime
                            
                            currentRoute.duration += currentDuration
                            currentRoute.distance += distances[index]
                            currentRoute.durations.push(currentDuration)
                            currentRoute.distances.push(distances[index])
                            currentRoute.deliveries.push(destinations[orders[index]].id)
                            index++
                        })
                        
                        response.end(JSON.stringify({status: 200, result: currentRoute}))
                    }
                })
            }
        })
        // var totalDeliveries = rets
        // var warehouseLocations = _.uniq(rets.map((d)=>(d.pickupcoordinates[1]+','+d.pickupcoordinates[0])))
        // async.mapSeries(warehouseLocations, (warehouseLocation, callback)=>{
            
        // }, (err, rets)=>{
        //     if(err) {
        //         return response.end(JSON.stringify({status: 500, msg: err}));
        //     }

        //     return response.end(JSON.stringify({status: 200, result: rets}));
        // })
    })
    .catch(err => {
        console.error(err);
        response.writeHead(500, {"Content-Type": "application/json"});
        return response.end(JSON.stringify({status: 500, msg: err}));
    });
}

function getDistance(origin, originalWays, cb) {
    apiCallCount++
    var ways = originalWays.map(o=>o.location)

    var request = require("request");
    var url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin}&destinations=${ways.join('|')}&key=${googleKey}`;
    var options = {
        method: 'GET',
        url: encodeURI(url),
        //qs: { origins: startLocation, destinations: ways, key: googleKey }
    };

    request(options, function (error, response, body) {
        if (error) {
           return cb(error)
        }
        body = JSON.parse(body)
        var index = 0, maxDuration = 0, idx=0;
        _.forEach(body.rows[0].elements, (o)=>{

            if(o.duration.value > maxDuration) {
                maxDuration = o.duration.value
                index = idx
            }
            idx++
        })

        destination = ways[index]
        ways.splice(index, 1)

        var url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&waypoints=optimize:true|${ways.join('|')}&key=${googleKey}`;
        var options = {
            method: 'GET',
            url: encodeURI(url)
        };
        request(options, function (error, response, body) {
            if (error) {
            return cb(error)
            }
            body = JSON.parse(body)
            console.log('directions result', error, body)
            if(body.status=='OK' && body.routes && body.routes[0]) {
                var legs = body.routes[0].legs
                var distances = []
                var durations = []
                var tempOrders = body.routes[0].waypoint_order
                var orders = []

                for(var idx = 0 ; idx < legs.length ; idx++) {
                    orders.push((idx != legs.length - 1)?(tempOrders[idx]<index?tempOrders[idx]:tempOrders[idx]+1):index)
                    distances.push(legs[idx].distance.value)
                    durations.push(legs[idx].duration.value)
                }

                return cb(null, {
                    durations, distances, orders
                })
            }
            cb('wrong result')
        })
    })
}

function getSimpleDistance(origin, destination, cb) {
    apiCallCount++
    var url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${googleKey}`;
    var options = {
        method: 'GET',
        url: encodeURI(url)
    };
    request(options, function (error, response, body) {
        if (error) {
           return cb(error)
        }
        body = JSON.parse(body)
        console.log('distancematrix result', encodeURI(url), error, body)
        if(body.status=='OK' && body.routes && body.routes[0]) {

            var duration = body.routes[0].legs[0].duration.value
           var distance = body.routes[0].legs[0].distance.value

            return cb(null, {
                duration, distance
            })
        }
        cb('wrong result')
    })
}

function getAddress(address, zip, city) {
    return address + ", " + zip  + " " + city;
}

function getNearestDelivery(dests, cb) {
    var origin = ORIGIN_LOCATION //deliveries[0].pickupcoordinates[1]+','+deliveries[0].pickupcoordinates[0]
    var ways = _.map(dests, (d)=>{
        return d.location
    })

    var request = require("request");
    var url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin}&destinations=${ways.join('|')}&key=${googleKey}`;
    var options = {
        method: 'GET',
        url: encodeURI(url),
        //qs: { origins: startLocation, destinations: ways, key: googleKey }
    };

    request(options, function (error, response, body) {
        if (error) {
           return cb(error)
        }
        body = JSON.parse(body)
        var index = 0, maxDistance = 100000000, idx=0;
        _.forEach(body.rows[0].elements, (o)=>{

            if(o.distance.value < maxDistance) {
                maxDistance = o.distance.value
                index = idx
            }
            idx++
        })

        cb(null, index)
    })
}

module.exports = (req, res) => { return new PlanRoutesForArea(req, res) };