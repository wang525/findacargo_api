let DriverZoneAssignment = require('../../models/DriverZoneAssignment'),
    DriverDepartmentAssignments = require('../../models/DriverDepartmentAssignments'),
    ScheduledDeliveryRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    AccountRepository = require('../user/repository/account.js'),
    _ = require('lodash'),
    RoutePlannerService = require('../../services/RoutePlannerService'),
    DepartmentRepository = require('../departments/repository/departmentRepository');
let PostalCodeRepository = require('../postal-codes/repository');

    let async = require('async')
var request = require("request");
let googleKey = 'AIzaSyAxGdRPq-XUIXeVRdGKOJHLNzO9Nj4h_hI'

const TYPE_DISTRIBUTION_ZONE = 'Distribution';
const TYPE_DEDICATED = 'Dedicated Drivers';

let maxDeliveryCount = 39
let maxDuration = 3600 * 3
let apiCallCount = 0

function PlanRoutesForWarehouse(request, response) { 
    // let resultCandidates = []
    // let cacheMap = {}
    // let routes = []
    // let seeds = []
    // let apiCallCount = 0
    // let deliveryCount = 0
    // let areas = []

    var deliveries = request.body.deliveries;
    let ids = deliveries.map(x => {
        return x.delivery_id.toObjectId();
    });
    
    return new Promise((resolve, reject) => {
        ScheduledDeliveryRepository.scheduled.findAll(
            {'_id': { $in: ids }}
        )
        .toArray((err, res) => {
            if (err) return reject(err);
            return resolve(res);
        })
    })
    .then(rets => {
        console.log('step1', rets.length, rets[0])
        deliveries = rets
        return PostalCodeRepository.getAll()
    })
    .then(postalCodes =>{
        console.log('step2', postalCodes.length, postalCodes[0])
        var list = deliveries.map((delivery)=>{
            var postal = _.find(postalCodes, (o)=>{
                return String(o.ZIP) == String(delivery.deliveryzip)
            })
            if(postal) {
                return {
                    area:postal.AREA + postal.ZONE, 
                    pickupZip: delivery.pickupzip,
                    deliveryZip: delivery.deliveryzip,
                    pickupLocation: getAddress(delivery.pickupaddress, delivery.pickupzip, delivery.pickupcity), //delivery.pickupcoordinates, 
                    deliveryLocation :  getAddress(delivery.deliveryaddress, delivery.deliveryzip, delivery.deliverycity)//delivery.deliverycoordinates
                }
            }
        })
        console.log('step3', list)

        //group by pickup address first
        var warehouseCodes = _.uniq(list.map(o=>{
            return o.pickupZip
        }))

        return warehouseCodes.map(zipCode => {
            var localDeliveries = _.filter(list, (o)=>{
                return o.pickupZip == zipCode
            })
            
            //group by area
            var areaCodes = _.uniq(localDeliveries.map(o=>{
                return o.area
            }))

            console.log('areaCodes', areaCodes)
            var areas = []
            areaCodes.map((areaCode, index)=>{
                var areaList = _.filter(localDeliveries, (item)=> item.area == areaCode)
                areas.push({
                    area: areaCode,
                    deliveryCount: areaList.length,
                    index,
                    from:  areaList[0].pickupLocation,//String(areaList[0].pickupLocation[1])+','+String(areaList[0].pickupLocation[0]),
                    locations: areaList.map(d=>{
                        return d.deliveryLocation //String(d.deliveryLocation[1]) + ',' + String(d.deliveryLocation[0])
                    }),
                    zipCodes : areaList.map((d)=>d.deliveryZip)
                })
            })

            return {
                warehouse:zipCode,
                areas
            }
        })
    })
    .then(warehouseAreas=>{
        async.mapSeries(warehouseAreas, (warehouseArea, _cb)=>{
            let resultCandidates = []
            let cacheMap = {}
            let routes = []
            let seeds = []
            let deliveryCount = 0
            let areas = warehouseArea.areas
            let warehouse = warehouseArea.warehouse

            areas.map((a)=>{
                deliveryCount += a.locations.length
            })

            let startTime = Date.now()

            console.log('areas', areas)
            apiCallCount = 0
            getNextSuit({
                seeds:[],
                seedsSum:0
            }, areas, resultCandidates)
            let suitCalcTime = Date.now() - startTime
            console.log('Candidates Count', resultCandidates.length)
            resultCandidates.forEach((route)=>{
                route.seeds.map((seed)=>{
                    if(seeds.indexOf(seed) == -1)
                        seeds.push(seed)
                })
            })
            console.log('Seeds Count', seeds.length)

            async.mapSeries(resultCandidates, (route, cb)=>{
                async.map(route.seeds, (seed, callback)=>{
                    var origins = []
                    var destinations = []
                    areas.map((area)=>{
                        if((1<<area.index)&seed) {
                            destinations = destinations.concat(area.locations)
                            if(origins.length == 0) {
                                origins.push(area.from)
                            }
                        }
                    })
                    var oldResult = cacheMap[String(seed)]
                    if(!oldResult) {
                        getDistance(origins[0], destinations, (err, ret)=>{
                            if(err) {
                                cacheMap[String(seed)] = 'error'
                                callback(err, ret)
                            } else if(ret.duration > maxDuration) {
                                cacheMap[String(seed)] = 'exceed'
                                // all other seeds which includes this seed will be exceed
                                var filteredSeeds = _.filter(seeds, (s)=>{
                                    return ((seed&s) == seed) && !cacheMap[String(s)]
                                })

                                filteredSeeds.map(s => {
                                    cacheMap[String(s)] = 'exceed'
                                })

                                console.log('filteredSeeds', seed, filteredSeeds.length)

                                callback('exceed')
                            } else {
                                cacheMap[String(seed)] = ret
                                ret.seed = seed
                                callback(err, ret)
                            }
                    })
                    } else {
                        if(oldResult == 'error' || oldResult == 'exceed') {
                            callback(oldResult)
                        } else
                            callback(null, oldResult)
                    }
                }, (err, subs)=>{
                    if(!err) {
                        var routeDistance = 0
                        var routeDuration = 0
                        subs.map(s=>{
                            routeDistance += s.distance
                            routeDuration += s.duration
                        })
                        //console.log('route result', route, routeDistance, routeDuration, subs)
                        routes.push({
                            route,
                            routeDistance,
                            routeDuration,
                            subs
                        })
                    }

                    cb(null)
                })
            }, (err, rets)=>{
                let totalTime = Date.now() - startTime
                var shortestRoute = _.sortBy(routes, (o)=>{
                    return o.routeDuration
                })[0]

                //console.log(err, rets)
                console.log('---------------------------- Summary ----------------------------')
                console.log('Area count = ', areas.length)
                console.log('Delivery count = ', deliveryCount)
                console.log('Suit calc time = ', suitCalcTime)
                console.log('Total time = ', totalTime)
                console.log('Seeds count = ', seeds.length)
                console.log('Candidates routes = ', resultCandidates.length)
                console.log('Available routes = ', routes.length)
                console.log('API call count = ', apiCallCount)
                console.log('Final result = ', shortestRoute)
                console.log('----------------------------   End   -----------------------------')

                _cb(null, {
                    areas,
                    warehouse,
                    deliveryCount,
                    apiCallCount,
                    shortestRoute
                })
            })
        }, (err, results)=>{
            if(err) {
                return response.end(JSON.stringify({status: 500, msg: err}));
            } else {
                return response.end(JSON.stringify({status: 200, result: results}));
            }
        })
    })
    .catch(err => {
        console.error(err);
        response.writeHead(500, {"Content-Type": "application/json"});
        return response.end(JSON.stringify({status: 500, msg: err}));
    });
}

function getNextSuit(current, areas, resultCandidates) {
    var remain = _.filter(areas, (a)=>{
        return !(current.seedsSum & 1<<a.index) 
    })

    //console.log('remain.length', remain.length)
    if(remain.length == 0) {
        resultCandidates.push(current)
        //console.log(current)
        return
    }

    for(var i = 0 ; i < (1 << (remain.length - 1)) ; i++) {
        var key = (i<<1) + 1
        var clone = {
         seeds:current.seeds.slice(0),
         seedsSum: current.seedsSum   
        }
        var newSeed = 0
        var sumOfDelivery = 0
        for(var idx = 0 ; idx < remain.length ; idx++) {
            if((key & (1<<idx)) > 0) {
                newSeed |= (1 << remain[idx].index)
                sumOfDelivery += remain[idx].deliveryCount
            }
        }
        if(sumOfDelivery > maxDeliveryCount) {
            continue
        } 

        clone.seeds.push(newSeed)
        clone.seedsSum |= newSeed
        getNextSuit(clone, areas, resultCandidates)
    }
    return
}

function getDistance(origin, ways, cb) {
    apiCallCount++
    var url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${origin}&waypoints=optimize:true|${ways.join('|')}&key=${googleKey}`;
    var options = {
        method: 'GET',
        url: encodeURI(url)
    };
    request(options, function (error, response, body) {
        if (error) {
           return cb(error)
        }
        body = JSON.parse(body)
        console.log('distancematrix result', error, body)
        if(body.routes && body.routes[0]) {
            var legs = body.routes[0].legs
            var distance = 0
            var duration = 0

            for(var idx = 0 ; idx < legs.length - 1 ; idx++) {
                distance += legs[idx].distance.value
                duration += legs[idx].duration.value
            }

            return cb(null, {
                duration, distance
            })
        }
        cb('wrong result')
    })
}

function getAddress(address, zip, city) {
    return address + ", " + zip  + " " + city;
}

module.exports = (req, res) => { return new PlanRoutesForWarehouse(req, res) };