let DriverZoneAssignment = require('../../models/DriverZoneAssignment'),
    DriverDepartmentAssignments = require('../../models/DriverDepartmentAssignments'),
    ScheduledDeliveryRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    AccountRepository = require('../user/repository/account.js'),
    _ = require('lodash'),
    RoutePlannerService = require('../../services/RoutePlannerService'),
    DepartmentRepository = require('../departments/repository/departmentRepository');


const TYPE_DISTRIBUTION_ZONE = 'Distribution';
const TYPE_DEDICATED = 'Dedicated Drivers';

function PlanRoutes(request, response) { 
    var deliveries = request.body.deliveries;
    let ids = deliveries.map(x => {
        return x.delivery_id.toObjectId();
    });

    var data;
    
    return new Promise((resolve, reject) => {
        ScheduledDeliveryRepository.scheduled.findAll(
            {'_id': { $in: ids }}, 
            { creator: 1 })
        .toArray((err, res) => {
            if (err) return reject(err);
            return resolve(res);
        })
    })
    .then(res => {
        data = res;
        return AccountRepository.findAll(
            { '_id': { $in: res.map(item => item.creator) }}, 
            { approved: 1 });
    }).then(creators => {
        data = data.filter(element => {
            let creator = creators.find(x => x._id.equals(element.creator));
            return creator.approved;
        })
        deliveries = deliveries.filter(element => {
            let approvedDelivery = data.find(x => x._id.equals(element.delivery_id.toObjectId()))
            if (approvedDelivery) return true; 
            })
        return this.getDriversPerDelivery(deliveries)
    })
    .then(this.groupByDriver)
    .then((list) => {
        return this.planRoutePerDriver(list)
            .then((data) => {
                response.writeHead(200, {"Content-Type": "application/json"});
                return response.end(JSON.stringify({
                    assigned: data.assigned || [],
                    notAssigned: data.withoutDriver || []
                }));
            });
    })
    .catch(err => {
        console.error(err);
        response.writeHead(500, {"Content-Type": "application/json"});
        return response.end(JSON.stringify({status: 500, msg: err}));
    });
}

PlanRoutes.prototype.getDriversPerDelivery= function (list) {
    let updatedList = list.map(delivery => {
        if (!delivery) return;
        return DepartmentRepository.findDepartmentOfCompany(delivery.company_id, delivery.department)
            .then(department => {
                if (department.typeOfDelivery === TYPE_DISTRIBUTION_ZONE)
                    return this.findDriverForZone(delivery);

                return this.findDriverForDepartment(delivery);
            })
    });

    return Promise.all(updatedList)
};

PlanRoutes.prototype.findDriverForZone = function (delivery) {
    return DriverZoneAssignment.findOne({
        zone: delivery.zone
    }).then((result) => {
        if (!result) {
            delivery.haveDriver = false;
        }
        else {
            delivery.haveDriver = true;
            delivery.driver_id = result.driver;
        }
        return delivery;
    });
};

PlanRoutes.prototype.findDriverForDepartment = function (delivery) {
    return DepartmentRepository.findByCompanyId(delivery.company_id)
        .then(Company => {
            if (!Company) {
                delivery.haveDriver = false;
                return delivery
            }

            return DriverDepartmentAssignments.findOne({
                company: Company._id,
                department: delivery.department
            }).then(assignment => {
                if (!assignment)
                    delivery.haveDriver = false;
                else {
                    delivery.haveDriver = true;
                    delivery.driver_id = assignment.driver;
                }

                return delivery;
            })
        })
};

PlanRoutes.prototype.groupByDriver = function (list) {
    let newList = _.groupBy(list, 'haveDriver');
    return {
        haveDriver: _.groupBy(newList.true, 'driver_id'),
        withoutDriver: newList.false
    }
};

PlanRoutes.prototype.planRoutePerDriver = function (list) {
    return RoutePlannerService.planRoute(list);
};

module.exports = (req, res) => { return new PlanRoutes(req, res) };