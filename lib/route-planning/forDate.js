let moment = require('moment'),
    ScheduledDeliveriesRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    PostalCodeRepository = require('../postal-codes/repository'),
    AccountRepository = require('../user/repository/account.js'),
    _ = require('lodash');

function DeliveriesForDate(date) {
    const forDate = moment(date, 'YYYY-MM-DD'),
          toDate = moment(date, 'YYYY-MM-DD').add(1, 'd');

    var deliveries;

    return new Promise((resolve, reject) => {
        ScheduledDeliveriesRepository.scheduled.forDate(forDate, toDate)
        .toArray((err, data) => {
            if (err) reject(err);
            resolve(data);
        })
    })
    .then(data => {
        deliveries = data;
        return AccountRepository.findAll(
            { '_id': { $in: data.map(item => item.creator) }}, 
            { approved: 1 })
    })
    .then(creators => {
        deliveries = deliveries.filter(element => {
            let creator = creators.find(x => x._id.equals(element.creator));
            return creator.approved;
        });
        
        let result = deliveries.map(delivery => {
            return PostalCodeRepository.getForZipCode(delivery.deliveryzip)
                .then((postalObject) => {
                    if (!postalObject) return;

                    return {
                        delivery_id: delivery._id,
                        zone: `${postalObject.AREA}${postalObject.ZONE}`,
                        zip: delivery.deliveryzip,
                        department: delivery.department,
                        company_id: delivery.creator,
                        address: delivery.deliveryaddress,
                        external_id: delivery.deliveryid,
                        status: delivery.status
                    };
                }).catch(err => {
                    console.error(err);
                })

        });

        return Promise.all(result)
            .then(data => {
                return data;
            })
    })
}


module.exports = (date) => { return new DeliveriesForDate(date); };