var vehicleTypes = require("../carrier/enum/vehicle-types").VehicleTypes;

var vehicle = function(){
    var types = [];

    for(var i in vehicleTypes){
        types.push({
            id: vehicleTypes[i].typeId,
            description: vehicleTypes[i].description,
            cargo: vehicleTypes[i].cargo,
            category: vehicleTypes[i].category
        })
    }

    return types;
}

module.exports = {
    vehicle : vehicle
}
