let carrierStatus = require("../delivery/carrier-state/enum"),
    moment = require('moment'),
    statusHelper = require('./statusHelper');
    require('dotenv').config();
    
module.exports = function (item) {
    let delivery = item.from ? item.from : item;
    let addressPickup = getFullAddress(delivery.pickupaddress, delivery.pickupaddress2, delivery.pickupzip, delivery.pickupcity);
    let addressDelivery = getFullAddress(delivery.deliveryaddress, delivery.deliveryaddress2, delivery.deliveryzip, delivery.deliverycity);
    return {
        "delivery_id": delivery.deliveryid||delivery.delivery_id,
        "is_default_pickup": delivery.is_default_pickup ? delivery.is_default_pickup : false,
        "department": delivery.department || "",
        "delivery_type": delivery.deliverytype,
        "delivery_date": delivery.deliverydate,
        "car_type": delivery.cartype,
        "distribution": delivery.distribution,
        "delivery_status": parseInt(delivery.status) || 1,
        "scheduled_id": delivery._id,
        "created_at": delivery.created,
        "packages": parseInt(delivery.deliverynumberofpackages) || 1,
        "notes": delivery.deliverynotes,
        "order": String(delivery.orderIndex || -1),
        "labels_url": process.env.LABELS_URL + `/${delivery._id}`,
        "track_url":  process.env.TRACK_URL + `/${delivery._id}`,
        "estimated_delivery_time": delivery.estimated_delivery_time || `${delivery.deliverywindowstart} - ${delivery.deliverywindowend}`,
        "pickup_deadline": delivery.pickupdeadline,
        "pickup_deadline_to": delivery.pickupdeadlineto,
        "pickup_location":{
            "info": delivery.pickupdescription,
            "description": addressPickup,
            "address": delivery.pickupaddress,
            "zip": delivery.pickupzip,
            "latitude": delivery.pickupcoordinates[1],
            "longitude": delivery.pickupcoordinates[0]
        },
        "dropoff_location":{
            "info": delivery.delilverydescription,
            "description": addressDelivery,
            "address": delivery.deliveryaddress,
            "zip": delivery.deliveryzip,
            "latitude": delivery.deliverycoordinates[1],
            "longitude": delivery.deliverycoordinates[0]
        },
        "pickup_date": moment(delivery.deliverydate).format('YYYY-MM-DDTHH:mm:ss'),
        "buyer": {
            "client_id": delivery.recipientclientid,
            "name": delivery.recipientname,
            "phone": getPhoneNumber(delivery.recipientphone),
            "email": delivery.recipientemail
        },
        "carrier_status": getStatus(item),
        "events": formatEvents(delivery.events),
        "carrier":delivery.carrier,
        "allow_signature": true,
        "type":delivery.type||'',
        "returned_delivery_id":delivery.returned_delivery_id||null,
        "return_request_by":delivery.return_request_by||'',
    };

    function getStatus(item){
        if(!item.deliveryData){
            return {
                carrier_status: 0,
                description: "Not Started",
            }
        }
        return {
            carrier_status: item.deliveryData.carriers[0].status,
            description: carrierStatus.getDescription(item.deliveryData.carriers[0].status)
        }
    }

    function getFullAddress(address1, address2 = "", zip, city, country) {
        // Fallback for old storage methods where full address was written in 1 field.
        if (address1.includes("Denmark"))
            return address1;

        address1 = address2 !== "" ? `${address1} ${address2}` : address1;

        return `${address1}, ${zip} ${city}, ${country||'Denmark'}`;
    }

    function getPhoneNumber(phone) {
        if (!phone || (typeof phone === 'string'))
            return phone;

        let dialCode = phone.country_dial_code || "45";

        return  `${dialCode} ${phone.phone}`;
    }

    function formatEvents(events) {
        if (!events || events.length === 0)
            return;

        return events.map(event => {
            return {
                event: event.event,
                event_data: event.event_data,
                event_description: statusHelper.getDescription(event.event_data),
                event_date: moment(event.created_at).toString()
            }
        })
    }
};