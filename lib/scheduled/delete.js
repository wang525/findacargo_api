let ScheduledDeliveriesRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository');
let Response = require('../../services/Response');
let UserRepository = require('../user/repository/account');
let ResponseError = require('../framework/responseError')
let qr = require('qr-image');
let barcode = require('pure-svg-code/barcode');

function DeleteDelivery(ver, deliveryId, token) {
    if (!token) {
        throw new ResponseError(401, 'Not authorized.');
    }

    let currentUser = null;
    return UserRepository.getByApiToken(token)
        .then(user => {
            if (!user) {
                throw new ResponseError(400, 'User not found.');
            }

            return user;
        }).then(user => {
            currentUser = user;

            return ScheduledDeliveriesRepository.scheduled.findOne(deliveryId.toObjectId());
        }).then(delivery => {
            if (!delivery) {
                throw new ResponseError(400, `Specified delivery could not be found. Delivery ID: ${deliveryId}.`);
            }

            if (delivery.creator.toString() != currentUser._id.toString()) {
                throw new ResponseError(401, `You are not authorized to delete specified delivery. Delivery ID: ${delivery._id}.`);
            }

            if (delivery.status > 1) {
                throw new ResponseError(400, `Planned or finished deliveris cannot be deleted. Delivery ID: ${delivery._id}.`);
            }

            if (ver == "v1.4") {
                return ScheduledDeliveriesRepository.scheduled.deleteMany([delivery._id]).then(function (res) {
                    let responsObj = {};
                    //Making of QR image                
                    // var qr_svg = qr.image(deliveryId, { type: 'png' });
                    // qr_svg.pipe(require('fs').createWriteStream(deliveryId + '.png'))

                    //Return of QR svg string
                    var svg_string = qr.imageSync(deliveryId, { type: 'svg' });
                    responsObj.qr_code = svg_string;

                    //Return of QR barcode string
                    var svgStringForBarcode = barcode(deliveryId, "code128", { width: '150', barWidth: 10, barHeight: 10 });
                    responsObj.barcode = svgStringForBarcode;

                    responsObj.message = 'Schedule deleted successfully';
                    return responsObj;
                });
            } else {
                return ScheduledDeliveriesRepository.scheduled.deleteMany([delivery._id])
            }
        }).then(result => {
            if (ver == "v1.4") {
                return {
                    http_status: 200,
                    http_body: result
                };
            } else {
                return {
                    http_status: 200
                };
            }
        });
};

module.exports = (ver, deliveryId, token) => { return new DeleteDelivery(ver, deliveryId, token) };