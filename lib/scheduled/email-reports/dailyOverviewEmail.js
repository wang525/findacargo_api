var moment = require('moment');
let ScheduledRepository = require('../repository/scheduled');
var AccountRepository = require('../../user/repository/account.js');
var DeliverySettingsRepository = require('../../user/repository/deliverysettings.js');
let EventsHistoryService = require('../../../services/EventHistoryService');
let ScheduledDeliveriesRepository = require('../../../lib/carrier-deliveries/repository/scheduleDeliveriesRepository');
var statusEnum = require('../../../public/const/enums');
let request = require('request-promise');
var distanceConfig = require("../../config/index").googledistance;
var distance = require('google-distance');
distance.apiKey = distanceConfig.key; // Not required, just for tracking
require('dotenv').config();


var getAccount = function(deliveryGroup) {
    return AccountRepository.getById(deliveryGroup._id)
    .then(account => {
        if (account && account.approved) {
            deliveryGroup.account = account;
            return DeliverySettingsRepository.getByClientId(account._id);
        }
    })
    .then(data => {
        if (data) {
            let deliverySettings = data._doc;
            deliveryGroup.account.permissons = deliverySettings;
            return AccountRepository.getCoWorkers(deliveryGroup._id);
        }
    })
    .then(coworkers => {
        if (coworkers) {
            deliveryGroup.coworkers = coworkers;
            return Promise.all(coworkers.map(coworker => {
                return DeliverySettingsRepository.getByClientId(coworker._id)
            }))
        }
    })
    .then(co_data => {
        if (co_data) {
            let coDeliverySettings = co_data;
            for (var i = 0; i < deliveryGroup.coworkers.length; i++) {
                deliveryGroup.coworkers[i].permissions = coDeliverySettings[i];
            }
            return deliveryGroup;
        }
    })
}

var getEventHistory = function(deliveryGroup) {
    if (deliveryGroup) {
        let buf = deliveryGroup;
        return Promise.all(
            deliveryGroup.data.map(delivery => {
                    return EventsHistoryService.getEventsForStatusChanged(delivery._id)
                    .then(eventHistory => {
                        delivery.eventHistory = eventHistory;
                        return delivery;
            })
        })).then(deliveries => {
            buf.data = deliveries;
            return buf;
        });
    }
}


// For daily reports at 20:00 copenhagen time
dailyOverviewEmail = function () {
    // console.log("daily overview email data===>")
    // test data 
    // var startDate = "2018-10-29";
    // var startDateLimit = new Date(startDate);
    // var endDateLimit = new Date(startDate);

    var startDateLimit = new Date();
    var endDateLimit = new Date();

    startDateLimit.setHours(0, 0, 0, 0);  
    endDateLimit.setDate(startDateLimit.getDate() + 1);
    endDateLimit.setHours(0, 0, 0, 0);

        return ScheduledRepository.getDeliveriesByTime(startDateLimit, endDateLimit)
        .then(deliveries => {
            if (!(deliveries.length > 0)) {
                error = new Error('There is no data in these times.');
                error.code = 403;
                throw error;
            }
            return Promise.all(deliveries.map(getAccount))
        })
        .then(deliveries => {
            if (!(deliveries.length > 0)) {
                error = new Error('There is no account data in these times.');
                error.code = 403;
                throw error;
            }
            deliveries = deliveries.filter(function( element ) {
                return element !== undefined;
            });
            return Promise.all(deliveries.map(getEventHistory))
        }).then(deliveries => {
            if (!(deliveries.length > 0)) {
                error = new Error('There is no event delivery in these times.');
                error.code = 403;
                throw error;
            }
            deliveries = deliveries.filter(function( element ) {
                return element !== undefined;
            });
            return Promise.all(deliveries.map(deliveryGroup => {

                // if (deliveryGroup && deliveryGroup.account.permissons.allow_to_receive_end_of_day_report) {
                if (deliveryGroup && deliveryGroup.coworkers && deliveryGroup.coworkers.length > 0) {
                    return Promise.all(deliveryGroup.coworkers.map(coworker => {
                        if (coworker.permissions.allow_to_receive_end_of_day_report) {
                            var nonDeliveredDeliveries = [];
                            var deliveredDeliveries = [];
                            var expectedDeliveredCount = 0;
        
                            deliveryGroup.data = deliveryGroup.data.map(delivery => {
                                if (delivery.status == 3) {
                                    deliveredDeliveries.push(delivery);
                
                                    var lastEvent = delivery.eventHistory.filter(event => {
                                        return parseInt(event.event_data, 10) == 3;
                                    })
                
                                    var startTime = "";
                                    var endTime = "";
                                    var status = "";
                                    
                                    if (lastEvent && lastEvent.length > 0) {
                                        delivery.deliveryTime = moment(lastEvent[0].created_at).format("hh:mm A");
                                    } else {
                                        delivery.deliveryTime = delivery.deliverywindowstart;
                                    }
                                    
                                    // if (moment(delivery.deliverywindowend, "HH:mm A") < moment(delivery.deliveryTime, "HH:mm A")) {
                                    //     status = "behind";
                                    //     startTime = moment(delivery.deliverywindowend, "HH:mm A");
                                    //     endTime = moment(delivery.deliveryTime, "HH:mm A");
                                    // } else {
                                    //     status = "ahead";
                                    //     startTime = moment(delivery.deliveryTime, "HH:mm A");
                                    //     endTime = moment(delivery.deliverywindowend, "HH:mm A");
                                    // }
                
                                    if (delivery.delivered_at) {
                                        if (delivery.estimated_delivery_time) {
                                            delivery.expectedDeliveryTime = delivery.estimated_delivery_time;
                                        }
                                        else {
                                            delivery.expectedDeliveryTime = delivery.deliverywindowend;
                                        }
        
                                        if (moment(expectedDeliveryTime, "HH:mm A") < moment(delivery.deliveryTime, "HH:mm A")) {
                                            status = "behind";
                                            startTime = moment(expectedDeliveryTime, "HH:mm A");
                                            endTime = moment(delivery.deliveryTime, "HH:mm A");
                                        } else {
                                            status = "ahead";
                                            startTime = moment(delivery.deliveryTime, "HH:mm A");
                                            endTime = moment(expectedDeliveryTime, "HH:mm A");
                                        }
        
                                        if (moment(expectedDeliveryTime).format('hh:mm') > moment(delivery.delivered_at).format('hh:mm')) {
                                            expectedDeliveredCount += 1;
                                        }
                                    }
        
                                    // var duration = moment.duration(endTime.diff(startTime));
                                    // var hours = parseInt(duration.asHours());
                                    // var minutes = parseInt(duration.asMinutes()) - hours * 60;
                                    // delivery.statusTime = (hours > 0 ? hours + " hr " : "") + (minutes > 0 ? minutes + " mins " : "") + status;
                                    // delivery.minutes = hours * 60 + minutes;
                                } else {
                                    nonDeliveredDeliveries.push(delivery);
                                }
                                delivery.statusDescription = statusEnum.SCHEDULED_DELIVERY_STATUS[delivery.status];
                                delivery.clientPhone = delivery.clientPhone['country_dial_code'] + delivery.clientPhone['phone'];
        
                                return delivery
                            })
        
                            var localData = {
                                username: deliveryGroup.account && deliveryGroup.account.name,
                                scheduledDeliveries: (deliveryGroup && deliveryGroup.data) || [],
                                plannedDelivery: deliveryGroup && deliveryGroup.data && deliveryGroup.data.length,
                                delayDelivery: nonDeliveredDeliveries.length,
                                delivered: deliveredDeliveries.length,
                                deliveredPercent: Math.floor((deliveredDeliveries.length / deliveryGroup.data.length) * 100),
                                delayPercent: Math.floor((nonDeliveredDeliveries.length / deliveryGroup.data.length) * 100),
                                totalDeliveryCount: deliveryGroup.data.length,
                                expectedDeliveryPercent: Math.floor((expectedDeliveredCount / deliveryGroup.data.length) * 100),
                                statusEnum: statusEnum.SCHEDULED_DELIVERY_STATUS,
                            };
        
                            var emailDetails = {
                                templateName: 'dailyscheduledroutereport',
                                email: coworker.email,//'jingwei8810@outlook.com', 
                                dealData: localData,
                                subject: 'IDAG: Daily Overview'
                            };
                            return emailDetails;
                        }
                    }))
                }
            }))
        })
        .then(emailData => {
            if (!emailData) {
                error = new Error('There is no emailData.');
                error.code = 403;
                throw error;
            }
            return Promise.all(emailData.map(emailDeliveriesDetails => {
                return Promise.all(emailDeliveriesDetails.map(emailDetails => {
                    return new Promise((resolve ,reject)=>{
                        setTimeout(()=>{
                            resolve(emailDetails);
                        }, 500);
                    })
                    .then(emailDetailData=>{
                        var options = {
                            method: 'POST',
                            uri: process.env.NOTIFY_URL + 'order/report', // 'http://notify.nemlevering.dk/api/' + 'order/report', 
                            body: emailDetailData,
                            json: true,
                            headers: {
                                'content-type': 'application/json'
                            },
                            //simple: false  
                        };
                        return request(options)
                    })
                }))
            }))
        })
        .then(responses => {
            if (!responses) {
                error = new Error('There is no response from email send.');
                error.code = 403;
                throw error;
            }
            process.exit();
            //return responses;
        })
        .catch(err => {
            console.log("err", err)
            process.exit();
        })           
}

dailyOverviewEmail();