var moment = require('moment');
let ScheduledRepository = require('../scheduled/repository/scheduled');
var AccountRepository = require('../user/repository/account.js');
var DeliverySettingsRepository = require('../user/repository/deliverysettings.js');
var statusEnum = require('../../public/const/enums');
let request = require('request-promise');
var distanceConfig = require("../config").googledistance;
var distance = require('google-distance');
distance.apiKey = distanceConfig.key; // Not required, just for tracking
require('dotenv').config();

var getETA = function(origin, destination){
    return new Promise(function(resolve, reject) {
        distance.get(
            {
                origin: origin,
                destination: destination
            },
            function(err, data) {
                if (err){
                    resolve({err: err})
                } else {
                    resolve({data: data})
                }
            });
    });
};

var getDeliveriesETA = function(request){
    if(request.origin && request.destination) {
        return getETA(request.origin, request.destination)
            .then(ans => {
                if (!ans.err) {
                    var eta = '';
                    var arrivingIn = '';
                    eta = {arrivingIn: ans.data && ans.data.duration , arrivingInSeconds: ans.data && ans.data.durationValue};
                    return {eta: eta, arrivingIn: arrivingIn};
                } else {
                    return {err: ans.err}
                }
            })
    } else {
        return {err: 'No origin or destination'};
    }
};

var getAccount = function(deliveryGroup) {
    return AccountRepository.getById(deliveryGroup._id)
        .then(account => {
            if (account && account.approved) {
                deliveryGroup.account = account;
                return DeliverySettingsRepository.getByClientId(account._id);
            }
        })
        .then(data => {
            if (data) {
                let deliverySettings = data._doc;
                deliveryGroup.account.permissons = deliverySettings;
                return deliveryGroup;
            }
        })
}

var getDeliveriesGroupETA = function(deliveryGroup) {
    if (deliveryGroup) {
        let buf = deliveryGroup;
        return Promise.all(
            deliveryGroup.data.map(delivery => {
                if (delivery.status != 3) {
                    var etarequest = {};
                    etarequest.origin = delivery && delivery.pickupAddress;
                    etarequest.destination = delivery && delivery.address;

                    return getDeliveriesETA(etarequest)
                        .then(ans => {
                            if (!ans.err) {
                                delivery.eta = ans.eta;
                            }
                            return delivery;
                        })
                }
            })
        ).then(deliveries => {
            buf.data = deliveries;
            return buf;
        });
    }
}


var scheduleRoutesEmail = function() {
    var startDateLimit = new Date();
    var endDateLimit = new Date();

    startDateLimit.setDate(startDateLimit.getDate() + 1);
    startDateLimit.setHours(0, 0, 0, 0);
    endDateLimit.setDate(startDateLimit.getDate() + 1);
    endDateLimit.setHours(0, 0, 0, 0);

    return ScheduledRepository.getDeliveriesByTime(startDateLimit, endDateLimit)
        .then(deliveries => {
            return Promise.all(deliveries.map(getAccount))
        })
        .then(deliveries => {
            deliveries = deliveries.filter(function( element ) {
                return element !== undefined;
            });
            return Promise.all(deliveries.map(deliveryGroup => {
                if (deliveryGroup && deliveryGroup.account.permissons.allow_to_receive_daily_report) {

                    var deliveredCount = deliveryGroup.data.filter(delivery => {
                        if (delivery.status == 3) return delivery;
                    }).length;
                    deliveryGroup.deliveredCount = deliveredCount;

                    return deliveryGroup;
                }
            }))
        }).then(deliveries => {
            return Promise.all(deliveries.map(getDeliveriesGroupETA))
        }).then(nonDeliveredDeliveries => {
            return Promise.all(nonDeliveredDeliveries.map(deliveryGroup => {
                deliveryGroup.data = deliveryGroup.data.filter(function( element ) {
                    return element !== undefined;
                });
                if (deliveryGroup && deliveryGroup.account.permissons.allow_to_receive_daily_report) {

                    deliveryGroup.data = deliveryGroup.data.map(delivery => {
                        var now = new Date();
                        var currentTime = moment(now).format('HH:mm A');
                        if (moment(currentTime, 'HH:mm A') < moment(delivery.deliverywindowstart, 'HH:mm A')) {
                            var t = moment(delivery.deliverywindowstart, 'HH:mm A');
                            now.setHours(t.get('hour'), t.get('minute'), 0, 0);
                        }
                        var deliveryDate = new Date(now.setSeconds(now.getSeconds() + (5 * 60 + (delivery.eta && delivery.eta.arrivingInSeconds ? delivery.eta.arrivingInSeconds : 0))));
                        delivery.expactedDeliveryTime = moment(deliveryDate).format('hh:mm A');
                        delivery.clientPhone = delivery.clientPhone['country_dial_code'] + delivery.clientPhone['phone'];
                        return delivery;
                    })

                    var localData = {
                        username: deliveryGroup.account && deliveryGroup.account.name,
                        scheduledDeliveries: deliveryGroup.data || [],
                        nonDelivered: deliveryGroup.data.length,
                        delivered: deliveryGroup.deliveredCount,
                        statusEnum: statusEnum.SCHEDULED_DELIVERY_STATUS,
                    };

                    var emailDetails = {
                        templateName: 'scheduledroutes',
                        email: deliveryGroup.account.email,
                        dealData: localData,
                        subject: deliveryGroup.account.name + " : 15:00 report  " + moment(startDateLimit).format('YYYY/MM/DD')
                    };

                    return emailDetails;
                }
            }))})
        .then(emailData => {
            return Promise.all(emailData.map(emailDetails => {
                var options = {
                    method: 'POST',
                    uri: process.env.NOTIFY_URL + 'order/report',
                    body: emailDetails,
                    json: true,
                    headers: {
                        'content-type': 'application/json'
                    },
                    //simple: false
                };

                return request(options)
            }))
        })
        .then(responses => {
            return responses;
        })
        .catch(err => {
            rr = err;
        })
};

scheduleRoutesEmail();