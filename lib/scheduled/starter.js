var moment = require("moment");
var di;

function Starter(options){
    di = options;
    var that = this;

    this.fromScheduled = fromScheduled;

    function fromScheduled(scheduledId) {
        var user = di.getUser();
        return di.ScheduledRepository.getById(scheduledId)
                    .then(verifyIsStarted)
                    .then(formatScheduled)

        function verifyIsStarted(scheduled){
            if(!scheduled) {
                throw "Invalid Scheduled ID";
            }

            if(scheduled.delivery_id){
                return scheduled;
            }

            return getVehicle()
                    .then(function(vehicle){
                        that.vehicle = vehicle;
                        return importDeliveries(scheduled);
                    });
        }

        function getVehicle(){
            return di.ScheduledRepository.getVehicle(user._id);
        }

        function importDeliveries(scheduled){
            deliveryDate = moment(scheduled.deliverydate).format("MM-DD-YYYY");
            return di.ScheduledDeliveries.deliveries(deliveryDate, scheduled.deliveryid)
                        .then(createDeliveries)
                        .then(function(inserted){
                            return di.ScheduledRepository.getById(scheduledId)
                        });
        }

        function createDeliveries(scheduledList){
            var deliveries = scheduledList.scheduled.map(function(item){
                return di.deliveryFactory.createFromScheduled(item, user, scheduledId, that.vehicle);
            });

            return di.ScheduledRepository.bulkCreateDelivery(deliveries);
        }

        function formatScheduled(scheduled) {
            return {
                delivery_id: scheduled.delivery_id
            }
        }


    }
}

module.exports = Starter
