let ScheduledDeliveriesRepostiroy = require('../carrier-deliveries/repository/scheduleDeliveriesRepository');
let Response = require('../../services/Response');
let UserRepository = require('../user/repository/account');

const DELIVERY_NOT_POSSIBLE_EVENT = "notpossible";
const CALLED_CLIENT_EVENT = "calledclient";
const EVENTS = [DELIVERY_NOT_POSSIBLE_EVENT, CALLED_CLIENT_EVENT];
var SocketService = require('../../services/SocketService');

function addEvent(deliveryId, event, token) {
    if (!EVENTS.includes(event))
        return Response.codeWithMessage(400, "Unsupported event.");

    if (!token)
        return Response.codeWithMessage(401, "Not authorized.");

    let creator = null;
    return UserRepository.getByApiToken(token)
        .then(user => {
            if (!user)
                throw "Token not found.";

            creator = user;
            return ScheduledDeliveriesRepostiroy.scheduled.findOne(deliveryId.toObjectId());
        })
        .catch(msg => {
            return Response.codeWithMessage(401, msg);
        })
        .then(delivery => {
            if (!delivery)
                throw {
                    code: 400,
                    msg: "Delivery wasn't found."
                };

            if (delivery.creator.toString() != creator._id.toString())
                throw {
                    code: 401,
                    msg: "Not authorized to add events to specified delivery."
                };

            let eventMessage = "";
            switch (event) {
                case DELIVERY_NOT_POSSIBLE_EVENT:
                    eventMessage = "Delivery not possible";
                    return EventHistoryService.addDeliveryNotPossibleEvent(delivery._id, eventMessage);
                case CALLED_CLIENT_EVENT:
                    eventMessage = `Driver called recipient (${delivery.recipientname}, +${delivery.recipientphone.country_dial_code} ${delivery.recipientphone.phone})`;
                    return EventHistoryService.addCalledClientEvent(delivery._id, eventMessage);
                    break;
            }
        }).catch(error => {
            return Response.codeWithMessage(error.code, error.msg);
        })
}

module.exports = (deliveryId, event, token) => { return addEvent(deliveryId, event, token) };