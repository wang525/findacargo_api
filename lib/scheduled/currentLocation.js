let Responses = require('../../services/Response'),
    UserRepository = require('../user/repository/account'),
    TrackingService = require('../../services/TrackingService');

function CurrentLocation(id, token) {
    if (!token)
        return Responses.codeWithMessage(401, "Not authorized.");

    return this.getAuthenticatedUser(token)
            .then(() => {
                return TrackingService.getTrackingData(id);
            })
            .catch(error => {
                if (error.error && error.error.status)
                    return error;
            });
}

CurrentLocation.prototype.getAuthenticatedUser = function(token) {
    return UserRepository.getByApiToken(token)
        .then(result => {
            if (!result)
                throw Responses.codeWithMessage(401, "Token not found.");

            return result;
        });
};

module.exports = ((id, token) => { return new CurrentLocation(id, token); });