let Responses = require('../../services/Response'),
    UserRepository = require('../user/repository/account'),
    ScheduledDeliveryRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository');

function SetSignature(id, body, token) {
    console.log('setSignature - id, token', id, token)
    var signature = body.signature
    // if (!token)
    //     return Responses.codeWithMessage(401, "Not authorized.");
    return ScheduledDeliveryRepository.scheduled.findOneAndUpdate(id.toObjectId(), {
        signature
    })
    .then((result)=>{
        //console.log('Signature result', result)
        return {
            "ststus":"success"
        }
    })
    .catch(error => {
        console.log('setSignature - error', error)
        if (error.error && error.error.status)
            return error;
    });
    // return this.getAuthenticatedUser(token)
    //         .then(() => {
    //             console.log('setSignature', id, signature)
    //             return ScheduledDeliveryRepository.scheduled.findOneAndUpdate(id.toObjectId(), {
    //                 signature
    //             });
    //         })
    //         .then((result)=>{
    //             //console.log('Signature result', result)
    //             return {
    //                 "ststus":"success"
    //             }
    //         })
    //         .catch(error => {
    //             console.log('setSignature - error', error)
    //             if (error.error && error.error.status)
    //                 return error;
    //         });
}

SetSignature.prototype.getAuthenticatedUser = function(token) {
    return UserRepository.getByApiToken(token)
        .then(result => {
            if (!result)
                throw Responses.codeWithMessage(401, "Token not found.");

            return result;
        });
};

module.exports = ((id, body, token) => { return new SetSignature(id, body, token); });