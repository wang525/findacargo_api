let formatScheduled = require('./formatScheduled'),
    ScheduledDeliveriesRepostiroy = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    Responses = require('../../services/Response'),
    UserRepository = require('../user/repository/account'),
    EventHistoryService = require('../../services/EventHistoryService'),
    Versioning = require('../../utils/Versioning');
    DriverDeliveryRepository = require('../carrier-deliveries/repository/driverDeliveryRepository');
    accountRepository = require("../user/repository/account");

function GetDelivery(vers, id, token) {
    if (!token)
        return Responses.codeWithMessage(401, "Not authorized.");
    let g_delivery = {};
    return this.getAuthenticatedUser(token)
            .then(() => {
                return this.fetchDelivery(id);
            })
            .then((delivery) => {
                g_delivery = delivery;
                if (vers !== Versioning.V1_1)
                    return delivery;
                return EventHistoryService.getEventsForScheduledDelivery(delivery._id)
                    .then((events) => {
                        delivery.events = events;
                        return delivery;
                    })
            })
            .then(() => {
                return this.fetchDriverDelivery(id);
            })
            .then((driver_delivery) => {
                if (driver_delivery) {
                return this.fetchDriver(driver_delivery.carrier_id);
                }
                return g_delivery;
            })
            .then((driver) => {
                if (driver.carrier == 1) {
                    g_delivery.carrier = {
                    name:driver.name,
                    phone:driver.phone
                    }
                }                
                return g_delivery
            })
            .then(formatScheduled)
            .catch(error => {
                if (error.error && error.error.status)
                    return error;
            });
}

GetDelivery.prototype.getAuthenticatedUser = function(token) {
    return UserRepository.getByApiToken(token)
        .then(result => {
            if (!result)
                throw Responses.codeWithMessage(401, "Token not found.");

            return result;
        });
};

GetDelivery.prototype.fetchDelivery = function (id) {
    id = ensureObjectId(id);
    return ScheduledDeliveriesRepostiroy.scheduled.findOne(id)
        .then(delivery => {
            if (!delivery)
                return Responses.codeWithMessage(400, "Delivery not found");
            return delivery;
        })
};

GetDelivery.prototype.fetchDriverDelivery = function (id) {
    id = ensureObjectId(id);
    return DriverDeliveryRepository.findOne({delivery_id: id.toString()})
            .then(result => {
                return result;
         })
};

GetDelivery.prototype.fetchDriver = function (id) {
    id = ensureObjectId(id);
    return accountRepository.getById(id)
            .then(result => {
                return result;
        })
}
module.exports = ((vers, id, token) => { return new GetDelivery(vers, id, token); });