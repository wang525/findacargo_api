let GeoCoder = require('../../services/GeoCoder'),
    AutoKeyService = require('../../services/AutoKeyService'),
    formatScheduled = require('./formatScheduled'),
    moment = require("moment"),
    ScheduledDeliveriesRepostiroy = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    Responses = require('../../services/Response'),
    UserRepository = require('../user/repository/account'),
    DepartmentsModel = require('../../models/Departments'),
    EventHistoryService = require('../../services/EventHistoryService'),
    Versioning = require('../../utils/Versioning'),
    DeliverySettingsRepository = require('../user/repository/deliverysettings.js'),
    request = require('request'),
    async = require('async'),
    ResponseError = require('../framework/responseError');
    require('dotenv').config();
    var mailgun = require('mailgun-js')({apiKey: process.env.MAILGUN_API_KEY||'key-52e3854a2fd0ebefea915d71ee3080d3', domain: process.env.MAILGUN_API_DOMAIN||'nemlevering.dk'});
    
    

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
let globalVersion;
let qr = require('qr-image');
let barcode = require('pure-svg-code/barcode');
function CreateUpdateDelivery(version, deliveries, token, isUpdate) {
    if (!token) {
        throw new ResponseError(401, 'Not authorized.');
    }

    return UserRepository.getByApiToken(token)
        .then(user => {
            if (!user) {
                throw new ResponseError(400, 'User not found.');
            }

            globalVersion = Versioning.normaliseVersion(version);

            if (isUpdate) {
                return this.updateMany(deliveries, token, user);
            } else {
                return this.createMany(deliveries, token, user);
            }
        });
}

CreateUpdateDelivery.prototype.createMany = function (deliveries, token, user) {
    return new Promise((resolve, reject) => {
        async.mapSeries(deliveries, (delivery, cb)=>{
            this.createDelivery(delivery, token).then(ret=>{
                cb(null, ret)
            })
        }, (error, results)=>{
            if(error) {
                reject(error)
                return
            }

            // var schedules = deliveries.map(delivery => {
            //     return {
            //         "deliverydate": delivery.delivery_date,
            //         "deliverywindowend": _tConvert(delivery.delivery_window.to + ':00'),
            //         "recipientemail": delivery.recipient_email,
            //         "recipientname": delivery.recipient_name,
            //         "recipientphone": delivery.recipient_phone.country_dial_code + delivery.recipient_phone.phone,
            //         "clientName": user.name ? user.name : '-'
            //     }
            // });
    
            if (process.env.IS_EMAIL_NOTIFICATION_SEND) {
                results.forEach(function (result) {
                    // schedule["track_url"] = result.track_url ? result.track_url : '';
                    // request.post(process.env.NOTIFY_URL + 'order/create', { json: schedule }, function (err, resp, bd) {
                    //     if (err) console.log(err)
                    //     console.log(bd);
                    // });
                    var cloneOfResult = JSON.parse(JSON.stringify(result));
                    var deliveryDetailsURL = process.env.TRACKING_BASE_URL + cloneOfResult.scheduled_id;
                    var buyerEmail = cloneOfResult.buyer && cloneOfResult.buyer.email ? cloneOfResult.buyer.email : '-';
                    var buyerName = cloneOfResult.buyer && cloneOfResult.buyer.name ? cloneOfResult.buyer.name : '-';
                    var phone = cloneOfResult.buyer && cloneOfResult.buyer.phone ? cloneOfResult.buyer.phone : '-';
                    var data = {
                        from: 'System Generated Email <noreply@goidag.com>',
                        to: process.env.MAILGUN_EMAIL_USERS,
                        subject: 'New Delivery has been created in the IDAG',
                        html: '<p>Hi Admin,</p><p>Below is the New Delivery Details</p><p><strong>Delivery Date:</strong>&nbsp; '+ result.pickup_date + ' </p><p><strong>Recipient Email:</strong>&nbsp; '+ buyerEmail + ' </p><p><strong>Recipient Name:</strong>&nbsp; '+ buyerName + ' </p><p><strong>Recipient Phone:</strong>&nbsp; '+ phone + ' </p><p><strong>Delivery Details Page:</strong>&nbsp; '+ deliveryDetailsURL + ' </p><p>Thank You</p>'
                    };
                    
                    var clientId = user.groupId||user._id.toString()
                    DeliverySettingsRepository.getByClientId(clientId.toObjectId()).then((setting)=>{
                        //console.log('setting', setting)
                        if(setting.email_notification_setting && setting.email_notification_setting.delivery_created) {
                            mailgun.messages().send(data, function (error, body) {
                                //console.log(body);
                                EventHistoryService.addCustomEvent(cloneOfResult.scheduled_id, "Sent email : " + data.subject)
                            });
                        }
                    })
                });
            }
            if (globalVersion == "v1.4") {    
                var cloneOfResults = JSON.parse(JSON.stringify(results));
                
                //Making of QR image                
                // var qr_svg = qr.image(cloneOfResult.scheduled_id, { type: 'png' });
                // qr_svg.pipe(require('fs').createWriteStream(cloneOfResult.scheduled_id + '.png'))
                
                //Return of QR svg string
                results = cloneOfResults.map((result)=>{
                    //console.log('result.scheduled_id', result.scheduled_id)
                    var svg_string = qr.imageSync(result.scheduled_id, { type: 'svg' });
                    result.qr_code = svg_string;
        
                    //Return barcode string
                    var svgStringForBarcode = barcode(result.scheduled_id, "code128", { width: '150', barWidth: 10, barHeight: 10 });
                    result.barcode = svgStringForBarcode;

                    return result
                })
            }

            if (results.length && results.length === 1)
                results = results[0];

            resolve({
                http_status: 201,
                http_body: results
            });
        })
    })
    
    // let storedDeliveries = deliveries.map(delivery => {
    //     return this.createDelivery(delivery, token);
    // });

    // return Promise.all(storedDeliveries)
    //     .then(result => {
            
    //     });
}

CreateUpdateDelivery.prototype.updateMany = function (deliveries, token, user) {
    let idsProvided = !deliveries.find(x => !x.scheduled_id);

    if (!idsProvided) {
        throw new ResponseError(400, 'scheduled_id is required.');
    }

    return UserRepository.getByApiToken(token)
        .then(result => {
            if (!result) {
                throw new ResponseError(400, 'User not found.');
            }

            return result._id.toString();
        })
        .then(creatorId => {
            let updatedDeliveries = deliveries.map(delivery => {
                return ScheduledDeliveriesRepostiroy.scheduled.findOneByFilter({
                    _id: delivery.scheduled_id.toObjectId()
                }).then(foundDelivery => {
                    if (!foundDelivery) {
                        throw new ResponseError(400, `Specified delivery could not be found. Delivery ID: ${delivery.scheduled_id}.`);
                    }

                    if (foundDelivery.creator.toString() != creatorId) {
                        throw new ResponseError(401, `You are not authorized to update specified delivery. Delivery ID: ${foundDelivery._id}.`);
                    }

                    if (foundDelivery.status > 1) {
                        throw new ResponseError(400, `Planned or finished deliveris cannot be updated. Delivery ID: ${foundDelivery._id}.`);
                    }

                    delivery.creator = foundDelivery.creator;

                    return this.updateDelivery(delivery);
                })
            });

            return Promise.all(updatedDeliveries)
                .then(result => {
                    if (result.length && result.length === 1)
                        result = result[0];

                    return {
                        http_status: 200,
                        http_body: result
                    };
                });
        });
}

CreateUpdateDelivery.prototype.updateDelivery = function (delivery) {
    return new Promise((resolve, reject) => {
        DeliverySettingsRepository.getByClientId(delivery.creator)
            .then((deliverySettings) => {
                if (!deliverySettings) {
                    throw new ResponseError(400, `User settings could not be found. User ID: ${delivery.creator.toString()}.`);
                }
                return {delivery, deliverySettings};
            })
            .then(this.codePickupAddress)
            //.then(this.checkAllowedPickupZones)
            .then(this.codeDeliveryAddress)
            .then(this.appendDates)
            .then(this.adjustDate)
            .then(({delivery, deliverySettings}) => {
                if (delivery.department) {
                    return this.appendDepartment(delivery);
                }

                return delivery;
            }).then(del => {
                del = this.cleanupProperties(del);
                let deliveryToUpdate = this.createDeliveryObject(del);

                // update only specified fields
                for (var property in deliveryToUpdate) {
                    if (deliveryToUpdate.hasOwnProperty(property)) {
                        let value = deliveryToUpdate[property];

                        if (value == null) {
                            delete deliveryToUpdate[property];
                        }
                    }
                }

                if (!Object.keys(deliveryToUpdate.recipientphone).length) {
                    delete deliveryToUpdate.recipientphone;
                }

                // creator cannot be changed
                delete deliveryToUpdate.creator;

                return ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(del.scheduled_id.toObjectId(), deliveryToUpdate);
            }).then(result => {
                if (delivery.dateChanged) {
                    var deliveryDate = result.value.deliverydate;
                    var eventMessage = `Delivery moved to the next delivery day (${result.value.deliverydayofweek}, ${deliveryDate.getUTCMonth() + 1}/${deliveryDate.getUTCDate()})`;
                    EventHistoryService.dateChangedScheduledDelivery(result.value._id, eventMessage);
                }

                resolve(formatScheduled({ from: result.value }));
            })
            .catch(reject);;
    });
};

CreateUpdateDelivery.prototype.createDelivery = function (delivery, token) {
    return new Promise((resolve, reject) => {

        this.appendCreatorUser(delivery, token)
            .then(this.checkDeliveryId)
            .then(this.codePickupAddress)
            //.then(this.checkAllowedPickupZones)
            .then(this.codeDeliveryAddress)
            .then(this.appendDates)
            .then(this.adjustDate)
            .then(this.appendDepartment)
            .then(del => {
                return this.store.call(this, del);
            })
            .then(delivery => {
                if (globalVersion === Versioning.V1)
                    return {
                        "success": true,
                        "message": "success",
                        "created_at": delivery.updated || (new Date()).toISOString(),
                        "delivery_id": delivery._id
                    };

                return formatScheduled({ from: delivery });
            })
            .then(delivery => {
                return resolve(delivery);
            })
            .catch(reject);
    });
};

CreateUpdateDelivery.prototype.appendCreatorUser = function (delivery, token) {
    return UserRepository.getByApiToken(token)
        .then(result => {
            if (!result) {
                throw new ResponseError(400, 'User not found.');
            }

            delivery.creator = result._id;
            var settingId = result._id
            if(result.groupId) {
                settingId = result.groupId.toObjectId()
            }
            return DeliverySettingsRepository.getByClientId(settingId)
                .then((deliverySettings) => {
                    if (!deliverySettings) {
                        throw new ResponseError(400, `User settings could not be found. User ID: ${settingId.toString()}.`);
                    }

                    //validation
                    if(!deliverySettings.addresses || deliverySettings.addresses.length == 0) {
                        if(!delivery.pickup_location.address_1) {
                            throw new ResponseError(400, `Pickup address is not found`);
                        }
                    }

                    if(!delivery.pickup_window && !deliverySettings.pickup_deadline) {
                        //throw new ResponseError(400, `Pickup window is not found`);
                        delivery.pickup_window = delivery.pickup_window||{}
                        delivery.pickup_window.from = "08:00"
                    }
                    if(!delivery.pickup_window && !deliverySettings.pickup_deadline_to) {
                        //throw new ResponseError(400, `Pickup window is not found`);
                        delivery.pickup_window = delivery.pickup_window||{}
                        delivery.pickup_window.to = "22:00"
                    }
                    if(!delivery.delivery_window && !deliverySettings.delivery_window_start) {
                        throw new ResponseError(400, `Delivery window is not found`);
                    }
                    if(!delivery.delivery_window && !deliverySettings.delivery_window_end) {
                        throw new ResponseError(400, `Delivery window is not found`);
                    }

                    delivery.delivery_window = delivery.delivery_window||{}
                    delivery.pickup_window = delivery.pickup_window||{}
                    delivery.pickup_location = delivery.pickup_location||{}

                    delivery.pickup_location.address_1 = delivery.pickup_location.address_1||deliverySettings.addresses[0].value
                    delivery.delivery_window.from = delivery.delivery_window.from||deliverySettings.delivery_window_start
                    delivery.delivery_window.to = delivery.delivery_window.to||deliverySettings.delivery_window_end
                    delivery.pickup_window.from = delivery.pickup_window.from||deliverySettings.pickup_deadline
                    delivery.pickup_window.to = delivery.pickup_window.to||deliverySettings.pickup_deadline_to

                    if(!delivery.pickup_location.zip || !delivery.pickup_location.city) {
                        return GeoCoder.geocode(delivery.pickup_location.address_1).then((ret)=>{
                            //console.log("geocode result", delivery.pickup_location.address_1, ret)
                            if(!ret || !ret[0].zipcode) {
                                throw new ResponseError(400, `Pickup address is not valid [${delivery.pickup_location.address_1}]`);
                            }

                            delivery.pickup_location.zip = ret[0].zipcode
                            delivery.pickup_location.city = ret[0].city
                            return {delivery, deliverySettings};
                        })
                    } else 
                        return {delivery, deliverySettings};
                })
        });
};

CreateUpdateDelivery.prototype.codePickupAddress = function ({delivery, deliverySettings}) {
    //console.log("codePickupAddress", delivery, deliverySettings)
    if (delivery.pickup_location.latitude && delivery.pickup_location.longitude) {
        return {delivery, deliverySettings};
    }

    return GeoCoder.codeAddress(
        delivery.pickup_location.address_1,
        delivery.pickup_location.zip,
        delivery.pickup_location.country
    ).then(coordinates => {
        delivery.pickup_location.latitude = coordinates.latitude;
        delivery.pickup_location.longitude = coordinates.longitude;
        return {delivery, deliverySettings};
    });
};

CreateUpdateDelivery.prototype.checkDeliveryId = function ({delivery, deliverySettings}) {
    //console.log("codePickupAddress", delivery, deliverySettings)
    if (delivery.delivery_id) {
        return {delivery, deliverySettings};
    }

    return AutoKeyService.createKey('delivery').then(id => {
        delivery.delivery_id = String(id)
        return {delivery, deliverySettings};
    });
};

CreateUpdateDelivery.prototype.checkAllowedPickupZones = function ({delivery, deliverySettings}) {
    if (globalVersion == Versioning.V1 || globalVersion == Versioning.V1_1 || globalVersion == Versioning.V1_2) {
        return {delivery, deliverySettings};
    }

    if (!deliverySettings.dropshipping_enabled) {
        return {delivery, deliverySettings};
    }

    let zip = parseInt(delivery.pickup_location.zip);
    if (deliverySettings.allowed_pickups_zones && deliverySettings.allowed_pickups_zones.some(x => parseInt(x.range_from) <= zip && zip <= parseInt(x.range_to))) {
        return {delivery, deliverySettings};
    } else {
        throw new ResponseError(400, `Pickup zone doesn't fall into allowed range. Zip code: ${delivery.pickup_location.zip}.`);
    }
};

CreateUpdateDelivery.prototype.codeDeliveryAddress = function ({delivery, deliverySettings}) {
    if (delivery.delivery_location.latitude && delivery.delivery_location.longitude) {
        return {delivery, deliverySettings};
    }

    return GeoCoder.codeAddress(
        delivery.delivery_location.address_1,
        delivery.delivery_location.zip,
        delivery.delivery_location.country
    ).then(coordinates => {
        delivery.delivery_location.latitude = coordinates.latitude;
        delivery.delivery_location.longitude = coordinates.longitude;
        return {delivery, deliverySettings};
    })
};

CreateUpdateDelivery.prototype.appendDates = function ({delivery, deliverySettings}) {
    return new Promise((resolve, reject) => {
        let deliveryDate = new Date(delivery.delivery_date);

        if (!deliveryDate.isValid())
            return reject("Invalid delivery_date.");

        delivery.weekNumber = deliveryDate.getISOWeek().toString();
        delivery.dayOfWeek = days[deliveryDate.getUTCDay()];
        delivery.delivery_date = deliveryDate;
        return resolve({delivery, deliverySettings});
    })
};

CreateUpdateDelivery.prototype.adjustDate = function ({delivery, deliverySettings}) {
    return DeliverySettingsRepository.getByClientId(delivery.creator).then(settings => {
        if (settings && settings._doc.webshipr_integration_enabled) {
            var deliveryDays = settings._doc.week_days_available;

            if (deliveryDays.includes(delivery.dayOfWeek)) {
                return {delivery, deliverySettings};
            }

            // needed in case if the nearest possible delivery day is on the next week
            var twoWeeksDays = days.concat(days);
            var followingDays = twoWeeksDays.slice(twoWeeksDays.indexOf(delivery.dayOfWeek));
            var nearestDeliveryDay = followingDays.find(x => {
                return deliveryDays.includes(x);
            })

            delivery.delivery_date.setDate(delivery.delivery_date.getDate() + followingDays.indexOf(nearestDeliveryDay));
            delivery.weekNumber = delivery.delivery_date.getISOWeek().toString();
            delivery.dayOfWeek = days[delivery.delivery_date.getUTCDay()];
            delivery.dateChanged = true;
        }

        return {delivery, deliverySettings};
    });
};

CreateUpdateDelivery.prototype.appendDepartment = function ({delivery, deliverySettings}) {
    let creator = delivery.creator,
        givenDepartment = delivery.department || 'undefined';

    return DepartmentsModel.findOne({
        companyId: creator
    }).then(Company => {
        //console.log("Company", Company)
        if (!Company)
            return {delivery, deliverySettings};

        let defaultDepartment,
            chosenDepartment;


        Company.departments.map(department => {
            if (department.default)
                defaultDepartment = department;

            if (department.name === givenDepartment)
                chosenDepartment = department;
        });

        if(!chosenDepartment) {
            if(defaultDepartment && givenDepartment == 'undefined')
                return defaultDepartment
            //create new department
            var newDepartment = { 
                default: (defaultDepartment?false:true),
                typeOfDelivery: 'Distribution',
                name: givenDepartment,
                editorEnabled: false
            }
            Company.departments.push(newDepartment)
            Company.save()
            return newDepartment
        } else {
            return chosenDepartment;
        }
    })
        .then(department => {
            delivery.department = department.name;
            return {delivery, deliverySettings};
        });
};

CreateUpdateDelivery.prototype.store = function ({delivery, deliverySettings}) {
    return new Promise((resolve, reject) => {
            if (deliverySettings && deliverySettings._doc.webshipr_integration_enabled) {
                let startDate = moment(delivery.delivery_date, "MM-DD-YYYY").startOf('day');
                let endDate = moment(delivery.delivery_date, "MM-DD-YYYY").add(1, "days");
                let filter = {
                    pickupaddress: delivery.pickup_location.address_1,
                    "recipientphone.phone": delivery.recipient_phone.phone,
                    deliverydate: {
                        $gte: startDate.toDate(),
                        $lt: endDate.toDate()
                    }
                };
                resolve(ScheduledDeliveriesRepostiroy.scheduled.findOneByFilter(filter))
            }
            else resolve(null);
        })
        .then(foundDelivery => {
            if (foundDelivery) {
                let deliverynumberofpackages = (foundDelivery.deliverynumberofpackages) ? foundDelivery.deliverynumberofpackages + 1 : 2;
                return ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(foundDelivery._id, { deliverynumberofpackages: deliverynumberofpackages })
                    .then((res) => {
                        return res.value;
                    });
            } else {
                delivery = this.cleanupProperties(delivery);
                let createObject = this.createDeliveryObject(delivery);

                if (!createObject.department) {
                    createObject.department = "";
                }

                createObject.status = 1; 

                return new Promise((resolve, reject) => {
                    ScheduledDeliveriesRepostiroy.scheduled.create(createObject)
                        .then(result => {
                            var statusMessage = 'Delivery created'
                            if(createObject.deliverytype=='Express / On-demand') {
                                statusMessage = 'Waiting confirmation'
                            }
                            EventHistoryService.addCustomEvent(result._id, statusMessage)
                                .then(() => {
                                    if (createObject.full_insurance && createObject.full_insurance_value) {
                                        EventHistoryService.addCustomEvent(result._id, `Delivery was created with ${createObject.full_insurance_value} kr in cargo insurance coverage`);
                                    }
                                });

                            return result;
                        })
                        .then(result => {
                            if (delivery.dateChanged) {
                                var deliveryDate = result.deliverydate;
                                var eventMessage = `Delivery moved to the next delivery day (${result.deliverydayofweek}, ${deliveryDate.getUTCMonth() + 1}/${deliveryDate.getUTCDate()})`;

                                // set timeout to make sure the events are created with different time
                                setTimeout(function () {
                                    EventHistoryService.dateChangedScheduledDelivery(result._id, eventMessage);
                                }, 5); 
                            }
                            return resolve(result);
                        })
                        .catch(reject);
                });
            }
        });
};

CreateUpdateDelivery.prototype.cleanupProperties = function (delivery) {
    if (!delivery.delivery_window)
        delivery.delivery_window = {};

    if (!delivery.pickup_window)
        delivery.pickup_window = {};

    if (!delivery.recipient_phone)
        delivery.recipient_phone = {};

    return delivery;
}

CreateUpdateDelivery.prototype.createDeliveryObject = delivery => {
    let createObject = {
        "deliveryid": delivery.delivery_id,
        "department": delivery.department,
        "deliverytype": delivery.delivery_type,
        "cartype": delivery.car_type,
        "distribution": delivery.distribution,
        "recipientname": delivery.recipient_name,
        "recipientid": delivery.recipient_id,
        "recipientemail": delivery.recipient_email,
        "recipientphone": delivery.recipient_phone,
        "pickupdescription": delivery.pickup_location.description,
        "pickupaddress": delivery.pickup_location.address_1,
        "pickupaddress2": delivery.pickup_location.address_2,
        "pickupzip": delivery.pickup_location.zip,
        "pickupcity": delivery.pickup_location.city,
        "pickupdeadline": delivery.pickup_window.from,
        "pickupdeadlineto": delivery.pickup_window.to,
        "delilverydescription": delivery.delivery_location.description,
        "deliveryaddress": delivery.delivery_location.address_1,
        "deliveryaddress2": delivery.delivery_location.address_2,
        "deliveryzip": delivery.delivery_location.zip,
        "deliverycity": delivery.delivery_location.city,
        "deliverydayofweek": delivery.dayOfWeek,
        "deliverywindowstart": delivery.delivery_window.from,
        "deliverywindowend": delivery.delivery_window.to,
        "deliverylabel": delivery.delivery_label,
        "deliverynumberofpackages": delivery.delivery_number_of_packages,
        "deliverynotes": delivery.delivery_notes,
        "deliverydate": delivery.delivery_date,
        "driveremail": delivery.driver_email,
        "weekNumber": delivery.weekNumber,
        "deliverycoordinates": [
            parseFloat(delivery.delivery_location.longitude),
            parseFloat(delivery.delivery_location.latitude)
        ],
        "pickupcoordinates": [
            parseFloat(delivery.pickup_location.longitude),
            parseFloat(delivery.pickup_location.latitude)
        ],
        "creator": delivery.creator,
        "type":delivery.type,
        "returned_delivery_id":delivery.returned_delivery_id,
        "return_request_by":delivery.return_request_by,
    };

    if (globalVersion !== Versioning.V1_1 && globalVersion !== Versioning.V1 && delivery.cargo_insurance) {
        createObject.full_insurance = delivery.cargo_insurance.enabled;
        createObject.full_insurance_value = delivery.cargo_insurance.value;
    };

    return createObject;
}

function _tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
        time = time.slice(1);  // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
}

module.exports = {
    create: ((ver, body, token) => {
        return new CreateUpdateDelivery(ver, body, token, false);
    }),
    update: ((ver, body, token) => {
        return new CreateUpdateDelivery(ver, body, token, true);
    })
}