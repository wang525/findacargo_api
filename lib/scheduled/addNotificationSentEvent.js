let Response = require('../../services/Response');
let EventHistoryService = require('../../services/EventHistoryService');
let ScheduledDeliveriesRepostiroy = require('../carrier-deliveries/repository/scheduleDeliveriesRepository');

function addNotificationSentEvent(deliveryId, body) {
    return ScheduledDeliveriesRepostiroy.scheduled.findOne(deliveryId)
    .then(delivery => {
        if (!delivery)
            return Response.codeWithMessage(400, "Delivery wasn't found.");

        return EventHistoryService.addNotificationSentEvent(delivery._id, body.eventMessage, body.eventDetail)
    })
    .catch(error => {
        return Response.codeWithMessage(500, error.message);
    })
}

module.exports = (deliveryId, body) => { return addNotificationSentEvent(deliveryId, body) };