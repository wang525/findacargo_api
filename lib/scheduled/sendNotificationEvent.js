let EventHistoryService = require('../../services/EventHistoryService');
let ScheduledRepository = require('../scheduled/repository/scheduled');
var AccountRepository = require('../user/repository/account.js');
let request = require('request-promise');
var NotificationsSettings = require('../../models/notificationssettings');
var DeliverySettingsRepository = require('../user/repository/deliverysettings.js');
require('dotenv').config();

function sendNotificationEvent(customEvents) {
    var responseArray = customEvents.deliveryIds.map(deliveryId => {
        let delivery;
        let creator;
        let clientSettings;
        let deliverySettings;
        let admin;

        return AccountRepository.getByApiToken(customEvents.token)
            .then(user => {
                admin = user ? false : true;
                if (!admin && !user.approved)
                    throw {
                        code: 400,
                        msg: "You are not approved to send messages"
                    };

                return ScheduledRepository.getByIdFromString(deliveryId);
            })
            .then(data => {
                if (!data)
                    throw {
                        code: 400,
                        msg: "Delivery wasn't found."
                    };

                delivery = data._doc;
                return AccountRepository.getById(delivery.creator);
            })
            .then(data => {
                if (!data.approved)
                    throw {
                        code: 400,
                        msg: "Delivery owner is not approved"
                    };

                creator = data;
                var groupId = creator._id
                if(creator.groupId) {
                    groupId = creator.groupId.toObjectId()
                }
                return DeliverySettingsRepository.getByClientId(groupId);
            })
            .then(data => {
                deliverySettings = data._doc;
                console.log('deliverySettings', deliverySettings)

                return NotificationsSettings.findOne({
                    $and: [
                        { client_id: creator._id }
                    ]
                });
            })
            .then(data => {
                clientSettings = data ? data._doc : null;

                // if (clientSettings == null) {
                //     throw {
                //         code: 400,
                //         msg: "Your notification settings not found"
                //     };
                // }

                let schedule = {
                    message: customEvents.notificationMessage,
                    clientemail: creator.email,
                    clientName: creator.name,
                    recipientname: delivery.recipientname,
                    deliveryId:deliveryId
                }

                if (!admin) {
                    if (deliverySettings.allow_to_send_email)
                        schedule.recipientemail = delivery.recipientemail;

                    //if (clientSettings.allow_to_send_SMS)
                        schedule.recipientphone = delivery.recipientphone['country_dial_code'] + delivery.recipientphone['phone'];

                    if (!deliverySettings.allow_to_send_email /*&& !clientSettings.allow_to_send_SMS */)
                        throw {
                            code: 400,
                            msg: "The recipient can not receive SMS/Email"
                        };
                }

                // if (clientSettings) {
                //     if (clientSettings.email_enabled)
                //         schedule.recipientemail = clientSettings.email;
                //     else
                //         schedule.recipientemail = '';

                //     if (clientSettings.sms_enabled)
                //         schedule.recipientphone = clientSettings.phone_number;
                //     else
                //         schedule.recipientphone = '';

                //     if (!clientSettings.sms_enabled && !clientSettings.email_enabled)
                //         throw {
                //             code: 400,
                //             msg: "The recipient can not receive SMS/Email"
                //         };
                // } else {
                //     if (admin)
                //         throw {
                //             code: 404,
                //             msg: "No recipient notification settings found"
                //         };
                // }

                var options = {
                    method: 'POST',
                    uri: process.env.NOTIFY_URL + 'order/notification',
                    body: schedule,
                    json: true,
                    //simple: false  
                };
                return request(options)
            })
            .then(resp => {
                if (!resp || !resp.success) {
                    return { status: 200, "deliveryId": deliveryId, success: false, "message": "Something wrong with Notification Service" };
                } else {
                    return EventHistoryService.addCustomEvent(deliveryId, 'Message to client was sent: "' + customEvents.notificationMessage + '"');
                }
            })
            .then(created => {
                return { status: 200, "deliveryId": deliveryId, success: true, "message": "Message was sent" };
            })
            .catch(error => {
                return {
                    status: error.code ? error.code : 505,
                    "deliveryId": deliveryId, success: false,
                    "message": error.msg ? error.msg : "Message was not send due server error"
                };
            })
    });

    return Promise.all(responseArray)
        .then(done => {
            return done;
        });
}

function getMessageTemplates() {
    return new Promise((resolve, reject) => {
        let templates = [
            'Template1',
            'Template2',
            'Template3',
            'Other'
        ];
        return resolve(templates);
    })
}

var SheduledNotifier = {
    getMessageTemplates: getMessageTemplates,
    sendNotificationEvent: sendNotificationEvent
}

module.exports = SheduledNotifier
