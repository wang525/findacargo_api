let ScheduledRepository = require('./repository/scheduled'),
    request = require('request'),
    StatusHelper = require('./statusHelper'),
    Responses = require('../../services/Response'),
    EventHistoryService = require('../../services/EventHistoryService');
require('dotenv').config();
UserRepository = require('../user/repository/account');
let ScheduledDeliveryRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository');
let DeliverySettingsRepository = require('../user/repository/deliverysettings.js');

var authToken = require("../framework/auth-token");
DriverDeliveryRepository = require('../carrier-deliveries/repository/driverDeliveryRepository');
var moment = require("moment");
const momentTZ = require('moment-timezone');
var SocketService = require('../../services/SocketService');

function StatusUpdateHandler(deliveryId, toStatus, token) {
    let self = this;
    toStatus = StatusHelper.getIdFromName(toStatus);

    if (toStatus === -1)
        return this.response(400, "Given status doesn't exist.");

    return ScheduledRepository.getByIdFromString(deliveryId).then((delivery) => {
        if (delivery === null)
            return this.response(400, "Delivery doesn't exist.");

        if (self.isInSameState(delivery, toStatus))
            return this.response(400, "Delivery already in given status.");

        return self.updateDeliveryStatus(deliveryId, toStatus)
            .then((updatedDelivery) => {
                updatedDelivery = updatedDelivery._doc;
                if (updatedDelivery.status == 2) {
                    let end_time = updatedDelivery.estimated_delivery_time;
                    let schedule = {
                        deliverydate: updatedDelivery.deliverydate,
                        deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                        recipientemail: updatedDelivery.recipientemail,
                        recipientname: updatedDelivery.recipientname,
                        recipientphone: updatedDelivery.recipientphone['country_dial_code'] + updatedDelivery.recipientphone['phone'],
                        deliveryId:deliveryId,
                    }
                    UserRepository.findOne(updatedDelivery.creator)
                        .then(user => {
                            if (user) {
                                //emit socket event
                                var socket = SocketService.getSocket();
                                if (socket) {
                                    socket.emit(`delivery_status_changed_${deliveryId}`, {'status':'crerate'});
                                }
                                
                                var clientId = user.groupId||user._id.toString()
                                return DeliverySettingsRepository.getByClientId(clientId.toObjectId()).then((setting)=>{
                                    setting = setting||{}
                                    schedule['notificationSetting'] = {
                                        email_notification_setting : setting.email_notification_setting,
                                        sms_notification_setting : setting.sms_notification_setting
                                    }

                                    //schedule['clientName'] = user.name ? user.name : '';
                                    if(user.groupId == 'undefined' || !user.groupId)
                                    {
                                            schedule['clientName'] = user.name ? user.name : '';
                                            
                                            //Notification
                                            //console.log('notification - create1', schedule)
                                            request.post(process.env.NOTIFY_URL + 'order/create', { json: schedule }, function (err, resp, bd) {
                                                if (err) console.error(err)
                                                //console.log(bd);
                                            })

                                    }
                                    else
                                    {
                                            UserRepository.findOne(user.groupId).then((profile, err)=>{
                                                if(err) {
                                                    console.error(err, profile);
                                                }
                                                schedule['clientName'] = profile.name ? profile.name : '';
                                                    //Notification
                                                //console.log('notification - create2', schedule)
                                                request.post(process.env.NOTIFY_URL + 'order/create', { json: schedule }, function (err, resp, bd) {
                                                    if (err) console.error(err)
                                                    //console.log(bd);
                                                })

                                            });
                                    }
                                })                               
                            }

                        });
                    //end
                }
                else if (updatedDelivery.status == 3) {
                    var today = moment.tz("Europe/Copenhagen");
                    var delivered_at = today.format('HH:mm');

                    self.setDeliveredAtTime(deliveryId, delivered_at)
                        .then(res => { });

                    let end_time = updatedDelivery.estimated_delivery_time;
                    let schedule = {
                        deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                        recipientemail: updatedDelivery.recipientemail,
                        recipientname: updatedDelivery.recipientname,
                        recipientphone: updatedDelivery.recipientphone.country_dial_code + updatedDelivery.recipientphone.phone,
                        deliveryId:deliveryId,
                    }

                    UserRepository.findOne(updatedDelivery.creator)
                        .then(user => {
                            //emit socket event
                            var socket = SocketService.getSocket();
                            if (socket) {
                                socket.emit(`delivery_status_changed_${deliveryId}`, {'status':'complete'});
                            }
                            var clientId = user.groupId||user._id.toString()
                            return DeliverySettingsRepository.getByClientId(clientId.toObjectId()).then((setting)=>{
                                setting = setting||{}
                                schedule['notificationSetting'] = {
                                    email_notification_setting : setting.email_notification_setting,
                                    sms_notification_setting : setting.sms_notification_setting
                                }

                                if(!user.groupId || user.groupId == 'undefined')
                                {
                                        schedule['clientName'] = user.name ? user.name : '';
                                        schedule.clientEmail = user.email;
                                        schedule.clientPhone = user.phone;

                                        //Notification
                                        //console.log('notification - complete', schedule)

                                        request.post(process.env.NOTIFY_URL + 'order/complete', { json: schedule }, function (err, resp, bd) {
                                            if (err) console.log(err)
                                            console.log(bd);
                                        })

                                }
                                else
                                {
                                        UserRepository.findOne(user.groupId).then((profile, err)=>{
                                            if(err) {
                                                console.log(err, profile);
                                            }
                                            schedule['clientName'] = profile.name ? profile.name : '';
                                            schedule.clientEmail = profile.email;
                                            schedule.clientPhone = profile.phone;
                                                    //Notification
                                            //console.log('notification - complete2', schedule)

                                            request.post(process.env.NOTIFY_URL + 'order/complete', { json: schedule }, function (err, resp, bd) {
                                                if (err) console.log(err)
                                                console.log(bd);
                                            })

                                        });
                                }
                            })
                        });
                    toSendDelayNotification(updatedDelivery, deliveryId);
                    //end
                }

                return {
                    delivery_id: deliveryId,
                    status: updatedDelivery.status
                }
            }).then(data => {
                EventHistoryService.addCustomEvent(deliveryId, "Delivery completed");
                return data;
            });
    });
}

function toSendDelayNotification(updatedDelivery, deliveryId) {
    var date = updatedDelivery.deliverydate;
    //console.log(date);
    var estimatedTime = updatedDelivery.estimated_delivery_time;
    //console.log("estimatedTime " + estimatedTime);
    DriverDeliveryRepository.findOne({delivery_id: deliveryId}).then((driverDelivery)=>{
        if(driverDelivery) {
            getZoneDeliveries(driverDelivery.carrier_id, date, deliveryId, updatedDelivery);
        }
    })
}

function getZoneDeliveries(carrierId, date, deliveryid, updatedDelivery) {

    var today = moment.tz("Europe/Copenhagen");
    var dateTime = today.format('HH:mm');

    var estimatedDeliveryTime = moment(updatedDelivery.estimated_delivery_time, "HH:mm");
    var endTime = moment(dateTime, "HH:mm");

    //calc delta between estimate and real arrived time
    var duration = moment.duration(endTime.diff(estimatedDeliveryTime));
    var hours = parseInt(duration.asHours());
    var minutes = parseInt(duration.asMinutes()) % 60;
    //console.log("********", dateTime, hours + ' hour and ' + minutes + ' minutes.');
    
    return new Promise((resolve, reject) => {
        //get all deliveries
        return DriverDeliveryRepository.getAssignedDeliveries(carrierId).toArray((error, deliveries) => {
            let start = moment(date).startOf('day');
            let end = moment(start).add(1, "days");
            console.log('date range', start, end)
            let ids = deliveries.map((delivery) => {
                if (delivery.delivery_id) {
                    return delivery.delivery_id.toObjectId();
                }
            });

            //get driver's all today deliveries
            return ScheduledDeliveriesRepository.zone.forDate(start, end, ids).toArray((error, res) => {
                console.log('found deliveries', res.length)
                let result = res.map((delivery) => {
                    var recipientname = '';
                    if (deliveryid != delivery._id.toString()) {
                        if ((Math.abs(hours) >= 1 || Math.abs(minutes) > 5) && delivery.status == 2) {
                             //console.log('Need delay', delivery._id)

                        if (delivery.status == 2) {
                            UserRepository.findOne(delivery.creator)
                                .then(user => {

                                    var nextEstimatedTime = moment(delivery.estimated_delivery_time, 'HH:mm')
                                        .add(hours, 'hours')
                                        .add(minutes, 'minutes')
                                        .format('HH:mm');
 
                                    console.log("Next Estimated Time", delivery._id, delivery.estimated_delivery_time, nextEstimatedTime);
 
                                    // var nextDeliveryTimeto = moment(nextEstimatedTime, "h:mm A").format("HH:mm");
                                    //console.log(nextDeliveryTimeto);
                                    var query = {};
                                    query.estimated_delivery_time = nextEstimatedTime;
                                    ScheduledDeliveryRepository.scheduled.findOneAndUpdate(delivery._id, query);

                                    var clientId = user.groupId||user._id.toString()
                                    //console.log(nextEstimatedTime);
                                    var schedule = {
                                        "deliverywindowend": delivery.deliverywindowend,
                                        "recipientemail": delivery.recipientemail,
                                        "recipientname": delivery.recipientname,
                                        "recipientphone": delivery.recipientphone['country_dial_code'] + delivery.recipientphone['phone'],
                                        "clientName": user.name,
                                        "clientEmail": user.email,
                                        "clientPhone": user.phone,
                                        "deliverydate": delivery.deliverydate,
                                        "estimated_delivery_time": nextEstimatedTime,
                                        "deliveryId": delivery.token||delivery._id
                                    };
                                    //console.log(schedule);
                                    return DeliverySettingsRepository.getByClientId(clientId.toObjectId()).then((setting)=>{
                                        setting = setting||{}
                                        schedule['notificationSetting'] = {
                                            email_notification_setting : setting.email_notification_setting,
                                            sms_notification_setting : setting.sms_notification_setting
                                        }

                                        if(user.groupId) {
                                            UserRepository.findOne(clientId.toObjectId()).then(userProfile => {
                                                schedule.clientName = userProfile.name;
                                                schedule.clientEmail = userProfile.email;
                                                schedule.clientPhone = userProfile.phone;
                                                //console.log('notification - driver1', schedule)

                                                request.post(process.env.NOTIFY_URL + 'order/delay', { json: schedule }, function (err, resp, bd) {
                                                    if (err) console.error(err)
                                                    //console.log(bd);
                                                });
                                            })
                                        } else {
                                            request.post(process.env.NOTIFY_URL + 'order/delay', { json: schedule }, function (err, resp, bd) {
                                                if (err) console.error(err)
                                                //console.log(bd);
                                            });    
                                        }
                                    })
                                });
                            }
                        }
                    }
                    return { from: delivery };
                });
                resolve(res);
            });
        });
    });
}

StatusUpdateHandler.prototype.isInSameState = function (delivery, toStatus, token) {
    return delivery.status == toStatus;
};

StatusUpdateHandler.prototype.updateDeliveryStatus = function (deliveryId, toStatus) {
    return ScheduledRepository
        .updateDeliveryStatus(deliveryId, toStatus);
};

StatusUpdateHandler.prototype.setDeliveredAtTime = function (deliveryId, time) {
    return ScheduledRepository
        .setDeliveredAtTime(deliveryId, time);
};

StatusUpdateHandler.prototype.response = function (code, message) {
    return Responses.codeWithMessage(code, message);
};

module.exports = ((deliveryId, toStatus, token) => { return new StatusUpdateHandler(deliveryId, toStatus, token); });