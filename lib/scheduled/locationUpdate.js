let Responses = require('../../services/Response'),
    UserRepository = require('../user/repository/account'),
    TrackingService = require('../../services/TrackingService');

function UpdateLocation(id, location, token) {
    if (!token)
        return Responses.codeWithMessage(401, "Not authorized.");

    return this.getAuthenticatedUser(token)
            .then(() => {
                return TrackingService.addTrackingData(id, {
                    lat: location.latitude,
                    lon: location.longitude
                })
            })
            .catch(error => {
                if (error.error && error.error.status)
                    return error;
            });
}

UpdateLocation.prototype.getAuthenticatedUser = function(token) {
    return UserRepository.getByApiToken(token)
        .then(result => {
            if (!result)
                throw Responses.codeWithMessage(401, "Token not found.");

            return result;
        });
};

module.exports = ((id, location, token) => { return new UpdateLocation(id, location, token); });