let EventsHistoryService = require('../../services/EventHistoryService');

function getEventsHistoryForDelivery(deliveryId) {
    return EventsHistoryService.getEventsForScheduledDelivery(deliveryId);
}

module.exports = delivery_id => { return getEventsHistoryForDelivery(delivery_id) };