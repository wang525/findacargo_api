var db = require('../framework/db-connector');
let scheduledDeliveries = db.collection('scheduleddeliveries');
var ScheduledRepository = require('../scheduled/repository/scheduled')

let clientProfile = db.collection('accounts');
var Versioning = require('../../utils/Versioning');
let PostalCodeModel = require('../../models/postalCode');

function ForPrintResponse(id, version) {
    globalVersion = Versioning.normaliseVersion(version);
    return new Promise((resolve, reject) => {
        ScheduledRepository.findOne(id)
        .then(doc => {
            let del = {};

            if (doc) {
                del.dest_name = doc.recipientname;
                del.dest_addr1 = doc.deliveryaddress;
                del.dest_addr2 = doc.deliveryaddress2;
                del.dest_zip = doc.deliveryzip;
                del.dest_city = doc.deliverycity;
                del.dest_phone = getPhoneNumber(doc.recipientphone);
                del.delivery_id = doc.deliveryid;
                del.delivery_notes = doc.deliverynotes;
                del.delivery_number_of_packages = doc.deliverynumberofpackages;
                del.client_id = doc.recipientid;
                del.order_index = doc.orderIndex;
                del.status = doc.status;

                clientProfile.findOne({ '_id': doc.creator }, function (e, profile) {
                    if (e) return reject(e);
    
                    if (profile) {
                        if (typeof profile.groupId == 'undefined') {
                            del.client_name = profile.name;
                            del.client_emailid = profile.supportEmail ? profile.supportEmail : '';
                            del.client_phone = profile.supportPhone ? profile.supportPhone : '';
                            del.client_addr = profile.companyDetails ? profile.companyDetails.companyAddress : '';
                            del.client_zip = profile.zip;
                            del.client_city = profile.city;
                            if (version === 'v1.4') {
                                PostalCodeModel.findOne({ 'ZIP': del.dest_zip }, function (error, zipCode) {
                                    if(!zipCode) {
                                        return resolve(del)
                                    }
                                    del.area = zipCode.AREA;
                                    del.zone = zipCode.ZONE;
                                    return resolve(del);
                                });
    
                            } else {
                                return resolve(del);
                            }
                        } else {
                            clientProfile.findOne({ '_id': profile.groupId.toObjectId() }, function (e, profileUser) {
                                if (e)  return reject(e);
                                if (profileUser) {
                                    del.client_name = profileUser.name;
                                    del.client_emailid = profileUser.supportEmail ? profileUser.supportEmail : '';
                                    del.client_phone = profileUser.supportPhone ? profileUser.supportPhone : '';
                                    del.client_addr = profileUser.companyDetails ? profileUser.companyDetails.companyAddress : '';
                                    del.client_zip = profileUser.zip;
                                    del.client_city = profileUser.city;
                                    if (version === 'v1.4') {
                                        PostalCodeModel.findOne({ 'ZIP': del.dest_zip }, function (error, zipCode) {
                                            del.area = zipCode.AREA;
                                            del.zone = zipCode.ZONE;
                                            return resolve(del);
                                        });
    
                                    } else {
                                        return resolve(del);
                                    }
                                }
                            });
                        }
                    }
                })
            } else {
                return reject();
            }
        })
        .catch((err)=>{
            return reject(err);
        })
    });
}

function getPhoneNumber(phone) {
    if (!phone || (typeof phone === 'string'))
        return phone;

    let dialCode = phone.country_dial_code || "45";
    if (!dialCode.startsWith("+")) {
        dialCode = '+' + dialCode
    }

    return `(${dialCode}) ${phone.phone}`;
}


module.exports = {
    print: ((id) => {
        return new ForPrintResponse(id, 'v1');
    }),
    printv4: ((id) => {
        return new ForPrintResponse(id, 'v1.4');
    })
}
