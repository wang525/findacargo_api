let Response = require('../../services/Response');
let EventHistoryService = require('../../services/EventHistoryService');
let ScheduledDeliveriesRepostiroy = require('../carrier-deliveries/repository/scheduleDeliveriesRepository');

function addAdminEvent(deliveryId, body) {
    return ScheduledDeliveriesRepostiroy.scheduled.findOne(deliveryId.toObjectId())
    .then(delivery => {
        if (!delivery)
            return Response.codeWithMessage(400, "Delivery wasn't found.");
        console.log("lib scheduled is called")
        return EventHistoryService.addAdminEvent(delivery._id, body.eventMessage)
    })
    .catch(error => {
        return Response.codeWithMessage(500, error.message);
    })
}

module.exports = (deliveryId, body) => { return addAdminEvent(deliveryId, body) };