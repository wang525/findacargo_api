let ScheduledDeliveriesRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository');
let Response = require('../../services/Response');
let UserRepository = require('../user/repository/account');
var mongodb = require("mongodb");

function ClientSearch(body, token) {
    if (!token)
        return Response.codeWithMessage(401, "Not authorized.");

    let result = {};
    let query = {};

    return UserRepository.getByApiToken(token)
        .then(user => {
            if (!user) {
                throw "User not found.";
            }

            var ids = []
            ids.push(user._id)
            var groupId = user._id.toString()
            if(user.groupId) {
                groupId = user.groupId
                ids.push(user.groupId.toObjectId())
            }
            return UserRepository.findAll({
                $or:[{_id:{$in:ids}}, {groupId:groupId}]
            }).then(users=>{
                ids = users.map((u)=>u._id)
                query = this.formatQuery(body, ids);

                console.log('query', JSON.stringify(query))

                return new Promise((resolve, reject) => {
                    let cursor = ScheduledDeliveriesRepository.scheduled
                        .all(query, null, this.getFieldsToReturn())
                        .sort({ deliverydate: -1 });

                    if (body.page && body.size) {
                        cursor = cursor.skip((body.page - 1) * body.size).limit(body.size);
                    }

                    return cursor.toArray((err, res) => {
                        if (err) reject(err);
                        return resolve(res);
                    });
                });      
            })
        }).then(deliveries => {
            deliveries.forEach(x => {
                if (x.recipientphone) {
                    x.recipientphone = x.recipientphone.phone;
                }
            });

            result.items = deliveries;
            return ScheduledDeliveriesRepository.scheduled.count(query);
        }).then(count => {
            result.total = parseInt(count);
            return result;
        });
};

ClientSearch.prototype.formatQuery = function (body, userIds) {
    let query = {
        creator: {$in:userIds}
    }

    var keyword = body.keyword.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")

    if(body.searchOption == 'id') {
        query.$where = `this._id.toString().indexOf("${keyword}")!=-1`
    } else {
        let regex = { $regex: new RegExp(keyword), $options: "i" };
        query.$or = [
            { deliveryid: regex },
            { recipientid: regex },
            { recipientname: regex },
            { deliveryaddress: regex },
            { "recipientphone.phone": regex },
            { recipientemail: regex }
        ];
    }
    return query;
};

ClientSearch.prototype.formatQuery2 = function (body, userIds) {
    let query = {
        creator: {$in:userIds}
    }

    var keyword = body.keyword.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
    query.$where = `this._id.toString().indexOf("${keyword}")!=-1`

    return query;
};

ClientSearch.prototype.getFieldsToReturn = function () {
    return {
        deliveryid: 1,
        recipientid: 1,
        recipientname: 1,
        deliveryaddress: 1,
        "recipientphone.phone": 1,
        recipientemail: 1,
        status: 1,
        token: 1,
        deliverydate: 1,
        estimated_delivery_time: 1
    };
};

module.exports = (body, token) => { return new ClientSearch(body, token) };