var getUser = require("../auth/user").getUser;
var ScheduledRepository = require("./repository/scheduled");
var ScheduledDeliveries = require("../carrier-deliveries/todaysDelivery");
var DeliveryFactory = require("../delivery/factory/delivery");

var Starter = require("./starter");
var starter = new Starter({
    getUser: getUser,
    ScheduledRepository: ScheduledRepository,
    ScheduledDeliveries: ScheduledDeliveries,
    deliveryFactory: DeliveryFactory
});

module.exports = {
    Starter: starter
}
