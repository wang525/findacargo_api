let ObjectId = require("mongodb").ObjectId;
let mongoose = require("mongoose");
var mongo = require('mongodb');
mongoose.Promise = require("bluebird");

let scheduledSchema = require("./models/scheduled");
let Scheduled = mongoose.models.Scheduled;
let Delivery = mongoose.models.Delivery;
let Vehicle = mongoose.models.truck;

let Repository = {
    findOne:findOne,
    getById: getById,
    getByIdFromString: getByIdFromString,
    bulkCreateDelivery: bulkCreateDelivery,
    getVehicle: getVehicle,
    setDeliveredAtTime: setDeliveredAtTime,
    updateDeliveryStatus: updateDeliveryStatus,
    getDeliveriesByTime: getDeliveriesByTime,
    getDeliveriesByWeek: getDeliveriesByWeek,
    findAndUpdateQuery: findAndUpdateQuery,
    getDeliveryByDeliveryID : getDeliveryByDeliveryID,
};

function findOne(id) {
    //console.log("typeof id ", typeof id )
    try {
        if (typeof id === "string") {
            if(id.length == 6) {
                return Scheduled.findOne({token: id});
            } else {
                return Scheduled.findOne({_id: ensureObjectId(id)});
            }
        } else {
            return Scheduled.findOne({_id: id});
        }
    } catch(e){
        console.error("[EXCEPTION - on fetch delivery]", id)
        return new Promise((resolve, reject) => {
            reject(e)
        })
    }
}

function getById(id) {
    return Scheduled.findOne({ "_id": new ObjectId(id) }).exec();
}

function getByIdFromString(id) {
    return Scheduled.findById(id);
}

function bulkCreateDelivery(deliveries) {
    let promises = deliveries.map(function (deliveryData) {
        let delivery = new Delivery(deliveryData);

        return delivery.save()
            .then(updateScheduled);
    });

    return Promise.all(promises);

    function updateScheduled(delivery) {
        return Scheduled.findOneAndUpdate(
            { _id: new ObjectId(delivery.reference) },
            {
                delivery_id: delivery._id.toString(),
                carrier: delivery.carriers[0]
            }
        );
    }
}

function getVehicle(userId) {
    return Vehicle.findOne({ userID: userId }).exec();
}

function getDeliveryByDeliveryID(deliveryId) {
    return Scheduled.findOne({ deliveryid: deliveryId }).exec();
}

/**
 * Update a progress log with new status change.
 *
 * @param deliveryId
 * @param fromStatus
 * @param toStatus
 * @returns {Query}
 */
function updateDeliveryStatus(deliveryId, toStatus) {
    let query = {
        $set: {
            "status": toStatus
        }
    };
    return scheduledSchema.findByIdAndUpdate(deliveryId, query, { new: true });
}

/**
 * Find delivery and update it.
 *
 * @param deliveryId
 * @param data
 * @returns {Query}
 */
function findAndUpdateQuery(deliveryId, data) {    
    return scheduledSchema.findByIdAndUpdate(deliveryId, { $set: data }, { upsert: true, new: true });
}

function setDeliveredAtTime(deliveryId, time) {
    let query = {
        $set: {
            "delivered_at": time
        }
    };
    return scheduledSchema.findByIdAndUpdate(deliveryId, query, { upsert: true, new: true });
}

function getDeliveriesByTime(startDateLimit, endDateLimit) {
    return Scheduled.aggregate([{ $match: { deliverydate: { $gte: startDateLimit, $lt: endDateLimit } } }, {
        $group: {
            _id: '$creator',
            data: {
                $push: {
                    _id: '$_id',
                    clientId: '$recipientid',
                    deliveryId: '$deliveryid',
                    clientName: '$recipientname',
                    clientPhone: '$recipientphone',
                    pickupAddress: '$pickupaddress',
                    address: '$deliveryaddress',
                    deliverywindowstart: '$deliverywindowstart',
                    deliverywindowend: '$deliverywindowend',
                    date: '$deliverydate',
                    status: '$status'
                }
            }
        }
    }])
    /*.toArray(function(error, scheduledDeliveries) {
        return scheduledDeliveries;
    });*/
}

function getDeliveriesByWeek(weekNum) {
    return Scheduled.aggregate([{ $match: { weekNumber: weekNum.toString() } }, {
        $group: {
            _id: '$creator',
            data: {
                $push: {
                    _id: '$_id',
                    clientId: '$recipientid',
                    deliveryId: '$deliveryid',
                    clientName: '$recipientname',
                    clientPhone: '$recipientphone',
                    pickupAddress: '$pickupaddress',
                    address: '$deliveryaddress',
                    deliverywindowstart: '$deliverywindowstart',
                    deliverywindowend: '$deliverywindowend',
                    date: '$deliverydate',
                    status: '$status'
                }
            }
        }
    }])
    /*.toArray(function(error, scheduledDeliveries) {
        return scheduledDeliveries;
    });*/
}

module.exports = Repository;
