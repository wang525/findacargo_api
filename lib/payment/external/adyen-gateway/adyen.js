var needle = require("needle");
var request = require("request");
var config = require("../../../config").adyen;
var requestStatus = require("../../enum/request-status");
var Promise = require("bluebird");

var https = require('https');

var Gateway = {
    authorize: authorize,
    capture: capture,
    refundOrCancel: refundOrCancel
};

var opts = {
  username: config.user,
  password: config.password,
  json: true
}
function authorize(encryptedData, value, currency, reference) {
    var requestData = {
        "additionalData": {
            "card.encrypted.json": encryptedData
        },
        "amount": {
            "value": value,
            "currency": currency
        },
        "reference": reference,
        "merchantAccount": config.merchantAccount
    };

    var adyenRequestHeader = getRequestHeader(config.path.authorise, config);

    return getRequest(adyenRequestHeader, requestData, "Authorised");
}

function capture(value, currency, externalReference, reference) {
    var requestData = {
        "merchantAccount": config.merchantAccount,
        "modificationAmount": {
            "currency": currency,
            "value": value
        },
        "originalReference": externalReference,
        "reference": reference
    }

    var header = getRequestHeader(config.path.capture, config);
    return getRequest(header, requestData, "[capture-received]");
}

function refundOrCancel(externalReference, reference) {
    var requestData = {
        "merchantAccount": config.merchantAccount,
        "originalReference": externalReference,
        "reference": reference
    }

    var header = getRequestHeader(config.path.cancelOrRefund, config);
    return getRequest(header, requestData, "[cancelOrRefund-received]");
}

function getRequestHeader(path, config) {
    return {
        host: config.host,
        method: 'POST',
        auth: config.user + ':' + config.password,
        path: path,
        headers: {
            'Content-type': 'application/json'
        }
    };
}

function getRequest(header, requestData, expected) {
    return new Promise(function(resolve, reject){
        needle.post('https://'+header.host+header.path, requestData, opts, function(err, resp, body) {
            if(err){
                reject(err.message);
            }

            if(resp.statusCode == 200){
                resolve(processResult(resp.body, expected));
            }

            reject("Payment Refused: "+resp.body.errorCode+" - "+resp.body.message);
        });
    });
}


function processResult(data, expected) {
    if(data.resultCode === expected || data.response === expected) {
        data.result = requestStatus.success;
        return data;
    }

    data.result = requestStatus.failed;
    return data;
}

module.exports = Gateway;
