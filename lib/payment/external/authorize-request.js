var q = require("q");
var gateway = require("./adyen-gateway/adyen");
var requestRegister = require("../repository/request-register");
var requestStatus = require("../enum/request-status");
var deliveryStatusChanger = require("../../delivery").StatusChanger;

var AuthorizeRequest = {
    request: request
};

var options = {
    PaymentModel: null,
    RequestResponse: null
};

function request(PaymentModel) {
    options.PaymentModel = PaymentModel;
    var deferred = q.defer();
    var encryptedData = PaymentModel.encryptedData,
        value = PaymentModel.value.toFixed(2)*100,
        currency = PaymentModel.currency,
        reference = PaymentModel._id.toString();

    gateway.authorize(encryptedData, value, currency, reference)
        .then(registerResponse)
        .then(processRequestStatus)
        .then(deferred.resolve, deferred.reject);

    return deferred.promise;
}

function registerResponse(RequestResponse) {
    options.RequestResponse = RequestResponse;
    return requestRegister.authorize(RequestResponse, options.PaymentModel);
}

function processRequestStatus(PaymentModel) {
    PaymentModel.externalReference = options.RequestResponse.pspReference;
    return {
        success: options.RequestResponse.result === requestStatus.success,
        PaymentModel: PaymentModel
    } 
}

module.exports = AuthorizeRequest;
