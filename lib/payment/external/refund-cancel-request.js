var di = {};
var options = {};

function RefundOrCancelRequest(depend) {
    di = depend

    var that = this;

    this.request = request;

    function request(Payment) {
        if(!Payment) {
            throw "Invalid Payment data";
        }
        options.Payment = Payment
        var externalReference = Payment.externalReference;
        var reference = Payment.deliveryId.toString();

        return di.gateway.refundOrCancel(externalReference, reference)
                    .then(registerResponse)
                    .then(processRequestStatus);
    }

    function registerResponse(Response) {
        options.Response = Response;
        return di.requestRegister.refundOrCancel(Response, options.Payment);
    }

    function processRequestStatus(Payment) {
        return {
            success: options.Response.result === di.requestStatus.success,
            Payment: Payment
        } 
    }
}

module.exports = RefundOrCancelRequest;
