var di = {};
var options = {};

function CaptureRequest(depend) {
    di = depend

    var that = this;

    this.request = request;

    function request(Payment) {
        if(!Payment) {
            throw "Invalid Payment data";
        }
        options.Payment = Payment
        var value = Payment.value;
        var currency = Payment.currency;
        var externalReference = Payment.externalReference;
        var reference = Payment.deliveryId.toString();

        return di.gateway.capture(value, currency, externalReference, reference)
                    .then(registerResponse)
                    .then(processRequestStatus);
    }

    function registerResponse(Response) {
        options.Response = Response;

        return di.requestRegister.capture(Response, options.Payment);
    }

    function processRequestStatus(Payment) {
        return {
            success: options.Response.result === di.requestStatus.success,
            Payment: Payment
        } 
    }
}

module.exports = CaptureRequest;
