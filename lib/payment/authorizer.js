var q = require("q");
var getUser = require("../auth/user").getUser;
var paymentCreator = require("./repository/creator");
var paymentRepository = require("./repository/payment");
var authorizeRequest = require("./external/authorize-request");
var states = require("./enum/payment-status");
var Mixpanel = require('mixpanel/lib/mixpanel-node');

var mixpanel = Mixpanel.init('35b08bf5d7f756c6462e052b48eb22df');


var Authorizer = {
    authorize: authorize
};

var options = {
    delivery: null,
}

var mixData = {};



function authorize(delivery, parameters) {
    options.delivery = delivery;

    var user = getUser();
    mixData.distinct_id = user.id;
    mixData.delivery_id = delivery._id;
    mixData.amount = delivery.price;
    mixData.type = 'CC';

    return createPayment(delivery._id, delivery.price, parameters.encryptedData)
        .then(requestPaymentAuthorization)
        .then(verifyRequestStatus)
        .then(updatePaymentState);
}

function createPayment(deliveryId, deliveryPrice, encryptedData) {
    var user = getUser();

    return paymentCreator.create(user._id, deliveryId, deliveryPrice, "DKK", encryptedData);
}

function requestPaymentAuthorization(PaymentModel) {
    return authorizeRequest.request(PaymentModel);
}

function verifyRequestStatus(requestResult) {

    var user = getUser();
    if (!requestResult.success) {


        mixpanel.track("Delivery Could Not Be Paid", mixData);

        throw "Payment Refused!";
    } else {
        mixpanel.track("Delivery Paid", mixData);
    }
    return requestResult.PaymentModel;
}

function updatePaymentState(Payment) {

    mixData.status = states.authorized.id;
    mixpanel.track("Delivery Status", mixData);

    return paymentRepository.setStatus(Payment, states.authorized.id)
        .then(function() {
            return options.delivery;
        });
}

module.exports = Authorizer;
