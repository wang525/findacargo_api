var mongoose = require("mongoose");
var paymentSchema = require("./models/payment");
var q = require("q");
var paymentStatus = require("../enum/payment-status");

var Payment = mongoose.models.Payment;

RequestRegister = {
    authorize: authorize,
    capture: capture,
    refundOrCancel: refundOrCancel
}

function authorize(RequestResponse, PaymentModel){
    var data = factory("AUTHORIZE", RequestResponse.result);
    return addToPaymentModel(data, PaymentModel);
}

function capture(RequestResponse, PaymentModel){
    var data = factory("CAPTURE", RequestResponse.result);
    return addToPaymentModel(data, PaymentModel);
}

function refundOrCancel(RequestResponse, PaymentModel){
    var data = factory("REFUND_OR_CANCEL", RequestResponse.result);
    return addToPaymentModel(data, PaymentModel);
}

function factory(type, response){
    return {
        type: type,
        response: response
    }
}

function addToPaymentModel(data, PaymentModel){
    var deferred = q.defer();

    var query = {
        $push: {"externalRequests": data}
    };

    PaymentModel.externalRequests.push(data);
    PaymentModel.save(function(err){
        if(err) {
            return deferred.reject(err);
        }

        deferred.resolve(PaymentModel);
    });

    return deferred.promise;
}

module.exports = RequestRegister;
