var mongodb = require("mongodb");
var mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
var paymentSchema = require("./models/payment");
var q = require("q");
var paymentStatus = require("../enum/payment-status");

var Payment = mongoose.models.Payment;

var Repository = {
    getByStatusAndDeliveryId: getByStatusAndDeliveryId,
    setStatus: setStatus
};

function getByStatusAndDeliveryId(statusId, deliveryId, userId) {
    var query = {
        "status": { "$in": (typeof statusId === "number")? [statusId] : statusId },
        "deliveryId": new mongodb.ObjectId(deliveryId.toString()),
        "accountId": new mongodb.ObjectId(userId.toString())
    }
debugger;
    return Payment.findOne(query).exec();
}

function setStatus(payment, status) {
    payment.status = status;

    return payment.save();
}
module.exports = Repository;
