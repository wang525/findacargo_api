var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var ExternalRequest = new Schema({
    "type": {
        "type": String,
        "required": true
    },
    "response": {
        "type": String,
    },
    "createdAt": {
        "type": Date,
        "default": Date.now
    },
});

var Payment = new Schema({
    "accountId": {
        "type": ObjectId,
        "required": true
    },
    "deliveryId": {
        "type": ObjectId,
        "required": true
    },
    "status": {
        "type": Number,
        "required": true
    },
    "value": {
        "type": Number,
        "required": true
    },
    "currency": {
        "type": String,
        "required":true
    },
    "encryptedData": {
        "type": String,
        "required": true
    },
    "createdAt": {
        "type": Date,
        "default": Date.now
    },
    "merchantReference": {
        "type": String
    },
    "externalReference": {
        "type": String
    },
    "externalRequests": {
        "type": [ExternalRequest]
    }
});

module.exports = mongoose.model("Payment", Payment);
