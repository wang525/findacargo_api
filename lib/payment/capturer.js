var di = {};

function Capturer(depend) {
    di = depend;

    this.capture = capture;

    function capture(delivery) {
        return getAuthorizedPayment(delivery)
                .then(requestCapture);
    }

    function getAuthorizedPayment(delivery) {
        var user = di.getUser();
        return di.paymentRepository.getByStatusAndDeliveryId(di.states.authorized.id, delivery._id, user._id);
    }

    function requestCapture(payment) {
        return di.captureRequest.request(payment);
    }
}

module.exports = Capturer;
