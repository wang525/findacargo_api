module.exports = {
    success: "SUCCESS",
    failed: "FAILED",
    processing: "PROCESSING",
    error: "ERROR"
}
