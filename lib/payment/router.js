var express = require('express');
var Controller = require("../framework/controller");
var Authorizer = require("./authorizer");

var AuthorizerController = new Controller({
    pay: Authorizer.pay
});

var router = express.Router();

router.post('/pay', AuthorizerController.pay);

module.exports = router;
