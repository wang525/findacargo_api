var Authorizer = require("./authorizer");
var Capturer = require("./capturer");
var CaptureRequest = require("./external/capture-request");
var RefundOrCancelRequest = require("./external/refund-cancel-request");
var Reverser = require("./reverser");

var capturer = new Capturer({
    paymentRepository: require("./repository/payment"),
    captureRequest: new CaptureRequest({
        gateway: require("./external/adyen-gateway/adyen"),
        requestRegister: require("./repository/request-register"),
        requestStatus: require("./enum/request-status")
    }),
    getUser: require("../auth/user").getUser,
    states: require("./enum/payment-status")
});

var reverser = new Reverser({
    paymentRepository: require("./repository/payment"),
    refundOrCancelRequest: new RefundOrCancelRequest({
        gateway: require("./external/adyen-gateway/adyen"),
        requestRegister: require("./repository/request-register"),
        requestStatus: require("./enum/request-status")
    }),
    getUser: require("../auth/user").getUser,
    states: require("./enum/payment-status")
});

module.exports = {
    Authorizer: Authorizer,
    Capturer: capturer,
    Reverser: reverser
}
