/*******************************************************************************
 * MessageNotification Job
 ******************************************************************************/
'use strict';
const Config = require('../config');
//var phoneChecker = require('./numberchecker');
var request = require('request-promise');
//const messagebird = require('messagebird')(Config.messagebird.accessKey);

module.exports = {
    send: (data) => {
        var content = `<request>
            <authentication apikey="${Config.inmobile}" />
            <data>
                <message>
                    <sendername>IDAG</sendername>
                    <text><![CDATA[${data.message}]]></text>
                    <recipients>
                        <msisdn>${data.to}</msisdn>
                    </recipients>
                </message>
            </data>
        </request>`

        return request.post(
            {url:'https://mm.inmobile.dk/Api/V2/SendMessages',
            body : content,
            headers: {'Content-Type': 'application/xml'}
            }
        );
    }
};
