let ScheduledDeliveriesRepostiroy = require('../carrier-deliveries/repository/scheduleDeliveriesRepository');
let Responses = require('../../services/Response');
let EventHistoryService = require('../../services/EventHistoryService');
let DriverDeliveryRepository = require('../carrier-deliveries/repository/driverDeliveryRepository');
var DeliverySettingsRepository = require('../user/repository/deliverysettings.js');

let accountRepository = require("../user/repository/account");
let statusHelper = require('../scheduled/statusHelper');
let moment = require('moment');
let PostalCodeRepository = require('../postal-codes/repository');
let fleetSchema = require("../user/repository/models/fleet");
let GeoCoder = require('../../services/GeoCoder');
let AutoKeyService = require('../../services/AutoKeyService');
let AddAdminEvent = require('../../services/EventHistoryService');

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const ExposedScheduledDeliveryController = {
    getDelivery: (req, res, next) => {
        //console.log("getDelivery")
        let deliveryId = "";
        let delivery;
        ScheduledDeliveriesRepostiroy.scheduled.findOne(req.params.id || '')
            .then(data => {
                //console.log("data", data)
                if (!data) {
                    Responses.noData(res, "Delivery not found", {});
                    throw new Error("Delivery not found");
                }
                delivery = data;
                deliveryId = data._id.toString()
                return EventHistoryService.getEventsForScheduledDelivery(deliveryId);
            })
            .then((events) => {
                delivery.events = events.map(event => {
                    return {
                        event: event.event,
                        event_data: event.event_data,
                        event_description: statusHelper.getDescription(event.event_data),
                        created_at: event.created_at,
                    };
                });
                return accountRepository.getById(delivery.creator);
            })
            .then((client) => {
                if(client && client.groupId) {
                    return accountRepository.getById(client.groupId);
                } 

                return client
            })
            .then((client) => {
                if (client) {
                    delivery.client = client;
                }

                return DeliverySettingsRepository.getByClientId(client._id).then((setting)=>{
                    // console.log("++++++++++++setting+++++++++++++++", setting)
                    if(setting) {
                        let stringSetting=JSON.stringify(setting);
                        let jsonSetting =JSON.parse(stringSetting);

                        delivery.allow_to_reschedule = (jsonSetting.allow_to_reschedule || false);
                        delivery.allow_signature = (jsonSetting.allow_signature || false);
                        delivery.allow_delivery_address = (jsonSetting.allow_delivery_address || false);
                        delivery.allow_delivery_time = (jsonSetting.allow_delivery_time || false);
                    } else {
                        delivery.allow_to_reschedule = false;
                        delivery.allow_signature = false;
                        delivery.allow_delivery_address = false;
                        delivery.allow_delivery_time = false;
                    }
                    return PostalCodeRepository.getForZipCode(delivery.deliveryzip)
                })
            })
            .then((postalObject) => {
                if (postalObject) {
                    delivery.area = postalObject.AREA;
                    delivery.zone = postalObject.ZONE;
                }

                return DriverDeliveryRepository.findOne({delivery_id: deliveryId});
            })
            .then((driver_delivery) => {
                if (driver_delivery) {
                    return accountRepository.getById(ensureObjectId(driver_delivery.carrier_id))
                }
                return null;
            })
            .then((driver) => {
                //console.log("Driver", driver)
                if (driver && parseInt(driver.carrier) === 1) {
                    delivery.carrier = {
                        name: driver.name,
                        phone: driver.phone
                    };

                    return fleetSchema.findOne({'userID': driver._id});
                } else {
                    return null
                }
            })
            .then((truck) => {
                //console.log("truck", truck)
                if (truck) {
                    delivery.truck = truck;
                }
                //console.log("delivery result",delivery)
                Responses.success(res, "Delivery found", delivery);
            })
            .catch((err) => {
                console.error(err);
            })
    },
    changeDeliveryAddress: (req, res, next) => {
        let deliveryId = ensureObjectId(req.params.id || '');
        let query = {
            deliveryaddress: req.body.route + " " + req.body.street_number,
            deliveryzip: req.body.zip,
            deliverycity: req.body.city,
        };
        let newEventMessage = "Delivery Address was changed to " + query.deliveryaddress + " " + query.deliveryzip + " " + query.deliverycity;
        GeoCoder.codeAddress(
            query.deliveryaddress,
            query.deliveryzip
        ).then(coordinates => {
            if (!coordinates) {
                Responses.noData(res, "Incorrect address", {});
                throw new Error("Incorrect address");
            }

            query.deliverycoordinates = [
                parseFloat(coordinates.longitude),
                parseFloat(coordinates.latitude)
            ];
            return ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(deliveryId, query);
        })
            .then(data => {
                if (!data) {
                    Responses.noData(res, "Delivery not found", {});
                    throw new Error("Delivery not found");
                }
                return EventHistoryService.addCustomEvent(deliveryId, newEventMessage);
            })
            .then(event => {
                if (event) {
                    event = {
                        event: event.event,
                        event_data: event.event_data,
                        event_description: statusHelper.getDescription(event.event_data),
                        created_at: event.created_at,
                    };
                }
                Responses.success(res, "Delivery found", {success: true, data: query, event: event});
            });
    },
    changeDeliveryDateTime(req, res, next) {
        let deliveryId = ensureObjectId(req.params.id || '');
        let query = Object.assign({}, req.body);
        let newEventMessage = "Delivery Date was changed";
        if (query.deliverydate) {
            let deliveryDate = new Date(query.deliverydate);

            if (!deliveryDate.isValid()){
                Responses.noData(res, "Invalid Date", {});
                throw new Error("Invalid Date");
            }

            query.weekNumber = deliveryDate.getISOWeek().toString();
            query.deliverydayofweek = days[deliveryDate.getUTCDay()];
            query.deliverydate = deliveryDate;
            newEventMessage += " to " + moment(query.deliverydate).format("DD-MM-YYYY");
        }

        newEventMessage += ". From " + query.deliverywindowstart + " to " + query.deliverywindowend;
        console.log("change date and time+++++++", query)
        ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(deliveryId, query)
            .then(data => {
                if (!data) {
                    Responses.noData(res, "Delivery not found", {});
                    throw new Error("Delivery not found");
                }
                return EventHistoryService.addCustomEvent(deliveryId, newEventMessage);
            })
            .then(event => {
                Responses.success(res, "Delivery found", {success: true, data: query});
            });
    },
    changeDeliveryNote(req, res, next) {
        let deliveryId = ensureObjectId(req.params.id || '');
        let query = {
            deliverynotes:(req.body.deliverynotes||'')
        };

        console.log('deliveryId', deliveryId)
        ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(deliveryId, query)
            .then(data => {
                if (!data) {
                    Responses.noData(res, "Delivery not found", {});
                    throw new Error("Delivery not found");
                }
                //return EventHistoryService.addCustomEvent(deliveryId, newEventMessage);
                if(req.body.applyall) {
                    //set this note as default setting
                    accountRepository.getById(data.value.creator)
                    .then((client) => {
                        if(client && client.groupId) {
                            return accountRepository.getById(client.groupId);
                        } 

                        return client
                    })
                    .then((client)=>{
                        return DeliverySettingsRepository.updateSetting(client._id, {'default_note':(req.body.deliverynotes||'')})
                    })
                    .then(()=>{
                        console.log('here')
                        Responses.success(res, "Delivery found", {success: true, data: query});    
                    })
                } else {
                    Responses.success(res, "Delivery found", {success: true, data: query});    
                }
            })
    },
    createIdByType(req, res, next) {
        var type = req.params.type
        AutoKeyService.createKey(type).then(id=>{
            Responses.success(res, "Id was created", {success: true, data: {id}});
        })
    },

    updateDelivery: function (req, res, next) {

        let deliveryId = ensureObjectId(req.params.id || '');
        let query = Object.assign({}, req.body);

        let insurance;
        let insurance_value;

        ScheduledDeliveriesRepostiroy.scheduled.findOne(deliveryId)
            .then(doc => {
                insurance = doc.full_insurance;
                insurance_value = doc.full_insurance_value;

                if (query.deliverydate) {
                    let deliveryDate = new Date(query.deliverydate);
        
                    if (!deliveryDate.isValid()){
                        Responses.noData(res, "Invalid Date", {});
                        throw new Error("Invalid Date");
                    }
        
                    query.weekNumber = deliveryDate.getISOWeek().toString();
                    query.deliverydayofweek = days[deliveryDate.getUTCDay()];
                    query.deliverydate = deliveryDate;
                }

                return ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(deliveryId, query)
            })
            .then(data => {
                if (!data) {
                    Responses.noData(res, "Delivery not found", {});
                    throw new Error("Delivery not found");
                }
                if (query.full_insurance !== insurance || query.full_insurance_value !== insurance_value) {
                    changeEventText += ` (with ${query.full_insurance_value} kr in cargo insurance coverage)`
                }
                return EventHistoryService.addAdminEvent(deliveryId, changeEventText);
            })
            .then(event => {
                Responses.success(res, "Delivery found", {success: true});
            });

/*
        let deliveryId = ensureObjectId(req.params.id || '');
        let query = {
            deliverynotes:(req.body.deliverynotes||'')
        };

        let insurance;
        let insurance_value;
        ScheduledDeliveriesRepostiroy.scheduled.findOne(deliveryId)
            .then(doc => {
                console.log("find one --------------", doc)
                insurance = doc.full_insurance;
                insurance_value = doc.full_insurance_value;

                let query = Object.assign({}, req.body);
                let deliveryDate = new Date(query.deliverydate);

                if (!deliveryDate.isValid()){
                    Responses.noData(res, "Invalid Date", {});
                    throw new Error("Invalid Date");
                }

                query.deliverydate = deliveryDate.toISOString();

                console.log("post req find query.deliverydate ====>", query)
                return ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(deliveryId, query)
            })
            ScheduledDeliveriesRepostiroy.scheduled.findOneAndUpdate(deliveryId, req.body)
            .then((doc) => {
                console.log("find one and update =================", doc)
                let changeEventText = "Delivery was changed";
                // if (doc.full_insurance !== insurance || doc.full_insurance_value !== insurance_value) {
                //     changeEventText += ` (with ${doc.full_insurance_value} kr in cargo insurance coverage)`
                // }
                return EventHistoryService.addAdminEvent(deliveryId, changeEventText);
            }).then((doc) => {
                console.log("add admin event create", doc)
                // if (err) return res.send(500, {error: err});
                return res.send({ success: true });
            });
*/            
    },
};

module.exports = ExposedScheduledDeliveryController;