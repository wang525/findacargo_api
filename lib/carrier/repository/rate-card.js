var mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
var rateCardSchema = require("./models/rate-card");
var rateSchema = require("./models/rate");
var RateCard = mongoose.models.RateCard;
var Rate = mongoose.models.Rate;

var RateCardRepository = {
    set: set,
    getForCarriersAndType: getForCarriersAndType,
    getForVehicles: getForVehicles
}

function set(userId, typeId, prices) {
    var query = {
        userId: userId,
        vehicleTypeId: typeId
    };

    var update = {
        $set: {
            values: prices
        }
    }

    return RateCard.findOneAndUpdate(query, update, {upsert:true}).exec();
}

function getForCarriersAndType(carriers, type){
    if(carriers.length <1){
        return false;
    }

    var query = {
        "userId": {"$in": carriers},
        "vehicleTypeId": type
    }

    return RateCard.findOne(query).lean().exec()
                .then(function(doc){
                    if(!doc){
                        return false;
                    }
                    
                    var rate = {};
                    rate[type] = doc.values;
                    return rate;
                })
                .catch(function(err){
                    return false;
                });
}

function getForVehicles(vehicles) {
    var ids = vehicles.map(function(){
        return vehicles.id
    });

    var query = {
        "vehicleID": {"$in":ids}
    }

    Rate.findOne(query).lean().exec()
                .then(function(doc){
                    if(!doc){
                        return false;
                    }

                    return doc.values;
                })
                .catch(function(err){
                    return false;
                });
}

module.exports = RateCardRepository;
