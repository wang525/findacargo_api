var mongodb = require("mongodb");
var db = require('../../framework/db-connector');
var q = require('q');
var moment = require('moment');

var NoVehicleAvailableError = require("./error/no-vehicle-available");
var mongoose = require("mongoose");
var rateCardSchema = require("./models/rate-card");
var RateCard = mongoose.models.RateCard;

var vehicleCollection = db.collection('truck');

var repository = {
    getTypesAvailable: getTypesAvailable,
    getLiveAvailable: getLiveAvailable,
    getLiveVehicles: getLiveVehicles,
    getVehicleDetails: getVehicleDetails,
    getVehicle: getVehicle,
    updateStatus: updateStatus,
    updateLocation: updateLocation,
    findByCarrierId: findByCarrierId,
    turnOffIdleVehicles: turnOffIdleVehicles,
    getLocation: getLocation
}

function getTypesAvailable(filter, type) {
    var deferred = q.defer();
    var query = [{
        "$geoNear": {
            "near": {
                "type": "Point",
                "coordinates": filter 
            },
            "distanceField": "dist.calculated",
            "spherical": true,
            "query": (type)?{"type":type,"liveDelivery.available": true}:{"liveDelivery.available": true},
            "includeLocs": "dist.location",
            "maxDistance": 30000
        }
    }, {
        $group: {
            "_id": {
                id: "$type",
                description: "$vehicalType",
                available: "$liveDelivery.available"
            },
            "distance": {
                $min: "$dist.calculated"
            },
            "carriers": {
                $addToSet: {
                    $cond: [
                        {$eq: ["$liveDelivery.available", true]},
                        "$userID",
                        null
                    ]
                }
            },
            "vehicles": {
                $addToSet: {
                    $cond: [
                        {$eq: ["$liveDelivery.available", true]},
                        {
                            id: "$_id",
                            description: "$identifier",
                            distance: "$dist"
                        },
                        null
                    ]
                }
            }
        }
    }];

    vehicleCollection.aggregate(query, function(error, response){
      if(response) {
            var typesAvailables = (type)? response : completeTypes(response);
            deferred.resolve(typesAvailables);
        }else{
            deferred.reject(error);
        }
    });

    return deferred.promise;
}

function completeTypes(typesResponse) {
    var vehicleTypes = require("../enum/vehicle-types").VehicleTypes;

    if(typesResponse.length === vehicleTypes.length) {
        return typesResponse;
    }

    var typesLookup = typesResponse.map(function(item){
        return parseInt(item._id.id);
    });

    var diff = [];

    function formatType(type) {
        return {
            "_id": {
                "id": type.typeId,
                "description": type.description
            },
            "distance": null,
            "carriers": [],
            "vehicles": []
        }
    }

    for(var i in vehicleTypes) {
        if(typesLookup.indexOf(vehicleTypes[i].typeId) === -1) {
            diff.push(formatType(vehicleTypes[i]));
        }
    }

    return typesResponse.concat(diff);
}

function getLiveAvailable(typeId, coordinates, blacklist) {
    var deferred = q.defer();
    var query = [{
        "$geoNear": {
            "near": {
                "type": "Point",
                "coordinates": coordinates 
            },
            "distanceField": "dist.calculated",
            "spherical": true,
            "query": {
                "type": typeId,
                "liveDelivery.available": true,
                "userID": {"$nin": blacklist}
            },
            "limit": 10,
            "includeLocs": "dist.location",
            "maxDistance": 30000
        }
    },{
        "$sort": {"dist.calculated":1}
    }];

    vehicleCollection.aggregate(query, function(error, response){
        if(response) {
            if(response.length == 0) {
                return deferred.reject("No vehicle available");
            }
            deferred.resolve(response.pop());
        }else{
            deferred.reject(error);
        }
    });

    return deferred.promise;
}

function getLiveVehicles(location) {
    var query = {
                "liveDelivery.available": true,
                "userID":{$exists:true}
            };
    if(location.latitude != 0 && location.longitude != 0) {
        var deferred = q.defer();
        query = [{
            "$geoNear": {
                "near": {
                    "type": "Point",
                    "coordinates": [location.longitude, location.latitude] 
                },
                "distanceField": "dist.calculated",
                "spherical": true,
                "query": {
                    "liveDelivery.available": true
                },
                "limit": 10,
                "includeLocs": "dist.location",
                "maxDistance": 30000
            }
        },{
            "$sort": {"dist.calculated":1}
        }];

        vehicleCollection.aggregate(query, function(error, response){
            if(response) {
                if(response.length == 0) {
                    return deferred.reject("No vehicle available");
                }
                deferred.resolve(response);
            }else{
                deferred.reject(error);
            }
        });
    
        return deferred.promise;
    } else {
        return new Promise((resolve, reject) => {
            vehicleCollection.find(query).toArray((err, rets) => {
                if (err) reject(err);
                 resolve(rets);
            });
        })
    }
}

function getVehicle(vehicleId, userId){
    var deferred = q.defer();

    var query = {
        "_id": new mongodb.ObjectId(vehicleId)
    };

    if(userId){
        query['userID'] = new mongodb.ObjectId(userId)
    }

    vehicleCollection.findOne(query, function(error, response){
        if(response) {
            deferred.resolve(response);
        }else{
            deferred.reject(error);
        }
    });

    return deferred.promise;
}

function getVehicleDetails(request){
    var deferred = q.defer();
    var query = {
        "_id": new mongodb.ObjectId(request.vehicle_id),
        "liveDelivery.available": true
    };

    vehicleCollection.findOne(query, function(error, response){
        if(response) {
            deferred.resolve(response);
        }else{
            deferred.reject(error);
        }
    });

    return deferred.promise;
}

function updateStatus(status, vehicleId, carrierId) {
    var deferred = q.defer();
    var query = {
        "_id": new mongodb.ObjectId(vehicleId),
        "userID": new mongodb.ObjectId(carrierId)
    };
    var update = {
        "$set": {"liveDelivery.available": status}
    };
    vehicleCollection.findOneAndUpdate(query, update, {returnOriginal : false}, function(err, doc){
        if(err){
            return deferred.reject(err);
        }
        if(!doc.value) {
            return deferred.reject("Invalid vehicle Id");
        }

        deferred.resolve(doc.value);
    });

    return deferred.promise;
}

function getLocation(vehicleId) {
    return new Promise(function(resolve, reject){
       //do query here and return the result
        //resolve with data
        //reject with error
        var query = {
            "_id": new mongodb.ObjectId(vehicleId)

        };
        vehicleCollection.findOne(query, function(err,data){
           if(err) return reject(err);
           if(!data.liveDelivery)
           {
               reject(new Error("Location not found"))
           }
//get livedelivery.location or error if doesnt exist
           resolve(data.liveDelivery); // need to transform data, exctact coordinates, etc...
        });
    });
}

function updateLocation(vehicleId, carrierId, coordinates) {
    //console.log("updateLocation", vehicleId, carrierId, coordinates)
    var deferred = q.defer();
    var query = {
        "_id": new mongodb.ObjectId(vehicleId),
        "userID": new mongodb.ObjectId(carrierId)
    };

    var update = {
        "$set": {"liveDelivery.location": {
                "type": "Point",
                "updated_at" : new Date(),
                "coordinates": [coordinates.longitude, coordinates.latitude]
            }
        }
    };

    vehicleCollection.findOneAndUpdate(query, update, function(error, response){
        if(response) {
            deferred.resolve(true);
        }else{
            deferred.reject(error);
        }
    });

    return deferred.promise;
}

function findByCarrierId(carrierId) {
    var deferred = q.defer();
    var query = {
        "userID": new mongodb.ObjectId(carrierId)
    };

    var rs = vehicleCollection.find(query);
    rs.toArray(function(err, response){
        if(err) {
            return deferred.reject(err);
        }
        deferred.resolve(response);
    });

    return deferred.promise;
}

function turnOffIdleVehicles() {
    var deferred = q.defer();
    var query = {
        "liveDelivery.available": true,
        "liveDelivery.location.updated_at": {$lt: moment().subtract(1, 'hours').toDate()}
    };

    var update = {
        "$set": {"liveDelivery.available": false}
    };

    vehicleCollection.update(query, update, {multi:true}, function(error, response){
        if(error){
            console.error(error);
        }

        deferred.resolve(true)
    });

    return deferred.promise;
}
module.exports = repository;
