var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var RateCard = new Schema({
    "userId": {
        "type": ObjectId,
        "required": true
    },
    "vehicleTypeId": {
        "type": Number,
        "required": true
    },
    "values": {
        "base": {
            "type": Number,
            "required": true
        },
        "costPerKm": {
            "type": Number,
            "required": true
        },
        "extra": {
            "type": Number,
            "required": true
        },
        "velocity": {
            "type": Number
        }
    }
});

module.exports = mongoose.model("RateCard", RateCard);


