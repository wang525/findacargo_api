var vehicleRepository = require('./repository/vehicle');
var carrierRepository = require('../carrier-deliveries/repository/carrierRepository');
var timeCalculator = require('../delivery').TimeCalculator;
var costCalculator = require('../delivery').CostCalculator;
var distanceCalculator = require('../delivery/calculator').GMapsDistance;
var vehicleFormatter = require('./formatter/vehicle');
var locationFactory = require('./factory/location');
var getUser = require('../auth/user').getUser;
var rateCardRepository = require("./repository/rate-card");
var AccountRepository = require("../user/repository/account");
var TrackService = require("../../services/TrackingService");
var Promise = require("bluebird");
var q = require('q');
var SocketService = require('../../services/SocketService');
var mongoose = require('mongoose');
var FleetModel = mongoose.models.truck;
var _ = require('underscore')
//var ScheduledDeliveriesModel = require('../../models/ScheduledDelivery')

if (!FleetModel) {
    FleetModel = require('../../models/Fleet');
}

var Inventary = {
    listAvailable: listAvailable,
    listAvailableAll: listAvailableAll,
    listVehicles: listVehicles,
    bidVehicle: bidVehicle,
    setVehicleLocation: setVehicleLocation,
    setDriverLocation: setDriverLocation,
    getTypeRateCard: getTypeRateCard,
    getAvailability: getAvailability,
    getVehicleLocation: getVehicleLocation
};

function listAvailable(pickUp, dropOff) {
    var pickUpLocation = locationFactory.createFromCommaSeparatedString(pickUp);
    var dropOffLocation = locationFactory.createFromCommaSeparatedString(dropOff);
    var filter = [pickUpLocation.longitude, pickUpLocation.latitude];

    return vehicleRepository.getTypesAvailable(filter)
        .then(function (typesList) {
            if (!dropOffLocation) {
                return addPriceAndTime(typesList, 0);
            }
            return distanceCalculator.calculate(pickUpLocation, dropOffLocation)
                .then(function (distance) {
                    return addPriceAndTime(typesList, distance);
                });
        })
        .then(function (data) {
            var costCalculator = require('../delivery').CostCalculator;
            return vehicleFormatter.availability(data, pickUpLocation, dropOffLocation, costCalculator.distance);
        });

    function addPriceAndTime(typesList, distance) {
        var promises = [];
        for (var n in typesList) {
            promises.push(rateCardRepository.getForVehicles(typesList[n]["vehicles"]));
        }

        return Promise.all(promises).then(function (rateCardList) {
            var costCalculator = require('../delivery').CostCalculator;
            var timeCalculator = require('../delivery').TimeCalculator;

            var defaultRateCard = costCalculator.getRateCard();
            for (var n in typesList) {
                costCalculator.setRateCard(defaultRateCard);
                var rateCard = costCalculator.getRateCard().getByType(typesList[n]["_id"].id);
                var base = rateCard.base;
                var perKm = rateCard.costPerKm;
                if (rateCardList[n]) {
                    base = rateCardList[n].basePrice;
                    perKm = rateCardList[n].pricePerKm;
                }
                typesList[n].price = costCalculator.estimate(typesList[n]["_id"].id, pickUpLocation, dropOffLocation, distance);
                typesList[n].timeToPickUp = timeCalculator.calculate(typesList[n].distance, typesList[n].vehicles.length > 0, rateCard.velocity);
                typesList[n].basePrice = base;
                typesList[n].pricePerKm = perKm;
            }
            return typesList;
        });

    }
}

function listAvailableAll(pickup) {
    var pickUpLocation = locationFactory.createFromCommaSeparatedString(pickup);
    // return new Promise((resolve, reject) => {
    //     vehicleRepository.getLiveVehicles(pickUpLocation).then((err, data) => {
    //         console.log("getLiveDeliveries", err, data)
    //         if (err) reject(err);
    //             resolve(data);
    //     });
    // })
    return vehicleRepository.getLiveVehicles(pickUpLocation).then(vehicles=>{
        var userIds = []
        _.map(vehicles, (o)=>{
            userIds.push(ensureObjectId(o.userID))
        })

        console.log('userIds', userIds)

        return AccountRepository.findAll({
            _id:{$in:userIds}
        }).then(users => {
            console.log('users', users)
            _.map(vehicles, (o)=>{
                var user = _.find(users, (u)=>{
                    return u._id.toString() == o.userID
                })

                if(user) {
                    o.user = {
                        email:user.email,
                        name:user.name,
                        phone:user.phone,
                        ccode:user.ccode,
                        date:user.date
                    }
                }
            })

            return vehicles
        })
    })
    // .then(function (vehicles) {
    //     return vehicles
    // })
    // .then(function (data) {
    //     return data
    // });
}

function getAvailability(delivery) {
    return vehicleRepository.getTypesAvailable(delivery.pickUp.location, delivery.vehicleType)
        .then(function (typesList) {
            typeData = typesList.pop();

            if (!typeData) {
                return false;
            }

            return typeData["carriers"].length > 0;
        });
}

function getTypeRateCard(typeId, pickup) {
    var filter = [parseFloat(pickup.longitude), parseFloat(pickup.latitude)];

    return vehicleRepository.getTypesAvailable(filter, typeId)
        .then(function (typesList) {
            typeData = typesList.pop();

            if (!typeData) {
                return false;
            }

            return rateCardRepository.getForCarriersAndType(typeData["carriers"], typeId);
        });
}

function bidVehicle(deliveryModel) {
    var blacklist = deliveryModel.carriers.map(function (item) {
        return item.accountId;
    });
    return vehicleRepository
        .getLiveAvailable(deliveryModel.vehicleType, deliveryModel.pickUp.location, blacklist)
        .then(vehicleFormatter.bidVehicle);
}

function setVehicleLocation(vehicleId, coordinates) {
    console.log("#################### setVehicleLocation ##############", vehicleId, coordinates)
    var User = getUser();
    var deferred = q.defer();
    if (vehicleId == 'driver') {
        FleetModel.findOne({ "userID": User._id, "identifier": "default vehicle" }, function (err, data) {
            if (err) {
                return true;
            }
            if (data == null) {
                var new_fleet = new FleetModel({
                    "identifier": "default vehicle",
                    "vehicalType": "Van",
                    "type": "2",
                    "allowCargo": "Machinery",
                    "userID": User._id
                });
                new_fleet.save(function (err, doc) {
                    
                    vehicleId = doc.id;
                    var deferred = q.defer();
                    vehicleRepository.updateLocation(vehicleId, User.id, coordinates)
                    .then(()=>{
                        return TrackService.addTrackingData(User.id, vehicleId, coordinates)
                    })
                        .then(() => {
                            var socket = SocketService.getSocket();
                            if (socket) {
                                socket.emit(`vehicle_location_changed_${vehicleId}`, coordinates);
                            }
                        })
                        .then(vehicleFormatter.updateStatus)
                        .then(deferred.resolve, deferred.reject);
                });
                return deferred.promise;
            } else {

                vehicleId = data.id;                
                vehicleRepository.updateLocation(vehicleId, User.id, coordinates)
                .then(()=>{
                    return TrackService.addTrackingData(User.id, vehicleId, coordinates)
                })
                .then(() => {
                        var socket = SocketService.getSocket();
                        if (socket) {
                            socket.emit(`vehicle_location_changed_${vehicleId}`, coordinates);
                        }
                    })
                    .then(vehicleFormatter.updateStatus)
                    .then(deferred.resolve, deferred.reject);
                //console.log(deferred.promise);
                
            } 
        });

        //update all in-progress delivery's track
        // carrierRepository.getZoneDeliveries(User._id, new Date()).then(deliveries => {
        //     console.log("deliveries", deliveries)
        // })
        return deferred.promise;
    } else {
        var deferred = q.defer();
        vehicleRepository
            .updateLocation(vehicleId, User.id, coordinates)
            .then(()=>{
                return TrackService.addTrackingData(User.id, vehicleId, coordinates)
            })
            .then(() => {
                var socket = SocketService.getSocket();
                if (socket) {
                    socket.emit(`vehicle_location_changed_${vehicleId}`, coordinates);
                }
            })
            .then(vehicleFormatter.updateStatus)
            .then(deferred.resolve, deferred.reject);

        // carrierRepository.getZoneDeliveries(User._id, new Date()).then(deliveries => {
        //     console.log("deliveries", deliveries)
        // })
    
        return deferred.promise;
    }
}

function setDriverLocation(coordinates, data, a, d) {
    var deferred = q.defer();
    var User = getUser();

    var new_fleet = new FleetModel({
        "identifier": "default vehicle",
        "vehicalType": "Van",
        "type": "2",
        "allowCargo": "Machinery",
        "userID": "dkvasani"
    });
    new_fleet.save(function (err, doc) {
        //console.log('err', doc, err)
        Response.success(res, 'Successfully created the carrier.');
    })

    vehicleRepository
        .updateLocation(vehicleId, User.id, coordinates)
        .then(() => {
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`vehicle_location_changed_${vehicleId}`, coordinates);
            }
        })
        .then(vehicleFormatter.updateStatus)
        .then(deferred.resolve, deferred.reject);

    return deferred.promise;
}

function getVehicleLocation(vehicleId) {
    return vehicleRepository.getLocation(vehicleId);
}

function listVehicles() {
    var user = getUser();

    return vehicleRepository
        .findByCarrierId(user.id)
        .then(vehicleFormatter.list)
}

module.exports = Inventary;
