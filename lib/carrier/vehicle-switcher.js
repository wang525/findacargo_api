var util = require("util");
var EventEmitter = require("events").EventEmitter;
var getUser = require("../auth/user").getUser;
var vehicleRepository = require("./repository/vehicle");
var vehicleFormatter = require("./formatter/vehicle");


function VehicleSwitcher() {
    EventEmitter.call(this);

    this.turnOn = turnOn;
    this.turnOff = turnOff;

    var that = this;

    function turnOn(vehicleId) {
        var User = getUser();
        return vehicleRepository
            .updateStatus(true, vehicleId, User.id)
            .then(emitTurnOn)
            .then(vehicleFormatter.updateStatus)
    }

    function turnOff(vehicleId) {
        var User = getUser();
        return vehicleRepository
            .updateStatus(false, vehicleId, User.id)
            .then(emitTurnOff)
            .then(vehicleFormatter.updateStatus)
    }

    function emitTurnOn(vehicleModel) {
        that.emit("turn-on", vehicleModel);
        return vehicleModel;
    }

    function emitTurnOff(vehicleModel) {
        that.emit("turn-off", vehicleModel);
        return vehicleModel;
    }
}

util.inherits(VehicleSwitcher, EventEmitter);

module.exports = VehicleSwitcher;
