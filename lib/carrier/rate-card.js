var di = {};

function RateCard(depend) {
    di = depend;

    this.set = set;
    this.get = get;

    function set(typeId, prices) {
        var user = di.getUser();

        return di.rateCardRepository.set(user.id, typeId, prices);
    }

    function get(){}
}

module.exports = RateCard;
