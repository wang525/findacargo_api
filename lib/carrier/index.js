var Inventary = require('./inventary');
var VehicleSwitcher = require('./vehicle-switcher');
var VehicleSwitcherEvents = require('./events/vehicle-switcher');
var RateCard = require('./rate-card');
var RateCardRepository = require('./repository/rate-card');

var vehicleSwitcher = new VehicleSwitcher();
var vehicleSwitcherEvents = new VehicleSwitcherEvents(vehicleSwitcher);

module.exports = {
    Inventary: Inventary,
    VehicleSwitcher: vehicleSwitcher,
    RateCard: new RateCard({
        rateCardRepository: RateCardRepository,
        getUser: require('../auth/user').getUser
    })
};
