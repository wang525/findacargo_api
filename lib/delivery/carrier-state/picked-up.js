var States = require("./enum");
var BaseState = require("./base-state");
var Delivered = require("./delivered");

function PickedUp(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.PickedUp);

    this.setDelivered = setDelivered;

    function setDelivered() {
        var state = new Delivered(this.Delivery, this.carrier, this.StateEngine, States.Delivered);

        return state.apply();
    }
}

PickedUp.prototype = Object.create(BaseState.prototype);
PickedUp.prototype.constructor = PickedUp;

module.exports = PickedUp;
