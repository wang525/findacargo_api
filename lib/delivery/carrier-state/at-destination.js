var States = require("./enum");
var BaseState = require("./base-state");
var Delivered = require("./delivered");

function AtDestination(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.AtDestination);

    this.setDelivered = setDelivered;

    function setDelivered() {
        var state = new Delivered(this.Delivery, this.carrier, this.StateEngine, States.Delivered);

        return state.apply();
    }
}

AtDestination.prototype = Object.create(BaseState.prototype);
AtDestination.prototype.constructor = AtDestination;

module.exports = AtDestination;
