var States = require("./enum");
var BaseState = require("./base-state");
var PickedUp = require("./picked-up");

function Started(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.OnStarted);

    this.setPickedUp = setPickedUp;

    function setPickedUp() {
        var state = new PickedUp(this.Delivery, this.carrier, this.StateEngine, States.PickedUp);

        return state.apply();
    }
}

Started.prototype = Object.create(BaseState.prototype);
Started.prototype.constructor = Started;

module.exports = Started;
