var States = require("./enum");
var BaseState = require("./base-state");
var Accepted = require("./accepted");
var Rejected = require("./rejected");
var deliveryState = require("../delivery-state");

function Invited(Delivery, carrier, StateEngine) {
    BaseState.call(this, Delivery, carrier, StateEngine, States.Invited);

    var that = this;

    this.setAccepted = setAccepted;
    this.setRejected = setRejected;

    function setAccepted() {
        var state = new Accepted(this.Delivery, this.carrier, this.StateEngine, States.Accepted);
        return state.apply()
                .then(updateDeliveryState);
    }

    function updateDeliveryState(delivery) {
        var current = deliveryState.load(delivery, that.StateEngine.getScheduler());
        return current.setCarrierApproved();
    }

    function setRejected() {
        var state = new Rejected(this.Delivery, this.carrier, this.StateEngine, States.Rejected);
        var scheduler = that.StateEngine.getScheduler();
        return state.apply()
                .then(scheduler.bidVehicle)
                .catch(function(err){
                    return this.Delivery;
                });
    }
}

Invited.prototype = Object.create(BaseState.prototype);
Invited.prototype.constructor = Invited;

module.exports = Invited;
