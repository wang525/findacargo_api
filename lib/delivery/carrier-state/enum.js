var StateEnum = {
    Invited : state(1, "Carrier Invited"),
    Accepted : state(2, "Carrier Accepted"),
    Rejected : state(3, "Carrier Rejected"),
    Arrived : state(4, "Carrier Arrived"),
    OnRoute : state(5, "Carrier On Route"),
    AtDestination : state(6, "At Destination"),
    Delivered: state(7, "Delivered"),
    Started: state(8, "Started"),
    PickedUp: state(9, "Picked Up"),
    getDescription: getDescription,
    getKey: getKey
}

function state(id, desc){
    return {
        id: id,
        desc: desc
    };
}

function getDescription(id) {
    var keys = Object.keys(StateEnum)
    var lookup = [];

    for(var i in keys){
        if(id == StateEnum[keys[i]].id){
            return StateEnum[keys[i]].desc;
        }
    }
}

function getKey(id) {
    var keys = Object.keys(StateEnum)
    var lookup = [];

    for(var i in keys){
        if(id == StateEnum[keys[i]].id){
            return keys[i];
        }
    }

    throw new Error("Invalid State ID");
}
module.exports = StateEnum;
