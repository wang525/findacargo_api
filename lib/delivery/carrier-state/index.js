var Invited = require("./invited");
var Accepted = require("./accepted");
var Rejected = require("./rejected");
var Arrived = require("./arrived");
var OnRoute = require("./on-route");
var AtDestination = require("./at-destination");
var Delivered = require("./delivered");
var Started = require("./started");
var PickedUp = require("./picked-up");

module.exports = {
    Invited: Invited,
    Accepted: Accepted,
    Rejected: Rejected,
    Arrived: Arrived,
    OnRoute: OnRoute,
    AtDestination: AtDestination,
    Delivered: Delivered,
    Started: Started,
    PickedUp: PickedUp
}
