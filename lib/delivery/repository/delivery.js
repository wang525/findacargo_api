var mongodb = require("mongodb");
var mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
var deliverySchema = require("./models/delivery");
var scheduledSchema = require("../../scheduled/repository/models/scheduled");
var EventHistoryService = require('../../../services/EventHistoryService');

var getUser = require("../../auth/user").getUser;

var Mixpanel = require('mixpanel/lib/mixpanel-node');

var mixpanel = Mixpanel.init('35b08bf5d7f756c6462e052b48eb22df');

var q = require("q");
var Delivery = mongoose.models.Delivery;
var scheduledDelivery = mongoose.models.Scheduled;

var Repository = {
    create: create,
    getById: getById,
    getByIdAndCarrier: getByIdAndCarrier,
    getByUserId: getByUserId,
    updateStatus: updateStatus,
    update: update,
    getByDateAndStatus: getByDateAndStatus,
    getCarrierEarnings: getCarrierEarnings,
    recoverDelivery: recoverDelivery,
    getBuyerPurchases: getBuyerPurchases,
    updateGroupReference: updateGroupReference,
    updateScheduled: updateScheduled,
    getRecent: getRecent
};

function create(deliveryData) {
    var deferred = q.defer();

    deliveryModel = new Delivery(deliveryData);
    deliveryModel.save(function(err, data) {
        var user = getUser();

        if (err) {
            deferred.reject(err);
        } else {

            var mixData = {};
            mixData.distinct_id = user.id;
            mixData.deliveryID = deliveryModel.delivery_id;
            mixData.requesterID = user._id;
            mixData.pickupID = "Not assigned";
            mixpanel.track("Delivery Requested", mixData);


            deferred.resolve(deliveryModel);
        }
    });

    return deferred.promise;
}

function getById(id, accountId) {
    var deferred = q.defer();
    var query = {
        "_id": new mongodb.ObjectId(id)
    };

    if(accountId) {
        query["buyer.accountId"] = new mongodb.ObjectId(accountId);
    }

    var rs = Delivery.findOne(query);
    rs.exec(query, function(error, deliveryData) {
        if (error) {
            return deferred.reject(error);
        }

        if (!deliveryData) {
            return deferred.reject("Invalid DeliveryId");
        }

        deferred.resolve(deliveryData);
    });

    return deferred.promise;
}

function getByIdAndCarrier(id, accountId) {
    var deferred = q.defer();

    var query = {
        "_id": new mongodb.ObjectId(id),
        "carriers": {
            $elemMatch: {
                "accountId": new mongodb.ObjectId(accountId)
            }
        }
    };

    var rs = Delivery.findOne(query);
    rs.exec(query, function(error, deliveryData) {
        if (error) {
            return deferred.reject(error);
        }

        if (!deliveryData) {
            return deferred.reject("Delivery not assigned to carrier.");
        }

        deferred.resolve(deliveryData);
    });

    return deferred.promise;
}

function getByUserId(id, ninStatus) {
    var query = {
        "buyer.accountId": new mongodb.ObjectId(id)
    };

    if (Array.isArray(ninStatus)) {
        query.status = { "$nin": ninStatus };
    }

    return Delivery.find(query).exec();
}

function updateStatus(id, carrierId, newStatus) {
    var deferred = q.defer();

    getByIdAndCarrier(id, carrierId)
        .then(function(doc) {
            doc.status = newStatus;
            doc.save();

            deferred.resolve(doc);
        }, deferred.reject);

    return deferred.promise;
}

function update(DeliveryModel) {
    var deferred = q.defer();
    DeliveryModel.save(function(err) {
        if (err) {
            return deferred.reject(err);
        }
        deferred.resolve(DeliveryModel);
    });

    return deferred.promise;
}

function getByDateAndStatus(startDate, statusList) {
    var query = {
        "pickUp.pickupDate": {
            "$gte": startDate,
        },
        "status": {
            "$in": statusList
        }
    };

    return Delivery.find(query).exec();
}
function getBuyerPurchases(userId, statusList) {
    var query = {
        "status": {
            "$in": statusList
        },
        "buyer.accountId": new mongodb.ObjectId(userId)
    };

    return Delivery.find(query).exec();
}
function getCarrierEarnings(userId, statusList, dateRange) {
    var query = {
        "carriers": {
            "$elemMatch": {
                "accountId": new mongodb.ObjectId(userId),
                "status": 7
            }
        },
        "status": {
            "$in": statusList
        }
    };

    if (dateRange.length > 0) {
        query["pickUp.pickupDate"] = {
            "$gte": dateRange[0].toDate(),
            "$lt": dateRange[1].toDate()
        };
    }

    var aggregate = [{
        $match: query
    }, {
        $group: {
            _id: null,
            total: { "$sum": "$price" },
            fees : { "$sum": "$priceDetails.fees" },
            count: { "$sum": 1 },
            deliveries: {
                "$push": "$$ROOT"
            }
        }
    }];

    return Delivery.aggregate(aggregate).exec();
}

function recoverDelivery(deliveryId) {
    var query = {
        "_id": new mongodb.ObjectId(deliveryId),
        "status": {
            "$in": [2, 8]
        },
        "pickUp.pickupDate": {
            "$gte": new Date()
        }
    };

    return Delivery.findOne(query).exec();
}

function updateGroupReference(delivery) {
    var carrierStatus = delivery.carriers[0].status;
    var deliveryStatus = delivery.status;

    return Delivery.find({groupReference: delivery.groupReference}).exec()
                .then(function(deliveries){
                    var promises = deliveries.map(function(ref){
                        ref.status = deliveryStatus;
                        ref.carriers[0].status = carrierStatus;
                        return ref.save();
                    });

                    return Promise.all(promises);
                })
                .then(function(deliveries){
                    var scheduledIds = deliveries.map(function(item){
                        return new mongodb.ObjectId(item.reference);
                    });

                    return scheduledDelivery.find({_id:{$in: scheduledIds}}).exec()
                            .then(function(scheduleds){
                                var promises = scheduleds.map(function(row){
                                    row.carrier = delivery.carriers[0];
                                    return row.save();
                                });

                                return Promise.all(promises);
                            });

                })
                .then(function(result){
                    return delivery;
                });
}

function updateScheduled(delivery) {
    if(!delivery.groupReference){
        return delivery;
    }
    let id =  new mongodb.ObjectId(delivery.reference);

    if (delivery.status && (parseInt(delivery.status) === 3)) {
        EventHistoryService.statusUpdatedScheduledDelivery(id, 3);
    }

    return scheduledDelivery.findOneAndUpdate(
        {_id: id},
        {
            carrier: delivery.carriers[0]
        }
    ).exec()
    .then(function(result){
        return delivery;
    });
}

function getRecent(userId) {
    var query = {
        "buyer.accountId": userId
    };

    return Delivery
            .find(query)
            .sort({"createdAt":-1})
            .limit(5)
            .exec()
}

module.exports = Repository;
