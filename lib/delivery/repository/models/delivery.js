var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var ProgressLog = new Schema({
    "fromStatus": {
        "type": Number,
    },
    "toStatus": {
        "type": Number,
    },
    "when": {
        "type": Date,
        "default": Date.now
    }
});

var Carrier = new Schema({
    "accountId": {
        "type": ObjectId,
        "required": true
    },
    "status": {
        "type": Number
    },
    "vehicle": {
        "id": { "type": ObjectId},
        "identifier": { "type": String },
        "type": { "type": String }
    },
    "price": {
        "type": Number
    },
    "payment": {
        "paymentId": {
            "type": ObjectId
        },
        "value": {
            "type": Number
        },
        "paymentDate": {
            "type": Date
        }
    },
    "progressLog": {
        "type": [ProgressLog]
    },
    "phone": {
        "type": String
    },
    "email": {
        "type": String
    },
    "name": {
        "type": String
    }
});


var Delivery = new Schema({
    "description": {
        "type": String
    },
    "status": {
        "type": Number,
        "required": true
    },
    "buyer": {
        "accountId": {
            "type": ObjectId,
        },
        "name": {
            "type": String
        },
        "phone": {
            "type": String
        },
        "email": {
            "type": String
        }
    },
    "pickUp": {
        "description": {
            "type": String
        },
        "location": {
            "type": [Number],
            "index": "2d",
            "required": true
        },
        "pickupDate": {
            "type": Date,
            "default": Date.now,
            "required": true
        },
        "when": {
            "type": String
        }
    },
    "dropOff": {
        "description": {
            "type": String
        },
        "location": {
            "type": [Number],
            "index": "2d",
        },
        "dropoffDate": {
            "type": Date
        }
    },
    "cargo": {
        "weight": {
            "type": Number
        },
        "size": {
            "type": Number
        }
    },
    "carriers": {
        "type": [Carrier]
    },
    "notes": {
        "type": String
    },
    "promocode": {
        "type": String
    },
    "options": {
        "type": [String]
    },
    "price": {
        "type": Number,
    },
    "extra": {
        "email": {
            "type": String
        },
        "refrigerated": {
            "type": Number
        },
        "menRequested": {
            "type": Number
        },
        "palletRequested": {
            "type": Number,
        },
    },
    "priceDetails": {
        "basePrice": {
            "type": Number
        },
        "discounts": {
            "type": Number
        },
        "optional": {
            "type": Number
        },
        "fees": {
            "type": Number,
        },
    },
    "distance": {
        "type": Number
    },
    "vehicleType": {
        "type": String,
    },
    "createdAt": {
        "type": Date,
        "default": Date.now
    },
    "deliveryType": {
        "type": Number
    },
    "reference": {
        "type": String,
    },
    "groupReference": {
        "type": String,
    },
});

module.exports = mongoose.model("Delivery", Delivery);


