var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Claims = new Schema({
    "ClaimType": {
        "type": Number
    },
    "location": {
        "type": String,
    },
    "message": {
        "type": String
    },
    "imgURLS": {
        "type": Array,
    },
    "notAtHome":{
        "type": Number
    },
    "notAnsweringCall":{
        "type": Number,
    },
    "deliveryId":{
        "type": ObjectId,
        "ref": 'scheduleddeliveries',
    },
    "image": {
        "type": String,
    },
    "created_at": {
        type: Date,
        default: new Date()
    },
    "userId": {
        "type": ObjectId,
        "ref": 'accounts',
    },
}, { collection: "claims"});

module.exports = mongoose.model("Claims", Claims);


