var mongoose = require('mongoose');
Schema = mongoose.Schema;
ObjectId = Schema.ObjectId;

var Alert = new Schema({
    "location": [{
        "readable":String,
        "geo":Array
    }],
    "cargotype": {
        "type": String
    },
    "creator": ObjectId,
    "created": {
        "type": Date ,
        "default": Date.now
    },
    "when": {},
    "time": String,
    "expire":{
        "type":Date
    }
});

module.exports = mongoose.model('Alert', Alert);
