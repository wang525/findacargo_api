var DeliveryState = require("./delivery-state");
var Repository= require("./repository/delivery");
var Formatter = require("./formatter/delivery");
var Cancelation = require("./cancelation");
var Scheduler = require("./scheduler");
var SchedulerEvents = require("./events/scheduler");
var CarrierActions = require("./carrier-actions");
var CarrierStateEngine = require("./carrier-state-engine");
var CarrierStateEvents = require("./events/carrier-state");
var CostCalculator = require("./calculator").Cost;
var TimeCalculator = require("./calculator").Time;
var DeliveryPayment = require("./pay");
var Payment = require("../payment");
var getUser = require("../auth/user").getUser;
var DeliveryExporter = require("./exporter");
var DeliveryExporterEvents = require("./events/exporter");
var TransRequestFactory = require("./factory/trans-request");
var NoVehicleAvailableError = require("../carrier/repository/error/no-vehicle-available");
var MyRequests = require("./my-requests");
var MonthlyInvoice = require("./monthly-invoice");
var Opportunities = require('./opportunities');
var CarrierEarnings= require('./carrier-earnings');
var Purchases = require('./purchases');
var DeliveryRecoveryActions = require('./recovery-actions');
var Claims = require('./claims');

var exporter = new DeliveryExporter({
    factory: new TransRequestFactory(),
    repository: require("./repository/trans-request"),
});

var exporterEvents = new DeliveryExporterEvents({
    exporter: exporter,
    transRequestRepository: require("./repository/trans-request"),
    accountRepository: require("../user/repository/account"),
    TransRequestAlertNotification: require("../notification").TransRequestAlertNotification,
    TransRequestAdminNotification: require("../notification").TransRequestAdminNotification,
    getType: require("../carrier/enum/vehicle-types").getType
});

var schedulerOptions = {
    carrierInventary : {},
    deliveryFactory: require("./factory/delivery"),
    deliveryRepository: Repository,
    formatter: Formatter,
    getUser: require("../auth/user").getUser,
    costCalculator: CostCalculator,
    paymentReverser: Payment.Reverser,
    Invited: require("./carrier-state").Invited,
    DeliveryState: DeliveryState,
    exporter: exporter,
    accountRepository: require("../user").accountRepository,
    NoVehicleAvailableError: NoVehicleAvailableError
};

var deliveryScheduler = new Scheduler(schedulerOptions);
var deliverySchedulerEvents = new SchedulerEvents(deliveryScheduler);

var carrierStateEngine = new CarrierStateEngine(deliveryScheduler);
var carrierStateEvents = new CarrierStateEvents(carrierStateEngine, Payment.Capturer, deliveryScheduler);
schedulerOptions.carrierStateEngine = carrierStateEngine;

var deliveryPayment = new DeliveryPayment({
    deliveryFormatter: Formatter,
    deliveryRepository: Repository,
    getUser: getUser,
    scheduler: deliveryScheduler,
    deliveryState: DeliveryState,
    paymentAuthorizer: Payment.Authorizer
});

var MonthlyInvoiceOptions = {
    getUser: require("../auth/user").getUser,
    deliveryRepository: Repository,
    deliveryFormatter: Formatter,
    scheduler: deliveryScheduler,
    deliveryState: DeliveryState,
}

module.exports = {
    Repository: Repository,
    DeliveryState: DeliveryState,
    Formatter: Formatter,
    Cancelation: new Cancelation(deliveryScheduler),
    Scheduler: deliveryScheduler,
    DeliveryCarrierActions: new CarrierActions(carrierStateEngine),
    CarrierStateEngine: carrierStateEngine,
    CostCalculator: CostCalculator,
    TimeCalculator: TimeCalculator,
    DeliveryPayment: deliveryPayment,
    MyRequests: new MyRequests({
        deliveryRepository: Repository,
        transRequestRepository: require("./repository/trans-request"),
        formatter: Formatter,
        getUser: require("../auth/user").getUser,
        getType: require("../carrier/enum/vehicle-types").getType,
        getStatus: require("./state/enum").getDescription
    }),
    MonthlyInvoice: new MonthlyInvoice(MonthlyInvoiceOptions),
    Opportunities: new Opportunities({
        deliveryRepository: Repository,
        deliveryFormatter: Formatter
    }),
    Purchases: new Purchases({
        deliveryRepository: Repository,
        deliveryFormatter: Formatter,
        getUser: require("../auth/user").getUser
    }),
    CarrierEarnings: new CarrierEarnings({
        deliveryRepository: Repository,
        deliveryFormatter: Formatter,
        getUser: require("../auth/user").getUser
    }),
    DeliveryRecoveryActions: new DeliveryRecoveryActions({
        vehicleRepository: require("../carrier/repository/vehicle"),
        deliveryRepository: require("./repository/delivery"),
        carrierStateEngine: carrierStateEngine,
        getUser: require("../auth/user").getUser,
        deliveryFormatter: Formatter,
        scheduler: deliveryScheduler,
        vehicleFormatter: require("../carrier/formatter/vehicle")
    }),
    Claims: new Claims({
        deliveryRepository: require("./repository/delivery"),
        getUser: require("../auth/user").getUser,
    })
};
