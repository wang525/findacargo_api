var moment = require("moment");
var States = require("./state/enum");
var di = {};

function Opportunities(depend){
    di = depend;
    var that = this;

    this.list = list;

    function list() {
        var data = {};

        return getLiveDeliveries()
            .then(di.deliveryFormatter.list);

        function getLiveDeliveries(){
            var startDate = new Date();
            return di.deliveryRepository.getByDateAndStatus(startDate, [States.CarrierPending.id, States.Moved.id]);
        }

    }
}

module.exports = Opportunities;
