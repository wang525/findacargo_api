var StateEnum = require("../state/enum");
var getType = require("../../carrier/enum/vehicle-types").getType;

var Formatter = {
    deliveryStatus: deliveryStatus,
    deliveryAndCarrierStatus: deliveryAndCarrierStatus,
    deliveryDetailsAsCarrier: deliveryDetailsAsCarrier,
    deliveryDetails: deliveryDetails,
    list: list,
    report: report
}

function deliveryStatus(data) {
    return {
        "delivery_id": data._id,
        "status": data.status
    }
}

function deliveryAndCarrierStatus(data, carrier) {
    return {
        "delivery_id": data._id,
        "status": data.status,
        "carrier_status": carrier.status
    }
}

function deliveryDetailsAsCarrier(data) {
    return {  
        "delivery_id": data._id,
        "status": {
            "description": StateEnum.getDescription(data.status),
            "id": data.status 
        },
        "created_at": data.createdAt,
        "options": data.options,
        "cargo": data.cargo,
        "carriers": formatCarriers(data.carriers),
        "pickup_location":{  
            "description": data.pickUp.description,
            "latitude": data.pickUp.location[1],
            "longitude": data.pickUp.location[0]
        },
        "dropoff_location":{  
            "description": data.dropOff.description,
            "latitude": data.dropOff.location[1],
            "longitude":data.dropOff.location[0] 
        },
        "pickup_date": data.pickUp.pickUpDate,
        "buyer": {
            "account_id": data.buyer.accountId,
            "name": data.buyer.name,
            "phone": data.buyer.phone,
            "email": data.buyer.email
        }
    }
}

function deliveryDetails(data, user) {
     var out = {  
        "delivery_id": data._id,
        "status": {
            "description": StateEnum.getDescription(data.status),
            "id": data.status 
        },
        "created_at": data.createdAt,
        "options": data.options,
        "cargo": data.cargo,
        "carriers": formatCarriers(data.carriers),
        "pickup_location":{  
            "description": "",
            "latitude": data.pickUp.location[1],
            "longitude": data.pickUp.location[0]
        },
        "dropoff_location":{  
            "description": "",
            "latitude": data.dropOff.location[1],
            "longitude":data.dropOff.location[0] 
        },
        "price": data.price,
        "price_details": {
            "base_price": data.priceDetails.basePrice,
            "discounts": data.priceDetails.discounts,
            "optional": data.priceDetails.optional
        },
        "pickup_date": data.pickUp.pickupDate,
        "buyer": {
            "account_id": data.buyer.accountId,
            "name": data.buyer.name,
            "phone": data.buyer.phone,
            "email": data.buyer.email
        }
    };

    if(user){
        out.monthlyInvoiceAllowed = (user.monthlyInvoice == "1")
    }

    return out;
}

function formatCarriers(carriers) {
    var out = [];

    if(carriers.length === 0){
        return out;
    }

    for(var i in carriers) {
        var item = {
            "account_id": carriers[i].accountId,
            "status": {
                "description": "",
                "id": carriers[i].status 
            },
            "price": carriers[i].price,
            "vehicle":{  
                "id": carriers[i].vehicle.id,
                "identifier": carriers[i].vehicle.identifier,
                "type": carriers[i].vehicle.type,
            },
            "phone": carriers[i].phone,
            "email": carriers[i].email,
            "name": carriers[i].name,
        }

        out.push(item);
    }

    return out;
}

function list(deliveries) {
    var out = [];

    if(deliveries.length === 0){
        return out;
    }

    for (var i in deliveries) {
        out.push(new FormattedItem(deliveries[i]));
    }

    return out;

    function FormattedItem(delivery){
        this.id = delivery._id.toString();
        this.type = delivery.type;
        this.pickup = delivery.pickUp.description;
        this.destination = delivery.dropOff.description;
        this.price = delivery.price;
        this.date = delivery.pickUp.pickupDate;
        this.status = { 
            "description": StateEnum.getDescription(delivery.status),
            "id": delivery.status 
        }
        this.vehicleType = { 
            "description": getType(delivery.vehicleType),
            "id": delivery.vehicleType
        };
        this.buyer = delivery.buyer;
    }
}

function report(data) {
    var out = {
        date : (data.date)?data.date:null,
        amount : data.amount,
        total: data.total,
        fees: data.fees,
        count : data.count,
        deliveries : list(data.deliveries),
        currency: "dkk"
    };

    return out;
}

module.exports = Formatter;
