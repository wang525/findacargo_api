var States = require("./enum");
var BaseState = require("./base-state");
var CarrierPending = require("./carrier-pending");
var Moved = require("./moved");

function PaymentAuthorized(Delivery, setState) {
    BaseState.call(this, Delivery, setState, States.PaymentAuthorized);

    this.setCarrierPending = setCarrierPending;
    this.setMoved = setMoved;

    function setCarrierPending() {
        var Delivery = this.Delivery;
        var setState = this.setState;

        var newState = new CarrierPending(Delivery, setState);
        setState(newState);
        return newState.apply();
    }

    function setMoved(){
        var Delivery = this.Delivery;
        var setState = this.setState;

        var newState = new Moved(Delivery, setState);
        setState(newState);
        return newState.apply();
    }
}

PaymentAuthorized.prototype = Object.create(BaseState.prototype);
PaymentAuthorized.prototype.constructor = PaymentAuthorized;

module.exports = PaymentAuthorized;
