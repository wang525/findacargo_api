var States = require("./enum");
var deliveryRepository = require("../repository/delivery");

var options = {
    currentState: "Base",
    Delivery: null,
    setState: function(){}
}

function BaseState(Delivery, setState, current){
    options.Delivery = Delivery;
    options.setState = setState;
    options.currentState = current;
    this.Delivery = Delivery;
    this.setState = setState;

    this.setPaymentPending = setPaymentPending;
    this.setPaymentAuthorized = setPaymentAuthorized;
    this.setCarrierPending = setCarrierPending;
    this.setCarrierApproved = setCarrierApproved;
    this.setCanceled = setCanceled;
    this.setInProgress = setInProgress;
    this.setFinished = setFinished;
    this.setMoved = setMoved;
    this.state = current;
    this.apply = apply;

    function setPaymentPending() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Payment Pending";
    }

    function setPaymentAuthorized() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Payment Authorized";
    }

    function setCarrierPending() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier Pending";
    }

    function setCarrierApproved() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Carrier Approved";
    }

    function setInProgress() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Delivery In Progress";
    }

    function setFinished() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Finished";
    }

    function setMoved() {
        throw "Can't set a Delivery with Status: "+options.currentState.desc+" to Moved";
    }

    function setCanceled(){
        var Delivery = this.Delivery;
        var setState = this.setState;
        var newState = new BaseState(Delivery, setState, States.Canceled);
        setState(newState);
        return newState.apply();
    };

    function apply() {
        this.Delivery.status = this.state.id;
        return deliveryRepository.update(this.Delivery);
    }
}

module.exports = BaseState;
