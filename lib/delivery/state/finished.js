var States = require("./enum");
var BaseState = require("./base-state");

function Finished(Delivery, setState) {
    BaseState.call(this, Delivery, setState, States.Finished);

    function setCanceled() {
        throw "You can't cancel a finished delivery!"
    }
}

Finished.prototype = Object.create(BaseState.prototype);
Finished.prototype.constructor = Finished;

module.exports = Finished;
