var States = require("./enum");
var BaseState = require("./base-state");
var Finished = require("./finished");

function InProgress(Delivery, setState) {
    BaseState.call(this, Delivery, setState, States.InProgress);

    this.setFinished = setFinished;

    function setFinished() {
        var Delivery = this.Delivery;
        var setState = this.setState;

        var newState = new Finished(Delivery, setState);
        setState(newState);
        return newState.apply();
    }

    function setCanceled() {
        throw "You can't cancel a Delivery in progress!"
    }
}

InProgress.prototype = Object.create(BaseState.prototype);
InProgress.prototype.constructor = InProgress;

module.exports = InProgress;
