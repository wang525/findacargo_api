var StateEnum = {
    PaymentPending: state(1, "Payment Pending"),
    PaymentAuthorized: state(2, "Payment Authorized"),
    CarrierPending: state(3, "Carrier Pending"),
    CarrierApproved: state(4, "Carrier Approved"),
    Canceled: state(5, "Canceled"),
    InProgress: state(6, "In Progress"),
    Finished: state(7, "Finished"),
    Moved: state(8, "Moved"),
    getDescription: getDescription
}

function state(id, desc){
    return {
        id: id,
        desc: desc
    };
}

function getDescription(id) {
    var keys = Object.keys(StateEnum)
    var lookup = [];

    for(var i in keys){
        if(id == StateEnum[keys[i]].id){
            return StateEnum[keys[i]].desc;
        }
    }
}

module.exports = StateEnum;
