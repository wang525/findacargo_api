var States = require("./enum");
var BaseState = require("./base-state");
var CarrierApproved = require("./carrier-approved");
var Moved = require("./moved");

function CarrierPending(Delivery, setState) {
    BaseState.call(this, Delivery, setState, States.CarrierPending);

    this.setCarrierApproved = setCarrierApproved;
    this.setMoved = setMoved;

    function setCarrierApproved() {
        var Delivery = this.Delivery;
        var setState = this.setState;

        var newState = new CarrierApproved(Delivery, setState);
        setState(newState);
        return newState.apply();
    }

    function setMoved(){
        var Delivery = this.Delivery;
        var setState = this.setState;

        var newState = new Moved(Delivery, setState);
        setState(newState);
        return newState.apply();
    }
}

CarrierPending.prototype = Object.create(BaseState.prototype);
CarrierPending.prototype.constructor = CarrierPending;

module.exports = CarrierPending;
