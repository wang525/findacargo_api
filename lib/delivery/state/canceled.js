var States = require("./enum");
var BaseState = require("./base-state");

function Canceled(Delivery, setState) {
    BaseState.call(this, Delivery, setState, States.Canceled);
}

Canceled.prototype = Object.create(BaseState.prototype);
Canceled.prototype.constructor = Canceled;

module.exports = Canceled;
