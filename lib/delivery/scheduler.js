var util = require("util");
var EventEmitter = require("events").EventEmitter;
var Mixpanel = require('mixpanel/lib/mixpanel-node');

var mixpanel = Mixpanel.init('35b08bf5d7f756c6462e052b48eb22df');

var di = {
    deliveryFactory: {},
    deliveryRepository: {},
    formatter: {},
    getUser: function() {},
    costCalculator: {}
};

var data = {
    requestData: {},
    deliveryModel: {},
    vehicle: {}
};

function Scheduler(options) {
    EventEmitter.call(this);

    di = options;
    var that = this;
    this.createDelivery = createDelivery;
    this.bidVehicle = bidVehicle;
    this.cancelPayment = cancelPayment;
    this.validateInvite = validateInvite;
    this.assignCarrierForRecovery = assignCarrierForRecovery;

    function createDelivery(requestData) {
        return createEntry(requestData)
            .then(function(deliveryModel){
                var user = di.getUser();
                return di.formatter.deliveryDetails(deliveryModel, user);
            });
    }

    function createEntry(requestData) {
        var carrierInventary = require("../carrier").Inventary;
        var user = di.getUser();

        data.requestData = requestData;

        return carrierInventary.getTypeRateCard(requestData.vehicle_type, requestData.pickup_location)
            .then(function(rateCard) {
                if (rateCard) {
                    di.costCalculator.setRateCard(rateCard)
                }

                return di.costCalculator.create(requestData.vehicle_type, requestData.pickup_location, requestData.dropoff_location, requestData.options, requestData.pickup_options, user.fixedDiscount);
            })
            .then(function(calculatedValues) {
                var deliveryData = di.deliveryFactory.createFromRequestDelivery(requestData, user, calculatedValues);

                return di.deliveryRepository.create(deliveryData);
                //.then(validateScheduled);
            });
    }

    function validateScheduled(delivery) {
        var carrierInventary = require("../carrier").Inventary;
        if (delivery.pickUp.when != "now") {
            return exportToDashboard(delivery)
                .then(function() {
                    return delivery;
                });
        }

        return carrierInventary.getAvailability(delivery)
            .then(function(isAvailable) {
                if (isAvailable) {
                    return delivery;
                }

                return exportToDashboard(delivery);
            });
    }

    function exportToDashboard(delivery) {
        var state = di.DeliveryState.load(delivery, that);
        return di.exporter.toDashboard(delivery)
            .then(function() {
                state.setMoved();
                return deliveryMovedEvent()
            })
    }

    function bidVehicle(deliveryModel) {
        data.deliveryModel = deliveryModel;
        var carrierInventary = require("../carrier").Inventary;
        return carrierInventary.bidVehicle(deliveryModel)
            .then(assignCarrier)
            .catch(function(err) {
                return exportToDashboard(deliveryModel);
            });
        //.catch(cancelDelivery);
    }

    function cancelPayment(delivery) {
        return di.paymentReverser.cancel(delivery);
    }

    function validateInvite(delivery, vehicleData) {
        var user = di.getUser();
        return di.deliveryRepository.getById(delivery._id.toString(), user.id)
            .then(function(delivery) {
                di.carrierStateEngine.load(delivery, vehicleData.carrier_id.toString());
                return di.carrierStateEngine.setRejected()
                    .then(bidVehicle);

            }).catch(function(err) {
                console.error(err);
            });
    }

    function formatOutput(deliveryModel) {
        return di.formatter.deliveryDetails(deliveryModel);
    }

    function assignCarrier(vehicle) {
        di.accountRepository = require("../user").accountRepository;
        return di.accountRepository.getById(vehicle.carrier_id)
            .then(function(account) {
                vehicle.price = data.deliveryModel.price;
                data.vehicle = vehicle;
                data.deliveryModel.carriers.push(di.deliveryFactory.createCarrier(vehicle, account));

                if (data.deliveryModel.carriers.length > 5) {
                    throw "5 Carriers limit";
                }

                return di.deliveryRepository.update(data.deliveryModel)
            })
            .then(setCarrierPendingState)
            .then(carrierAssignedEvent);
            //.catch(cancelDelivery);
    }

    function assignCarrierForRecovery(delivery, vehicle) {
        return di.accountRepository.getById(vehicle.carrier_id)
            .then(function(account) {
                vehicle.price = delivery.price;
                data.vehicle = vehicle;
                if (!Array.isArray(delivery.carriers)) {
                    delivery.carriers = [];
                }
                delivery.carriers.push(di.deliveryFactory.createCarrier(vehicle, account));

                return di.deliveryRepository.update(delivery)
            })
            .then(setCarrierPendingState);
    }

    function setCarrierPendingState(deliveryModel) {
        if (deliveryModel.status == 3) {
            return deliveryModel;
        }

        var state = di.DeliveryState.load(deliveryModel, that);
        return state.setCarrierPending();
    }

    function cancelDelivery(err) {
        if (data.deliveryModel.status == 5) {
            return data.deliveryModel;
        }

        var state = di.DeliveryState.load(data.deliveryModel, that);
        state.setCanceled();
        return deliveryCanceledEvent();
    }
    function deliveryMovedEvent(err) {
        that.emit("delivery-moved", data.deliveryModel);
        return data.deliveryModel;
    }

    function deliveryCanceledEvent(err) {
        that.emit("delivery-canceled", data.deliveryModel);
        return data.deliveryModel;
    }

    function carrierAssignedEvent(deliveryModel) {
        try {
            that.emit("carrier-assigned", data.deliveryModel, data.vehicle);
        } catch (err) {
            console.error(err);
        }

        return deliveryModel;
    }
}

util.inherits(Scheduler, EventEmitter);

module.exports = Scheduler;
