var di = {};
var mongoose = require("mongoose");
var Promise = require("bluebird");
mongoose.Promise = require("bluebird");

var ClaimsSchema = require("./repository/models/claims"),
    ClaimsModel = mongoose.models.Claims;
var AccountRepository = require("../user/repository/account");
var ScheduledRepository = require('../scheduled/repository/scheduled')
var DeliverySettingsRepository = require('../user/repository/deliverysettings.js')

var SocketService = require('../../services/SocketService');
var request = require('request')
let nodeGeoCoder = require('node-geocoder');
const geoCoderKey = 'AIzaSyBmP1A3mxSdf5EH3zI0PIsDa2kUXiF2eao';
const coderOptions = {
    provider: 'google',
    httpAdapter: 'https',
    apiKey: geoCoderKey,
    formatter: null
};

const GeoCoder = nodeGeoCoder(coderOptions);

function Claims(depend) {
    di = depend;
    var that = this;

    this.create = create;
    this.getClaim = getClaim;
    function create(deliveryId, data) {
        var user = di.getUser();
        var delivery = {}
        return save(deliveryId)
            //.then(save)
            .then(function (claim) {
                //emit socket event
                var socket = SocketService.getSocket();
                if (socket) {
                    socket.emit(`delivery_status_changed_${deliveryId}`, {'status':'claim'});
                }

                ScheduledRepository.getById(deliveryId)
                .then(ret=>{
                    delivery = ret
                    return AccountRepository.findOne(delivery.creator)
                })
                .then(profile => {
                    var groupId = profile._id
                    if(profile.groupId) {
                        groupId = profile.groupId.toObjectId()
                    }

                    DeliverySettingsRepository.getByClientId(groupId).then((setting)=>{
                        if(setting && setting.allow_to_receive_issue_report) {
                            AccountRepository.findAll({
                                $or: [
                                    { 'groupId': groupId },
                                    { '_id': groupId }
                                ]
                            })
                            .then(coworkers=>{
                                var users = coworkers.map((user)=>{
                                    return {
                                        name:user.name,
                                        email:user.email
                                    }
                                })
                                var body = Object.assign({}, claim._doc, {users}, {
                                    driver:{
                                        name : user.name,
                                        email: user.email,
                                        phone: user.phone
                                    },
                                    deliveryExternalId:delivery.deliveryid,
                                    deliveryId : delivery._id
                                })

                                if(claim._doc.location) {
                                    GeoCoder.geocode(claim._doc.location).then((ret)=>{
                                        //console.log("geocode result", delivery.pickup_location.address_1, ret)
                                        if(ret && ret[0].latitude) {
                                            body.latitude = ret[0].latitude
                                            body.longitude = ret[0].longitude   

                                            request.post(process.env.NOTIFY_URL + 'order/claim', { json:  body}, function (err, resp, bd) {
                                                if (err) console.error(err)
                                                //console.log(bd);
                                            });
                                        }            
                                    })
                                } 
                            }).catch(err => {
                                console.log('error', err)
                            })
                        }
                    })
                })

                return {
                    message: "Report posted successfully."
                }
            });

        function getDelivery(deliveryId) {
            return ScheduledRepository.getById(deliveryId);
        }

        function save(deliveryId) {
            //console.log('data', data)
            var model = new ClaimsModel({
                ClaimType: data.ClaimType,
                image: data.image,
                imgURLS: data.imgURLS,
                location: data.location,
                message: data.message,
                deliveryId: deliveryId,
                message: data.message,
                notAnsweringCall: data.notAnsweringCall,
                notAtHome: data.notAtHome,
                userId: user.id,
                created_at: new Date()
            });

            return model.save(function (err, doc) {
                if(err) {
                    console.error(err, doc)
                }
            });
        }
    }

    function getClaim(deliveryId) {
        return ClaimsModel.find({
            deliveryId: deliveryId
        })
    }
}

module.exports = Claims;
