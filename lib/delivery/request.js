var q = require("q");
var deliveryFactory = require("./factory/delivery");
var carrierInventary = require("../carrier/inventary");
var deliveryRepository = require("./repository/delivery");
var accountRepository = require("../user/repository/account");
var formatter = require("./formatter/delivery");
var pushFormatter = require("./formatter/push");
var getUser = require("../auth/user").getUser;

var RequestDelivery = {
    getDetails: getDetails,
    getDetailsAsCarrier: getDetailsAsCarrier,
    getStatus: getStatus,
}

function getStatus(id) {
    var User = getUser();

    return deliveryRepository
        .getById(id, User.id)
        .then(function(delivery){
            var carrier = verifyCarrierAccepted(delivery);

            if(!carrier) {
                return pushFormatter.carrierAcceptedWithStatus(delivery);
            }

            return accountRepository.getVehicleById(carrier.vehicle.id)
                    .then(function(vehicleData){
                        return pushFormatter.carrierAcceptedWithStatus(delivery, carrier, vehicleData.vehicle, vehicleData.driver);
                    });
        });
}

function verifyCarrierAccepted(delivery) {
    for(var i in delivery.carriers) {
        if(delivery.carriers[i].status === 2) {
            return delivery.carriers[i];
        }
    }

    return false;
}

function getDetailsAsCarrier(id) {
    var deferred = q.defer();
    var User = getUser();

    deliveryRepository
        .getByIdAndCarrier(id, User.id)
        .then(formatter.deliveryDetailsAsCarrier)
        .then(deferred.resolve, deferred.reject);

    return deferred.promise;
}

function getDetails(id) {
    var deferred = q.defer();
    var User = getUser();

    deliveryRepository
        .getById(id, User.id)
        .then(formatter.deliveryDetails)
        .then(deferred.resolve, deferred.reject);

    return deferred.promise;
}

module.exports = RequestDelivery;
