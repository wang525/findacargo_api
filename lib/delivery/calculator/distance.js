var geolib = require("geolib");

function DistanceCalculator() {
    this.calculate = calculate;

    function calculate(pickup, dropoff) {
        if(!validateLocation(pickup)) {
            throw "Invalid pickup location!"
        }

        if(!validateLocation(dropoff)) {
            throw "Invalid dropoff location!"
        }

        return geolib.getDistance(pickup, dropoff);
    }

    function validateLocation(location) {
        var keys = Object.keys(location);

        if(keys.indexOf("latitude") < 0 || keys.indexOf("longitude") < 0) {
            return false;
        }

        return true;
    }
}

module.exports = DistanceCalculator;
