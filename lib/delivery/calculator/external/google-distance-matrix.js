var Promise = require("bluebird");
var distance = require("google-distance-matrix");
var config = require("../../../config").googlemaps;

distance.key(config.key);

function GMapsDistanceCalculator(){
    var that = this;

    this.calculate = calculate;

    function calculate(pickup, dropoff) {
        return new Promise(function(resolve, reject){
            var origin = [pickup.latitude+","+pickup.longitude];
            var destination = [dropoff.latitude+","+dropoff.longitude];
            distance.matrix(origin, destination, function(err, distances){
                if(err){
                    reject(err);
                }

                if(!distances){
                    reject("Can't calculate distance");
                }

                if(distances.status == 'OK') {
                    if(distances.rows[0].elements[0].status == 'OK') {
                        return resolve(distances.rows[0].elements[0].distance.value);
                    }
                }

                reject("Dropoff location is not reachable by land from pickup location");
            });
        });
    }
}

module.exports = GMapsDistanceCalculator;


