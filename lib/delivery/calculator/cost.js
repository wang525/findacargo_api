var moment = require("moment");

function CostCalculator(distanceCalculator, timeCalculator, rateCard, GMapsDistance) {
    var that = this;

    this.distanceCalculator = distanceCalculator;
    this.timeCalculator = timeCalculator;
    this.rateCard = rateCard;
    this.gmapsDistance = GMapsDistance;
    this.distance = 0;
    this.time = 0;
    this.rate = {};

    this.estimate = estimate;
    this.create = create;
    this.setRateCard = setRateCard;
    this.getRateCard = getRateCard;

    function setRateCard(rateCard) {
        if(!rateCard.getByType){
            this.rateCard = {
                getByType: function(id){
                    return rateCard[id];
                }
            };
        }else{
            this.rateCard = rateCard;
        }
    }

    function getRateCard() {
        return this.rateCard;
    }

    function estimate(vehicleType, pickUpLocation, dropOffLocation, distance){
        try {
            return calculeBasePrice(vehicleType, pickUpLocation, dropOffLocation, distance);
        } catch (err) {
            return 0;
        }
    }

    function create(vehicleType, pickUpLocation, dropOffLocation, extra, pickupOptions, fixedPercent){
        return that.gmapsDistance.calculate(pickUpLocation, dropOffLocation)
                .then(calculatePrices)

        function calculatePrices(distance){
            var basePrice = calculeBasePrice(vehicleType, pickUpLocation, dropOffLocation, distance);
            var optional = calculateOptional(vehicleType, extra);
            var discounts = calculateDiscounts(basePrice, optional, pickupOptions, fixedPercent);
            var price = Math.floor((basePrice + optional - discounts)*100)/100;
            var fees = Math.floor((price*15/100)*100)/100;

            return {
                basePrice: basePrice,
                optional: optional,
                discounts: discounts,
                price: price,
                distance: distance/1000,
                fees: fees
            };
        }
    }

    function calculeBasePrice(type, pickup, dropoff, distance) {
        if(!distance){
            distance = that.distanceCalculator.calculate(pickup, dropoff);
        }

        var rate  = that.rateCard.getByType(type);
        var time = that.timeCalculator.calculate(distance, true, rate.velocity);
        var price = 0;

        if(distance > 0) {
            price = ((distance/1000)*rate.costPerKm) + rate.base;
        }

        that.distance = distance;
        that.time = time;
        that.rate = rate;
        return Math.floor(price*100)/100;
    }

    function calculateOptional(type, extra) {
        var optional = 0;
        var rate  = that.rateCard.getByType(type);

        if(typeof extra === "object"){
            if(extra.length > 0){
                optional = rate.extra*that.time*extra.length;
            }
        }

        return Math.floor(optional*100)/100;
    }

    function calculateDiscounts(basePrice, optional, options, fixedPercent) {
        var discount = 0;

        if(options.when !== "now"){
            var date = moment(options.date+" "+options.time);

            discount = 0.1;
            if(date > moment().add(4, "hours")){
                discount = 0.2;
            }
        }

        if(fixedPercent > 0 && fixedPercent < 80) {
            discount += (fixedPercent/100);
        }

        return Math.floor((basePrice+optional)*discount*100)/100;
    }
}

module.exports = CostCalculator;
