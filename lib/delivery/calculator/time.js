function TimeCalculator() {
    this.calculate = calculate;

    function calculate(distance, hasVehicles, customVelocity) {
        var velocity = 650; // meter/min

        if(!hasVehicles) {
            return 0;
        }

        if(!isNaN(customVelocity)){
            velocity = customVelocity;
        }

        var time =  Math.ceil(distance/velocity);

        return (time>3)? time: 3;
    }
}

module.exports = TimeCalculator;
