var di = {};

function MyRequests(depend){
    di = depend;
    var that = this;

    this.list = list;

    function list() {
        var user = di.getUser();
        var data = {};

        return getLiveDeliveries()
            .then(getScheduledDeliveries)
            .then(joinResults);

        function getLiveDeliveries(){
            return di.deliveryRepository.getByUserId(user.id, [1,8])
                    .then(function(liveDeliveries){
                        data.liveDeliveries = liveDeliveries;
                    });
        }

        function getScheduledDeliveries(){
            return di.transRequestRepository.getByUserId(user.id)
                    .then(function(scheduledDeliveries){
                        data.scheduledDeliveries = scheduledDeliveries;
                    });
        }

        function joinResults(){
            var result = [];

            for(var i in data.liveDeliveries) {
                var item = data.liveDeliveries[i];
                var type = di.getType(parseInt(item.vehicleType));
                var vehicle = {
                    id: (type)?type.typeId:null,
                    description: (type)?type.description:null
                };
                result.push(new Delivery(item._id, 'live', item.pickUp.description, item.dropOff.description, item.price, item.pickUp.pickupDate, {id:item.status, description:di.getStatus(item.status)}, vehicle));
            }

            for(var i in data.scheduledDeliveries) {
                var item = data.scheduledDeliveries[i];
                var vehicle = {
                    id:null,
                    description:null
                }
                result.push(new Delivery(item._id, 'scheduled', item.pickuplocreadable, item.destinationreadable, item.budget, item.pickupdate, {}, vehicle));
            }
 
            return result;
        }

        function Delivery(id, type, pickup, destination, price, date, status, vehicle){
            this.id = id;
            this.type = type;
            this.pickup = pickup;
            this.destination = destination;
            this.price = price;
            this.date = date;
            this.status = status;
            this.vehicleType = vehicle;
        }
    }
}

module.exports = MyRequests;
