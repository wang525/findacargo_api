var di = {};
var options = {};

function DeliveryPayment(depend) {
    di = depend;
    var that = this;

    this.pay = pay;

    function pay(deliveryId, parameters) {
        options.parameters = parameters;

        return getDelivery(deliveryId)
            .then(requestPaymentAuthorization)
            .then(updateDeliveryStatus)
            .then(formatOutput);
    }

    function getDelivery(deliveryId) {
        var user = di.getUser();

        return di.deliveryRepository.getById(deliveryId, user.id);
    }

    function requestPaymentAuthorization(delivery) {
        return di.paymentAuthorizer.authorize(delivery, options.parameters);
    }

    function updateDeliveryStatus(delivery) {
        var state = di.deliveryState.load(delivery, di.scheduler);

        return state.setPaymentAuthorized();
    }

    function formatOutput(delivery) {
        return di.deliveryFormatter.deliveryStatus(delivery)
    }

}

module.exports = DeliveryPayment;
