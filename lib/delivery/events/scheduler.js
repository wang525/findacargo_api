var adminNotifier = require('../../notification').AdminNotifier;
var CarrierInvitedNotification = require('../../notification').CarrierInvitedNotification;
var DeliveryCanceledNotification = require('../../notification').DeliveryCanceledNotification;
var DeliveryStatusNotification = require('../../notification').DeliveryStatusNotification;
var deliveryState = require('../delivery-state');
var pushFormatter = require('../formatter/push');
var stateHandler = require('../delivery-state');
var accountRepository = require('../../user/repository/account');
var getUser = require('../../auth/user').getUser;

var schedulerEmitter = {}

function SchedulerEvents(scheduler){
    schedulerEmitter = scheduler;

    scheduler.on("delivery-canceled", cancelDeliveryNotification);
    scheduler.on("delivery-moved", deliveryStatusNotification);
    scheduler.on("carrier-assigned", notifyCarrierAssigned);
    scheduler.on("carrier-assigned", startInviteTimer);
    scheduler.on("future-delivery-created", notifyAdminFutureDeliveryCreated);
}

function cancelDeliveryNotification(delivery) {
    var carrier = false;

    for(var i in delivery.carriers) {
        if(delivery.carriers[i].status <= 2) {
            carrier =  delivery.carriers[i];
        }
    }

    if(!carrier){
        return;
    }

    var notification = new DeliveryCanceledNotification(carrier.accountId.toString());
    var data = {
        "_id": delivery._id.toString(),
        "reason": "canceled by user"
    }

    return notification.send(data);
}
function deliveryStatusNotification(delivery) {
    var notification = new DeliveryStatusNotification(delivery.buyer.accountId.toString());

    return notification.send(pushFormatter.deliveryStatus(delivery));
}
function notifyAdminFutureDeliveryCreated(deliveryModel) {
    return adminNotifier.deliveryScheduledAlert(deliveryModel);
}

function notifyCarrierAssigned(deliveryModel, vehicleData) {
    try{
        var user = getUser();
        return accountRepository.getVehicleById(vehicleData.id)
                .then(function(vehicle){
                    var notification = new CarrierInvitedNotification({_id:vehicleData.carrier_id});
                    return notification.send(pushFormatter.carrierInvited(deliveryModel, vehicleData, vehicle, user));
                });

    }catch(err){
        console.error(err);
    }
}

function startInviteTimer(delivery, vehicleData){
    setTimeout(function(){
        schedulerEmitter.validateInvite(delivery, vehicleData);
    }, 60000)
}

module.exports = SchedulerEvents;
