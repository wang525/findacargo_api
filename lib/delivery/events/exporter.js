var stateEngineEmitter = {}
var di = {};

function ExporterEvents(depend){
    di = depend;

    di.exporter.on("delivery-exported", notifyAdmin);
    di.exporter.on("delivery-exported", verifyAlerts);
}

function notifyAdmin(transRequest, delivery) {
    var notification = new di.TransRequestAdminNotification(transRequest, delivery);
    var data = {
        transRequest: transRequest,
        delivery: delivery,
        vehicleType: di.getType(delivery.vehicleType)
    }
    return notification.send(data)
            .catch(console.error);
}

function verifyAlerts(transRequest, delivery, vehicleType) {
    return di.transRequestRepository.getAlertsForTransRequest(transRequest)
                .then(notifyCarriers)
                .catch(console.error);

    function notifyCarriers(alerts) {
        return alerts.map(function(item){
            return di.accountRepository.getById(item.creator)
                        .then(notifyCarrier)
        });
    }

    function notifyCarrier(carrier) {
        var notification = new di.TransRequestAlertNotification(carrier);
        return notification.send({
            user: carrier,
            transrequest: transRequest
        });
    }
}


module.exports = ExporterEvents;
