function TransRequestFactory() {
    this.fromDelivery = fromDelivery;

    function fromDelivery(delivery) {
        return new TransRequest(
            "Delivery Request",
            delivery.price,
            "Transportation Request created throught the delivery request API",
            "",
            delivery.pickUp.location,
            delivery.pickUp.description,
            delivery.dropOff.description,
            delivery.dropOff.location,
            delivery.buyer.accountId,
            delivery.buyer.name,
            null,
            ["Road"],
            delivery.pickUp.pickupDate
        );
    }

    function TransRequest(title, budget, description, cargoType, pickupLocation, pickupDescription, dropoffDescription, dropoffLocation, buyerId, buyerName, dimensions, transportType, pickupDate) {
        this.requestTitle = title;
        this.budget = budget;
        this.description = description
        this.cargotype = cargoType
        this.pickuploc = pickupLocation
        this.pickuplocreadable = pickupDescription;
        this.destinationreadable = dropoffDescription;
        this.destination = dropoffLocation;
        this.creator = buyerId;
        this.name = buyerName;
        this.bids = [];
        this.dimensions = dimensions;
        this.transtype = transportType;
        this.pickupdate = pickupDate;
    }
}

module.exports = TransRequestFactory;
