let PostalCodeModel = require('../../models/postalCode');

module.exports = {
    getForZipCode: function (zipCode) {
        return PostalCodeModel.findOne({
            ZIP: zipCode
        });
    },
    getAll: function() {
        return PostalCodeModel.find({
        });
    }
};