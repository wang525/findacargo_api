var Promise = require("bluebird");
var di;

function CarrierAssigned(options){
    di = options;
    var that = this;

    this.assign = assign;
    this.setStatus = setStatus;

    function assign(deliveryId, vehicleId) {
        var delivery;
        var vehicle;

        var promises = [
            di.deliveryRepository.getDelivery(deliveryId),
            di.vehicleRepository.getVehicle(vehicleId)
        ];

        return Promise.all(promises)
                .then(function(result){
                    delivery = result[0];
                    vehicle = di.vehicleFormatter.bidVehicle(result[1]);
                })
                .then(assignCarrier);

        function assignCarrier(){
            return di.scheduler.assignCarrierForRecovey(delivery, vehicle)
                .then(function(){
                    di.scheduler.emit("carrier-assigned", delivery, vehicle);
                })
        }

    }

    function setStatus(deliveryId, data) {
        return di.deliveryRepository.getByIdAndCarrier(deliveryId, data.carrier)
                .then(loadState)
                .then(setNewState)

        function loadState(delivery) {
            return di.carrierStateEngine.load(delivery, data.carrier)
        }

        function setNewState() {
            var newState = "set"+di.CarrierStateEnum.getKey(data.status);
            return di.carrierStateEngine[newState].apply(this);
        }
    }
}

module.exports = CarrierAssigned;
