var mongoose = require("mongoose");
var Promise = require("bluebird");

var PreferredLocationsSchema = require("./repository/models/preferred-location");
    PreferredLocations = mongoose.models.PreferredLocations

var di = {};

function Locations(dependencies) {
    var di = dependencies;
    var that = this;

    this.getLocations = getLocations;
    this.add = add;
    this.remove = remove;

    function getLocations() {
        var user = di.getUser();

        var promises = [
            getRecentLocations(),
            getPreferredLocations()
        ];
        
        return Promise.all(promises)
                .then(function(result){
                    return result[0].concat(result[1]);
                });

        function getRecentLocations(){
            var locations = [];

            return di.deliveryRepository.getRecent(user.id)
                    .then(function(deliveries){
                        for(var i in deliveries){
                            locations.push({
                                description: deliveries[i].pickUp.description,
                                latitude: deliveries[i].pickUp.location[1],
                                longitude: deliveries[i].pickUp.location[0]
                            });
                            locations.push({
                                description: deliveries[i].dropOff.description,
                                latitude: deliveries[i].dropOff.location[1],
                                longitude: deliveries[i].dropOff.location[0]
                            });
                        }

                        return locations;
                    })
        }

        function getPreferredLocations(){
            return di.preferredLocationsRepository.getByUser(user.id);
        }
    }

    function add(data) {
        var user = di.getUser();
        data.userId = user.id;
        var preferredLocation = new PreferredLocation(data);
        return di.preferredLocationsRepository.save(preferredLocation);
    }

    function remove(locationId) {
        return di.preferredLocationsRepository.remove(locationId);
    }
}

module.exports = Locations;
