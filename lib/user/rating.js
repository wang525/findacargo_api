var mixpanel = require ('mixpanel/lib/mixpanel-node');
var di = {};

function Rating(dependencies) {
    var di = dependencies;
    var that = this;

    this.rate = rate;

    function rate(targetUserId, rateData) {
        var user = di.getUser();
        return di.repository
            .setUserRating(rateData, user, targetUserId)
            .then(formatOutput);
    }

    function formatOutput(accountModel) {
        return di.formatter.setRating(accountModel);
    }
}

module.exports = Rating;
