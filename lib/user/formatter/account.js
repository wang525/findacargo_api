var Formatter = {
    login: login,
    onBoardingStepFirst:onBoardingStepFirst,
    onBoardingStepSecond:onBoardingStepSecond,
    setRating: setRating,
    apiKey: apiKey
}

function login(account) {
    return {
        _id: account._id,
        name: account.name || "",
        email: account.email,
        mobile: account.mobile,
        buyer: (account.buyer === "1")? "1":"0",
        carrier: (account.carrier === "1")? "1":"0",
        freightForwarder: (account.freightForwarder === "1")? "1":"0",
        countryDialCode : account.countryDialCode,
        countryISOCode: account.countryISOCode,
        phone : account.phone,
        phone1 : account.countryDialCode+account.phone,
        phone2 : "0"+account.phone,
        phone3 : "00"+account.phone,
        imagePath : account.imagePath,
        token: account._token,
        monthlyInvoiceAllowed: (account.monthlyInvoice === "1") ? "1":"0",
        active: (account.active === "1")? "1":"0"
    }
}

function apiKey(account) {
    return {
        _id: account._id,
        apikey: account.apikey,
        name: account.name || ""
    }
}


function onBoardingStepFirst(userData)
{
    return {
        identifier : userData.identifier,
        vehicleType : userData.vehicleType,
        type : userData.type,
        allowedCargo : userData.allowedCargo,
        userID : userData.userID
    }
}

function onBoardingStepSecond(userData)
{
    return {
        address : userData.address,
        userID : userData.userID
    }

}

function setRating(account) {
    return {
        result: true
    }
}

module.exports = Formatter;
