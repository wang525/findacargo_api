var Factory = {
     createFromRequestUser: createFromRequestUser,
     createFromRequestFacebookUser: createFromRequestFacebookUser,
     onBoardingStepFirst: createFromOnBoardingStepFirst,
     onBoardingStepSecond: createFromOnBoardingStepSecond
 };

 var User = function(name, email, country, countryISO, phone, buyer, carrier, password, date) {
     this.name = name;
     this.email = email.toLowerCase();
     this.countryDialCode = country;
     this.countryISOCode = countryISO;
     this.phone = phone;
     this.phone1 = country + phone;
     this.phone2 = "0" + phone;
     this.phone3 = "00" + phone;
     this.buyer = buyer;
     this.freightForwarder = '0';
     this.carrier = carrier;
     this.password = password;
     this.imagePath = '';
     this.facebookRegistration = '0';
     this.manager = '0';
     this.approved = '0';
     this.monthlyInvoice = '0';
     this.activeCarrier = '0';
     this.parent = '';
     this.date = date;
     this.monthlyInvoiceAllowed = false;
 };

 var FacebookUser = function(name, email, country, countryISO, phone, buyer, carrier, password, date, secondaryEmail) {
     this.name = name;
     this.email = email.toLowerCase();
     this.countryDialCode = country;
     this.countryISOCode = countryISO;
     this.phone = phone;
     this.phone1 = country + phone;
     this.phone2 = "0" + phone;
     this.phone3 = "00" + phone;
     this.buyer = buyer;
     this.freightForwarder = '0';
     this.carrier = carrier;
     this.pass = password;
     this.fbPassword = password;
     this.imagePath = '';
     this.facebookRegistration = '1';
     this.manager = '0';
     this.approved = '0';
     this.monthlyInvoice = '0';
     this.activeCarrier = '0';
     this.parent = '';
     this.date = date;
     this.secondaryEmail = secondaryEmail;
     this.monthlyInvoiceAllowed = false;
 };

 var onBoardingStepFirst = function(identifier, vehicleType, type, allowCargo, userID) {

     this.identifier = identifier;
     this.vehicalType = vehicleType;
     this.type = type;
     this.allowCargo = allowCargo;
     this.userID = userID;

 };

 var onBoardingStepSecond = function(address, userID) {
     this.address = address;
     this.userID = userID;
 };

 function createFromRequestUser(request) {
     var name = request.name;
     var email = request.email;
     var country = request.countryDialCode;
     var countryISO = request.countryISOCode;
     var phone = request.phone;
     var buyer = request.buyer;
     var carrier = request.carrier;
     var password = request.password;
     var date = request.date;

     return new User(name, email, country, countryISO, phone, buyer, carrier, password, date);
 }

 function createFromRequestFacebookUser(request) {
     var name = request.name;
     var email = request.email;
     var country = request.countryDialCode;
     var countryISO = request.countryISOCode;
     var phone = request.phone;
     var buyer = request.buyer;
     var carrier = request.carrier;
     var password = request.password;
     var date = request.date;
     var secondaryEmail = request.secondaryEmail;

     return new FacebookUser(name, email, country, countryISO, phone, buyer, carrier, password, date, secondaryEmail);
 }

 function createFromOnBoardingStepFirst(request, userID) {
     var identifier = request.identifier;
     var vehicalType = request.vehicleType;
     var type = request.type;
     var allowCargo = request.allowCargo;

     return new onBoardingStepFirst(identifier, vehicalType, type, allowCargo, userID);
 }

 function createFromOnBoardingStepSecond(request, userID) {
     var address = request.address;
     return new onBoardingStepSecond(address, userID);
 }

 module.exports = Factory;
