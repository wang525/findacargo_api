var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var PreferredLocation = new Schema({
    "userId": {
        "type": ObjectId,
        "required": true
    },
    "description": {
        "type": String,
        "required": true
    },
    "latitude": {
        "type": String,
        "required": true
    },
    "longitude": {
        "type": String,
        "required": true
    }
}, {collection: "preferredlocations"});

module.exports = mongoose.model("PreferredLocation", PreferredLocation);


