var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Otp = new Schema({
    "phone": {
        "type": String
    },
    "otpNumber": {
        "type": String
    },
    "timestamp": {
        "type": Number
    },
});

module.exports = mongoose.model("otps", Otp);


