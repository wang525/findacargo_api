var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Rating = new Schema({
    "userId": {
        "type": ObjectId,
        "required": true
    },
    "rating": {
        "type": Number,
        "required": true
    },
    "comment": {
        "type": String
    },
    "when": {
        "type": Date,
        "default": Date.now
    }
});

var Account = new Schema({
    "name": {
        "type": String
    },
    "email": {
        "type": String
    },
    "groupId": {
        "type": String
    },
    "countryDialCode": {
        "type": String
    },
    "phone": {
        "type": String
    },
    "phone1": {
        "type": String
    },
    "phone2": {
        "type": String
    },
    "phone3": {
        "type": String
    },
    "buyer": {
        "type": String
    },
    "freightForwarder": {
        "type": String
    },
    "carrier": {
        "type": String
    },
    "password": {
        "type": String,
        "required":true
    },
    "fbPassword": {
        "type": String
    },
    "imagePath": {
        "type": String
    },
    "facebookRegistration": {
        "type": String
    },
    "manager": {
        "type": String
    },
    "approved": {
        "type": Boolean
    },
    "date": {
        "type": Date
    },
    "monthlyInvoice":{
        "type":String
    },
    "active":{
        "type":String,
        "default": "1"
    },
    "carrierActive":{
        "type":String
    },
    "parent":{
        "type":String
    },
    "countryISOCode":{
        "type":String
    },
    "ratings": {
        "type": [Rating]
    },
    "secondaryEmail": {
        "type": String
    },
    "deviceToken": {
        "type": String
    },
    "token": {
        "type": ObjectId
    },
    "apiKey": {
        "type": String
    },
    "apikey": {
        "type": String
    },
    "iOSDevice": {
        "type": Boolean,
        "default": false
    },
    "fixedDiscount": {
        "type": Number,
        "default": 0
    },
    "fixedPrice": {
        "type": Number
    },
    "info": {
        "type": Object
    }
});

module.exports = mongoose.model("accounts", Account);


