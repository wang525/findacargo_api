var mongoose = require("mongoose");
    mongoose.Promise = require("bluebird");

var mongodb = require("mongodb");
var PreferredLocationsSchema = require("./models/preferred-location");
    PreferredLocation = mongoose.models.PreferredLocation;

var Repository = {
    getByUser: getByUser,
    save: save,
    remove: remove
};

function getByUser(userId) {
    var query = {
        userId: userId
    };

    return PreferredLocation.find(query).exec();
}

function save(preferredLocation) {
    return preferredLocation.save();
}

function remove(locationId) {
    return PreferredLocation.find({_id: new mongodb.ObjectId(locationId)}).remove();
}

module.exports = Repository;
