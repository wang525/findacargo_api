var mongodb = require("mongodb");
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var accountSchema = require("./models/account");
var addressSchema = require("./models/address");
var fleetSchema = require("./models/fleet");
var q = require("q");
mongoose.Promise = require("bluebird");
var Account = mongoose.models.accounts;
var Fleet = mongoose.models.truck;
var Address = mongoose.models.addresses;

var Repository = {
    getById: getById,
    findOne: findOne,
    getVehicleById: getVehicleById,
    getByEmail: getByEmail,
    createAccount: createAccount,
    obsf: obsf,
    obss: obss,
    setUserRating: setUserRating,
    findOneAndUpdate: findOneAndUpdate,
    updateAccount: updateAccount,
    updateDeviceToken: updateDeviceToken,
    getByApiToken: getByApiToken,
    findAll: findAll,
    updateAccountApikey: updateAccountApikey,
    getCoWorkers: getCoWorkers
}

function setUserRating(rating, user, targetUserId) {
    rating.userId = user._id;

    var filter = {
        "_id": ObjectId(targetUserId),
        "ratings.$.userId": ObjectId(user._id)
    };

    return Account.findById(targetUserId).exec()
        .then(function (targetUser) {
            if (!targetUser) {
                throw "Invalid user";
            }

            var getById = function (item) {
                if (item.userId.equals(user._id)) {
                    return item;
                }
            }

            var data = targetUser.ratings.filter(getById).pop();

            if (!data) {
                targetUser.ratings.push(rating);
                return targetUser.save();
            } else {
                data.rating = rating.rating;
                data.comment = rating.comment;
                return targetUser.save();
            }
        });

}

function findOne(id) {
    return Account.findOne({_id: id});
}

function findAll(query, fields) {
    return Account.find(query, fields);
}

function updateDeviceToken(id, token, isIOS) {
    return getById(id)
        .then(function (account) {
            account.deviceToken = token;
            account.iOSDevice = isIOS;
            return updateAccount(account);
        })
}

function updateAccount(account) {
    return account.save();
}

function updateAccountApikey(id, apikey) {
    return new Promise((resolve, reject) => {
        accountSchema.update(
        {_id: id}, 
        {apikey: apikey}, 
        function(err, data){
            if (err) {
                reject(err);
            }  
            resolve(id);
        })
    });
}

function findOneAndUpdate(id, data) {
    return Account.findOneAndUpdate({_id: id}, {$set: data});
}

function createAccount(userData) {
    var deferred = q.defer();
    var accountModel = new Account(userData);
    accountModel.save(function (err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(accountModel);
        }
    });

    return deferred.promise;
}

function obsf(userData) {

    var deferred = q.defer();
    fleetModel = new Fleet(userData);

    fleetModel.save(function (err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {

            //console.log(data);
            deferred.resolve(data);
        }
    });

    return deferred.promise;

}

function obss(userData) {

    var deferred = q.defer();
    addressModel = new Address(userData);
    addressModel.save(function (err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(addressModel);
        }
    });

    return deferred.promise;

}

function getVehicleById(id) {

    return Fleet.findById(id).exec()
        .then(function (vehicle) {
            return Account.findById(vehicle.userID).exec()
                .then(function (driverAccount) {
                    return {
                        vehicle: vehicle.toObject(),
                        driver: driverAccount
                    };
                })
        })
        .catch(console.log);
}

function getById(id) {
    var deferred = q.defer();

    Account.findById(id, function (err, userAccount) {
        if (err) {
            return deferred.reject(err);
        }

        deferred.resolve(userAccount);
    });

    return deferred.promise;
}

function getByEmail(email) {
    var deferred = q.defer();
    var query = {
        email: email.toLowerCase(),
    };

    Account.findOne(query, function (err, account) {
        if (err) {
            return deferred.reject(err);
        }

        deferred.resolve(account);
    });

    return deferred.promise;
}

function getByApiToken(token) {
    let ObjIdTest = new RegExp("^[0-9a-fA-F]{24}$");
    let query = {$or: [{apikey: token}, {apiKey: token}]};

    if (ObjIdTest.test(token))
        query = {token: token};

    return Account.findOne(query).exec();
}

function getCoWorkers(clientId) {
    let accountId = clientId;
    let account;
    return Account.findOne({_id: accountId})
            .then(function (res) {
                account = res;
                let query = {$or: [{"_id": accountId}, {"groupId": accountId}]};
                if (account.groupId) {
                    query.$or.push({"groupId": account.groupId}, {"_id": account.groupId});
                }
                return Account.find(query).exec()
                        .then(function (data) {
                            return data;
                        })
                    })
}
module.exports = Repository;
