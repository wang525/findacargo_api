var DeliverySettings = require("../../../models/deliverysettings");

var Repository = {
    getByClientIds: getByClientIds,
    getByClientId: getByClientId,
    updateSetting: updateSetting
};

function getByClientIds(clientIds) {
    return DeliverySettings.find({
        'clientId': { $in: clientIds }
    });
}

function getByClientId(clientId) {
    return DeliverySettings.findOne({
        'clientId': clientId
    });
}

function updateSetting(clientId, query) {
    return DeliverySettings.findOneAndUpdate({'clientId': clientId}, {$set:query})
}

module.exports = Repository;
