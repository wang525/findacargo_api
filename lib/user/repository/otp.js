var mongodb = require("mongodb");
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var otpSchema = require("./models/otp");
var q = require("q");
mongoose.Promise = require("bluebird");
var Otp = mongoose.models.otps;

var Repository = {
    findOne: findOne,
    getByPhone: getByPhone,
    create: create,
    findOneAndUpdate: findOneAndUpdate,
    update: update,
}
function findOne(id) {
    return Otp.findOne({_id: id});
}

function update(otp) {
    return otp.save();
}
function findOneAndUpdate(id, data) {
    return Otp.findOneAndUpdate({_id: id}, {$set: data});
}

function create(data) {
    var deferred = q.defer();
    var otpModel = new Otp(data);
    otpModel.save(function (err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(otpModel);
        }
    });

    return deferred.promise;
}

function getByPhone(phone) {
    var deferred = q.defer();
    var query = {
        phone: phone,
    };

    Otp.findOne(query, function (err, otp) {
        if (err) {
            return deferred.reject(err);
        }

        deferred.resolve(otp);
    });

    return deferred.promise;
}

module.exports = Repository;
