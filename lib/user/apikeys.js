var accountRepository = require("./repository/account");
var Crypto = require('crypto');
var formatter = require("./formatter/account");

const ApiKeyController = {
    getApiKey: function (clientId) {
        return accountRepository.getById(clientId.toObjectId())
            .then(account => {

                if (!account) {
                    throw "Invalid User";
                }

                return formatter.apiKey(account);
            });
    },
    updateApiKey: function (clientId) {
        var apikey;
        return new Promise((resolve, reject) => {
            Crypto.randomBytes(16, function (err, buffer) {
                if (err) {
                    reject(err);
                }
                var apikey = buffer.toString('hex');
                resolve(apikey);
            })
        })
        .then(generated => {
            apikey = generated;
            return accountRepository.getById(clientId.toObjectId())
        })
        .then(account => {

            if (!account) {
                throw "Invalid User";
            }
           
            return accountRepository.updateAccountApikey(account._id, apikey);
        })
        .then((id) => {
            return accountRepository.getById(id)
        })
        .then(account => {
            return formatter.apiKey(account);
        });    
    }
}

module.exports = ApiKeyController;