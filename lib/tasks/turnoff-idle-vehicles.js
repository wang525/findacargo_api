vehicleRepository = require("../carrier/repository/vehicle");

function TurnOffIdleVehicles() {
    return setInterval(function(){
        vehicleRepository.turnOffIdleVehicles();
    }, 3600000);
}

module.exports = TurnOffIdleVehicles
