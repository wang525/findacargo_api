module.exports = (type, restricted, current, context) => {
    context = generateLocation(context);

    const VALIDATION_ERRORS = {
        ArrayValidationError: `Minimum items in body array is ${restricted} but we received ${current}.`,
        defaultError: `Expected: ${restricted} but we received ${current}, at ${context}.`
    };

    if (!type) return VALIDATION_ERRORS.defaultError;

    return VALIDATION_ERRORS[type];
};

let generateLocation = function (context) {
    if (context[0] === '#')
        context = context.slice(2);

    return context.replace(/\//g, '.');
};