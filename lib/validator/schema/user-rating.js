var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "UserRating",
    "description": "Validate user rating",
    "type": "object",
    "properties": {
        "rating": {
            "description": "Rating",
            "type": "number",
            "minimum": 0,
            "maximum": 5
        },
        "comment": {
            "description": "Private comment about the user",
            "type": "string"
        },
    },
    "required": ["rating"]
};

module.exports = Schema;
