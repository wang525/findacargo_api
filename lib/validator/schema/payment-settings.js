var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "payment-settings",
    "description": "Add new payment settings",
    "type": "object",
    "properties": {
        "type": {
            "description": "Type of deliveries",
            "type": "string"
        },
        "when": {
            "description": "Day of delivery",
            "type": "string"
        },
        "company_details": {
            "type": "object",
            "properties": {
                "company_address": {
                    "type": "object",
                    "properties": {
                        "street": {
                            "type": "string"
                        },
                        "city": {
                            "type": "string"
                        },
                        "floor": {
                            "type": "string"
                        },
                        "zip": {
                            "type": "string"
                        },
                        "country": {
                            "type": "string"
                        },
                    }
                },
                "pickup_address": {
                    "type": "object",
                    "properties": {
                        "street": {
                            "type": "string"
                        },
                        "city": {
                            "type": "string"
                        },
                        "floor": {
                            "type": "string"
                        },
                        "zip": {
                            "type": "string"
                        },
                        "country": {
                            "type": "string"
                        },
                    }
                },
                "cvr_nr": {
                    "type": "number"
                },
                "company_name": {
                    "type": "string"
                },
            }
        },
        "details": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "phone": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "needs": {
                    "type": "string"
                },
            }
        },
        "destinations": {
            "description": "Estimated time of delivery",
            "type": "array",
            "items": {
                "type": "string"
            }
        }
    },
    "required": ["type", "company_details"]
};

module.exports = Schema;
