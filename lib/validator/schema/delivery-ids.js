var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "delivery-ids",
    "description": "Change deliveries statuses",
    "type": "array",
    "items": {
        "type": "string"
    }

};

module.exports = Schema;