var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "zone-deliveries",
    "description": "Manually assign deliveries to drivers",
    "type": "object",
    "items": {
        "type": "object",
        "properties": {
            "driver_id": {
                "description": "Id of driver",
                "type": "string"
            },            
            "estimated_delivery_time": {
                "description": "Estimated time of delivery",
                "type": "string"
            }
        }
    }

};

module.exports = Schema;