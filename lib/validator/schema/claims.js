var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "UserRating",
    "description": "Validate user/delivery claims",
    "type": "object",
    "properties": {
        "ClaimType": {
            "description": "Claim type 1/2/3/4",
            "type": "integer",
            "min": 1,
            "max": 4
        },
        "location": {
            "description": "Address",
            "type": "string"
        },
        "message": {
            "description": "Claim message",
            "type": "string"
        },
        "image": {
            "type":"string"
        },
        "notAtHome": {
            "type": "integer",
        },
        "notAnsweringCall": {
            "type": "integer",
        }
    },
    "required": ["ClaimType", "message"]
};

module.exports = Schema;
