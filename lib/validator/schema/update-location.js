var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "UpdateLocation",
    "description": "Vehicle location coordinates",
    "type": "object",
    "properties": {
        "latitude": {
            "description": "The vehicle latitude",
            "type": "number"
        },
        "longitude": {
            "description": "The vehicle longitude",
            "type": "number"
        },
    },
    "required": ["latitude", "longitude"]
};

module.exports = Schema;
