var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "zone-delivery",
    "description": "Manually assign deliveries to driver",
    "type": "object",
    "properties": {
        "driver_id": {
            "description": "Id of driver",
            "type": "string"
        },
        "delivery_id": {
            "description": "Id of delivery",
            "type": "string"
        },
        "order": {
            "description": "Order for todays delivery",
            "type": ["string", "integer"]
        },
        "estimated_delivery_time": {
            "description": "Estimated time of delivery",
            "type": "string"
        }
    },
    "required": ["driver_id", "delivery_id"]
};

module.exports = Schema;
