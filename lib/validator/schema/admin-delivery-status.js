var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "AdminSetCarrierStatus",
    "description": "Validate data to set delivery carrier status",
    "type": "object",
    "properties": {
        "carrier": {
            "description": "Carrier ID",
            "type": "string"
        },
        "status": {
            "description": "The new status",
            "type": "string"
        },
    },
    "required": ["carrier", "status"]
};

module.exports = Schema;
