var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "OTPCheck",
    "description": "check otp number",
    "type": "object",
    "properties": {
        "phone": {
            "description": "The user phone number",
            "type": "string"
        },
        "otpNumber": {
            "description": "The otp number sent by sms",
            "type": "string"
        }
    },
    "required": ["phone", "otpNumber"]
};

module.exports = Schema;
