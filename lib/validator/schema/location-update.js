var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "Location Update",
    "description": "Updates the location for delivery tracking",
    "type": "object",
    "properties": {
        "latitude": {
            "description": "latitude",
            "type": "string"
        },
        "longitude": {
            "description": "longitude",
            "type": "string"
        }
    }
};

module.exports = Schema;
