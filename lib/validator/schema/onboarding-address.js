var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "OnboardingAddress",
    "description": "Validate address data after signup",
    "type": "object",
    "properties": {
        "address": {
            "description": "User Address",
            "type": "string"
        },
    },
    "required": ["address"]
};

module.exports = Schema;
