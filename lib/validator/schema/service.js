var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "Service",
    "description": "Add Service To Data Base",
    "type": "object",
    "properties": {
        "name": {
            "description": "service name",
            "type": "string"
        },
        "description": {
            "description": "service description",
            "type": "string"
        },
        "enabled": {
            "description": "service available",
            "type": "boolean"
        }
    },
    "required": ["name"]
};

module.exports = Schema;