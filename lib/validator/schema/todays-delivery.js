var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "TodaysDelivery",
    "description": "Fetch Todays deliveries",
    "type": "object",
    "properties": {
        "date": {
            "description": "Today's Date",
            "type": "string"
        },
    }
};

module.exports = Schema;
