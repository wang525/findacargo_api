var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "CustomEvent",
    "description": "Add Custom Event To Data Base",
    "type": "object",
    "properties": {
        "eventMessage": {
            "description": "Massage about custom event",
            "type": "string"
        }
    },
    "required": ["eventMessage"]
};

module.exports = Schema;