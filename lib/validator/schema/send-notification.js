var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "SendNotification",
    "description": "Send Notification To Client",
    "type": "object",
    "properties": {
        "notificationMessage": {
            "description": "Notification Message",
            "type": "string"
        },
        "deliveryIds": {
            "type": "array",
            "items": {
                "type": "string"
            }  
        },
        "token": {
            "description": "Identification Token",
            "type": "string"
        }
    },
    "required": ["notificationMessage", "deliveryIds"]
};

module.exports = Schema;
