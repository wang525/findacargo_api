var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "Signature",
    "description": "Signature",
    "type": "object",
    "properties": {
        "signature": {
            "description": "signature",
            "type": "string"
        },
    },
    "required": ["signature"]
};

module.exports = Schema;
