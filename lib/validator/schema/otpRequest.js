var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "OTPRequest",
    "description": "Send otp code by sms",
    "type": "object",
    "properties": {
        "phone": {
            "description": "The user phone number",
            "type": "string"
        }
    },
    "required": ["phone"]
};

module.exports = Schema;
