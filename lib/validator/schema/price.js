var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "CarrierRateCard",
    "description": "Validate price data",
    "type": "object",
    "properties": {
        "base": {
            "description": "Base price",
            "type": "number"
        },
        "costPerKm": {
            "description": "Price per km",
            "type": "number"
        },
        "extra": {
            "description": "Extras price",
            "type": "number"
        }
    },
    "required": ["base", "costPerKm", "extra"]
};

module.exports = Schema;
