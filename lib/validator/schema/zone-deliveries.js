var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "zone-deliveries",
    "description": "Manually assign deliveries to drivers",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "driver_id": {
                "description": "Id of driver",
                "type": "string"
            },
            "delivery_id": {
                "description": "Id of delivery",
                "type": "string"
            },
            "order": {
                "description": "Order for todays delivery",
                "type": ["string", "integer"]
            },
            "estimated_delivery_time": {
                "description": "Estimated time of delivery",
                "type": "string"
            }
        },
        "required": ["delivery_id"]
    }

};

module.exports = Schema;