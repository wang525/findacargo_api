var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "DeliveryPayment",
    "description": "Validate delivery payment data",
    "type": "object",
    "properties": {
        "encryptedData": {
            "description": "Encrypted payment data",
            "type": "string"
        },
    },
    "required": ["encryptedData"]
};

module.exports = Schema;
