var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "Search",
    "description": "Search through deliveries",
    "type": "object",
    "properties": {
        "keyword": {
            "description": "keyword to search for",
            "type": "string"
        },
        "date": {
            "description": "Date of delivery",
            "type": "string"
        },
        "status": {
            "description": "Status of delivery",
            "type": "string"
        },
        "page": {
            "type": "number"
        },
        "size": {
            "type": "number"
        }
    },
    "required": ["keyword"]
};

module.exports = Schema;
