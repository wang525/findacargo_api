var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "RequestDelivery",
    "description": "A input model to request a delivery",
    "type": "object",
    "properties": {
        "vehicle_type": {
            "description": "The vehicle type identifier",
            "type": "string"
        },
        "cargo_details": {
            "description": "The cargo to deliver information",
            "type": "object",
            "properties": {
                "weight": {
                    "description": "Cargo weight",
                    "type": "number"
                },
                "size": {
                    "description": "Cargo area",
                    "type": "number"
                }
            }
        },
        "pickup_location": {
            "description": "Pick Up location definitions",
            "$ref": "#/definitions/location"
        },
        "pickup_options": {
            "description": "Pick Up options",
            "type": "object",
            "oneOf": [
                {"$ref": "#/definitions/pickupOptionLive" },
                {"$ref": "#/definitions/pickupOptionScheduled" },
            ]
        },
        "dropoff_location": {
            "description": "Drop Off location definitions",
            "$ref": "#/definitions/location"
        },
        "dropoff_options": {
            "description": "Drop Off options",
            "type": "object",
            "properties": {
                "date": {
                    "description": "Drop off date",
                    "type": "string"
                },
                "time": {
                    "description": "Drop off time",
                    "type": "string"
                }
            },
        },
        "notes": {
            "description": "Driver notes",
            "type": "string"
        },
        "promocode": {
            "description": "Promo code",
            "type": "string"
        },
        "options": {
            "description": "Extra options",
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "extra":{
            "description": "Extra information",
            "type":"object",
            "properties": {
                "email": {
                    "type":"string"
                },
                "menRequested": {
                    "type":"number"
                },
                "palletRequested": {
                    "type":"number"
                },
                "refrigerated": {
                    "type":"number"
                }
            }
        }
    },
    "definitions": {
        "location": {
            "properties": {
                "description": {
                    "description": "Location description",
                    "type": "string"
                },
                "latitude": {
                    "description": "Location latitude",
                    "type": "string"
                },
                "longitude": {
                    "description": "Location longitude",
                    "type": "string"
                }
            },
            "required": ["latitude", "longitude"],
            "additionalProperties": false
        },
        "pickupOptionLive": {
            "properties": {
                "when": { "enum": ["now"]},
                "date": {
                    "description": "Pickup date",
                    "type": "string",
                },
                "time": {
                    "description": "Pickup time",
                    "type": "string"
                }
            },
            "required": ["when"]
        },
        "pickupOptionScheduled": {
            "properties": {
                "when": { 
                    "type": "string",
                    "maxLength": 0
                },
                "date": {
                    "description": "Pick up date",
                    "type": "string",
                    "format": "date"
                },
                "time": {
                    "description": "Pickup time",
                    "type": "string",
                    "format": "time2"
                }
            },
            "required": ["date", "time"]
        }
    },
    "required": ["vehicle_type", "pickup_location", "pickup_options", "dropoff_location"]
};

module.exports = Schema;
