var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "NotificationSentEvent",
    "description": "Add Notification Sent Event To Data Base",
    "type": "object",
    "properties": {
        "eventMessage": {
            "description": "Massage about notification sent event",
            "type": "string"
        },
        "eventDetail": {
            "description": "Event detail includes eme/email content as json string",
            "type": "string"
        }
    },
    "required": ["eventMessage"]
};

module.exports = Schema;