var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "UserLogin",
    "description": "Validate user data to login",
    "type": "object",
    "properties": {
        "token": {
            "description": "Device Token",
            "type": "string"
        },
    },
    "required": ["token"]
};

module.exports = Schema;
