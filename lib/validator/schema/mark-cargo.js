var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "setCargoStatus",
    "description": "Sets status of cargo",
    "type": "object",
    "properties": {
        "cargoID": {
            "description": "Cargo Id",
            "type": "string"
        },
        "status":{
            "description":"Status",
            "type": "number"
        }
    }
};

module.exports = Schema;
