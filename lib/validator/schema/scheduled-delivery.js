var Schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "ScheduledDelivery",
    "description": "Validate scheduled delivery",
    "type": "array",
    "minItems": 1,
    "items": [
        {
            "type": "object",
            "properties": {
                "scheduled_id": {
                    "description": "Stored delivery ID. Used for update operation. Ignored for create",
                    "type": "string"
                },
                "delivery_id": {
                    "description": "ID of delivery",
                    "type": "string"
                },
                "recipient_id": {
                    "description": "Id of recipient",
                    "type": "string"
                },
                "recipient_email": {
                    "description": "Email of recipient",
                    "type": "string"
                },
                "recipient_name": {
                    "description": "Name of the recipient",
                    "type": "string"
                },
                "delivery_date": {
                    "description": "Date of delivery",
                    "type": "string"
                },
                "pickup_window": {
                    "description": "Pickup window",
                    "type": "object",
                    "properties": {
                        "from": {
                            "description": "Start time",
                            "type": "string"
                        },
                        "to": {
                            "description": "End time",
                            "type": "string"
                        }
                    }
                },
                "recipient_phone": {
                    "description": "Phone of the recipient",
                    "type": "object",
                    "properties": {
                        "country_iso_code": {
                            "type": "string"
                        },
                        "country_dial_code": {
                            "type": "string"
                        },
                        "phone": {
                            "description": "Phone number",
                            "type": "string"
                        }
                    }
                },
                "pickup_location": {
                    "description": "Location of the pickup",
                    "type": "object",
                    "properties": {
                        "zip": {
                            "description": "Zip for pickup",
                            "type": "string"
                        },
                        "city": {
                            "description": "City for pickup",
                            "type": "string"
                        },
                        "address_1": {
                            "description": "Address of pickup",
                            "type": "string"
                        },
                        "address_2": {
                            "description": "Address of pickup",
                            "type": "string"
                        },
                        "description": {
                            "type": "string",
                            "description": "Additional description",
                        },
                        "latitude": {
                            "type": "string",
                            "description": "Latitude of location"
                        },
                        "longitude": {
                            "type": "string",
                            "description": "Longitude of location"
                        }
                    },
                    "required": []
                },
                "delivery_location": {
                    "description": "Location of the delivery",
                    "type": "object",
                    "properties": {
                        "zip": {
                            "description": "Zip for delivery",
                            "type": "string"
                        },
                        "city": {
                            "description": "City for delivery",
                            "type": "string"
                        },
                        "address_1": {
                            "description": "Address of delivery",
                            "type": "string"
                        },
                        "address_2": {
                            "description": "Address of pickup",
                            "type": "string"
                        },
                        "description": {
                            "type": "string",
                            "description": "Additional description"
                        },
                        "latitude": {
                            "type": "string",
                            "description": "Latitude of location"
                        },
                        "longitude": {
                            "type": "string",
                            "description": "Longitude of location"
                        }
                    },
                    "required": ["zip", "city"]
                },
                "delivery_window": {
                    "description": "Delivery window",
                    "type": "object",
                    "properties": {
                        "from": {
                            "description": "Start time",
                            "type": "string"
                        },
                        "to": {
                            "description": "End time",
                            "type": "string"
                        }
                    }
                },
                "delivery_label": {
                    "description": "Label of delivery",
                    "type": "string"
                },
                "delivery_number_of_packages": {
                    "description": "Number of packages",
                    "type": "string"
                },
                "delivery_notes": {
                    "description": "Notes for delivery",
                    "type": ["string", "null"]
                },
                "driver_email": {
                    "description": "Email of the driver",
                    "type": "string"
                },
                "cargo_insurance": {
                    "description": "Insurance",
                    "type": "object",
                    "properties": {
                        "enabled": {
                            "type": "boolean"
                        },
                        "value": {
                            "type": "number"
                        }
                    }
                }
            },
            "required": [
                "delivery_date",
                "pickup_location", "delivery_location"
            ]
        }
    ]
};
// ISO CODE PHONE
module.exports = Schema;
