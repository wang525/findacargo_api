var JaySchema = require("jayschema");
var requestDeliverySchema = require("./schema/request-delivery");
var updateLocationSchema = require("./schema/update-location");

var js = new JaySchema();
var Validator = {
    requestDelivery: requestDelivery,
    updateLocation: updateLocation
 };

function requestDelivery(instance){
    return js.validate(instance, requestDeliverySchema);
}

function updateLocation(instance){
    return js.validate(instance, updateLocationSchema);
}

 module.exports = Validator;
