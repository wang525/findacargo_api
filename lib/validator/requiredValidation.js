  /**
   * Check required parameters validations
   * @param {object} event 
   * @param {Array} params 
   */
module.exports = (event, params) => {
    var message = ''
    var apiCredentials = event
    params.forEach(element => {
        if (element in apiCredentials && apiCredentials[element] !== '' && (apiCredentials[element].length !== 0)) {
        } else {
            message = message + element + ', '
        }
    });
    if (message !== '') {
        var validationmsg = message.replace(/,(?=[^,]*$)/, '') + "parameters are required and can not be blank."
        return validationmsg
    } else {
        return true
    }
}