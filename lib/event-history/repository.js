let EventModel = require('../../models/eventHistory');


module.exports = {
    create: (assigned_to, assigned_to_type, event, event_data, event_detail) => {
        return EventModel.create({
            assigned_to, assigned_to_type, event, event_data, event_detail
        });
    },
    createWithTime: (assigned_to, assigned_to_type, event, event_data, event_detail, created_at) => {
        return EventModel.create({
            assigned_to, assigned_to_type, event, event_data, event_detail, created_at
        });
    },
    findForIdOfType: (assigned_to, assigned_to_type) => {
        return EventModel.find({
            assigned_to, assigned_to_type
        });
    },
    findForIdOfEventType: (assigned_to, event) => {
        return EventModel.find({
            assigned_to, event
        });
    },
};