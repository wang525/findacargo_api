var mongodb = require("mongodb");
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var db = require('../../framework/db-connector');

var q = require("q");
var Account = mongoose.models.accounts;
var Cargo =  db.collection('cargo');
var Repository = {
    getById: getById,
    getByEmail: getByEmail,
    setStatus:setStatus
}



function setStatus(userData)
{
    var deferred = q.defer();





    var query = {"_id":new mongodb.ObjectId(userData.id)}


    Cargo.findOne(query,function(err,res){
                  if(res)
                  {
                  res.status = userData.status;
                  //res.mobile = newData.mobile;
                  //res.distance = newData.distance;
                  Cargo.save(res, {safe: true}, function(err,objInserted) {
                             if (err) deferred.reject(err);


                             });
                  deferred.resolve(res);
                  }
                  else
                  {
                  return deferred.reject("No cargo found for given id");
                  }
                  });

    return deferred.promise;
}


function getById(id) {
    var deferred = q.defer();

    Account.findById(id, function(err, userAccount){
                     if(err) {
                     return deferred.reject(err);
                     }

                     deferred.resolve(userAccount);
                     });
    
    return deferred.promise;
}

function getByEmail(email) {
    var deferred = q.defer();
    var query = {
        email: email,
    };

    Account.findOne(query, function(err, account){
        if(err){
            return deferred.reject(err);
        }

        deferred.resolve(account);
    });

    return deferred.promise;
}

module.exports = Repository;
