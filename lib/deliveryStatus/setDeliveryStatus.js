var q = require("q");
var crypto = require("crypto");
var authToken = require("../framework/auth-token");
var carrierRepository = require("./repository/carrierRepository");
var formatter = require("./formatter/delivery");
var Responses = require("../../services/Response");
var getUser = require("../auth/user").getUser;

var Authenticator = {
  setStatus: setStatus
};

var statusKey = {
  1: "Created",
  2: "In progress",
  3: "Completed",
  4: "Not Received",
  5: "For re-delivery"
};

function setStatus(status, id) {
  var deferred = q.defer();

  var user = getUser();

  var userData = { id: id, status: status };

  return carrierRepository
    .getById(user.id)
    .then(function(account) {
      if (!account) {
        throw "No user registered with given id";
      }
      return carrierRepository.setStatus(userData);
    })
    .then(function(statusUpdate) {
      return Responses.codeWithMessage(
        200,
        "Delivery status updated to " + statusKey[statusUpdate.status]
      );
    });
}

function generateSalt() {
  var set = "0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ";
  var salt = "";
  for (var i = 0; i < 10; i++) {
    var p = Math.floor(Math.random() * set.length);
    salt += set[p];
  }
  return salt;
}

function md5(str) {
  return crypto
    .createHash("md5")
    .update(str)
    .digest("hex");
}

function encrypt(pass) {
  var salt = generateSalt();
  return salt + md5(pass + salt);
}

function validatePass(plain, hashed) {
  var salt = hashed.substr(0, 10);
  return hashed === salt + md5(plain + salt);
}

module.exports = Authenticator;
