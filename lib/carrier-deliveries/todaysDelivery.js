var carrierRepository = require("./repository/carrierRepository");
var Promise = require("bluebird");
var getUser = require("../auth/user").getUser;
;
let _ = require('underscore'),
    scheduledFormat = require('../scheduled/formatScheduled');

var TodaysDeliveries = {
    deliveries: deliveries
};

function deliveries(userData, scheduledId){
    var user = getUser();

    var p1 = carrierRepository
            .getDeliveries(userData, user.id)
            .then(addContact)
            .catch(function(err){
                return [];
            });
    var p2 = getScheduledDeliveries();
    let p3 = getZoneDeliveries();

    return Promise.all([p1, p2, p3])
            .then(function(out){
                return {
                    deliveries: out[0],
                    scheduled: sortScheduledDeliveries(filterReturnedDeliveries(_.union(out[1], out[2])))
                }
            });

    function sortScheduledDeliveries(deliveries) {
        return _.sortBy(deliveries, function (delivery) {
            if (!delivery.order || delivery.order == -1)
                return 99999;

            return delivery.order;
        })
    }

    function filterReturnedDeliveries(deliveries) {
        return _.filter(deliveries, function (delivery) {
            return delivery.delivery_status != 6 && delivery.type != 'return';
        })
    }

    function addContact(deliveries){
        var promises = deliveries.map(function(delivery){
            return carrierRepository.getContact(delivery.value.ClientID)
                .then(function(contact){
                    delivery.value.ClientDetails = contact;
                })
                .catch(function(){
                    delivery.value.ClientDetails = {};
                });
        });

        return Promise.all(promises)
                .then(function(){
                    return deliveries;
                });
    }

    function getScheduledDeliveries() {
        return carrierRepository
            .getRoutesAndScheduledDeliveries(userData, user._id, scheduledId)
            .then(formatScheduled);
    }

    function getZoneDeliveries() {
        return carrierRepository
            .getZoneDeliveries(user._id, userData)
            .then(formatScheduled);
    }

    function formatScheduled(list){
        return list.map(function(item){
             return scheduledFormat(item);
        });
    }
}

module.exports = TodaysDeliveries;
