var Formatter = {
    login: login,
    onBoardingStepFirst:onBoardingStepFirst,
    onBoardingStepSecond:onBoardingStepSecond
}

function login(account) {
debugger;
    return {
        _id: account._id,
        name: account.name,
        email: account.email,
        mobile: account.mobile,
        buyer: account.buyer,
        carrier: account.carrier,
        freightForwarder: account.freightForwarder,
        countryDialCode : account.countryDialCode,
        countryISOCode: account.countryISOCode,
        phone : account.phone,
        phone1 : account.countryDialCode+account.phone,
        phone2 : "0"+account.phone,
        phone3 : "00"+account.phone,
        imagePath : '',
        token: account.token
    }
}


function onBoardingStepFirst(userData)
{
    return {
        identifier : userData.identifier,
        vehicleType : userData.vehicleType,
        type : userData.type,
        allowedCargo : userData.allowedCargo,
        userID : userData.userID
    }
}

function onBoardingStepSecond(userData)
{
    return {
        address : userData.address,
        userID : userData.userID
    }

}

module.exports = Formatter;
