let db = require('../../framework/db-connector'),
    DriverDelivery = db.collection('driver_delivery');


module.exports = {
    getAssignedDeliveries: function (carrierId) {
        return DriverDelivery.find({
            carrier_id: carrierId
        });
    },
    findOne: function (query) {
        return DriverDelivery.findOne(query);
    },
    findAll: function (query) {
        return DriverDelivery.find(query);
    },
    create: function (data) {
        return DriverDelivery.insertOne(data);
    },
    createMany: function (data) {
        return DriverDelivery.insertMany(data);
    },
    findOneAndUpdate: function (searchQuery, query) {
        return DriverDelivery.findOneAndUpdate(searchQuery, { $set: query })
    },
    delete: function (query) {
        return DriverDelivery.deleteOne(query);
    },
    deleteMany: function (deliveryIds) {
        return DriverDelivery.deleteMany({
            delivery_id: { $in: deliveryIds }
        });
    }
};