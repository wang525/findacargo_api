var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Account = new Schema({
    "name": {
        "type": String
    },
    "email": {
        "type": String,
        "required": true
    },
    "countryDialCode": {
        "type": String
    },
    "phone": {
        "type": String
    },
    "phone1": {
        "type": String
    },
    "phone2": {
        "type": String
    },
    "phone3": {
        "type": String
    },
    "buyer": {
        "type": String
    },
    "freightForwarder": {
        "type": String
    },
    "carrier": {
        "type": String
    },
    "pass": {
        "type": String,
        "required":true
    },
    "imagePath": {
        "type": String
    },
    "facebookRegistration": {
        "type": String
    },
    "manager": {
        "type": String
    },
    "approved": {
        "type": String
    },
    "date": {
        "type": Date
    },
    "monthlyInvoice":{
        "type":String
    },
    "active":{
        "type":String
    },
    "carrierActive":{
        "type":String
    },
    "parent":{
        "type":String
    },
    "countryISOCode":{
        "type":String
    }
});

module.exports = mongoose.model("accounts", Account);


