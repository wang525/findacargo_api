var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Route = new Schema({
    "weekNo": {
        "type": String
    },
    "weekDay": {
        "type": String
    },
    "creator": {
        "type": ObjectId
    },
    "routeDate": {
        "type": Date
    },

});

module.exports = mongoose.model("routes", Route);


