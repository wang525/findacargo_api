let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let ObjectId = Schema.ObjectId;

let ZoneDelivery = new Schema({
    //TEMP structure
    delivery_id: ObjectId,
    carrier_id: ObjectId
});

module.exports = mongoose.model("zone_delivery", ZoneDelivery);


