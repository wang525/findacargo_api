var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Address = new Schema({
    "address": {
        "type": String,
        "required": true
    },
    "userID": {
        "type": ObjectId,
        "required": true
    }
});

module.exports = mongoose.model("addresses", Address);


