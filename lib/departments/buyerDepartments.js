var departmentRepository = require("./repository/departmentRepository");
var Promise = require("bluebird");
var getUser = require("../auth/user").getUser;
var carrierStatus = require("../delivery/carrier-state/enum");

var BuyersDepartments = {
        get: getDepartments,
        create: addDepartments,
        update: updateDepartments,
        delete: deleteDepartments
};

function getDepartments(userData, scheduledId){
    //console.log("came here getDepartments++");
    var user = getUser();
    var out = {};
    return {};
}

function addDepartments(userData, scheduledId){
    
    var user = getUser();
    var out = {};

    return Promise.all(getScheduledDeliveries())
            .then(function(out){
                return {
                    deliveries: out[0],
                    scheduled: out[1]
                }
            });

}

function updateDepartments(userData, scheduledId){
    
    var user = getUser();
    var out = {};

    return Promise.all(getScheduledDeliveries())
            .then(function(out){
                return {
                    deliveries: out[0],
                    scheduled: out[1]
                }
            });

}

function deleteDepartments(userData, scheduledId){
    
    var user = getUser();
    var out = {};

    return Promise.all(getScheduledDeliveries())
            .then(function(out){
                return {
                    deliveries: out[0],
                    scheduled: out[1]
                }
            });

}

module.exports = BuyersDepartments;
