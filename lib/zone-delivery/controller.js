let DriverDeliveryRepository = require('../carrier-deliveries/repository/driverDeliveryRepository'),
    ScheduledDeliveryRepository = require('../carrier-deliveries/repository/scheduleDeliveriesRepository'),
    PostalCodeRepository = require('../postal-codes/repository'),
    AccountRepository = require('../user/repository/account.js'),
    CarrierRepository = require('../carrier-deliveries/repository/carrierRepository'),
    DeliverySettingsRepository = require('../user/repository/deliverysettings.js'),
    Formatter = require('../scheduled/formatScheduled'),
    moment = require('moment'),
    request = require('request'),
    ScheduledRepository = require('../scheduled/repository/scheduled'),
    EventHistoryService = require('../../services/EventHistoryService'),
    _ = require('lodash');
let checkedRequiredParams = require('../validator/requiredValidation');
let ScheduledDeliveryModel = require('../../models/ScheduledDelivery');
const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var config = require("../config").googlemaps;

const googleKey = config.key;
console.log('googleKey=', googleKey)
var async = require('async');
var SocketService = require('../../services/SocketService');

require('dotenv').config();
require('moment-timezone')
var momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);
typeof moment.duration.fn.format === "function";
typeof moment.duration.format === "function";

let ZoneDeliveryController = {
    getAllForDriver: function (id) {
        let deliveries;

        return new Promise((resolve, reject) => {
            DriverDeliveryRepository.getAssignedDeliveries(id.toObjectId())
                .toArray((err, listOfIds) => {
                    if (err) reject(err);
                    resolve(listOfIds);
                })
        })
            .then(listOfIds => {

                let ids = listOfIds.map((element) => {
                    return element.delivery_id.toObjectId();
                });

                return ScheduledDeliveryRepository.scheduled.findAll(
                    { '_id': { $in: ids } },
                    { creator: 1 })
            })
            .then(data => {
                deliveries = data;
                return AccountRepository.findAll(
                    { '_id': { $in: data.map(x => x.creator) } },
                    { approved: 1 });
            })
            .then(creators => {
                deliveries = deliveries.filter(element => {
                    let creator = creators.find(x => x._id.equals(element.creator));
                    return creator.approved;
                })

                let ids = deliveries.map(x => x._id);

                return ScheduledDeliveryRepository.zone.all(ids)
            })
            .then(data => {
                let formattedDeliveries = data.map(delivery => {
                    return Formatter({ from: delivery });
                });

                return formattedDeliveries;
            })
    },

    findAssignment: function (deliveryId) {
        return ScheduledDeliveryRepository.scheduled.findOne(deliveryId)
            .then(delivery => {
                return AccountRepository.findOne(delivery.creator);
            })
            .then(creator => {
                if (creator.approved) {
                    return DriverDeliveryRepository.findOne({ delivery_id: deliveryId.toString() });
                }
                return;
            })
            .then((result) => {
                return result;
            }).catch(err => {
                Promise.reject(err)
            });
    },

    create: function (body) {
        return ScheduledDeliveryRepository.scheduled.findOne(body.delivery_id.toObjectId())
            .then(delivery => {
                return AccountRepository.findOne(delivery.creator);
            })
            .then(creator => {
                if (creator.approved) {
                    DriverDeliveryRepository.create({
                        carrier_id: body.driver_id,
                        delivery_id: body.delivery_id
                    });

                    let query = {};

                    if (body.order)
                        query.orderIndex = body.order;

                    if (body.estimated_delivery_time)
                        query.estimated_delivery_time = body.estimated_delivery_time;

                    if (body.order && body.estimated_delivery_time)
                        query.status = 2;

                    if (query === {})
                        return Promise.resolve();

                    ScheduledDeliveryRepository.scheduled.update(body.delivery_id.toObjectId(), query);
                }
                return;
            });
    },

    update: function (body) {
        return ScheduledDeliveryRepository.scheduled.findOne(body.delivery_id.toObjectId())
            .then(delivery => {
                return AccountRepository.findOne(delivery.creator);
            })
            .then(creator => {
                if (creator.approved) {
                    let query = {};

                    if (body.order) {
                        query.status = 2;
                        query.orderIndex = body.order;
                    }

                    if (body.estimated_delivery_time)
                        query.estimated_delivery_time = body.estimated_delivery_time;

                    if (query === {})
                        return resolve();

                    ScheduledDeliveryRepository.scheduled.findOneAndUpdate(body.delivery_id.toObjectId(), query);
                    DriverDeliveryRepository.findOneAndUpdate({
                        delivery_id: body.delivery_id
                    }, {
                            carrier_id: body.driver_id
                        });
                }
                return;
            })
    },

    updateMany: function (body) {

        let deliveries;
        let drivers;
        var assignments;
        var plannedDeliveries = [];
        var delivery_clients;

        // var ids = body.map(x =>  x.delivery_id.toObjectId());
        var ids = [];
        var driverIds = [];
        for (let k = 0; k < body.length; k++) {
            if (body[k].selected === true) {
                ids.push(body[k].delivery_id.toObjectId());
                if(body[k].driver_id)
                    driverIds.push(body[k].driver_id.toObjectId());
            }
        }

        return new Promise((resolve, reject) => {
            console.log('ids',ids)
            ScheduledDeliveryRepository.scheduled.findAll(
                { '_id': { $in: ids } },
                { creator: 1 })
                .toArray((err, deliveries) => {
                    //console.log('err, deliveries', err, deliveries)
                    if (err) 
                        reject(err);
                    else 
                        resolve(deliveries);
                })
            })
            .then(data => {
                deliveries = data;
                return AccountRepository.findAll(
                    { '_id': { $in: driverIds } },
                    { approved: 1 });
            })
            .then(data => {
                drivers = data;
                return AccountRepository.findAll(
                    { '_id': { $in: deliveries.map(x => x.creator) } },
                    { approved: 1, groupId: 1 });
            })
            .then(creators => {
                delivery_clients = creators;

                //get valid deliveries
                // deliveries = deliveries.filter(element => {
                //     let creator = creators.find(x => x._id.equals(element.creator));
                //     return creator.approved;
                // })


                body = body.filter(element => {
                    let exists = deliveries.find(x => x._id.equals(element.delivery_id.toObjectId()))
                    if (exists) return true;
                });

                //deliveries which has driver id
                assignments = body.filter(x => x.driver_id);

                var deleteIds = body.map(x => x.delivery_id)
                if(deleteIds.length > 0)
                    return DriverDeliveryRepository.deleteMany(deleteIds);
                else
                    return {}    
            }).then(function () {
                if (assignments && assignments.length > 0) {
                    var newAssignments = assignments.map(x => {
                        return {
                            carrier_id: x.driver_id,
                            delivery_id: x.delivery_id
                        }
                    });

                    return DriverDeliveryRepository.createMany(newAssignments);
                } else {
                    return {}
                }
            }).then(function () {
                var promises = [];
                body.forEach(assignment => {
                    let query = {};

                    if (assignment.order)
                        query.orderIndex = assignment.order;

                    if (assignment.estimated_delivery_time)
                        query.estimated_delivery_time = assignment.estimated_delivery_time;

                    if (assignment.order && assignment.estimated_delivery_time && assignment.driver_id) {
                        query.status = 2;

                        plannedDeliveries.push({
                            delivery_id: assignment.delivery_id,
                            estimated_delivery_time: assignment.estimated_delivery_time
                        });
                    }

                    if (Object.keys(query).length) {
                        var promise = ScheduledDeliveryRepository.scheduled.findOneAndUpdate(assignment.delivery_id.toObjectId(), query);
                        promises.push(promise);
                    }
                });

                return Promise.all(promises);
            }).then(function (data) {
                var promises = [];

                plannedDeliveries.forEach(x => {
                    var eventMessage = `Delivery is planned and is expected to arrive ~${x.estimated_delivery_time}`;
                    var promise = EventHistoryService.deliveryPlannedScheduledDelivery(x.delivery_id, eventMessage);
                    promises.push(promise);
                });
                if (typeof body === 'object') {
                    body.map(function (schedule) {
                        if (schedule.notificationSend == true && schedule.selected == true) {
                            var client = deliveries.find(x => x._id.equals(schedule.delivery_id));
                            var driver = drivers.find(x => x._id.equals(schedule.driver_id));
                            if (client && driver) {
                                var profile = delivery_clients.find(x => x._id.equals(client.creator));

                                var socket = SocketService.getSocket();
                                if (socket) {
                                    socket.emit(`delivery_status_changed_${schedule.delivery_id}`, {status:'driver'});
                                }
                                var clientId = profile.groupId||profile._id.toString()
                                return DeliverySettingsRepository.getByClientId(clientId.toObjectId()).then((setting)=>{
                                    setting = setting||{}
                                    schedule['notificationSetting'] = {
                                        email_notification_setting : setting.email_notification_setting,
                                        sms_notification_setting : setting.sms_notification_setting
                                    }
                                    schedule.deliveryId = schedule.deliveryId||schedule.delivery_id
                                    schedule.deliveryType = schedule.deliveryType||schedule.delivery_type
                                    schedule.driverName = driver.name;
                                    schedule.driverPhone = driver.phone;
                                    schedule.pickupDeadline = client.pickup_deadline || client.pickupdeadline
                                    schedule.pickupDeadlineTo = client.pickup_deadline_to || client.pickupdeadlineto
                                    schedule.pickupAddress = client.pickupaddress || client.pickup_address

                                    if (profile && profile.groupId && profile.groupId != 'undefined') {
                                        AccountRepository.findOne(profile.groupId).then(userProfile => {
                                            if(userProfile)
                                            {
                                                schedule.clientName = userProfile.name;
                                                schedule.clientEmail = userProfile.email;
                                                schedule.clientPhone = userProfile.phone;
                                                //console.log('notification - driver1', schedule)

                                                request.post(process.env.NOTIFY_URL + 'order/driver', { json: schedule }, function (err, resp, bd) {
                                                    if (err) console.error(err)
                                                    //console.log(bd);
                                                });
                                            } else {
                                                schedule.clientName = profile.name;
                                                schedule.clientEmail = profile.email;
                                                schedule.clientPhone = profile.phone;
                                                //console.log('notification - driver2', schedule)

                                                request.post(process.env.NOTIFY_URL + 'order/driver', { json: schedule }, function (err, resp, bd) {
                                                    if (err) console.error(err)
                                                    //console.log(bd);
                                                });
                                            }
                                        });
                                    } else {
                                        //console.log('notification - driver3', schedule)

                                        request.post(process.env.NOTIFY_URL + 'order/driver', { json: schedule }, function (err, resp, bd) {
                                            if (err) console.error(err)
                                            //console.log(bd);
                                        });
                                    }
                                })
                            }
                        }
                    });
                }
                return Promise.all(promises);
            })
    },

    updateStatus: function (status, deliveryIds) {
        let ids = deliveryIds.map(x => {
            return x.toObjectId();
        });

        let deliveries;

        return new Promise((resolve, reject) => {
            ScheduledDeliveryRepository.scheduled.findAll(
                { '_id': { $in: ids } },
                { creator: 1 })
                .toArray((err, deliveries) => {
                    if (err) reject(err);
                    resolve(deliveries);
                })
        })
            .then(data => {
                deliveries = data;
                return AccountRepository.findAll(
                    { '_id': { $in: data.map(x => x.creator) } },
                    { approved: 1 });
            })
            .then(creators => {
                deliveries = deliveries.filter(element => {
                    let creator = creators.find(x => x._id.equals(element.creator));
                    return creator.approved;
                })

                ids = deliveries.map(x => x._id);

                var promises = [];
                ids.forEach((item) => {
                    if (parseInt(status) === 3) {
                        var promise = EventHistoryService.statusUpdatedScheduledDelivery(item.toString(), 3);
                        promises.push(promise);
                    }
                });

                return Promise.all(promises);
            })
            .then(data => {
                return ScheduledDeliveryRepository.scheduled.updateStatus(ids, status)
            })
            .then(data => {
                ids.forEach(id => {
                    ScheduledRepository.getById(id)
                        .then((delivery) => {
                            ScheduledRepository.updateDeliveryStatus(id.toString(), status)
                                .then((updatedDelivery) => {
                                    updatedDelivery = updatedDelivery._doc;
                                    if (updatedDelivery.status == 2) {
                                        let end_time = updatedDelivery.estimated_delivery_time ? updatedDelivery.estimated_delivery_time : updatedDelivery.deliverywindowend;
                                        let schedule = {
                                            deliverydate: updatedDelivery.deliverydate,
                                            deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                                            recipientemail: updatedDelivery.recipientemail,
                                            recipientname: updatedDelivery.recipientname,
                                            recipientphone: updatedDelivery.recipientphone['country_dial_code'] + updatedDelivery.recipientphone['phone'],
                                            deliveryId: id,
                                            track_url: process.env.TRACK_URL + id
                                        }

                                        //Notification
                                        //console.log('notification - create3', schedule)

                                        request.post(process.env.NOTIFY_URL + 'order/create', { json: schedule }, function (err, resp, bd) {
                                            if (err) console.error(err)
                                            //console.log(bd);
                                        })
                                        //end

                                        //emit socket event
                                        var socket = SocketService.getSocket();
                                        if (socket) {
                                            socket.emit(`delivery_status_changed_${schedule.deliveryId}`, {status:'create'});
                                        }
                                    }
                                    // else if (updatedDelivery.status == 3) {
                                    //     let end_time = updatedDelivery.estimated_delivery_time;
                                    //     let schedule = {
                                    //         deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                                    //         recipientemail: updatedDelivery.recipientemail,
                                    //         recipientname: updatedDelivery.recipientname,
                                    //         recipientphone: updatedDelivery.recipientphone.country_dial_code + updatedDelivery.recipientphone.phone,
                                    //         delivery_id: id,
                                    //         track_url: process.env.TRACK_URL + id
                                    //     }
                                    //     //Notification
                                    //     request.post(process.env.NOTIFY_URL + 'order/complete', { json: schedule }, function (err, resp, bd) {
                                    //         if (err) console.log(err)
                                    //         console.log(bd);
                                    //     })
                                    //     //end
                                    // }

                                });
                        });
                });
            });
    },

    updateDate: function (date, deliveryIds, changeIdStatus) {
        let ids;
        if (changeIdStatus) {
            ids = deliveryIds.map(x => {
                return x
            });
        } else {
            ids = deliveryIds.map(x => {
                return x.toObjectId();
            });
        }

        let deliverydate = new Date(date);

        if (isNaN(deliverydate.getTime())) {
            return;
        }

        let weekNumber = deliverydate.getISOWeek().toString();
        let deliverydayofweek = days[deliverydate.getUTCDay()];

        let deliveries;

        return new Promise((resolve, reject) => {
            ScheduledDeliveryRepository.scheduled.findAll(
                { '_id': { $in: ids } },
                { creator: 1 })
                .toArray((err, deliveries) => {
                    if (err) reject(err);
                    resolve(deliveries);
                })
        })
            .then(data => {
                deliveries = data;
                return AccountRepository.findAll(
                    { '_id': { $in: data.map(x => x.creator) } },
                    { approved: 1 });
            })
            .then(creators => {
                deliveries = deliveries.filter(element => {
                    let creator = creators.find(x => x._id.equals(element.creator));
                    return creator.approved;
                })

                return ScheduledDeliveryRepository.scheduled.updateDate(deliveries.map(x => x._id), deliverydate, weekNumber, deliverydayofweek);
            })
    },

    deleteMany: function (deliveryIds) {
        let ids = deliveryIds.map(x => {
            return x.toObjectId();
        });
        let deliveries;

        return new Promise((resolve, reject) => {
            ScheduledDeliveryRepository.scheduled.findAll(
                { '_id': { $in: ids } },
                { creator: 1 })
                .toArray((err, deliveries) => {
                    if (err) reject(err);
                    resolve(deliveries);
                })
        })
            .then(data => {
                deliveries = data;
                return AccountRepository.findAll(
                    { '_id': { $in: data.map(x => x.creator) } },
                    { approved: 1 });
            })
            .then(creators => {
                deliveries = deliveries.filter(element => {
                    let creator = creators.find(x => x._id.equals(element.creator));
                    return creator.approved;
                })

                return ScheduledDeliveryRepository.scheduled.deleteMany(deliveries.map(x => x._id));
            })
    },

    delete: function (deliveryId) {
        return ScheduledDeliveryRepository.scheduled.findOne(deliveryId.toObjectId())
            .then(delivery => {
                return AccountRepository.findOne(delivery.creator);
            })
            .then(creator => {
                if (creator.approved) {
                    return DriverDeliveryRepository.delete({
                        delivery_id: deliveryId
                    });
                }
                return;
            });
    },

    forDate: function (date) {
        var data = [];
        return new Promise((resolve, reject) => {
            let startDate = moment.utc(date, "MM-DD-YYYY").startOf('day');
            let endDate = moment.utc(date, "MM-DD-YYYY").add(1, "days");

            ScheduledDeliveryRepository.scheduled.forDate(startDate, endDate)
                .toArray((err, deliveries) => {
                    //console.log("deliveries", deliveries)
                    if (err) reject(err);

                    resolve(deliveries);
                });
        }).then(deliveries => {
            data = deliveries;
            return AccountRepository.findAll(
                { '_id': { $in: deliveries.map(x => x.creator) } },
                { name: 1, approved: 1 });
        }).then(creators => {

            let deliveries = data.filter(element => {
                let creator = creators.find(x => x._id.equals(element.creator));
                return creator.approved;
            })

            deliveries = deliveries.map(element => {
                let creator = creators.find(x => x._id.equals(element.creator));
                element.creatorName = creator && creator.name ? creator.name : '';
                return element;
            });

            return Promise.all(deliveries.map(delivery => {
                return ZoneDeliveryController.appendDeliveryData(delivery);
            }));
        });
    },

    forDatePeriod: function (fromDate, toDate) {
        var data = [];
        return new Promise((resolve, reject) => {
            let startDate = moment.utc(fromDate, "MM-DD-YYYY").startOf('day');
            let endDate = moment.utc(toDate, "MM-DD-YYYY").add(1, "days");

            ScheduledDeliveryRepository.scheduled.forDate(startDate, endDate)
                .toArray((err, deliveries) => {
                    //console.log("deliveries", deliveries)
                    if (err) reject(err);

                    resolve(deliveries);
                });
        }).then(deliveries => {
            data = deliveries;
            return AccountRepository.findAll(
                { '_id': { $in: deliveries.map(x => x.creator) } },
                { name: 1, approved: 1 });
        }).then(creators => {

            let deliveries = data.filter(element => {
                let creator = creators.find(x => x._id.equals(element.creator));
                return creator.approved;
            })

            deliveries = deliveries.map(element => {
                let creator = creators.find(x => x._id.equals(element.creator));
                element.creatorName = creator && creator.name ? creator.name : '';
                return element;
            });

            return Promise.all(deliveries.map(delivery => {
                return ZoneDeliveryController.appendDeliveryData(delivery);
            }));
        });
    },

    clientPickups: function (creator) {
        return ZoneDeliveryController.getPickups(null, creator, false);
    },

    clientNewPickups(creator, startDate) {
        return ZoneDeliveryController.getPickups(startDate, creator, true);
    },

    pickups: function (date) {
        return ZoneDeliveryController.getPickups(date, null, false);
    },

    getPickups: function (date, creator, showNew) {
        let data = [];
        return new Promise((resolve, reject) => {
            ScheduledDeliveryRepository.scheduled.pickups(creator, date, showNew)
                .toArray((err, data) => {
                    if (err) reject(err);
                    resolve(data);
                });
        })
            .then((res) => {
                data = res;
                return AccountRepository.findAll(
                    { '_id': { $in: data.map(item => item._id.creator) } },
                    { name: 1, approved: 1 });
            }).then((creators) => {
                if (!creator) {
                    data = data.filter(element => {
                        let creatorObj = creators.find(item => item._id.equals(element._id.creator));
                        return creatorObj && creatorObj.hasOwnProperty("approved") ? creatorObj.approved : false;
                    });
                }

                data = data.map(element => {
                    let creatorObj = creators.find(item => item._id.equals(element._id.creator));
                    return Object.assign({}, element._id, {
                        creator: {
                            id: element._id.creator,
                            name: creatorObj.name
                        },
                        deliveriesCount: element.deliveriesCount
                    });
                });

                return data.filter(x => x.creator.name);
            }).then((res) => {
                data = res;
                return DeliverySettingsRepository.getByClientIds(data.map(x => x.creator.id));
            }).then((deliverySettings) => {
                if (deliverySettings.length <= 0) {
                    return data;
                }

                return data.map(element => {
                    let deliverySetting = deliverySettings.find(item => item._doc.clientId.equals(element.creator.id)) || null;

                    if (!deliverySetting) {
                        return element;
                    }

                    element.pickup_deadline = deliverySetting.pickup_deadline;
                    element.pickup_deadline_to = deliverySetting.pickup_deadline_to;
                    element.delivery_window_start = deliverySetting.delivery_window_start;

                    return element;
                });
            }).then(data => {
                // when pickup window is a later date than delivery window, assume pickup happens on the previous day
                data.map(x => {
                    if (moment(x.pickup_deadline, 'HH:mm') > moment(x.delivery_window_start, 'HH:mm')) {
                        x.date.setDate(x.date.getDate() - 1);
                    }
                });

                return data.sort(function (a, b) {
                    return (a.creator.name).localeCompare((b.creator.name)) || (new Date(b.date)).getTime() - (new Date(a.date)).getTime();
                });
            });
    },

    appendDeliveryData: function (delivery) {
        return ZoneDeliveryController.assignZipCodeToDelivery(delivery)
            .then(delivery => {
                return ZoneDeliveryController.appendCarrierAssignmentToDelivery(delivery);
            });
    },

    assignZipCodeToDelivery: function (delivery) {
        if (!delivery.deliveryzip || isNaN(delivery.deliveryzip)) {
            return new Promise((resolve, reject) => {
                resolve(delivery)
            })
        }

        return PostalCodeRepository.getForZipCode(delivery.deliveryzip)
            .then((postalObject) => {
                if (postalObject) {
                    delivery.area = postalObject.AREA;
                    delivery.zone = postalObject.ZONE;
                }

                return delivery;
            });
    },

    appendCarrierAssignmentToDelivery: function (delivery) {
        return ZoneDeliveryController.findAssignment(delivery._id).then(assignment => {
            if (assignment && assignment.carrier_id)
                delivery.assigned_to = assignment.carrier_id;

            return delivery;
        });
    },

    codeVerification: function (driverId, deliveryId) {
        let resObj = {};
        return ScheduledDeliveryRepository.scheduled.findOne(deliveryId.toObjectId())
            .then(delivery => {
                if (delivery) {
                    if(delivery.status == 6 || delivery.type == 'return') {
                        console.log("Delivery was returned")
                        resObj.error_message = "Package should not be delivered since it needs to be returned to the warehouse"
                        resObj.verification_status = false;
                        return resObj;
                    }
                    // if(delivery.status != 3) {
                    //     resObj.verification_status = true;
                    //     resObj.delivery_id = deliveryId;
                    //     return resObj;
                    // } else {
                    //     console.log("Delivery was completed")
                    //     resObj.error_message = "Delivery was completed"
                    //     resObj.verification_status = false;
                    //     return resObj;
                    // }
                    resObj.verification_status = true;
                    resObj.delivery_id = deliveryId;
                    return resObj;
                } else {
                    console.log("Delivery was not found")
                    resObj.error_message = "Delivery was not found"
                    resObj.verification_status = false;
                    return resObj;
                }
            }).catch(err => {
                console.log("QRCode is wrong")
                resObj.verification_status = false;
                resObj.error_message = "QRCode is wrong"
                return resObj;
            })
    },
    submitQrWithDetail: function (body) {
        console.log('=======> Checkin-cargo request', body)
        let params = ['driver_id', 'timestamp', 'location', 'is_current_time', 'delivery_ids'];
        let bodyParams = body;
        let message = checkedRequiredParams(bodyParams, params);
        let responseObject = {};
        let errorType = ''
        if (message == true) {
            params = ['delivery_id'];
            let reuiredArryParam = true;
            body.delivery_ids.map(x => {
                message = checkedRequiredParams(x, params)
                if (message != true) { reuiredArryParam = message; }
            });
            if (reuiredArryParam == true) {
                let deliveries;
                let driverId = body.driver_id;
                let startLocation = body.location.replace(/ /g, ''); //"55.722582,12.6082901"
                let startTimeDate = body.timestamp;
                let isCurrentTime = body.is_current_time;
                let today = moment.tz("Europe/Copenhagen");
                let startDate = today.format('YYYY-MM-DD');
                let startTime = today.format('HH:mm');
                let ids = []
                let maxOrderIndex = 0
                if(!isCurrentTime) {
                    startTime = startTimeDate.split(" ")[1];
                    startDate = startTimeDate.split(" ")[0];
                }


                 console.log("startTime", startTime);
                 console.log("startDate", startDate);
                 console.log('requested delivery count', body.delivery_ids.length)
                let bufferTime = 180; //changed by anibal request, 2018/12/18
                let outOfLatLongFlag = true;
                return new Promise((resolve, reject) => {
                    //get request date's in-progress deliveries
                    let sDate = moment(startDate, "YYYY-MM-DD").startOf('day');
                    let eDate = moment(startDate, "YYYY-MM-DD").add(1, "days");

                    ScheduledDeliveryRepository.scheduled.findAll(
                    { status:{$in:[2, 3]}, type:{$ne:"return"}, deliverydate:{$gte: sDate.toDate(), $lt: eDate.toDate()} }, {_id:1, status:1, orderIndex:1})
                    .toArray((err, deliveries) => {
                        if (err) reject(err);
                        var inProgressDeliveryIds = deliveries.filter(o=>o.status==2).map((d=>d._id.toString()))
                        var deliveryIds = deliveries.map((d=>d._id.toString()))


                        //get current driver's in-progress deliveries
                        DriverDeliveryRepository.findAll({carrier_id:driverId, delivery_id:{$in:deliveryIds}}).toArray((err, driverDeliveries)=>{
                            if (err) reject(err);
                            driverDeliveryIds = driverDeliveries.map((o)=>{
                                return o.delivery_id
                            })
                            console.log('driver deliveries', driverDeliveryIds.length)
                            ids = driverDeliveryIds.filter((o)=>{
                                return inProgressDeliveryIds.indexOf(o) != -1
                            })
                            console.log('driver in-progress deliveries', ids.length)

                            oldOrderIndexes = deliveries.filter(o=>driverDeliveryIds.indexOf(o._id.toString()) != -1).map(o=>Number(o.orderIndex))
                            if(oldOrderIndexes && oldOrderIndexes.length > 0) {
                                maxOrderIndex = Math.max(...oldOrderIndexes)
                            }
                            console.log('maxOrderIndex', maxOrderIndex)                       

                            body.delivery_ids.map((delivery)=>{
                                if(ids.indexOf(delivery.delivery_id) == -1) {
                                    ids.push(delivery.delivery_id)
                                }
                            })

                            ids = ids.map(id => id.toObjectId());
                            console.log('total delivery count', ids.length)
                            ScheduledDeliveryRepository.scheduled.findAll({ '_id': { $in: ids } })
                            .toArray((err, deliveries) => {
                                if (err) reject(err);
                                resolve(deliveries);
                            })
                        })
                    })
                })
                .then(deliveries=>{
                    return Promise.all(deliveries.map(delivery => {
                        return ZoneDeliveryController.appendDeliveryData(delivery);
                    }));
                })
                .then(function (data) {
                    allDeliveries = _.sortBy(data, (o)=>{
                        return o.area + o.zone
                    })
                    console.log('deliveries', allDeliveries.length)
                    var routeList = []
                    var MAX_ROUTING_SIZE = 100
                    for(var i = 0 ; i < Math.ceil(allDeliveries.length/MAX_ROUTING_SIZE) ; i++) {
                        routeList.push(allDeliveries.slice(i * MAX_ROUTING_SIZE, (i + 1) * MAX_ROUTING_SIZE))
                    }
                    var startDeliveryTime = startTime
                    var skipIndex = 0
                    return new Promise((resolve, reject) => {
                        async.mapSeries(routeList, (deliveries, callback)=>{
                            var ways = deliveries.map((o)=>(String(o.deliverycoordinates[1]) + "," + String(o.deliverycoordinates[0])))

                            var request = require("request");
                            var url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${startLocation}&destinations=${ways.join('|')}&key=${googleKey}`;
                            var options = {
                                method: 'GET',
                                url: encodeURI(url),
                                //qs: { origins: startLocation, destinations: ways, key: googleKey }
                            };
                        
                        
                            request(options, function (error, response, body) {
                                if (error) {
                                    responseObject.responseStatus = "failure";
                                    responseObject.message = 'Service error';
                                    return responseObject;
                                }
                                body = JSON.parse(body)
    
                                if(body.status != 'OK') {
                                    responseObject.responseStatus = "failure";
                                    responseObject.message = 'Please provide valid latitude and longitude.';
                                    return responseObject;
                                }
    
                                var index = 0, maxDuration = 0, idx=0;
    
                                _.forEach(body.rows[0].elements, (o)=>{
                                    if(o.duration.value > maxDuration) {
                                        maxDuration = o.duration.value
                                        index = idx
                                    }
                                    idx++
                                })
    
                                destination = ways[index]
                                ways.splice(index, 1)
                                lastDelivery = deliveries[index]
                                deliveries.splice(index, 1)
                                deliveries.push(lastDelivery)
    
                                //google 
                                // var url = `https://maps.googleapis.com/maps/api/directions/json?origin=${startLocation}&destination=${destination}&waypoints=optimize:true|${ways.join('|')}&key=${googleKey}`;
                                // console.log('url', url)
                                // var options = {
                                //     method: 'GET',
                                //     url: encodeURI(url)
                                // };

                                //routexl
                                var url = `https://api.routexl.com/tour/`;
                                var routerxl_username = 'anibal@goidag.com'
                                var routerxl_password = '32234#%w2!!!12#32'
                                var locations = []
                                var startPoint = startLocation.split(",")
                                locations.push({"address":"1","lat":startPoint[0],"lng":startPoint[1]})
                                deliveries.map((delivery, index)=>{
                                    locations.push({
                                        "address":String(index + 2),
                                        "lat":String(delivery.deliverycoordinates[1]),
                                        "lng":String(delivery.deliverycoordinates[0])
                                    })
                                })

                                var requestData = `locations=` + JSON.stringify(locations)
                                var options = {
                                    method: 'POST',
                                    url: encodeURI(url),
                                    body: requestData,
                                    headers: {
                                        'Authorization':'Basic ' + Buffer.from(`${routerxl_username}:${routerxl_password}`).toString('base64')
                                    }
                                };    

                                request(options, function (error, response, body) {
                                    if (error) {
                                        errorType = 'SERVICE_ERROR'
                                        return reject(error)
                                    }
                                    body = JSON.parse(body)
                                    console.log('directions result', error, body)
                                    if(body.id && body.route) {
                                        var lastTime = startDeliveryTime
                                        for(var idx = 1 ; idx <= deliveries.length ; idx++) {
                                            var leg = body.route[String(idx)]
                                            var prior = body.route[String(idx-1)]
                                            var deliveryIndex = Number(leg.name) - 2
                                            var delivery = deliveries[deliveryIndex]
                                            //console.log('delivery',delivery)
                                            var duration = (leg.arrival - prior.arrival) * 60 + bufferTime
                                            lastTime = moment(lastTime, 'HH:mm').add(duration, 's').format('HH:mm')
                                            delayedlastTime = moment(lastTime, 'HH:mm').add(15, 'm').format('HH:mm')
                                            delayedEndTime = moment(delivery.deliverywindowend, 'HH:mm').add(15, 'm').format('HH:mm')
                                            delivery.deliverydate = startDate + 'T00:00:00.000Z'
                                            // if(lastTime > delayedEndTime) {
                                            //     //console.log('exceed', i, delivery, lastTime)
                                            //     errorType = 'EXCEED_DELIVERY_WINDOW'
    
                                            //     return reject("Exceed delivery window:" + delivery._id)
                                            // }
    
                                            delivery.estimated_delivery_time = delayedlastTime
                                            delivery.orderIndex = (skipIndex + idx)
                                        }
                                        startLocation = destination
                                        skipIndex += MAX_ROUTING_SIZE
                                        startDeliveryTime = lastTime
                                        return callback(null, deliveries)
                                    } else {
                                        errorType = 'WRONG_LOCATION'
                                        outOfLatLongFlag = false
                                    } 
                                    return reject("Cannot routing:" + url)
                                })
            
                            })
                        }, (err, rets)=>{
                            if(err) {
                                console.log("routing error", err)
                                errorType = 'UNKNOWN_ERROR'
                                return reject("Cannot routing")
                            }
                            var finalDeliveries = []
                            _.map(rets, (deliveries)=>{
                                finalDeliveries = finalDeliveries.concat(deliveries)
                            })

                            resolve(finalDeliveries)
                        })
                    })
                }).then(data => {
                    //console.log('data', data)
                    var promises = [];
                    maxOrderIndex = maxOrderIndex||0
                    data.forEach(updatedDate => {
                        var orderIndex = ('0' + String(maxOrderIndex + Number(updatedDate.orderIndex||1)))
                        orderIndex = orderIndex.substr(orderIndex.length - 2)
                        let deliveryDate = new Date(updatedDate.deliverydate);
                        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                        let query = {
                            orderIndex: orderIndex,
                            status: 2,
                            estimated_delivery_time: updatedDate.estimated_delivery_time,
                            deliverydate: updatedDate.deliverydate,
                            deliverydayofweek:days[deliveryDate.getUTCDay()],
                            weekNumber:deliveryDate.getISOWeek().toString()
                        }
                        //console.log("::query", query);
                        updatedDate.deliveryId = updatedDate._id.toString();
                        var promise = ScheduledRepository.findAndUpdateQuery(updatedDate.deliveryId, query).then(updatedDelivery => {
                            //console.log("updatedDelivery", updatedDelivery)
                            updatedDelivery = updatedDelivery._doc;
                            let end_time = updatedDelivery.estimated_delivery_time;
                            let schedule = {
                                deliverydate: updatedDelivery.deliverydate,
                                deliverywindowend: end_time ? end_time.substring(end_time.indexOf("-") + 2) : '',
                                recipientemail: updatedDelivery.recipientemail,
                                recipientemail: updatedDelivery.recipientemail,
                                recipientname: updatedDelivery.recipientname,
                                recipientphone: updatedDelivery.recipientphone['country_dial_code'] + updatedDelivery.recipientphone['phone'],
                                deliveryId: updatedDate.deliveryId,
                                deliveryType: updatedDelivery.deliverytype,
                                track_url: process.env.TRACK_URL + updatedDate.deliveryId
                            }
                            // console.log('schedule', schedule)                    
                            // Notification
                            //if(updatedDate.status == 1) {
                                // request.post(process.env.NOTIFY_URL + 'order/driver', { json: schedule }, function (err, resp, bd) {
                                //     if (err) console.error(err)
                                //     //console.log(bd);
                                // })
                            //}

                            //emit socket event
                            var socket = SocketService.getSocket();
                            if (socket) {
                                socket.emit(`delivery_status_changed_${schedule.delivery_id}`, {status:'driver'});
                            }

                            return updatedDelivery;
                        })
                        promises.push(promise);
                    })
                    return Promise.all(promises);
                }).then(data => {
                    var deleteIds = ids.map(x => {
                        return x.toString()
                    });
                    return DriverDeliveryRepository.deleteMany(deleteIds).then(()=>{
                        var newAssignments = ids.map(x => {
                            return {
                                carrier_id: driverId,
                                delivery_id: x.toString()
                            }
                        });
                        return DriverDeliveryRepository.createMany(newAssignments);
                    })
                }).then(data => {
                    // console.log("::data", data)
                    if (data.result && data.result.ok && outOfLatLongFlag == true) {
                        responseObject.responseStatus = true;
                        return responseObject;
                    } else {
                        responseObject.responseStatus = false;
                        return responseObject;
                    }
                }).catch(err => {
                    console.log('error', err)
                    responseObject.responseStatus = false;
                    if (errorType == 'WRONG_LOCATION') {
                        responseObject.message = 'Please provide valid latitude and longitude.';
                    } else if(errorType == 'EXCEED_DELIVERY_WINDOW') {
                        responseObject.message = 'You cannot delivery within specified delivery window';
                    } else if(errorType == 'SERVICE_ERROR') {
                        responseObject.message = 'Service error';
                    } else if(errorType == 'UNKNOWN_ERROR') {
                        responseObject.message = 'Service error';
                    } else {
                        responseObject.message = 'Service error';
                    }
                    return responseObject;
                })
            } else {
                responseObject.message = reuiredArryParam;
                responseObject.responseStatus = false;
                return responseObject;
            }
        } else {
            responseObject.message = message;
            responseObject.responseStatus = false;
            return responseObject;
        }
    },
};

function getAddress(address, zip, city) {
    return address + ", " + zip  + " " + city;
}

module.exports = ZoneDeliveryController;
