class ResponseError {
    constructor(code, message) {
        this.code = code;
        this.message = message;
    }

    getError() {
        var error = new Error(this.message);
        error.status = this.code;

        return error;
    }
}

module.exports = ResponseError;