var jwt = require("jsonwebtoken");
var passport = require("passport");
var JwtStrategy = require("passport-jwt").Strategy;
var ExtractJwt = require("passport-jwt").ExtractJwt;
var accountRepository = require("../user/repository/account");
var secret = "2F4AD2A54DB4D6852C28C1949B6AA";
var AuthUser = require("../auth/user");
var options = {
    secretOrKey: secret,
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    ignoreExpiration: true
};
passport.use(new JwtStrategy(options, function(jwt_payload, done){
    accountRepository
        .getById(jwt_payload.sub)
        .then(function(user){
            if(!user){
                return done(new Error("Invalid User"), false);
            }
            AuthUser.setUser(user);
            done(null, user);
        })
        .catch(function(err){
            done(err, false)
        });
}));

var AuthToken = {
    sign: sign,
    verify: verify
}

function sign(account) {
    var payload = {
        sub: account._id,
        name: account.name
    };
    
    return jwt.sign(payload, secret);
}

function verify(token) {
    var token = token.split(" ");
    return jwt.verify(token[1], secret);
}

module.exports = AuthToken;
