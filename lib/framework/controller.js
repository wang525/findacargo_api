var express = require('express');
var JaySchema = require("jayschema");
var passport = require("passport");
var authToken = require("./auth-token");
var AuthUser = require("../auth/user");
var q = require('q');
const MessageGenerator = require('../validator/MessageGenerator');
var ResponseError = require('./responseError');
var ApiRequestLogModel = require('../../models/apirequestlogs')

// Returns ISO 8601 week number and year
Date.prototype.getISOWeek = function () {
    var jan1, week, date = new Date(this);
    date.setDate(date.getUTCDate() + 4 - (date.getUTCDay() || 7));
    jan1 = new Date(date.getUTCFullYear(), 0, 1);
    week = Math.ceil((((date - jan1) / 86400000) + 1) / 7);
    return week;
};

function ResponseHandler(req, res) {
    return function(data) {
        logRequest(null, req, data);

        if (data && data.http_status) {
            res.status(data.http_status);
            return res.json(data.http_body);
        }

        res.json(data);
    }
}

function ErrorHandler(req, next) {
    return function(err) {
        logRequest(err, req);

        if (ResponseError.prototype.isPrototypeOf(err)) {
            console.error(err.message);
            return next(err.getError());
        }

        err.status = 505;
        console.error(err);
        next(err);
    }
}

function logRequest(err, req, data) {
    try {
        let log = new ApiRequestLogModel({
            date: new Date(),
            method: req.method,
            url: req.url,
            userToken: req.headers.token
        });

        if (err) {
            log.succeeded = false;
            log.response = err;
        } else {
            log.succeeded = true;
            log.response = data;
        }

        ApiRequestLogModel.create(log);
    } catch (err) {
        // nothing
    }
}

function UserAgent(req, res, next)
{
    console.error(req);
}

function Controller(actions, options) {

    if(typeof options === "undefined") {
        options = {};
    }



    var js = new JaySchema();

    js.addFormat("time2", function(time){
        if(time.length === 5){
            var timeRegex = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/
            if(timeRegex.test(time)) {
                return null;
            }
        }
        
        if(time.length === 8){
            var timeRegex = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/
            if(timeRegex.test(time)) {
                return null;
            }
        }
        return "Invalid Time";
    });

    var loadParams = function(options, req, res, next) {
        var out = [];

        for(var i in req.params) {
            out.push(req.params[i]);
        }
        if (typeof req.headers['authorization'] != 'undefined') {
          //  out.push(req.headers['authorization']);
        }
        return out;
    };

    var validateSchema = function(options, req, data) {
        if(typeof options.schema !== "undefined"){
            var deferred = q.defer();
            var schema = require('../'+options.schema);
            var instance = req.body;

            js.validate(instance, schema, function(errs){
                if(errs) {
                    console.error(errs);
                    let desc = errs[0].desc || MessageGenerator(errs[0].kind, errs[0].constraintValue, errs[0].testedValue, errs[0].instanceContext);
                    return deferred.reject("Input validation: "+ desc);
                }
                
                data.push(instance);
                deferred.resolve(data);
            });
            return deferred.promise;
        }

        return data;
    }

    function Action(action, options){
        if(typeof options === "undefined") {
            options = {};
        }

        if(typeof action !== "function") {
            throw new Error("Invalid Action: "+action);
        }
        var stack = [];

        if(options.auth !== false){
            stack.push(passport.authenticate('jwt', {session:false}));

            function validateAdminRestriction(req, res, next){
                if(!ControllerActions.adminLevel) {
                    return next();
                }

                var user = AuthUser.getUser();

                if(user.category !== 3){
                    var err = new Error("Admin required");
                    err.status = 401
                    return next(err);
                }

                return next();
            }

            stack.push(validateAdminRestriction);
        }

        var UserAgent = function(req, res, next)
        {
            var ua = req.headers['user-agent'],
            $ = {};

            if (/mobile/i.test(ua))
                $.Mobile = true;

            if (/like Mac OS X/.test(ua)) {
                $.iOS = /CPU( iPhone)? OS ([0-9\._]+) like Mac OS X/.exec(ua)[2].replace(/_/g, '.');
                $.iPhone = /iPhone/.test(ua);
                $.iPad = /iPad/.test(ua);
            }

            AuthUser.setIsIOS(false);
            if(/iPhone|iPod/.test(ua)) {
                AuthUser.setIsIOS(true);
            }

            if (/Android/.test(ua))
                $.Android = /Android ([0-9\.]+)[\);]/.exec(ua)[1];

            if (/webOS\//.test(ua))
                $.webOS = /webOS\/([0-9\.]+)[\);]/.exec(ua)[1];

            if (/(Intel|PPC) Mac OS X/.test(ua))
                $.Mac = /(Intel|PPC) Mac OS X ?([0-9\._]*)[\)\;]/.exec(ua)[2].replace(/_/g, '.') || true;
                
            if (/Windows NT/.test(ua))
                $.Windows = /Windows NT ([0-9\._]+)[\);]/.exec(ua)[1];

            AuthUser.setUserAgent(ua);
        };

        var appendAPIToken = function (data, req) {
            if (req.headers.token)
                data.push(req.headers.token);
            return data;
        };

        var apiAction = function (req, res, next) {

                UserAgent(req, res, next);
            
            q(loadParams(options, req, res, next))
            .then(function(data){
                return validateSchema(options, req, data);
            })
            .then((data) => {
                return appendAPIToken(data, req)
            })
            .then(function(data){
                return action.apply(this, data);
            })
            .then(ResponseHandler(req, res))
            .catch(ErrorHandler(req, next))
        };

        stack.push(apiAction);

        return stack;
    }

    function setAdmin(value) {
        ControllerActions.adminLevel = value;
    }

    var ControllerActions = {
        adminLevel: false,
        setAdmin: setAdmin
    };
    for(var i in actions){
        ControllerActions[i] = Action(actions[i], options[i])
    }

    return ControllerActions;
}

module.exports = Controller;
