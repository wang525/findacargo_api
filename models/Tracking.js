let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Tracking = new Schema({
    carrier_id: {
        type: Schema.ObjectId,
        ref: 'accounts'
    },
    truck_id: {
        type: Schema.ObjectId,
        ref: 'trucks'
    },
    latitude: Number,
    longitude: Number,
    created_at: {
        type: Date,
        default: new Date()
    },
    updated_at: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model('Tracking', Tracking);