/**
 * Created by jeton on 7/21/2017.
 */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let DeliverySettingsSchema = new Schema({
    clientId: Schema.Types.ObjectId,
    addresses:  [
        {
            value: String,
            default: Boolean
        }
    ],
    pickup_deadline: String,
    delivery_window_start: String,
    delivery_window_end: String,
    allow_to_reschedule: Boolean,
    allow_signature: Boolean,
    allow_to_send_email: Boolean,
    email_notification_setting:{
        delivery_created: Boolean,
        delivery_assigned:Boolean,
        delivery_status_changed:Boolean,
        delivery_arrive_before:Boolean,
        delivery_finished:Boolean,
        delivery_delayed:Boolean
    },
    allow_to_send_SMS: Boolean,
    sms_notification_setting:{
        delivery_created: Boolean,
        delivery_assigned:Boolean,
        delivery_status_changed:Boolean,
        delivery_arrive_before:Boolean,
        delivery_finished:Boolean,
        delivery_delayed:Boolean
    },
    allow_to_receive_daily_report: Boolean,
    allow_to_receive_end_of_day_report: Boolean,
    allow_to_receive_end_of_week_report: Boolean,
    allow_to_receive_issue_report: Boolean,
    home_delivery: {
        type: Boolean,
        required: [true, 'home_delivery is required']
    },
    store_pickup: {
        type: Boolean,
        required: [true, 'store_pickup is required']
    },
    week_days_available: {
        type: Array,
        required: [true, 'week_days_available is required']
    },
    zip: [
        {
            range_from: {
                type: String,
                // required: [true, 'range_from is required']
            },
            range_to: {
                type: String
                // required: [true, 'range_to is required']
            },
            price: {
                value: Number,
                currency: String
            }
        }
    ],
    allowed_pickups_zones: [
        {
            range_from: {
                type: String,
            },
            range_to: {
                type: String
            }
        }
    ],
    delivery_windows: [
        {
            zip_from: String,
            zip_to: String,
            time_from: String,
            time_to: String
        }
    ],
    pickup_deadline: String,
    pickup_deadline_to: String,
    delivery_window_start: String,
    dropshipping_enabled: Boolean,
    webshipr_integration_enabled: Boolean,
    deliveryaddress: String,
    default_note: String
});


let deliverySettings = mongoose.model('deliverySettings', DeliverySettingsSchema);
module.exports = deliverySettings;