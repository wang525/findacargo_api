let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Departments = new Schema({
    name: {
        type: String
    },
    typeOfDelivery: {
        type: String
    },
    default: {
        type: Boolean
    },
    editorEnabled: {
        type: Boolean
    }
}, {
    _id : false
});

let Company = new Schema({
    departments: {
        type: [Departments]
    },
    companyId: {
        type: Schema.ObjectId
    },
    companyName: {
        type: String
    }
});

module.exports = mongoose.model('departments', Company);