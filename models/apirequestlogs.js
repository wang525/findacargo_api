let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ApiRequestLogSchema = new Schema({
    date: Date,
    method: String,
    url: String,
    userToken: String,
    succeeded: Boolean,
    response: Schema.Types.Mixed
});

let apirequestlogs = mongoose.model('apirequestlogs', ApiRequestLogSchema);
module.exports = apirequestlogs;