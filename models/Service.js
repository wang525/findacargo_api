let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Service = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    enabled: {
        type: Boolean
    },
    delivery_area: {
        type: String
    },
    delivery_window: {
        type: String
    },
    delivery_window_default: {
        type: String
    }
});

module.exports = mongoose.model('services', Service);