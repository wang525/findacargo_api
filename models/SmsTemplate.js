let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let SmsTemplate = new Schema({
    client_id: {
        type: Schema.ObjectId,
        ref: 'accounts'
    },
    content: {
        type: String
    },
    type: {
        type: String
    },
    created_at: {
        type: Date,
        default: new Date()
    },
    updated_at: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model('sms_templates', SmsTemplate);