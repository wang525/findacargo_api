let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let StreetAddr = new Schema({
    dm_id: {
        type: String,
        required: true
    },
    adresseringsnavn: {
        type: String,
        required: false
    },
    
});

module.exports = mongoose.model('street_addresses', StreetAddr);