/**
 * Created by jeton on 7/11/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NotificationsSettingsLogsSchema = new Schema({
    notification_settings_id: {
        type:  Schema.Types.ObjectId,
    },
    client_id: {
        type:  Schema.Types.ObjectId,
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
}
);


var notificationsSettingsLogsSchema = mongoose.model('notificationsSettingsLogs', NotificationsSettingsLogsSchema);
module.exports = notificationsSettingsLogsSchema;