let mongoose = require('mongoose');
let Schema = mongoose.Schema;

var AutoKey = new Schema({
    "type": {
        "type": String,
    },
    "currentVal": {
        "type": Number,
    }
});

module.exports = mongoose.model('AutoKey', AutoKey);