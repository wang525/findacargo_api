let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let RecipientPhone = new Schema({
    country_iso_code: {
        type: String
    },
    country_dial_code: {
        type: String
    },
    phone: {
        type: String
    }
}, {
    _id : false
});

let ScheduledDelivery = new Schema({
    deliveryid: {
        type: String,
        required: true
    },
    department: {
        type: String
    },
    deliverytype: {
        type: String
    },
    cartype: {
        type: String
    },
    distribution: {
        type: String
    },
    recipientname: {
        type: String
    },
    recipientid: {
        type: String
    },
    recipientemail: {
        type: String
    },
    recipientphone: {
        type: RecipientPhone
    },
    pickupdescription: {
        type: String
    },
    pickupaddress: {
        type: String,
    },
    pickupaddress2: {
        type: String
    },
    pickupzip: {
        type: String,
        required: true
    },
    pickupcity: {
        type: String,
        required: true
    },
    pickupdeadline: {
        type: String
    },
    pickupdeadlineto: {
        type: String
    },
    delilverydescription: {
        type: String
    },
    deliveryaddress: {
        type: String,
        required: true
    },
    deliveryaddress2: {
        type: String
    },
    deliveryzip: {
        type: String,
        required: true
    },
    deliverycity: {
        type: String,
        required: true
    },
    deliverydayofweek: {
        type: String,
        required: true
    },
    deliverywindowstart: {
        type: String
    },
    deliverywindowend: {
        type: String
    },
    deliverylabel: {
        type: String
    },
    deliverynumberofpackages: {
        type: Number
    },
    deliverynotes: {
        type: String
    },
    deliverydate: {
        type: Date,
        required: true
    },
    delivered_at: {
        type: String
    },
    driveremail: {
        type: String
    },
    weekNumber: {
        type: String
    },
    status: {
        type: Number,
        required: true
    },
    deliverycoordinates: {
        type: [],
        required: true
    },
    pickupcoordinates: {
        type: [],
        required: true
    },
    creator: {
        type: Schema.Types.ObjectId,
        required: true
    },
    estimated_delivery_time: {
        type: String
    },
    token: {
        type: String
    },
    orderIndex: {
        type: String
    },
    updated: {
        type: Date,
        default: Date.now
    },
    created: {
        type: Date
    },
    full_insurance: {
        type: Boolean
    },
    full_insurance_value: {
        type: Number
    },
    signature: {
        type: String
    },
    returned_delivery_id:Schema.Types.ObjectId,
    type: {
        type: String
    },
    return_request_by:{
        type: String
    }

});

module.exports = mongoose.model('scheduleddeliveries', ScheduledDelivery);