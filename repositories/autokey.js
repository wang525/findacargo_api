var mongodb = require("mongodb");
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var AutoKey = require("../models/AutoKey");
var q = require("q");
mongoose.Promise = require("bluebird");
//var AutoKey = mongoose.models.AutoKey;

var Repository = {
    findOne: findOne,
    create: create,
    findOneAndUpdate: findOneAndUpdate,
    update: update,
}

function findOne(type) {
    return AutoKey.findOne({type});
}

function update(autoKey) {
    return autoKey.save();
}

function findOneAndUpdate(data) {
    return AutoKey.findOneAndUpdate({type: data.type}, {$set: data});
}

function create(data) {
    var deferred = q.defer();
    var autoKey = new AutoKey(data);
    autoKey.save(function (err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(autoKey);
        }
    });

    return deferred.promise;
}

module.exports = Repository;
