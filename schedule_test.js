let _ = require('underscore')
let async = require('async')
var request = require("request");
let googleKey = 'AIzaSyAxGdRPq-XUIXeVRdGKOJHLNzO9Nj4h_hI'

let areas = [
    {area:'A90', deliveryCount:7, index:0, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Jægersborg Allé 47, 2920 Charlottenlund', 
    'Strandvejen 177, 2900 Hellerup', 
    'Ordrup Have 16, 2920 Charlottenlund', 
    'Taarbæk Strandvej 105F, 2930 Klampenborg',
    'Egehøjvej 18, 2920 Charlottenlund',
    'Strandlund 92, 2920 Charlottenlund',
    'Strandvejen 647, 2930 Klampenborg',
    ]},
    {area:'A30', deliveryCount:3, index:1, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Dag Hammarskjölds Allé 40, 2100 København Ø',
        'Dampfærgevej 24A, 2100 København Ø',
        'Kanslergade 2, 2100 København Ø'
    ]},
    {area:'A1', deliveryCount:11, index:2, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Kongens Nytorv 13, 1095 København K',
        'Frederiksborggade 21, 1360 København K',
        'Vesttoften 32, 4040 Jyllinge',
        'Plantagevej 11, 3600 Frederikssund',
        'Lynghøjvej 15, 3540 Lynge',
        'Valhalvej 32, 4000 Roskilde',
        'Enghavevej 34, 1674 København V',
        'Puggaardsgade 5, 1573 København V',
        'Birketoften 14, 3500 Værløse',
        'Elmekrogen 9, 3500 Værløse',
        'Ryttergårdsvej 66, 3520 Farum'
    ]},
    {area:'A50', deliveryCount:7, index:3, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Thorshavnsgade 24, 2300 København S',
        'Diamantgangen 89, 2300 København S	',
        'Italiensvej 59, 2300 København S',
        'Korfuvej 8, 2300 København S',
        'Tom Kristensens Vej 117, 2300 København S',
        'Engvej 178, 2300 København S',
        'Amagerbrogade 263B, 2300 København S',
    ]},
    {area:'A80', deliveryCount:7, index:4, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Handelstorvet, 2800 Kgs. Lyngby',
        'Møllemarken 30, 2880 Bagsværd',
        'Borgevej 35A, 2800 Kongens Lyngby',
        'Ved Fortunen 14B, 2800 Kongens Lyngby',
        'Elletoften 40, 2800 Kongens Lyngby',
        'Bagsværdvej 66, 2800 Kongens Lyngby',
        'Danmarksvej 31A, 2800 Kongens Lyngby'
    ]},
    {area:'A20', deliveryCount:6, index:5, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Peter Bangs Vej 93, 2000 Frederiksberg',
        'Frederiksberg Allé 2, 1820 Frederiksberg',
        'Bentzonsvej 16, 2000 Frederiksberg',
        'Grundtvigsvej 7, 1864 Frederiksberg C',
        'Lykkesholms Allé 13, 1902 Frederiksberg C',
        'Godthåbsvænget 15, 2000 Frederiksberg',
    ]},
    {area:'A6', deliveryCount:10, index:6, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Egevolden 10, 2650 Hvidovre',
        'Kongelysvej 12, 2640 Hedehusene',
        'Rønnevangshusene 110, 2630 Taastrup',
        'Ishøj Bygade 135D, 2635 Ishøj',
        'Rørhaven 15, 2690 Karlslunde',
        'Litorinaparken 57, 2680 Solrød Strand',
        'Roskildevej 264A, 2610 Rødovre',
        'Storegade 21, 2650 Hvidovre',
        'Parkdammen 22, 2605 Brøndby',
        'Egevolden 10, 3 mf, 2650, Hvidovre, 2650 Hvidovre'
    ]},
    {area:'A97', deliveryCount:3, index:7, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Vedbæk Stationsvej 19, 2950 Vedbæk',
        'Flintemarken 12, 2950 Vedbæk',
        'Femsølyngvej 3, 2970 Hørsholm'
    ]},
    {area:'A21', deliveryCount:5, index:8, from:'Telefonvej 6, 2860, Søborg', locations:[
        'Vigerslev Allé 7, 2500 Valby',
        'Valby Langgade 266 , 2500 Valby',
        'Folehaven 131, 2500 Valby',
        'Solskrænten 56, 2500 Valby',
        'Vigerslevvej 269A, 2500 Valby'
    ]},
    // {area:'A40', deliveryCount:5, index:9, from:'Telefonvej 6, 2860, Søborg', locations:[
    //     'Slangerupgade 23, 2200 København N',
    //     'Kronborggade 24, 2200 København N',
    //     'Henrik Rungs Gade 2, 2200 København N',
    //     'Ravnsborg Tværgade 3, 2200 København N',
    //     'Rantzausgade 11A, 2200 København N'
    // ]},
    // {area:'A83', deliveryCount:6, index:10, from:'Telefonvej 6, 2860, Søborg', locations:[
    //     'Dronninggårds Alle 44B, 2840 Holte',
    //     'Solbakken 117, 2840 Holte',
    //     'Oldenvej 11, 2830 Virum',
    //     'Parkvej 12A, 2830 Virum',
    //     'Egebækvej 49, 2840 Holte',
    //     'Elledamsvej 6, 2840 Holte'
    // ]},
    // {area:'A2', deliveryCount:4, index:11, from:'Telefonvej 6, 2860, Søborg', locations:[
    //     'Lindevangsvej 5, 3460 Birkerød',
    //     'Postmosen 50, 3400 Hillerød',
    //     'Veksebovej 5, 3480 Fredensborg',
    //     'Røglevænget 21, 3450 Allerød',
    // ]},
]

let maxDeliveryCount = 20
let maxDuration = 3600 * 3

let resultCandidates = []
let cacheMap = {}
let routes = []
let seeds = []
let apiCallCount = 0
let deliveryCount = 0
areas.map((a)=>{
    deliveryCount += a.locations.length
})

function getNextSuit(current) {
    var remain = _.filter(areas, (a)=>{
        return !(current.seedsSum & 1<<a.index) 
    })

    //console.log('remain.length', remain.length)
    if(remain.length == 0) {
        resultCandidates.push(current)
        //console.log(current)
        return
    }

    for(var i = 0 ; i < (1 << (remain.length - 1)) ; i++) {
        var key = (i<<1) + 1
        var clone = {
         seeds:current.seeds.slice(0),
         seedsSum: current.seedsSum   
        }
        var newSeed = 0
        var sumOfDelivery = 0
        for(var idx = 0 ; idx < remain.length ; idx++) {
            if((key & (1<<idx)) > 0) {
                newSeed |= (1 << remain[idx].index)
                sumOfDelivery += remain[idx].deliveryCount
            }
        }
        if(sumOfDelivery > maxDeliveryCount) {
            continue
        } 

        clone.seeds.push(newSeed)
        clone.seedsSum |= newSeed
        getNextSuit(clone)
    }
    return
}

function getDistance(origin, ways, cb) {
    apiCallCount++
    var url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${origin}&waypoints=optimize:true|${ways.join('|')}&key=${googleKey}`;
    var options = {
        method: 'GET',
        url: encodeURI(url)
    };
    request(options, function (error, response, body) {
        if (error) {
            responseObject.responseStatus = "failure";
            return responseObject;
        }
        body = JSON.parse(body)
        //console.log('distancematrix result', error, body)
        if(body.routes && body.routes[0]) {
            var legs = body.routes[0].legs
            var distance = 0
            var duration = 0

            for(var idx = 0 ; idx < legs.length - 1 ; idx++) {
                distance += legs[idx].distance.value
                duration += legs[idx].duration.value
            }

            return cb(null, {
                duration, distance
            })
        }
        cb('wrong result')
    })
}

let startTime = Date.now()
getNextSuit({
    seeds:[],
    seedsSum:0
})
let suitCalcTime = Date.now() - startTime
console.log('Candidates Count', resultCandidates.length)
resultCandidates.forEach((route)=>{
    route.seeds.map((seed)=>{
        if(seeds.indexOf(seed) == -1)
            seeds.push(seed)
    })
})
console.log('Seeds Count', seeds.length)

async.mapSeries(resultCandidates, (route, cb)=>{
    async.map(route.seeds, (seed, callback)=>{
        var origins = []
        var destinations = []
        areas.map((area)=>{
            if((1<<area.index)&seed) {
                destinations = destinations.concat(area.locations)
                if(origins.length == 0) {
                    origins.push(area.from)
                }
            }
        })
        var oldResult = cacheMap[String(seed)]
        if(!oldResult) {
            getDistance(origins[0], destinations, (err, ret)=>{
                if(err) {
                    cacheMap[String(seed)] = 'error'
                    callback(err, ret)
                } else if(ret.duration > maxDuration) {
                    cacheMap[String(seed)] = 'exceed'
                    // all other seeds which includes this seed will be exceed
                    var filteredSeeds = _.filter(seeds, (s)=>{
                        return ((seed&s) == seed) && !cacheMap[String(s)]
                    })

                    filteredSeeds.map(s => {
                        cacheMap[String(s)] = 'exceed'
                    })

                    console.log('filteredSeeds', seed, filteredSeeds.length)

                    callback('exceed')
                } else {
                    cacheMap[String(seed)] = ret
                    ret.seed = seed
                    callback(err, ret)
                }
           })
        } else {
            if(oldResult == 'error' || oldResult == 'exceed') {
                callback(oldResult)
            } else
                callback(null, oldResult)
        }
    }, (err, subs)=>{
        if(!err) {
            var routeDistance = 0
            var routeDuration = 0
            subs.map(s=>{
                routeDistance += s.distance
                routeDuration += s.duration
            })
            //console.log('route result', route, routeDistance, routeDuration, subs)
            routes.push({
                route,
                routeDistance,
                routeDuration,
                subs
            })
        }

        cb(null)
    })
}, (err, rets)=>{
    let totalTime = Date.now() - startTime
    var shortestRoute = _.sortBy(routes, (o)=>{
        return o.routeDuration
    })[0]

    //console.log(err, rets)
    console.log('---------------------------- Summary ----------------------------')
    console.log('Area count = ', areas.length)
    console.log('Delivery count = ', deliveryCount)
    console.log('Suit calc time = ', suitCalcTime)
    console.log('Total time = ', totalTime)
    console.log('Seeds count = ', seeds.length)
    console.log('Candidates routes = ', resultCandidates.length)
    console.log('Available routes = ', routes.length)
    console.log('API call count = ', apiCallCount)
    console.log('Final result = ', shortestRoute)
    console.log('----------------------------   End   -----------------------------')
})