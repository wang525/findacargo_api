const V1 = 'v1';
const V1_1 = 'v1.1';
const V1_2 = 'v1.2';
const V1_3 = 'v1.3';
const V1_4 = 'v1.4';
const V1_5 = 'v1.5';

module.exports.V1 = V1;
module.exports.V1_1 = V1_1;
module.exports.V1_2 = V1_2;
module.exports.V1_3 = V1_3;
module.exports.V1_4 = V1_4;
module.exports.V1_5 = V1_5;

module.exports.normaliseVersion = (version) => {
    let normalized;
    switch (version) {
        case V1_1:
            normalized = V1_1;
            break;
        case V1_2:
            normalized = V1_2;
            break;
        case V1_3:
            normalized = V1_3;
            break;
        case V1_4:
            normalized = V1_4;
            break;
        case V1_5:
            normalized = V1_5;
            break;
        default:
            normalized = V1;
            break;
    }
    return normalized;
};