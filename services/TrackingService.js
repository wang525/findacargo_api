let TrackingModel = require('../models/Tracking'),
    request = require('request');

const TRACKING_SERVER = 'http://track.goidag.com/location/';

class TrackingService {
    static addTrackingData(carrierId, truckId, location) {
        TrackingModel.find({carrier_id:carrierId}).sort({"created_at": -1}).limit(1).then(trackings=>{
            var tracking = trackings?trackings[0]:null
            if(tracking && tracking.latitude == location.latitude && tracking.longitude == location.longitude) {
                //update
                tracking.updated_at = new Date()
                return tracking.save()
            } else {
                return TrackingModel.create({
                    carrier_id: carrierId,
                    truck_id: truckId,
                    latitude: location.latitude,
                    longitude: location.longitude
                });
            }
        })        
    }

    static getTrackingDataByCarrierId(carrierId) {
        return TrackingModel.find({carrier_id:carrierId});
    }

    static getTrackingDataByTruckId(truckId) {
        return TrackingModel.find({truck_id:truckId});
    }

    static getTrackingData(carrierId) {
        return TrackingModel.find({carrier_id:carrierId});
    }
}

module.exports = TrackingService;