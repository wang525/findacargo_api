var socket = null;

class SocketService {
    static setSocket(newSocket) {
        socket = newSocket;
    }

    static getSocket() {
        return socket;
    }
}

module.exports = SocketService;