let nodeGeoCoder = require('node-geocoder');

const geoCoderKey = 'AIzaSyBmP1A3mxSdf5EH3zI0PIsDa2kUXiF2eao';
const coderOptions = {
    provider: 'google',
    httpAdapter: 'https',
    apiKey: geoCoderKey,
    formatter: null
};

const GeoCoder = nodeGeoCoder(coderOptions);

module.exports = {
    codeAddress: function (address, zipCode, country = "Denmark") {
        // return GeoCoder.geocode({
        //     address: address,
        //     country: country,
        //     zipcode: zipCode
        // }).then(location => {
        //     if (!location.length)
        //         throw new Error();

        //     return {
        //         latitude: location[0].latitude,
        //         longitude: location[0].longitude
        //     };
        // }).catch(err => {
        //     throw new Error(`Invalid address supplied: Address: ${address}, Zip: ${zipCode}, Country: ${country}`);
        // });
        return GeoCoder.geocode(address + "," + zipCode + "," + country).then(location => {
            if (!location.length)
                throw new Error();

            return {
                latitude: location[0].latitude,
                longitude: location[0].longitude
            };
        }).catch(err => {
            throw new Error(`Invalid address supplied: Address: ${address}, Zip: ${zipCode}, Country: ${country}`);
        });
    },
    geocode: function (address) {
        // return GeoCoder.geocode({
        //     address: address,
        //     country: country,
        //     zipcode: zipCode
        // }).then(location => {
        //     if (!location.length)
        //         throw new Error();

        //     return {
        //         latitude: location[0].latitude,
        //         longitude: location[0].longitude
        //     };
        // }).catch(err => {
        //     throw new Error(`Invalid address supplied: Address: ${address}, Zip: ${zipCode}, Country: ${country}`);
        // });
        return GeoCoder.geocode(address).then(location => {
            if (!location.length)
                return null

            return location
        }).catch(err => {
            return null
        });
    }
};