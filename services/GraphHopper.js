let GraphHopperRoutingOptimization = require('graphhopper-js-api-client/src/GraphHopperOptimization'),
    moment = require('moment');

// const ApiKey = "bff3028e-f616-46f2-9587-8348d58ce991";
const ApiKey = "4754940f-b44a-4b0b-bb1f-b88c421347f1";
const profile = "car";
let host;

class GraphHopperService {
    constructor(forDate = moment()) {
        this.forDate = forDate;

        this.Router = new GraphHopperRoutingOptimization({
            key: ApiKey,
            host: host,
            vehicle: profile,
            earliest_start: new Date().toUTCString(),
            elevation: false
        });
    }

    setDate(date) {
        this.forDate = date;
    }

    addLocation(location_id, lat, lng) {
        this.Router.addPoint({
            location_id, lat, lng
        });
    }

    doRoutePlanning() {
        let result = this.generateRoutePlanRequest();
        this.Router.clear();
        return result;
    }

    generateRoutePlanRequest(vehicles = 1) {
        var that = this.Router;

        var firstPoint = that.points[0];
        var servicesArray = [];
        for (var pointIndex in that.points) {
            if (pointIndex < 1)
                continue;
            var point = that.points[pointIndex];
            var obj = {
                "id": "_" + pointIndex,
                "type": "pickup",
                "name": "maintenance " + pointIndex,
                "address": {
                    "location_id": "_location_" + pointIndex,
                    "lon": point.lng,
                    "lat": point.lat
                },
                "duration": 180
            };
            servicesArray.push(obj);
        }

        var list = [];
        for (var i = 0; i < vehicles; i++) {
            list.push({
                "vehicle_id": "_vehicle_" + i,
                "start_address": {
                    "location_id": "_start_location",
                    "lon": firstPoint.lng,
                    "lat": firstPoint.lat
                },
                "type_id": "_vtype_1",
                "earliest_start": moment(this.forDate).set({hour: 8, minute: 0}).unix(),
                "latest_end": moment(this.forDate).set({hour: 16, minute: 0}).unix()
            });
        }

        var jsonInput = {
            "algorithm": {
                "problem_type": "min-max"
            },
            "vehicles": list,
            "vehicle_types": [{
                "type_id": "_vtype_1",
                "profile": this.profile
            }],
            "services": servicesArray

        };
        //console.log(JSON.stringify(jsonInput.services, null, 4));

        return that.doRequest(jsonInput);
    }
}

module.exports = GraphHopperService;