// let ScheduledDeliveryModel = require('../models/ScheduledDelivery'),
//     DriverDeliveryRepository = require('../lib/carrier-deliveries/repository/driverDeliveryRepository'),
//     _ = require('lodash'),
//     moment = require('moment');

// class RoutePlanService {
//     static planRoute(listToAssign) {
//         return new Promise((resolve, reject) => {
//             let updatedValueTosave = [];
//             let index = 1;
//             async.eachSeries(Object.keys(deliveries), function (key, next) {
//                 let deliveryLocation = '';
//                 deliveries.forEach(d => {
//                     deliveryLocation = deliveryLocation + d.deliverycoordinates[1] + ',' + d.deliverycoordinates[0] + '|';
//                 })
//                 // console.log("deliveries", deliveries);
//                 // console.log("::start location", startLocation);
//                 // console.log("::deliveryLocation", deliveryLocation);
//                 var request = require("request");
//                 var url = 'https://maps.googleapis.com/maps/api/distancematrix/json';
//                 var options = {
//                     method: 'GET',
//                     url: url,
//                     qs: { origins: startLocation, destinations: deliveryLocation + 'key=' + googleKey }
//                 };
//                 request(options, function (error, response, body) {
//                     if (error) {
//                         responseObject.responseStatus = "failure";
//                         return responseObject;
//                     }
//                     body = JSON.parse(body)
//                     let distanceAndTimeList = body.rows && body.rows[0] && body.rows[0].elements ? body.rows[0].elements : null;
//                     // console.log(distanceAndTimeList);
//                     let order = _.minBy(distanceAndTimeList, function (o) { return o.distance && o.distance.value ? o.distance.value : null });
//                     let deliveryIdIndex = _.findIndex(distanceAndTimeList, function (o) { return o.distance.value == order.distance.value; });
//                     // console.log("order", order.distance.text)
//                     // console.log("order duration", order.duration.text)
//                     let finalEstimatedTime;
//                     if (isCurrentTime == false) {
//                         finalEstimatedTime = startTime;
//                         startTime = finalEstimatedTime;
//                         isCurrentTime = true;
//                         ZoneDeliveryController.updateDate(startDate, ids, true);
//                     } else {
//                         let seconds = order.duration.value + bufferTime;
//                         let durations = moment.duration(seconds, 'seconds');
//                         let formattedTime = durations.format("mm");
//                         finalEstimatedTime = moment(startTime, 'HH:mm:ss').add(formattedTime, 'm').format('HH:mm:ss');
//                         startTime = finalEstimatedTime;
//                     }
//                     startLocation = deliveries[deliveryIdIndex].deliverycoordinates[1] + ',' + deliveries[deliveryIdIndex].deliverycoordinates[0];
//                     updatedValueTosave.push({
//                         deliveryId: deliveries[deliveryIdIndex]._id,
//                         orderIndex: index++,
//                         estimatedDeliveryTime: finalEstimatedTime
//                     });
//                     // console.log("::deliverie Id", deliveries[deliveryIdIndex]._id)
//                     // console.log("isCurrentTime", isCurrentTime);
//                     // console.log("Final estimation time", finalEstimatedTime);
//                     _.pullAt(deliveries, [deliveryIdIndex]);
//                     // console.log("::deliveries", deliveries)                        
//                     next()
//                 });
//             }, function (err) {
//                 // console.log('iterating done');
//                 resolve(updatedValueTosave);
//             });
//         })
//     }   

//     static UpdateScheduledDelivery(id, index, estimation) {
//         ScheduledDeliveryModel.findByIdAndUpdate(id, {
//             $set: {
//                 orderIndex: index + 1,
//                 estimated_delivery_time: moment('08:00', 'HH:mm').add(estimation, 'm').format('HH:mm')
//             }
//         }, () => { });
//     }
// }

// module.exports = RoutePlanService;