let AutoKeyRepository = require('../repositories/autokey');
var ScheduledRepository = require('../lib/scheduled/repository/scheduled');

class AutoKeyService {

    static createKey(type) {
        return AutoKeyRepository.findOne(type)
            .then(autoKey => {
                if(type == 'delivery') {
                    var currentId = 1
                    if(autoKey) {
                        currentId = autoKey.currentVal + 1
                    }
    
                    return this.getUnusedDeliveryId(currentId).then(id => {
                        //save
                        if(autoKey) {
                            return AutoKeyRepository.findOneAndUpdate({
                                type,
                                currentVal:id
                            }).then(()=>{
                                return id
                            })
                        } else {
                            return AutoKeyRepository.create({
                                type,
                                currentVal:id
                            }).then(()=>{
                                return id
                            })
                        }
                    })
                } else {
                    return 1
                }
            })
            .catch((e) => {
                console.log(e)
            })
    }

    static getUnusedDeliveryId(id) {
        return ScheduledRepository.getDeliveryByDeliveryID(String(id))
        .then((delivery) => {
            console.log('delivery', delivery)
            if(!delivery) return id
            id++
            return this.getUnusedDeliveryId(id)
        })
    }
}

module.exports = AutoKeyService;