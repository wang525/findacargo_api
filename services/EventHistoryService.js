let EventsRepository = require('../lib/event-history/repository');
var SocketService = require('../services/SocketService');

const SCHEDULED_DELIVERY = 'SCHEDULED_DELIVERY';
const STATUS_UPDATE = 'STATUS_UPDATE';
const CUSTOM_EVENT = 'CUSTOM_EVENT';
const DATE_CHANGED = 'DATE_CHANGED';
const DELIVERY_PLANNED = 'DELIVERY_PLANNED';
const DELIVERY_NOT_POSSIBLE = 'DELIVERY_NOT_POSSIBLE';
const CALLED_CLIENT = 'CALLED_CLIENT';
const NOTIFICATION_SENT = 'NOTIFICATION_SENT';
const EMAIL_SENT = 'EMAIL_SENT';
const SMS_SENT = 'SMS_SENT';
const ADMIN_EVENT = 'ADMIN_EVENT';

class EventHistoryService {

    static createEvent(assigned_to, assigned_to_type, event, event_data, event_detail) {
        assigned_to = ensureObjectId(assigned_to);
        return EventsRepository.create(assigned_to, assigned_to_type, event, event_data, event_detail);
    }

    static createEventWithTime(assigned_to, assigned_to_type, event, event_data, event_detail, created_at) {
        assigned_to = ensureObjectId(assigned_to);
        return EventsRepository.createWithTime(assigned_to, assigned_to_type, event, event_data, event_detail, created_at);
    }

    static getEventForType(type, id) {
        id = ensureObjectId(id);
        return EventsRepository.findForIdOfType(id, type);
    }

    static getEventForEventType(event, id) {
        //id = ensureObjectId(id);
        return EventsRepository.findForIdOfEventType(id, event);
    }

    static getEventsForStatusChanged(id) {
        return this.getEventForEventType(STATUS_UPDATE, id);
    }

    static statusUpdatedScheduledDelivery(id, newStatus) {
        return this.createEvent(id, SCHEDULED_DELIVERY, STATUS_UPDATE, newStatus).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':newStatus});
            }
            return event    
        });
    }
    
    static statusUpdatedScheduledDeliveryWithTime(id, newStatus, created_at) {
        return this.createEventWithTime(id, SCHEDULED_DELIVERY, STATUS_UPDATE, newStatus, created_at).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':newStatus});
            }
            return event    
        });
    }
    
    static dateChangedScheduledDelivery(id, event_data) {
        return this.createEvent(id, SCHEDULED_DELIVERY, DATE_CHANGED, event_data).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':'data changed'});
            }
            return event    
        });
    }

    static addDeliveryNotPossibleEvent(id, event_data) {
        return this.createEvent(id, SCHEDULED_DELIVERY, DELIVERY_NOT_POSSIBLE, event_data).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':'delivery isnot possible'});
            }
            return event    
        });
    }

    static addCalledClientEvent(id, event_data) {
        return this.createEvent(id, SCHEDULED_DELIVERY, CALLED_CLIENT, event_data).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':'called client'});
            }
            return event    
        });
    }

    static addNotificationSentEvent(id, event_data, event_detail) {
        return this.createEvent(id, SCHEDULED_DELIVERY, NOTIFICATION_SENT, event_data, event_detail).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':'notification sent'});
            }
            return event    
        });
    }

    static addCustomEvent(id, event_data) {
        return this.createEvent(id, SCHEDULED_DELIVERY, CUSTOM_EVENT, event_data).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':'custom event'});
            }
            return event    
        });
    }

    static addCustomEventWithTime(id, event_data, created_at) {
        return this.createEventWithTime(id, SCHEDULED_DELIVERY, CUSTOM_EVENT, event_data, '', created_at).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':'custom event'});
            }
            return event    
        });
    }

    static addAdminEvent(id, event_data) {
        return this.createEvent(id, SCHEDULED_DELIVERY, ADMIN_EVENT, event_data).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':'admin event'});
            }
            return event    
        });
    }

    static deliveryPlannedScheduledDelivery(id, event_data) {
        return this.createEvent(id, SCHEDULED_DELIVERY, DELIVERY_PLANNED, event_data).then((event)=>{
            var socket = SocketService.getSocket();
            if (socket) {
                socket.emit(`delivery_status_changed_${id}`, {'status':'Planned'});
            }
            return event    
        });
    }

    static getEventsForScheduledDelivery(id) {
        return this.getEventForType(SCHEDULED_DELIVERY, id);
    }
}

module.exports = EventHistoryService;