/**
 * Created by jeton on 7/11/2017.
 */
module.exports = {
    success: function(res, message = null, data = null){
        res.status(201);
        res.send({
            success: true,
            message: message,
            data
        });
    },
    created: function(res, message = null, data = null){
        res.status(201);
        res.send({
            success: true,
            message: message,
            body: data
        });
    },
    malformed: function(res, message = null, data = null){
        res.status(400);
        res.send({
            success: false,
            message: message
        });
    },
    noData: function(res, message = null, data = null){
        res.status(200);
        res.send({
            success: false,
            body: data,
            message: message
        });
    },
    error: function(res, error){
        res.status(400)
    },
    badRequest: function(res, msg = "Invalid parameters.", data = null){
        res.status(400);
        res.send({
            msg: message,
            body: data
        });
    },
    unAuthorized: function(res, message = "Not authorized."){
        res.status(401);
        res.send({
            success: false,
            message: message
        });
    },
    dbError: function(res, data, msg){
        res.status(500);
        res.send({
            success: false,
            body: data,
            body: msg
        });
    },
    msg:{
        invalidApiKey: 'Invalid API Key.',
        successLogin: 'Successfully logged in.',
        wrongUserPass: 'Wrong username or password.',
        logout: 'Logged out successfully.'
    },
    mongooseValidation: function(errors){
        var required = [];
        for(var key in errors){
            var error = errors[key];
            required.push(error.message)
        }
        return required;
    },
    missingParameters: function(inputs){
        return 'Missing parameters, or empty parameters: ' + inputs.join(', ');
    },
    codeWithMessage: function (code, message) {
        return {
            "message": message,
            "error": {
                "status": code
            }
        }
    }
};