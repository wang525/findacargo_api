let GraphHopperService = require('./GraphHopper'),
    ScheduledDeliveryModel = require('../models/ScheduledDelivery'),
    DriverDeliveryRepository = require('../lib/carrier-deliveries/repository/driverDeliveryRepository'),
    _ = require('lodash'),
    moment = require('moment');

class RoutePlannerService {
    static planRoute(listToAssign) {
        let promisesList = [];

        for(let key in listToAssign.haveDriver) {
            promisesList.push(new Promise((resolve, reject) => {
                let currentList = listToAssign.haveDriver[key],
                    deliveriesList = [],
                    routePlanner = new GraphHopperService();
                const ROUTE_INDEX = Object.keys(listToAssign.haveDriver).indexOf(key);

                if (currentList.length === 1) {
                    let deliveryId = currentList[0].delivery_id;

                    RoutePlannerService.UpdateScheduledDelivery(deliveryId, 0, "08:00");
                    DriverDeliveryRepository.create({
                        delivery_id: deliveryId,
                        carrier_id: key
                    });

                    RoutePlannerService.addToAssigned(listToAssign, {
                        deliveryId: delivery_id,
                        order: index + 1,
                        driverId: key,
                        driverTime: "08:00",
                        routeIndex: ROUTE_INDEX + 1
                    });
                    resolve();
                }

                let locations = currentList.map(assignment => {
                    return ScheduledDeliveryModel.findOne({_id: assignment.delivery_id}).then((delivery) => {
                        deliveriesList.push(assignment.delivery_id);
                        routePlanner.setDate(delivery.deliverydate);
                        return routePlanner.addLocation(assignment.delivery_id, delivery.deliverycoordinates[0], delivery.deliverycoordinates[1]);
                    })
                });

                Promise.all(locations).then(() => {
                    return routePlanner.doRoutePlanning()
                        .then(result => {
                            let solution = result.solution.routes[0].activities;
                            let plan = [];

                            solution.map((route, index) => {
                                if (route.type === 'end')
                                    return;

                                let currentElement = route.type === 'start' ? currentList[0] : currentList[_.trimStart(route.id, '_')];
                                let delivery_id = currentElement.delivery_id;

                                let arrivingTimeOffset = route.service ? route.driving_time / 60 + (route.service.duration / 60 * index) : route.driving_time / 60;

                                plan.push({
                                    delivery_id: delivery_id,
                                    carrier_id: key,
                                });

                                RoutePlannerService.addToAssigned(listToAssign, {
                                    deliveryId: delivery_id,
                                    address: currentElement.address,
                                    driverId: key,
                                    order: index + 1,
                                    driverTime: moment('08:00', 'HH:mm').add(arrivingTimeOffset, 'm').format('HH:mm'),
                                    routeIndex: ROUTE_INDEX
                                });

                                RoutePlannerService.UpdateScheduledDelivery(delivery_id, index, arrivingTimeOffset);

                            });

                            result.solution.unassigned_services.map((route) => {
                                let delivery = currentList[_.trimStart(route.id, '_')];
                                let delivery_id = deliveriesList[_.trimStart(route.id, '_')];
                                listToAssign.withoutDriver.push({
                                    delivery_id: delivery_id,
                                    address: delivery.address,
                                    haveDriver: true,
                                    timeWindow: true
                                })
                            });

                            return plan;
                        });
                })
                    .then(data => {
                        DriverDeliveryRepository.createMany(data);
                        resolve();
                    })
                    .catch(err => {
                        if(err)
                            console.error(err);
                        currentList = currentList.map(assignment => {
                            assignment.apiLimit = true;
                            return assignment
                        });

                        listToAssign.withoutDriver = _.concat(listToAssign.withoutDriver, currentList);

                        return reject(err);
                    });
            }));
        }

        return Promise.all(promisesList)
            .then((result) => {
                return listToAssign;
            })
            .catch(err => {

                return listToAssign;
            });
    }

    static addToAssigned(list, data) {
        if (!list.assigned)
            list.assigned = [];

        list.assigned.push(data);
    }

    static UpdateScheduledDelivery(id, index, estimation) {
        ScheduledDeliveryModel.findByIdAndUpdate(id, {
            $set: {
                orderIndex: String(index + 1),
                estimated_delivery_time: moment('08:00', 'HH:mm').add(estimation, 'm').format('HH:mm')
            }
        }, () => {});
    }
}

module.exports = RoutePlannerService;