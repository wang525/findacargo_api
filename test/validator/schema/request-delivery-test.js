var JaySchema = require("jayschema");
var moment = require("moment");
var js = new JaySchema();
var chai = require("chai");
var expect = chai.expect;
var requestSchema = require("./../../../lib/validator/schema/request-delivery.js");

js.addFormat("time2", function(time){
    if(time.length === 5){
        var timeRegex = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/
        if(timeRegex.test(time)) {
            return null;
        }
    }
    
    if(time.length === 8){
        var timeRegex = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/
        if(timeRegex.test(time)) {
            return null;
        }
    }
    return "Invalid Time";
});

describe("ValidRequests", function(){
    it("Delivery is Now and date and time empty", function(){
        var instance = {
            "vehicle_type":"2",
            "pickup_location":{"latitude":"-12.123", "longitude":"46.1551"},
            "pickup_options":{"when":"now", "date":"", "time":""},
            "dropoff_location":{"latitude":"12.7472", "longitude":"54.13488"},
            "options": []
        };

        js.validate(instance, requestSchema, function(err){
            expect(err).to.be.null;
        });
    });

    it("Scheduled Delivery", function(){
        var instance = {
            "vehicle_type":"2",
            "pickup_location":{"latitude":"-12.123", "longitude":"46.1551"},
            "pickup_options":{"when":"", "date":"2016-10-10", "time":"12:00"},
            "dropoff_location":{"latitude":"12.7472", "longitude":"54.13488"},
            "options": []
        };

        js.validate(instance, requestSchema, function(err){
            expect(err).to.be.null;
        });
    })

    it("Scheduled Delivery complete time", function(){
        var instance = {
            "vehicle_type":"2",
            "pickup_location":{"latitude":"-12.123", "longitude":"46.1551"},
            "pickup_options":{"when":"", "date":"2016-10-10", "time":"12:00:10"},
            "dropoff_location":{"latitude":"12.7472", "longitude":"54.13488"},
            "options": []
        };

        js.validate(instance, requestSchema, function(err){
            expect(err).to.be.null;
        });
    })

    it("Scheduled Delivery 24h format time", function(){
        var instance = {
            "vehicle_type":"2",
            "pickup_location":{"latitude":"-12.123", "longitude":"46.1551"},
            "pickup_options":{"when":"", "date":"2016-10-10", "time":"22:00"},
            "dropoff_location":{"latitude":"12.7472", "longitude":"54.13488"},
            "options": []
        };

        js.validate(instance, requestSchema, function(err){
            expect(err).to.be.null;
        });
    })
})
