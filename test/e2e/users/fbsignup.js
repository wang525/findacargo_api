var needle = require("needle");
var chai = require("chai");
var Promise = require("bluebird");
var MongoDB = require('mongodb').Db;
var Server = require('mongodb').Server;
var expect = chai.expect;
var assert = chai.assert;

var db = new MongoDB("findacargo_test", new Server("api.findacargo.dev", 27017, {auto_reconnect: true}), {w: 1});
db.open(function(e, d){
    if (e) {
        console.error(e);
    }
});

describe("Facebook register for existing user", function(){
    var request = {
        name: "Test User",
        email: "testuser@email.com",
        countryDialCode: "",
        countryISOCode: "",
        phone: "123123123",
        buyer: 1,
        carrier: 1,
        password: "teste123",
        date: "2016-10-10 10:00:00" 
    };

    var url = "http://api.findacargo.dev:3010/v1"
    var token = {};

    it("Create user using common endpoint", function(){
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/register", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                token = resp.body.token;
                resolve(resp.body);
            });
        });
    });

    it("Create user with facebook", function(){
        request.password = 123460131371029758;
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/facebookRegister", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                token = resp.body.token;
                resolve(resp.body);
            });
        });
    });

    it("Facebook Login", function(){
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/facebookLogin", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.token).to.equal(token);
                resolve(resp.body);
            })
        });
    })

    it("Common Login", function(){
        request.password = "teste123";
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/login", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.token).to.equal(token);
                resolve(resp.body);
            })
        });
    })

    after(function(){
        db.collection("accounts").remove({});
    });
});

describe("User sign up with facebook only", function(){
    var request = {
        name: "Test User",
        email: "testuser@email.com",
        countryDialCode: "",
        countryISOCode: "",
        phone: "123123123",
        buyer: 1,
        carrier: 1,
        password: "teste123",
        date: "2016-10-10 10:00:00" 
    };

    var url = "http://api.findacargo.dev:3010/v1"
    var token = {};

    it("Create user with facebook", function(){
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/facebookRegister", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                token = resp.body.token;
                resolve(resp.body);
            });
        });
    });

    it("Common Login", function(){
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/login", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.token).to.equal(token);
                resolve(resp.body);
            })
        });
    })

    it("Facebook Login", function(){
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/facebookLogin", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.token).to.equal(token);
                resolve(resp.body);
            })
        });
    })

    it("Invalid facebook Login", function(){
        request.password = 6491649123646291347;
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/facebookLogin", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(505);
                expect(err).to.be.a("null");
                expect(resp.body.message).to.equal("Invalid Facebook Login");
                resolve(resp.body);
            })
        });
    })
    after(function(){
        db.collection("accounts").remove({});
    });
});

describe("Normal sign up with logins through fb and normal", function(){
    var request = {
        name: "Test User",
        email: "testuser@email.com",
        countryDialCode: "",
        countryISOCode: "",
        phone: "123123123",
        buyer: 1,
        carrier: 1,
        password: "teste123",
        date: "2016-10-10 10:00:00" 
    };

    var url = "http://api.findacargo.dev:3010/v1"
    var token = {};
    var newToken = {};

    it("Create user using common endpoint", function(){
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/register", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                token = resp.body.token;
                resolve(resp.body);
            });
        });
    });

    it("Common Login", function(){
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/login", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.token).to.equal(token);
                resolve(resp.body);
            })
        });
    });

    it("Facebook login updates fb-pass", function(){
        request.password = "901236402346092342834692634927834";
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/facebookLogin", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                newToken = resp.body.token;
                resolve(resp.body);
            })
        });
    });

    it("Common Login after change token", function(){
        request.password = "teste123";
        var options = {};
        return new Promise(function (resolve, reject){
            needle.post(url+"/auth/login", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                expect(resp.body.token).to.equal(newToken);
                resolve(resp.body);
            })
        });
    });

    after(function(){
        db.collection("accounts").remove({});
    });
});


describe("User sign up with facebook and secondaryEmail", function(){
    var request = {
        name: "Test User",
        email: "testuser@email.com",
        countryDialCode: "",
        countryISOCode: "",
        phone: "123123123",
        buyer: 1,
        carrier: 1,
        password: "teste123",
        date: "2016-10-10 10:00:00",
        secondaryEmail: "testsecondary@email.com"
    };

    var url = "http://api.findacargo.dev:3010/v1"
    var token = {};

    it("Create user with facebook", function(){
        var options = {};
        return new Promise(function(resolve, reject){
            needle.post(url+"/auth/facebookRegister", request, options, function(err, resp){
                expect(resp.statusCode).to.equal(200);
                expect(err).to.be.a("null");
                token = resp.body.token;
                resolve(resp.body);
            });
        })
        .then(function(){
            db.collection("accounts").findOne({secondaryEmail:request.secondaryEmail},function(err, doc){
                expect(err).to.be.a("null");
                expect(doc.name).to.be.equal(request.name);
                return;
            });
        });
    });

    after(function(){
        db.collection("accounts").remove({});
    });
});

