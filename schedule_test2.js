moment = require('moment')
require('moment-timezone')
var momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);
//typeof moment.duration.fn.format === "function";
//typeof moment.duration.format === "function";

let today = moment.tz("Europe/Copenhagen");
let startDate = today.format("YYYY-MM-DD");
let startTime = today.format("HH:mm");