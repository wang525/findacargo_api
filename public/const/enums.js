//Scheduled delivery live status
exports.SCHEDULED_DELIVERY_STATUS = {
  1: "Created",
  2: "In Progress",
  3: "Finished",
  4: "Not Received",
  5: "For re-delivery"
};
