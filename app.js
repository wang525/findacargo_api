var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var https = require('https');
var fs = require('fs');
var socketIo = require('socket.io');
require('./services/Extender');
require('./services/EventHistoryService');
var SocketService = require('./services/SocketService');
var routes = require('./routes/index');
var users = require('./routes/users');
var auth = require('./routes/auth');
var carriers = require('./routes/carriers');
var deliveries = require('./routes/deliveries');
var scheduleds = require('./routes/scheduleds');
var todaysDeliveries = require('./routes/carrier_delivery');
var departments = require('./routes/department');
var types = require('./routes/types');
var cargoStatus = require('./routes/deliveryStatus');
var admin = require('./routes/admin');
var scheduledDeliveries = require('./routes/plan');
//var delivery = require('./routes/delivery');
var exposed = require('./routes/exposed');
var settings = require('./routes/settings');
var notifcationSettings = require('./routes/notificationSettings');
var zoneDelivery = require('./routes/zone_delivery');
var routePlanning = require('./routes/routePlanning');
var apiKeys = require('./routes/apiKeys');
var upload = require('./routes/upload');
var webshipper = require('./routes/webshipper');
var Auth = require('./middlewares/Auth');
var templateSetting = require('./routes/templates');
var deliveryRatings = require('./routes/deliveryRatings');

var taskIdleVehicles = require('./lib/tasks/turnoff-idle-vehicles');
taskIdleVehicles();

var sendNotification = require('./routes/sendNotification');

var app = express();
const options = {
    key: fs.readFileSync('./key/findacargo.com.key'),
    cert: fs.readFileSync('./key/a2a576fc7f5f6e34.crt')
};

var cors = require('cors');
var corsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    "preflightContinue": true
};
app.use(cors(corsOptions));

app.use(logger('dev'));
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.json({limit: '50mb', extended: true, parameterLimit:50000}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit:50000}));
app.use(bodyParser.raw({limit: "50mb", extended: true, parameterLimit:50000}));
//app.use(bodyParser.json());
app.use(bodyParser.json({limit:1024102420, type:'application/json'}));

app.use(cookieParser());
app.use('/public',express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/v1/users', users);
app.use('/v1/auth', auth);
app.use('/v1/carriers', carriers);
app.use('/v1/deliveries', deliveries);
app.use('/v1/scheduleds', scheduleds);
app.use('/v1/todayDelivery', todaysDeliveries);
app.use('/v1/departments', departments);
app.use('/v1/types', types);
app.use('/v1/setCargoStatus', cargoStatus);
app.use('/v1/sendNotification', sendNotification);
app.use('/v1/admin', admin);
app.use('/', scheduledDeliveries);
//app.use('/', delivery);
app.use('/', settings);
app.use('/v1/user/notifications/settings', Auth.isAuthenticated, notifcationSettings);
app.use('/v1/zone-delivery', zoneDelivery);
app.use('/v1/route-planning', routePlanning);
app.use('/v1/apikeys', apiKeys);
app.use('/v1/upload', upload);

app.use('/webshipper', webshipper);
app.use('/', exposed);
app.use('/v1/template', templateSetting);
app.use('/v1/delivery-ratings', deliveryRatings);

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: err,
    });
});

// http.createServer(app).listen(8000, function() {
//     console.log('Express server listening on port ' + 8000);
// });

var server = http.createServer(app).listen(3333, function() {
    console.log('Express server listening on port ' + 3333);
});
SocketService.setSocket(socketIo(server));
module.exports = app;