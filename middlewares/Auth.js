/**
 * Created by jeton on 7/11/2017.
 */
var Response = require('../services/Response');
var ApiKeys = require('../models/apiKeys');
var mongoose = require('mongoose');



var Auth = {
    isAuthenticated: function (req, res, next) {
        try {
            var clientKey = req.header('token');
            mongoose.connection.db.collection('accounts').find({ $or: [{apikey: clientKey}, {apiKey: clientKey}]}, function (err, response) {
                response.toArray(function (err, apiKey) {
                        if(apiKey.length){
                            req.user = apiKey[0];
                            next();
                        }else{
                            Response.unAuthorized(res, Response.msg.invalidApiKey);
                        }
                    })
                })
            // Accounts.findOne({apitKey: clientKey})
            //     .then(function(apiKey){
            //         console.log(apiKey)
            //         if(apiKey){
            //             next();
            //         }else{
            //             Response.unAuthorized(res, Response.msg.invalidApiKey);
            //         }
            //     })
        } catch(err) {
            Response.unAuthorized(res, Response.msg.invalidApiKey);
        }
    },
    validateJSON: function(error, req, res, next){
        if(error){
            Response.badRequest(res, 'Invalid JSON Format.');
        }
    }
};


module.exports = Auth;