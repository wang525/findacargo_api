class DeliverySettingsViewModel {
    constructor(deliverySettings) {
        for (var prop in deliverySettings) {
            this[prop] = deliverySettings[prop];
        }

        delete this._id;
        delete this.__v;
        delete this.clientId;

        // rename fields
        this.allowed_pickup_areas = this.allowed_pickups_zones || [];
        delete this.allowed_pickups_zones;

        this.allowed_delivery_areas = this.zip || [];
        delete this.zip;

        this.allowed_pickup_areas.forEach(x => {
            let doc = x._doc;
            delete doc._id;

            doc.zip_from = doc.range_from;
            delete doc.range_from;

            doc.zip_to = doc.range_to;
            delete doc.range_to;
        });

        this.allowed_delivery_areas.forEach(x => {
            let doc = x._doc;
            delete doc._id;

            doc.zip_from = doc.range_from;
            delete doc.range_from;

            doc.zip_to = doc.range_to;
            delete doc.range_to;
        });
    }
}

module.exports = DeliverySettingsViewModel;